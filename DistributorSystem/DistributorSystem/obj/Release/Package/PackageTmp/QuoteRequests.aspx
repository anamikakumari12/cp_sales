﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuoteRequests.aspx.cs" Inherits="DistributorSystem.QuoteRequests" MasterPageFile="~/MasterPage.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <%--    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>--%>
    <script src="js/vfs_fonts.js"></script>

    <script src="js/bundle.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>

    <link href="css/Tabs.css" rel="stylesheet" />
    <style>
        .modal
        {
            position: fixed;
            top: 0;
            left: 0;
            background-color: black;
            z-index: 99;
            opacity: 0.8;
            filter: alpha(opacity=80);
            -moz-opacity: 0.8;
            min-height: 100%;
            width: 100%;
        }

        .loading
        {
            font-family: Arial;
            font-size: 10pt;
            border: 5px solid #67CFF5;
            width: 200px;
            height: 100px;
            display: none;
            position: fixed;
            background-color: White;
            z-index: 999;
        }

        .disabled
        {
            background-color: #e4e4e4!important;
            border: 1px solid #aaa!important;
            border-radius: 4px!important;
            cursor: default!important;
            float: left!important;
            /* margin-right: 5px; */
            /* margin-top: 5px; */
            padding: 0 5px!important;
        }

        .dataTables_wrapper
        {
            min-height: 300px;
        }

        .order
        {
            padding-left: 10px;
            padding-right: 10px;
        }

        .divorder
        {
            border: 1px solid #316f8f;
            height: 90px;
            padding: 10px;
        }

        .ddl
        {
            background-color: #fff;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            height: 28px;
        }

        .required:after
        {
            content: " *";
            color: red;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#MainContent_ddlitem').on("select2:select", function (e) {
                debugger;
                console.log($(this).val());
                var item_value = $(this).val();
                $('#MainContent_ddlitemdesc').val(item_value);
                $.ajax({
                    url: 'QuoteRequests.aspx/LoadItemDetails',
                    method: 'post',
                    datatype: 'json',
                    data: '{"item":"'+item_value+'"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        BindItemDetails(data);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });

            });
            //$.ajax({
            //    url: 'QuoteRequest.aspx/LoadDetailedGrid',
            //    method: 'post',
            //    datatype: 'json',
            //    data: '{}',
            //    contentType: "application/json; charset=utf-8",
            //    success: function (data) {
            //        console.log(data);
            //        BindItems(data);

            //    },
            //    error: function (xhr, ajaxOptions, thrownError) {
            //        alert(xhr.responseText);
            //    }
            //});

            //LoadDropdowns();
            LoadTable();
        });
        function BindItemDetails(data) {
            debugger;
            $('#MainContent_txtWHS').val(data.d.WHS);
            $('#MainContent_txtDLP').val(data.d.LP);
            $('#MainContent_txtAP').val(data.d.AP);
            $('#MainContent_hdnStockCode').val(data.d.stockCode);
            $('#MainContent_ddlitemdesc').select2('data', { id: data.d.item, text: data.d.item_desc });
        }
        function BindItems(msg) {
            debugger;
            console.log(msg);
            $("#MainContent_ddlitem").select2({
                "width": "200px", "minimumInputLength": 3,
                data: msg.d
            });
           
            //var testObjs = data.d;
            //for (var j = 0; j < 25600; j++) {
            //    testObjs.push({
            //        'name': "test" + j,
            //    });
            //}
            //if (typeof swl == 'undefined' && Alex) swl = Alex;
            //if (Alex) {
            //    var select2 = new Alex.lgSelect({
            //        options: testObjs,
            //        title: "Select AO2",
            //        containerId: "container2",
            //    });

            //} else {
            //    console.error("lgSelect component not exist.");
            //}

            //$("#MainContent_grdPriceRequest_ddlitem").append($("<option></option>").val('0').html("--Select--"));
            ////for (i = 0; i < data.d.length; i++) {
            ////    $("#MainContent_grdPriceRequest_ddlitem_0").append($("<option></option>").val(data.d[i].item).html(data.d[i].item));
            ////}
            //$.each(data, function (key, value) {
            //    $("#MainContent_grdPriceRequest_ddlitem").append($("<option></option>").val(value.item).html(value.item));
            //});
        }
        function ShowMOQCondition(item, stock, ddlidesc_id) {
            debugger;
            console.log("hello");
            console.log(item);
            console.log(stock);
            if (stock == "1") {
                //alert(item + " may involve MOQ, do you want to proceed?");
                if (window.confirm(item + " may involve MOQ, do you want to proceed?")) {
                    // They clicked Yes
                }
                else {
                    console.log(ddlidesc_id);
                    var item_id = ddlidesc_id.replace("ddlitemdesc", "ddlitem");
                    var WHS_id = ddlidesc_id.replace("ddlitemdesc", "txtWHS");
                    var LP_id = ddlidesc_id.replace("ddlitemdesc", "txtDLP");
                    var AP_id = ddlidesc_id.replace("ddlitemdesc", "txtAP");
                    $('#' + WHS_id).val("");
                    $('#' + LP_id).val("");
                    $('#' + AP_id).val("");
                    $('#' + item_id).val("");
                    $('#' + ddlidesc_id).val("");

                }
            }
        }

        function LoadPage() {
            //LoadDropdowns();
            //LoadTable();
        }
        function LoadTable() {
            debugger;

            var head_content = $('#MainContent_grdPriceRequest tr:first').html();
            var foot_content = $('#MainContent_grdPriceRequest tr:last').html();
            $('#MainContent_grdPriceRequest').prepend('<thead></thead>')
            $('#MainContent_grdPriceRequest thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdPriceRequest tbody tr:first').hide();
            //$('#MainContent_grdPriceRequest').append('<tfoot></tfoot>')
            //$('#MainContent_grdPriceRequest tfoot').html('<tr>' + foot_content + '</tr>');
            //$('#MainContent_grdPriceRequest tbody tr:last').hide();
            var table = $('#MainContent_grdPriceRequest').dataTable({
                "ordering": false,
                "pagination":false,
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'lBfrtip',
                //dom: 'Bfrtip',
                buttons: [
                    //'copy', 'csv',
                    //'excel', 'pdf', 'print'
                ],
                //"scrollY": 200,
                "scrollX": true
            });
            $.ajax({
                url: 'QuoteRequests.aspx/LoadItems',
                method: 'post',
                datatype: 'json',
                data: '',
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    console.log(data);
                    BindItems(data);

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                }
            });

            $("#MainContent_ddlitemdesc").select2({ "width": "200px", "minimumInputLength": 3 });
        }

        function LoadDropdowns() {
            debugger;


            var totalRowCount = $("[id*=grdPriceRequest] tr").length;
            var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;
            for (var i = 0; i < rowCount; i++) {
                $("#MainContent_grdPriceRequest_ddlitem_" + i).select2({ "width": "110px", "minimumInputLength": 3 });
                $("#MainContent_grdPriceRequest_ddlitemdesc_" + i).select2({ "width": "200px", "minimumInputLength": 3 });
                $("#MainContent_grdPriceRequest_ddlCustName_" + i).select2({ "multiple": "true" });
                var str = $("#MainContent_grdPriceRequest_hdnCustomers_" + i).val();
                if (!IsnullOrEmpty(str)) {
                    $("#MainContent_grdPriceRequest_ddlCustName_" + i).val(str.split(","));
                    $("#MainContent_grdPriceRequest_ddlCustName_" + i).select2({ text: str });
                }


                var ordertype = $("#MainContent_grdPriceRequest_ddlOrder_" + i).val();
                if (ordertype == "schedule") {
                    $("#MainContent_grdPriceRequest_ddlFrequency_" + i).removeClass('disabled');
                    $("#MainContent_grdPriceRequest_ddlFrequency_" + i).prop("disabled", false);
                    $("#MainContent_grdPriceRequest_txtQTYPO_" + i).removeClass('disabled');
                    $("#MainContent_grdPriceRequest_txtQTYPO_" + i).prop("disabled", false);
                }
                else {
                    $("#MainContent_grdPriceRequest_ddlFrequency_" + i).addClass('disabled');
                    $("#MainContent_grdPriceRequest_ddlFrequency_" + i).prop("disabled", true);
                    $("#MainContent_grdPriceRequest_txtQTYPO_" + i).addClass('disabled');
                    $("#MainContent_grdPriceRequest_txtQTYPO_" + i).prop("disabled", true);
                }

            }
        }


        document.addEventListener("DOMContentLoaded", function () {

            var tabs = document.querySelectorAll('.tabbed li');
            var switchers = document.querySelectorAll('.switcher-box a');
            var skinable = document.getElementById('skinable');

            for (var i = 0, len = tabs.length; i < len; i++) {
                tabs[i].addEventListener("click", function () {
                    if (this.classList.contains('active')) {

                        return;
                    }
                    var parent = this.parentNode,
                        innerTabs = parent.querySelectorAll('li');

                    for (var index = 0, iLen = innerTabs.length; index < iLen; index++) {
                        innerTabs[index].classList.remove('active');
                    }

                    this.classList.add('active');
                });
            }

            for (var i = 0, len = switchers.length; i < len; i++) {
                switchers[i].addEventListener("click", function () {
                    if (this.classList.contains('active'))
                        return;

                    var parent = this.parentNode,
                        innerSwitchers = parent.querySelectorAll('a'),
                        skinName = this.getAttribute('skin');

                    for (var index = 0, iLen = innerSwitchers.length; index < iLen; index++) {
                        innerSwitchers[index].classList.remove('active');
                    }

                    this.classList.add('active');
                    skinable.className = 'tabbed round ' + skinName;
                });
            }
        });

        function tabchange(e) {

            if (e.id == "MainContent_freqList") {
                $('#MainContent_divFrequent').css("display", "block");
                $('#MainContent_divRecent').css("display", "none");
                $('#MainContent_divRecent').removeClass("active");
                $('#MainContent_divFrequent').addClass("active");
            }
            else {
                $('#MainContent_divFrequent').css("display", "none");
                $('#MainContent_divRecent').css("display", "block");
                $('#MainContent_divRecent').addClass("active");
                $('#MainContent_divFrequent').removeClass("active");
            }

        }

        function isNumberKey(evt, obj) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                     // Number of digits right of decimal point.
                     (match[1] ? match[1].length : 0)
                     // Adjust for scientific notation.
                     - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;
                if (charCode == 46) return false;
            }
            else {
                if (value.length > 10) {
                    if (charCode == 46) return true;
                    else return false;
                }
            }
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function calculateYearlyQty(evt, obj) {
            var value = obj.value;
            if (value != undefined && value != '') {
                var mqty_id = obj.id;
                var aqty_id = mqty_id.replace("txtMQTY", "txtAQTY");
                var actual_id = "#" + aqty_id;
                $(actual_id).val(value * 12);
            }
        }
        function calculateMonthlyQty(evt, obj) {
            var value = obj.value;
            if (value != undefined && value != '') {
                var mqty_id = obj.id;
                var aqty_id = mqty_id.replace("txtAQTY", "txtMQTY");
                var actual_id = "#" + aqty_id;
                $(actual_id).val(value / 12);
            }
        }
        function calDCrate(evt, obj) {
            var value = obj.value;
            if (value != undefined && value != '') {
                var ep_id = obj.id;
                var lp_id = ep_id.replace("txtTargetPrice", "txtDLP");
                var dc_id = ep_id.replace("txtTargetPrice", "txtDCRate");
                var actual_lp_id = "#" + lp_id;
                var actual_dc_id = "#" + dc_id;
                var listprice = $(actual_lp_id).val();
                var rate = (listprice - value) * 100 / listprice;
                console.log(rate);
                $(actual_dc_id).val(parseFloat(rate).toFixed(2));
            }
        }

        function calExpectedPrice(evt, obj) {

            var value = obj.value;
            if (value != undefined && value != '') {
                var dc_id = obj.id;
                var lp_id = dc_id.replace("txtDCRate", "txtDLP");
                var ep_id = dc_id.replace("txtDCRate", "txtTargetPrice");
                var actual_lp_id = "#" + lp_id;
                var actual_ep_id = "#" + ep_id;
                var listprice = $(actual_lp_id).val();
                var targetprice = listprice * (1 - (value / 100));
                console.log(targetprice);
                $(actual_ep_id).val(parseFloat(targetprice).toFixed(2));
            }
        }
        function Deletepopup(evt, obj) {
            debugger;
            var id = obj.id;
            var item_id = id.replace("imgbtnDel", "ddlitemdesc");
            var item = $("#" + item_id + " option:selected").text().replace(/\s+/g, " ");
            if (!confirm("Do you want to delete " + item + "?")) { return false; };
        }
        function validateFields(evt, obj) {

            var table = $('#MainContent_grdPriceRequest');
            var errFlag = 0;
            var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;
            for (var i = 0; i < rowCount; i++) {
                if (IsnullOrEmpty($("#MainContent_grdPriceRequest_ddlitem_" + i).val())) {
                    $("#MainContent_grdPriceRequest_ddlitem_" + i).css("border", "1px solid red");

                    $('.select2-container').css({ "border": "1px solid red;" });

                    errFlag++;
                }
                else {
                    $('.select2-container').css({ "border": "1px solid #aaa;" });
                }
                if (IsnullOrEmpty($("#MainContent_grdPriceRequest_ddlitemdesc_" + i).val())) {
                    $("#MainContent_grdPriceRequest_ddlitemdesc_" + i).css("border", "1px solid red");
                    errFlag++;
                }
                else {
                    $("#MainContent_grdPriceRequest_ddlitemdesc_" + i).css("border", "");
                }
                if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtTotQTY_" + i).val())) {
                    $("#MainContent_grdPriceRequest_txtTotQTY_" + i).css("border", "1px solid red");
                    errFlag++;
                }
                else {
                    $("#MainContent_grdPriceRequest_txtTotQTY_" + i).css("border", "");
                }

                var ordertype = $("#MainContent_grdPriceRequest_ddlOrder_" + i).val();
                if (ordertype == "schedule") {
                    if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtQTYPO_" + i).val())) {
                        $("#MainContent_grdPriceRequest_txtQTYPO_" + i).css("border", "1px solid red");
                        errFlag++;
                    }
                    else {
                        $("#MainContent_grdPriceRequest_txtQTYPO_" + i).css("border", "");
                    }
                }

                if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtTargetPrice_" + i).val())) {
                    $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "1px solid red");
                    errFlag++;
                }
                else {
                    $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "");
                    var tp = $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).val();
                    var ap = $("#MainContent_grdPriceRequest_hdnAgreementPrice_" + i).val();
                    if (!IsnullOrEmpty(ap) && ap > 0) {
                        if (tp >= ap) {
                            $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "1px solid red");
                            errFlag++;
                            $("#MainContent_grdPriceRequest_lblPriceError_" + i).text("Expected price should be less than the agreement price.");

                        }
                        else {
                            $("#MainContent_grdPriceRequest_lblPriceError_" + i).text("");
                            $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "");
                        }
                    }
                    else {
                        $("#MainContent_grdPriceRequest_lblPriceError_" + i).text("");
                        $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "");
                    }
                }
                if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtDCRate_" + i).val())) {
                    $("#MainContent_grdPriceRequest_txtDCRate_" + i).css("border", "1px solid red");
                    errFlag++;
                }
                else {
                    $("#MainContent_grdPriceRequest_txtDCRate_" + i).css("border", "");
                }
                if (IsnullOrEmpty($("#MainContent_grdPriceRequest_ddlCustName_" + i).val())) {
                    $("#MainContent_grdPriceRequest_ddlCustName_" + i).css("border", "1px solid red");
                    $("#MainContent_grdPriceRequest_lblCustError_" + i).text("Required");
                    errFlag++;
                }
                else {
                    $("#MainContent_grdPriceRequest_ddlCustName_" + i).css("border", "");
                    var cust = $("#MainContent_grdPriceRequest_ddlCustName_" + i).val();
                    $("#MainContent_grdPriceRequest_lblCustError_" + i).text("");
                    $("#MainContent_grdPriceRequest_hdnCustomers_" + i).val(cust.toString());
                }
                //if (IsnullOrEmpty($("#MainContent_grdPriceRequest_ddlCompanyName_" + i).val())) {
                //    $("#MainContent_grdPriceRequest_ddlCompanyName_" + i).css("border", "1px solid red");
                //    errFlag++;
                //}
                //else {
                //    $("#MainContent_grdPriceRequest_ddlCompanyName_" + i).css("border", "");
                //}
            }
            if (errFlag > 0) {
                return false;
            }
            else {
                return true;
            }
        }
        function IsnullOrEmpty(val) {
            if (val != '' && val != undefined && val != '--Select--')
                return false;
            else
                return true;
        }
        function OrderChange(e) {

            var ordertype = e.options[e.selectedIndex].value;
            var id = e.id;
            var feq_id = id.replace("ddlOrder", "ddlFrequency");
            var qty_id = id.replace("ddlOrder", "txtQTYPO");
            if (ordertype == "schedule") {
                $('#' + feq_id).removeClass('disabled');
                $('#' + feq_id).prop("disabled", false);
                $('#' + qty_id).removeClass('disabled');
                $('#' + qty_id).prop("disabled", false);
            }
            else {
                $('#' + feq_id).addClass('disabled');
                $('#' + feq_id).prop("disabled", true);
                $('#' + qty_id).addClass('disabled');
                $('#' + qty_id).prop("disabled", true);
            }
        }

        function ShowProgress() {
            debugger;
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Quote</a>
                        </li>
                        <li class="current">Request For Quote</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mn_margin">
        <asp:ScriptManager ID="SM1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>

        <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">
            <%--<asp:Panel runat="server" ID="pnlData">--%>
            <ContentTemplate>
                <asp:Panel ID="panelref" runat="server" CssClass="filter_panel">
                    <div class="col-md-12 nopadding">
                        <div class="col-md-1">
                            <asp:Label ID="lblRef" runat="server" Text="Reference"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <div class="controls">
                                <asp:TextBox ID="txtRef" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <asp:Button runat="server" ID="btnSave" CssClass="btnSubmit" Text="Save" OnClientClick="return validateFields(event,this);" OnClick="btnSave_Click" />
                        </div>
                    </div>
                </asp:Panel>
                <asp:Label runat="server" ID="lblmessage"></asp:Label>

                <asp:GridView ID="grdPriceRequest" CssClass="display compact" runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="Item Code">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" Value='<%# Eval("ItemNumber")%>' />
                                <asp:HiddenField runat="server" ID="hdnStockCode" />
                                <asp:Label CssClass="ddl_item" ID="lblitem" runat="server"></asp:Label>
                            </ItemTemplate>
                           <%-- <FooterTemplate>
                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("ItemNumber")%>' />
                            <asp:HiddenField runat="server" ID="hdnStockCode" />
                            <asp:DropDownList CssClass="ddl_item" ID="ddlitem" runat="server"></asp:DropDownList>
                        </FooterTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" HeaderStyle-Width="200px">
                            <ItemTemplate>
                                <asp:Label CssClass="ddl_item" ID="lblitemdesc" runat="server"></asp:Label>
                            </ItemTemplate>
                       <%--      <FooterTemplate>
                            <asp:DropDownList CssClass="ddl_item" ID="ddlitemdesc" runat="server"></asp:DropDownList>
                        </FooterTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderWHS" Text="WHS" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblWHS" CssClass="ddl" runat="server" Style="width: 70px;"></asp:Label>
                            </ItemTemplate>
                            <%--<FooterTemplate>
                            <asp:TextBox ID="txtWHS" ReadOnly="true" Text='<%#Bind("WHS") %>' CssClass="ddl" runat="server" Style="width: 70px;"></asp:TextBox>
                            </FooterTemplate>--%>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderOrder" Text="Order Type" runat="server" CssClass="required"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblOrder" runat="server" CssClass="ddl">
                                </asp:Label>
                            </ItemTemplate>
                           <%-- <FooterTemplate>
                            <asp:DropDownList ID="ddlOrder" runat="server" CssClass="ddl" onchange="OrderChange(this);">
                                <asp:ListItem Text="One Time" Value="onetime"></asp:ListItem>
                                <asp:ListItem Text="Schedule" Value="schedule"></asp:ListItem>
                            </asp:DropDownList>
                        </FooterTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label1" Text="Order Frequency" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblFrequency" runat="server" CssClass="ddl">
                                </asp:Label>
                            </ItemTemplate>
                            <%-- <FooterTemplate>
                            <asp:DropDownList ID="ddlFrequency" runat="server" CssClass="ddl">
                                <asp:ListItem Text="Weekly" Value="weekly"></asp:ListItem>
                                <asp:ListItem Text="Fortnightly" Value="fortnightly"></asp:ListItem>
                                <asp:ListItem Text="Monthly" Value="monthly"></asp:ListItem>
                            </asp:DropDownList>
                        </FooterTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderAQTY" Text="Total QTY" runat="server" CssClass="required"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblTotQTY" CssClass="ddl" runat="server" Style="width: 80px;"></asp:Label>
                            </ItemTemplate>
                       <%--    <FooterTemplate>
                            <asp:TextBox ID="txtTotQTY" Text='<%#Bind("Total_QTY") %>' onkeypress="return isNumberKey(event,this);" CssClass="ddl" runat="server" Style="width: 80px;"></asp:TextBox>
                        </FooterTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderQTYPO" Text="QTY Per Order" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblQTYPO" CssClass="ddl" runat="server" Style="width: 80px;"></asp:Label>
                            </ItemTemplate>
                   <%--         <FooterTemplate>
                            <asp:TextBox ID="txtQTYPO" Text='<%#Bind("QTYPO") %>' onkeypress="return isNumberKey(event,this);" CssClass="ddl" runat="server" Style="width: 80px;"></asp:TextBox>
                        </FooterTemplate>--%>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderDLP" Text="List Price" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDLP" CssClass="ddl disabled" runat="server" Style="width: 80px;"></asp:Label>
                            </ItemTemplate>
                         <%--    <FooterTemplate>
                            <asp:TextBox ID="txtDLP" ReadOnly="true" Text='<%#Bind("DLP") %>' CssClass="ddl disabled" runat="server" Style="width: 80px;"></asp:TextBox>
                       </FooterTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderAP" Text="Agreement Price" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAP" CssClass="ddl disabled" runat="server" Style="width: 80px;"></asp:Label>
                            </ItemTemplate>
                     <%--        <FooterTemplate>
                            <asp:TextBox ID="txtAP" ReadOnly="true" CssClass="ddl disabled" runat="server" Style="width: 80px;"></asp:TextBox>
                        </FooterTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderTargetPrice" Text="Expected Price" runat="server" CssClass="required"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblTargetPrice" CssClass="ddl" runat="server" Style="width: 80px;"></asp:Label>
                            </ItemTemplate>
                      <%--      <FooterTemplate>
                            <asp:TextBox ID="txtTargetPrice" Text='<%#Bind("TargetPrice") %>' onkeypress="return isNumberKey(event,this);" onkeyup="calDCrate(event,this);" CssClass="ddl" runat="server" Style="width: 80px;"></asp:TextBox>
                            <asp:HiddenField ID="hdnAgreementPrice" runat="server" />
                            <asp:Label ID="lblPriceError" runat="server" Style="color: red;" Text=""></asp:Label>
                        </FooterTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderDCRate" Text="Discount Rate(%)" runat="server" CssClass="required"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDCRate" CssClass="ddl" runat="server" Style="width: 80px;"></asp:Label>
                            </ItemTemplate>
             <%--                <FooterTemplate>
                            <asp:TextBox ID="txtDCRate" Text='<%#Bind("DCRate") %>' onkeypress="return isNumberKey(event,this);" onkeyup="calExpectedPrice(event,this);" CssClass="ddl" runat="server" Style="width: 80px;"></asp:TextBox>
                        </FooterTemplate>--%>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderCustName" Text="Cust Name" runat="server" CssClass="required"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCustName" runat="server" CssClass="ddl" Style="width: 200px;">
                                </asp:Label>
                            </ItemTemplate>
                       <%--      <FooterTemplate>
                            <asp:DropDownList ID="ddlCustName" runat="server" CssClass="ddl" Style="width: 200px;">
                            </asp:DropDownList>
                            <asp:HiddenField ID="hdnCustomers" runat="server" Value='<%#Bind("CustName") %>' />
                            <asp:Label ID="lblCustError" runat="server" Style="color: red;" Text=""></asp:Label>
                        </FooterTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderCustSP" Text="Sales Price" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCustSP" CssClass="ddl" Style="width: 80px;" runat="server"></asp:Label>
                            </ItemTemplate>
                    <%--         <FooterTemplate>
                            <asp:TextBox ID="txtCustSP" Text='<%#Bind("CustSP") %>' CssClass="ddl" Style="width: 80px;" onkeypress="return isNumberKey(event,this);" runat="server"></asp:TextBox>
                        </FooterTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderCompanyName" Text="Competitor Company" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCompanyName" runat="server" CssClass="ddl" Style="width: 200px;">
                                </asp:Label>
                            </ItemTemplate>
                 <%--            <FooterTemplate>
                            <asp:DropDownList ID="ddlCompanyName" runat="server" CssClass="ddl" Style="width: 200px;">
                            </asp:DropDownList>
                        </FooterTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderDescription" Text="Competitor Product" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" Style="width: 200px;" CssClass="ddl" runat="server"></asp:Label>
                            </ItemTemplate>
                         <%--    <FooterTemplate>
                            <asp:TextBox ID="txtDescription" Text='<%#Bind("Description") %>' Style="width: 200px;" CssClass="ddl" runat="server"></asp:TextBox>
                        </FooterTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderCompanySP" Text="Competitor Sales Price" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCompanySP" CssClass="ddl" Style="width: 80px;" runat="server"></asp:Label>
                            </ItemTemplate>
                  <%--          <FooterTemplate>
                            <asp:TextBox ID="txtCompanySP" Text='<%#Bind("CompanySP") %>' onkeypress="return isNumberKey(event,this);" CssClass="ddl" Style="width: 80px;" runat="server"></asp:TextBox>
                        </FooterTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label Text="Actions" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:ImageButton CommandName="Delete" OnClientClick="return Deletepopup(event,this);" runat="server" Style="width: 25px;" ID="imgbtnDel" ImageUrl="images/delete.png" />
                            </ItemTemplate>
                  <%--           <FooterTemplate>
                            <asp:ImageButton OnClientClick="return validateFields(event,this);" CommandName="AddItem" CommandArgument='<%# Eval("ItemNumber") %>' runat="server" Style="width: 25px;" ID="imgbtnAdd" ImageUrl="images/add-icon.jpeg" />
                        </FooterTemplate>--%>
                        </asp:TemplateField>
                    </Columns>
                    
                </asp:GridView>

                <table id="tbladd">
                    <tr>
                        <td>
                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("ItemNumber")%>' />
                            <asp:HiddenField runat="server" ID="hdnStockCode" />
                            <asp:DropDownList CssClass="ddl_item" ID="ddlitem" runat="server" ></asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList CssClass="ddl_item" ID="ddlitemdesc" runat="server" ></asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox ID="txtWHS" ReadOnly="true" Text='<%#Bind("WHS") %>' CssClass="ddl" runat="server" Style="width: 70px;"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlOrder" runat="server" CssClass="ddl" onchange="OrderChange(this);">
                                <asp:ListItem Text="One Time" Value="onetime"></asp:ListItem>
                                <asp:ListItem Text="Schedule" Value="schedule"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlFrequency" runat="server" CssClass="ddl">
                                <asp:ListItem Text="Weekly" Value="weekly"></asp:ListItem>
                                <asp:ListItem Text="Fortnightly" Value="fortnightly"></asp:ListItem>
                                <asp:ListItem Text="Monthly" Value="monthly"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox ID="txtTotQTY" Text='<%#Bind("Total_QTY") %>' onkeypress="return isNumberKey(event,this);" CssClass="ddl" runat="server" Style="width: 80px;"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtQTYPO" Text='<%#Bind("QTYPO") %>' onkeypress="return isNumberKey(event,this);" CssClass="ddl" runat="server" Style="width: 80px;"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtDLP" ReadOnly="true" Text='<%#Bind("DLP") %>' CssClass="ddl disabled" runat="server" Style="width: 80px;"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAP" ReadOnly="true" CssClass="ddl disabled" runat="server" Style="width: 80px;"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtTargetPrice" Text='<%#Bind("TargetPrice") %>' onkeypress="return isNumberKey(event,this);" onkeyup="calDCrate(event,this);" CssClass="ddl" runat="server" Style="width: 80px;"></asp:TextBox>
                            <asp:HiddenField ID="hdnAgreementPrice" runat="server" />
                            <asp:Label ID="lblPriceError" runat="server" Style="color: red;" Text=""></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtDCRate" Text='<%#Bind("DCRate") %>' onkeypress="return isNumberKey(event,this);" onkeyup="calExpectedPrice(event,this);" CssClass="ddl" runat="server" Style="width: 80px;"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCustName" runat="server" CssClass="ddl" Style="width: 200px;">
                            </asp:DropDownList>
                            <asp:HiddenField ID="hdnCustomers" runat="server" Value='<%#Bind("CustName") %>' />
                            <asp:Label ID="lblCustError" runat="server" Style="color: red;" Text=""></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCustSP" Text='<%#Bind("CustSP") %>' CssClass="ddl" Style="width: 80px;" onkeypress="return isNumberKey(event,this);" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCompanyName" runat="server" CssClass="ddl" Style="width: 200px;">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox ID="txtDescription" Text='<%#Bind("Description") %>' Style="width: 200px;" CssClass="ddl" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCompanySP" Text='<%#Bind("CompanySP") %>' onkeypress="return isNumberKey(event,this);" CssClass="ddl" Style="width: 80px;" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:ImageButton OnClientClick="return validateFields(event,this);" CommandName="AddItem" CommandArgument='<%# Eval("ItemNumber") %>' runat="server" Style="width: 25px;" ID="imgbtnAdd" ImageUrl="images/add-icon.jpeg" />
                        </td>
                    </tr>
                </table>


                <div id="divOrders" runat="server" style="display: none;">
                    <div class="tabbed skin-turquoise round" id="skinable" style="margin-bottom: 10px;">
                        <ul>
                            <li id="freqList" runat="server" class="active" onclick="tabchange(this);">Frequently Ordered</li>
                            <li id="recentList" runat="server" onclick="tabchange(this);">Recently Ordered</li>
                        </ul>
                    </div>

                    <div class="col-md-12 nopad" id="divFrequent" runat="server" style="display: block">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 order" id="divItem1" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkItem1" runat="server" OnClick="lnkItem1_Click">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item :<strong>
                                            <asp:Label ID="lblItem1" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4>
                                            <strong>
                                                <asp:Label ID="lblDesc1" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div></div>
                        <div class="col-md-2 order" id="divItem2" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkItem2" runat="server" OnClick="lnkItem2_Click">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblItem2" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4>
                                            <strong>
                                                <asp:Label ID="lblDesc2" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-2 order" id="divItem3" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkItem3" runat="server" OnClick="lnkItem3_Click">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblItem3" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4>
                                            <strong>
                                                <asp:Label ID="lblDesc3" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-2 order" id="divItem4" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkItem4" runat="server" OnClick="lnkItem4_Click">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblItem4" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4>
                                            <strong>
                                                <asp:Label ID="lblDesc4" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-2 order" id="divItem5" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkItem5" runat="server" OnClick="lnkItem5_Click">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblItem5" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4>
                                            <strong>
                                                <asp:Label ID="lblDesc5" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-1"></div>
                    </div>

                    <div class="col-md-12 nopad" id="divRecent" runat="server" style="display: none">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 order" id="divOItem1" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkOItem1" runat="server" OnClick="lnkOItem1_Click">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblOItem1" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4>
                                            <strong>
                                                <asp:Label ID="lblODesc1" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div></div>
                        <div class="col-md-2 order" id="divOItem2" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkOItem2" runat="server" OnClick="lnkOItem2_Click">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblOItem2" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4>
                                            <strong>
                                                <asp:Label ID="lblODesc2" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-2 order" id="divOItem3" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkOItem3" runat="server" OnClick="lnkOItem3_Click">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblOItem3" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4><strong>
                                            <asp:Label ID="lblODesc3" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-2 order" id="divOItem4" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkOItem4" runat="server" OnClick="lnkOItem4_Click">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblOItem4" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4><strong>
                                            <asp:Label ID="lblODesc4" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-2 order" id="divOItem5" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkOItem5" runat="server" OnClick="lnkOItem5_Click">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblOItem5" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4><strong>
                                            <asp:Label ID="lblODesc5" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>

            </ContentTemplate>
            <%-- </asp:Panel>--%>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="updateProgress" runat="server">
            <ProgressTemplate>
                <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                    <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="loader.gif" alt="" />
        </div>
    </div>



</asp:Content>


