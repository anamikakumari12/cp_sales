﻿using DistributorSystemBO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DistributorSystemDAL
{
   public class FileUplaodDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataTable getFileDetailsDAL()
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getDocumentDetails, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable getsaleFileDetailsDAL(int flag,int fileid)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getSalesDocumentDetails, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.insertflag, SqlDbType.Int).Value = flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.file_id, SqlDbType.Int).Value = fileid;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataSet getdocumenttypeDAL()
        {
            DataSet dtOutput = new DataSet();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getDocumentType, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }
        public FileUploadBO UploadFileDetailsDAL(FileUploadBO objfileBO)
        {
            FileUploadBO objOutput = new FileUploadBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.insert_fileInfo, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.FileName, SqlDbType.VarChar, -1).Value = objfileBO.FileName;
                sqlcmd.Parameters.Add(ResourceFileDAL.UploadedDate, SqlDbType.DateTime).Value = objfileBO.UploadDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.FromDate, SqlDbType.DateTime).Value = objfileBO.FromDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.Todate, SqlDbType.DateTime).Value = objfileBO.ToDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.FilePath, SqlDbType.VarChar, -1).Value = objfileBO.FilePath;
                sqlcmd.Parameters.Add(ResourceFileDAL.doctype, SqlDbType.VarChar, -1).Value = objfileBO.doctype;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.err_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.error_code].Value);
                objOutput.err_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        public FileUploadBO UploadSalesFileDetailsDAL(FileUploadBO objfileBO,DataTable dt)
        {
            FileUploadBO objOutput = new FileUploadBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.insert_SalesfileInfoAndItemDetails, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.FileName, SqlDbType.VarChar, -1).Value = objfileBO.FileName;
                sqlcmd.Parameters.Add(ResourceFileDAL.UploadedDate, SqlDbType.VarChar,50).Value = objfileBO.UploadDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.FromDate, SqlDbType.DateTime).Value = objfileBO.FromDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.Todate, SqlDbType.DateTime).Value = objfileBO.ToDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.insertflag, SqlDbType.Int).Value = objfileBO.insertflag;
                sqlcmd.Parameters.Add(ResourceFileDAL.file_id, SqlDbType.Int).Value = objfileBO.file_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.salesname, SqlDbType.VarChar).Value = objfileBO.salesName;
                sqlcmd.Parameters.AddWithValue(ResourceFileDAL.salesTable, dt);
                sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.err_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.error_code].Value);
                objOutput.err_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        public FileUploadBO UpdateIsUploadColumnDetails(string distributornumber)
        {
            FileUploadBO objOutput = new FileUploadBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_UpdateLoginUser, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.distributorNumber, SqlDbType.VarChar, 100).Value = distributornumber;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.err_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.error_code].Value);
                objOutput.err_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        public FileUploadBO DeleteFileDAL(string fileName)
        {
            FileUploadBO objOutput = new FileUploadBO();

            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand(ResourceFileDAL.DeleteDocument, sqlconn);
                sqlconn.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(ResourceFileDAL.FileName, SqlDbType.VarChar, -1).Value = fileName;
                cmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();

                objOutput.err_code = Convert.ToInt32(cmd.Parameters[ResourceFileDAL.error_code].Value);
                objOutput.err_msg = Convert.ToString(cmd.Parameters[ResourceFileDAL.error_msg].Value);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        public FileUploadBO DeleteSaleFileDAL(int fileid1)
        {
            FileUploadBO objOutput = new FileUploadBO();

            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand(ResourceFileDAL.DeleteSalesItem, sqlconn);
                sqlconn.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(ResourceFileDAL.file_id, SqlDbType.VarChar, -1).Value = fileid1;
                cmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();

                objOutput.err_code = Convert.ToInt32(cmd.Parameters[ResourceFileDAL.error_code].Value);
                objOutput.err_msg = Convert.ToString(cmd.Parameters[ResourceFileDAL.error_msg].Value);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }
    }
}
