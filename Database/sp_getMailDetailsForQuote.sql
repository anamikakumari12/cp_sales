USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_getMailDetailsForQuote]    Script Date: 8/20/2019 9:44:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_getMailDetailsForQuote]
(
@Module VARCHAR(100),
@Component VARCHAR(100),
@RefNumber VARCHAR(100),
@item VARCHAR(100)=null
)
AS
BEGIN

DECLARE @Requestor VARCHAR(10), @RequestedFlag VARCHAR(20), @RequestorMailId VARCHAR(100), @Customer VARCHAR(10), @Cust_name VARCHAR(MAX)
DECLARE @TO VARCHAR(MAX), @CC VARCHAR(MAX), @Subject VARCHAR(MAX), @Body VARCHAR(MAX)

DECLARE @BRANCH VARCHAR(100),@BMFLAG VARCHAR(10),@HOFLAG VARCHAR(10),@DeskFLAG VARCHAR(10)

SELECT TOP 1 @Requestor=RequestedBy, @RequestedFlag=RequestedBy_Flag FROM Quote_ref WHERE Ref_number=@RefNumber



IF(@RequestedFlag='CUSTOMER')
BEGIN
	SELECT @Customer=Cust_number, @Cust_name=Cust_Name FROM Quote_ref WHERE Ref_number=@RefNumber
	SELECT @RequestorMailId=EmailId FROM LoginInfo WHERE EngineerId=@Requestor
END
ELSE IF (@RequestedFlag='DISTRIBUTOR')
BEGIN

	SELECT @RequestorMailId=Email_Id  FROM tbl_LoginInfoForDistributor WHERE Id=@Requestor
	SELECT @Customer=CP_number, @Cust_name=CP_Name FROM Quote_ref WHERE Ref_number=@RefNumber
END
--SELECT @Requestor 'Requestor'
IF(@RequestorMailId IS NOT NULL OR @RequestorMailId<>'')
BEGIN
	IF(@Module ='Quote' AND (@Component='SubmitQuote' OR @Component='ReSubmitQuote'))
	BEGIN
		SELECT @BMFLAG=BM, @HOFLAG=HO,@DeskFLAG=Desk FROM MailConfiguration where Module =@Module AND Component=@Component

		SELECT @BRANCH=BranchCode FROM LoginInfo WHERE EngineerId =(SELECT assigned_salesengineer_id FROM tt_customer_master WHERE customer_number=@Customer)
		SELECT  @TO=STUFF(
					(   SELECT ';' + CONVERT(VARCHAR(MAX), EmailId) 
						FROM
						(
						SELECT EmailIds EmailId FROM MailConfiguration WHERE Module =@Module AND Component=@Component
						UNION
						SELECT EmailId FROM LoginInfo WHERE RoleId ='BM' AND LoginStatus=1 AND BranchCode=@BRANCH AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE EngineerId IN (
						SELECT territory_engineer_id FROM tt_territory_region_relation WHERE region_code=@BRANCH) AND LoginStatus=1 AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE RoleId ='HO' AND LoginStatus=1 AND @HOFLAG='YES'
						UNION
						SELECT Email_id from tbl_DeskLogin WHERE Status=1 AND @DeskFLAG='YES') t
					FOR xml path('')
                   )
                   , 1
                   , 1
                   , '')
		
		--SELECT  @TO 'TO'

		SELECT @CC=@RequestorMailId

		SELECT @Subject='Quote is submitted with reference number : '+@RefNumber

		SELECT @Body='Hi,<br/> The quote request is submitted with reference number '+@RefNumber+' for Customer/Channel Partner : '+@Cust_name+'. Please update the status as required.<br/><br/> Do not reply to this mail.'

	END
	ELSE IF (@Module ='Quote' AND @Component='StatusChange')
	BEGIN
		IF EXISTS(SELECT * FROM Quote_ref WHERE Ref_number=@RefNumber AND Status='Sent For Approval')
		BEGIN
		SELECT @TO='',@CC='',@Subject='',@Body=''
		END
		ELSE
		BEGIN
			SELECT @BMFLAG=BM, @HOFLAG=HO,@DeskFLAG=Desk FROM MailConfiguration where Module =@Module AND Component=@Component
			SELECT @BRANCH=BranchCode FROM LoginInfo WHERE EngineerId =(SELECT assigned_salesengineer_id FROM tt_customer_master WHERE customer_number=@Customer)
			SELECT  @CC=STUFF(
					(   SELECT ';' + CONVERT(VARCHAR(MAX), EmailId) 
						FROM
						(
						SELECT EmailIds EmailId FROM MailConfiguration WHERE Module =@Module AND Component=@Component
						UNION
						SELECT EmailId FROM LoginInfo WHERE RoleId ='BM' AND LoginStatus=1 AND BranchCode=@BRANCH AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE EngineerId IN (
						SELECT territory_engineer_id FROM tt_territory_region_relation WHERE region_code=@BRANCH) AND LoginStatus=1 AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE RoleId ='HO' AND LoginStatus=1 AND @HOFLAG='YES'
						UNION
						SELECT Email_id from tbl_DeskLogin WHERE Status=1 AND @DeskFLAG='YES') t
					FOR xml path('')
                   )
                   , 1
                   , 1
                   , '')
			
			SELECT @TO=@RequestorMailId

			SELECT @Subject='Quote approval for reference number : '+@RefNumber

			SELECT @Body='Hi,<br/> The requested quote with reference number '+@RefNumber+' for Customer/Channel Partner : '+@Cust_name+' is completed. Please place order for approved quotes and have a look for rejected quotes.<br/><br/> Do not reply to this mail.'
		END
	END
	ELSE IF(@Module ='Quote' AND @Component='PlaceOrder')
	BEGIN
		SELECT @BMFLAG=BM, @HOFLAG=HO,@DeskFLAG=Desk FROM MailConfiguration where Module =@Module AND Component=@Component

		SELECT @BRANCH=BranchCode FROM LoginInfo WHERE EngineerId =(SELECT assigned_salesengineer_id FROM tt_customer_master WHERE customer_number=@Customer)
		SELECT  @TO=STUFF(
					(   SELECT ';' + CONVERT(VARCHAR(MAX), EmailId) 
						FROM
						(
						SELECT EmailIds EmailId FROM MailConfiguration WHERE Module =@Module AND Component=@Component
						UNION
						SELECT EmailId FROM LoginInfo WHERE RoleId ='BM' AND LoginStatus=1 AND BranchCode=@BRANCH AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE EngineerId IN (
						SELECT territory_engineer_id FROM tt_territory_region_relation WHERE region_code=@BRANCH) AND LoginStatus=1 AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE RoleId ='HO' AND LoginStatus=1 AND @HOFLAG='YES'
						UNION
						SELECT Email_id from tbl_DeskLogin WHERE Status=1 AND @DeskFLAG='YES') t
					FOR xml path('')
                   )
                   , 1
                   , 1
                   , '')
		
		SELECT @CC=@RequestorMailId

		SELECT @Subject='Order placed for reference number : '+@RefNumber+' and item number :'+@item

		SELECT @Body='Hi,<br/> The order is placed for reference number '+@RefNumber+'  and item number :'+@item+'. <br/><br/> Do not reply to this mail.'

	END
END

Select @TO 'To', @CC 'CC', @Subject 'Subject', @Body 'Message'

END



