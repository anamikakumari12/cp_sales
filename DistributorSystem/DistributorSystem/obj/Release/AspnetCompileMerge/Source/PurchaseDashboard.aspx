﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurchaseDashboard.aspx.cs" Inherits="DistributorSystem.PurchaseDashboard" MasterPageFile="~/MasterPage.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/radar.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    <script type="text/javascript">
        function LoadCharts() {
            debugger;
            LoadChartHistoricalPurchase();
            LoadChartConsolidatedPurchaseView();
            LoadChartFamilyView();
        }
        function LoadChartHistoricalPurchase() {
            var tmp = null;
            $.ajax({
                type: "POST",
                url: 'PurchaseDashboard.aspx/LoadChartHistoricalPurchase',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];
                    var chart = AmCharts.makeChart("ChartHistoricalPurchase", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "gridColor": "#FFFFFF",
                            "gridAlpha": 0,
                            "dashLength": 0
                        }],
                        "gridAboveGraphs": true,
                        "startDuration": 1,
                        "graphs": [{
                            "type": "column",
                            "title": "value",
                            "balloonText": "[[title]]: <b>[[value]]</b>",
                            "bullet": "round",
                            "bulletSize": 10,
                            "bulletBorderColor": "#ffffff",
                            "bulletBorderAlpha": 1,
                            "bulletBorderThickness": 2,
                            "fillAlphas": 0.8,
                            "lineAlpha": 0.2,
                            "fillColorsField": "color",
                            "valueField": "value"
                        }],
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "Targets",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "gridAlpha": 0
                        },
                        "legend": {}
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartConsolidatedPurchaseView() {
            var tmp = null;
            $.ajax({
                type: "POST",
                url: 'PurchaseDashboard.aspx/LoadChartConsolidatedPurchaseView',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];
                    var chart = AmCharts.makeChart("ChartConsolidatedPuchaseView", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "gridColor": "#FFFFFF",
                            "gridAlpha": 0,
                            "dashLength": 0
                        }],
                        "gridAboveGraphs": true,
                        "startDuration": 1,
                        "graphs": [{
                            "type": "column",
                            "title": "value",
                            "balloonText": "[[title]]: <b>[[value]]</b>",
                            "bullet": "round",
                            "bulletSize": 10,
                            "bulletBorderColor": "#ffffff",
                            "bulletBorderAlpha": 1,
                            "bulletBorderThickness": 2,
                            "fillAlphas": 0.8,
                            "lineAlpha": 0.2,
                            "fillColorsField": "color",
                            "valueField": "value"
                        }],
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "Targets",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "gridAlpha": 0
                        },
                        "legend": {}
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        
        function LoadChartFamilyView() {
            var tmp = null;
            $.ajax({
                type: "POST",
                url: 'PurchaseDashboard.aspx/LoadChartFamilyView',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    var chart = AmCharts.makeChart("ChartFamilyView", {
                        "type": "serial",
                        "theme": "light",
                        "depth3D": 20,
                        "angle": 30,
                        "legend": {
                            "horizontalGap": 10,
                            "useGraphSettings": true,
                            "markerSize": 10
                        },
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "stackType": "regular",
                            "axisAlpha": 0,
                            "gridAlpha": 0
                        }],
                        "graphs": [{
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 0.8,
                            "labelText": "[[value]]",
                            "lineAlpha": 0.3,
                            "title": "YTD Year-2 Actual",
                            "type": "column",
                            "color": "#000000",
                            "valueField": "YTD Year-2 Actual"
                        }, {
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 0.8,
                            "labelText": "[[value]]",
                            "lineAlpha": 0.3,
                            "title": "YTD Year-1 Actual",
                            "type": "column",
                            "color": "#000000",
                            "valueField": "YTD Year-1 Actual"
                        }, {
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 0.8,
                            "labelText": "[[value]]",
                            "lineAlpha": 0.3,
                            "title": "YTD Budget",
                            "type": "column",
                            "newStack": true,
                            "color": "#000000",
                            "valueField": "YTD Budget"
                        }, {
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 0.8,
                            "labelText": "[[value]]",
                            "lineAlpha": 0.3,
                            "title": "YTD Actuals",
                            "type": "column",
                            "color": "#000000",
                            "valueField": "YTD Actuals"
                        }],
                        "categoryField": "item_family_name",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            "position": "left"
                        }

                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        
        </script>

      <style>
        #ChartHistoricalPurchase
        {
            height:500px;
            width:50%;
        }
        #ChartConsolidatedPuchaseView{
            height:500px;
            width:50%;
        }
          #ChartFamilyView
          {
              height:500px;
              width:100%;
          }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <div class="col-md-12 mn_margin">
        <div>
        <div class="col-md-6" id="ChartHistoricalPurchase"></div>
        <div class="col-md-6" id="ChartConsolidatedPuchaseView"></div>
            
        </div>
        <div>
            <div class="col-md-10 col-lg-offset-1" id="ChartFamilyView"></div>
        </div>
            </div>
    </asp:Content>
