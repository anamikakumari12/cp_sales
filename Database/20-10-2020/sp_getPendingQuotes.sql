USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_getPendingQuotes]    Script Date: 22-10-2020 09:43:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from logi


--exec sp_getPendingQuotes null,null,'Desk',null,null
ALTER PROCEDURE [dbo].[sp_getPendingQuotes]
(
@StartDate VARCHAR(100)=null,
@EndDate VARCHAR(100)=null,
@role VARCHAR(10),
@cter VARCHAR(10)=null,
@assigned_salesengineer_id VARCHAR(10)=null
)
AS
BEGIN

IF(@StartDate='')
SET @StartDate = null
IF(@EndDate='')
SET @EndDate = null
if(@role='Desk')
	SELECT qref.ID
,Ref_number
,Item_code
,Item_Desc
,WHS
,Order_type
,Order_frequency
,Total_QTY
,QTY_perOrder
,List_Price
,Expected_price
,DC_rate
,Discount_Price
,Agreement_Price
,CONVERT(INT,CONVERT(decimal(38,2),Offer_Price)) 'Offer_Price'
,Cust_number
,Cust_Name
,Cust_SP
,Comp_Name
,Comp_Desc
,Comp_SP
,CP_number
,CP_Name
,MOQ
,Status
,Requested_date
,Approval_date
,Rejected_date
,Approved_Rejected_By
,Rejected_Reason
,RequestedBy
,RequestedBy_Flag
,ReRequested_Date
,ReRequested_By
,Assigned_To
,Accept_Visible_Flag
,Multiple_Order_Flag
,CASE WHEN  qref.New_OfferPrice IS NULL OR  qref.New_OfferPrice=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.New_OfferPrice,0))) END New_OfferPrice
,CASE WHEN  qref.Recommended_Price IS NULL OR  qref.Recommended_Price=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.Recommended_Price,0))) END Recommended_Price
,StatusChange_Count
,Quotation_no
,Order_Validity
,Approved_OrderQty
,Approved_OrderFreq,  xpc.ICS 'StockCode', xpc.Price25 AverageSellingPrice, (Select IVTV FROM tt_GALF6TA_II WHERE ICAT=qref.item_code) UnitPrice, 
	dbo.fn_getStock(qref.Item_code) Stock 
	,CASE WHEN qref.Order_Validity is null OR qref.Order_Validity='' 
		THEN  (select CONVERT(INT,VALUE) from TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY')
		ELSE qref.Order_Validity END 'OrderValidity'
		,dbo.fn_getGP(qref.Ref_number,qref.Item_code) GP
,'' TotOrderValue
, '' CP
, '' 'CommentBySE'
,'Auto-Approved By SE' SE_Status
,reg.region_description 'Branch'
	FROM Quote_ref qref JOIN [dbo].[tt_GAL6TA_XPRICE] xpc ON qref.Item_code=xpc.ICAT
	JOIN tt_customer_master cus ON cus.customer_number=ISNULL(qref.CP_number, qref.Cust_number)
	JOIN tt_region reg ON cus.Customer_region=reg.region_code
	WHERE Status IN ('Sent For Approval') 
	AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date)
	 AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
	 AND UPPER(Assigned_To)=UPPER(@role)
	--AND GETDATE()> DATEADD(DD,1, Requested_date)
	AND (qref.Agreement_Price is null OR qref.Agreement_Price='' OR qref.Agreement_Price='0.0')
	UNION
		SELECT qref.ID
,Ref_number
,Item_code
,Item_Desc
,WHS
,Order_type
,Order_frequency
,Total_QTY
,QTY_perOrder
,List_Price
,Expected_price
,DC_rate
,Discount_Price
,Agreement_Price
,CONVERT(INT,CONVERT(decimal(38,2),Offer_Price)) 'Offer_Price'
,Cust_number
,Cust_Name
,Cust_SP
,Comp_Name
,Comp_Desc
,Comp_SP
,CP_number
,CP_Name
,MOQ
,Status
,Requested_date
,Approval_date
,Rejected_date
,Approved_Rejected_By
,Rejected_Reason
,RequestedBy
,RequestedBy_Flag
,ReRequested_Date
,ReRequested_By
,Assigned_To
,Accept_Visible_Flag
,Multiple_Order_Flag
,New_OfferPrice
,Recommended_Price
,StatusChange_Count
,Quotation_no
,Order_Validity
,Approved_OrderQty
,Approved_OrderFreq,  xpc.ICS 'StockCode', xpc.Price25 AverageSellingPrice, (Select IVTV FROM tt_GALF6TA_II WHERE ICAT=qref.item_code) UnitPrice, 
	dbo.fn_getStock(qref.Item_code) Stock 
	,CASE WHEN qref.Order_Validity is null OR qref.Order_Validity='' 
		THEN  (select CONVERT(INT,VALUE) from TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY')
		ELSE qref.Order_Validity END 'OrderValidity'
		,dbo.fn_getGP(qref.Ref_number,qref.Item_code) GP
,'' TotOrderValue
, '' CP
, (Select top 1 StatusChange_Comment from tbl_QuoteStatusLog where Ref_number= qref.Ref_number AND Item_code=qref.Item_code AND Status='Approved By SE' order by ID desc) 'CommentBySE'
,'Approved By SE' SE_Status
,reg.region_description 'Branch'
	FROM Quote_ref qref JOIN [dbo].[tt_GAL6TA_XPRICE] xpc ON qref.Item_code=xpc.ICAT
	JOIN tt_customer_master cus ON cus.customer_number=ISNULL(qref.CP_number, qref.Cust_number)
	JOIN tt_region reg ON cus.Customer_region=reg.region_code
	WHERE Status IN ('Approved By SE') 
	AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date)
	 AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
	AND UPPER(Assigned_To)=UPPER(@role)
else if(@role='HO')
	SELECT ID
,Ref_number
,Item_code
,Item_Desc
,WHS
,Order_type
,Order_frequency
,Total_QTY
,QTY_perOrder
,CONVERT(INT,CONVERT(decimal(38,2),qref.List_Price)) 'List_Price'
,Expected_price
,DC_rate
,Discount_Price
,Agreement_Price
,CONVERT(INT,CONVERT(decimal(38,2),Offer_Price)) 'Offer_Price'
,Cust_number
,Cust_Name
,Cust_SP
,Comp_Name
,Comp_Desc
,Comp_SP
,CP_number
,CP_Name
,MOQ
,Status
,Requested_date
,Approval_date
,Rejected_date
,Approved_Rejected_By
,Rejected_Reason
,RequestedBy
,RequestedBy_Flag
,ReRequested_Date
,ReRequested_By
,Assigned_To
,Accept_Visible_Flag
,Multiple_Order_Flag
,CASE WHEN  qref.New_OfferPrice IS NULL OR  qref.New_OfferPrice=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.New_OfferPrice,0))) END New_OfferPrice
,CASE WHEN  qref.Recommended_Price IS NULL OR  qref.Recommended_Price=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.Recommended_Price,0))) END Recommended_Price
,StatusChange_Count
,Quotation_no
,Order_Validity
,Approved_OrderQty
,Approved_OrderFreq, Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy, xpc.ICS 'StockCode'
	, CONVERT(INT,xpc.Price25) AverageSellingPrice, (Select CONVERT(INT,CONVERT(decimal(38,2), IVTV)) FROM tt_GALF6TA_II WHERE ICAT=qref.item_code) UnitPrice, 
	 CONVERT(INT,dbo.fn_getStock(qref.Item_code)) Stock 
	,CASE WHEN qref.Order_Validity is null OR qref.Order_Validity='' 
		THEN  (select CONVERT(INT,VALUE) from TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY')
		ELSE qref.Order_Validity END 'OrderValidity'
		,dbo.fn_getGP(qref.Ref_number,qref.Item_code) GP
,'' TotOrderValue
, '' CP
FROM Quote_ref qref JOIN [dbo].[tt_GAL6TA_XPRICE] xpc ON qref.Item_code=xpc.ICAT WHERE 
		Status IN ('Request For ReApproval','Approved By BM') AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
		AND (CP_number IN (Select customer_number from tt_customer_master WHERE cter=ISNULL(@cter,'TTA')) 
			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE cter=ISNULL(@cter,'TTA'))) 
		AND UPPER(Assigned_To)=UPPER(@role)
else if(@role='BM')
	SELECT ID
,Ref_number
,Item_code
,Item_Desc
,WHS
,Order_type
,Order_frequency
,Total_QTY
,QTY_perOrder
,List_Price
,Expected_price
,DC_rate
,Discount_Price
,Agreement_Price
,CONVERT(INT,CONVERT(decimal(38,2),Offer_Price)) 'Offer_Price'
,Cust_number
,Cust_Name
,Cust_SP
,Comp_Name
,Comp_Desc
,Comp_SP
,CP_number
,CP_Name
,MOQ
,Status
,Requested_date
,Approval_date
,Rejected_date
,Approved_Rejected_By
,Rejected_Reason
,RequestedBy
,RequestedBy_Flag
,ReRequested_Date
,ReRequested_By
,Assigned_To
,Accept_Visible_Flag
,Multiple_Order_Flag
,CASE WHEN  qref.New_OfferPrice IS NULL OR  qref.New_OfferPrice=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.New_OfferPrice,0))) END New_OfferPrice
,CASE WHEN  qref.Recommended_Price IS NULL OR  qref.Recommended_Price=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.Recommended_Price,0))) END Recommended_Price
,StatusChange_Count
,Quotation_no
,Order_Validity
,Approved_OrderQty
,Approved_OrderFreq,  Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy, xpc.ICS 'StockCode'
	, xpc.Price25 AverageSellingPrice, (Select IVTV FROM tt_GALF6TA_II WHERE ICAT=qref.item_code) UnitPrice, 
	dbo.fn_getStock(qref.Item_code) Stock 
	,CASE WHEN qref.Order_Validity is null OR qref.Order_Validity='' 
		THEN  (select CONVERT(INT,VALUE) from TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY')
		ELSE qref.Order_Validity END 'OrderValidity'
		,dbo.fn_getGP(qref.Ref_number,qref.Item_code) GP
,'' TotOrderValue
, '' CP
, '' 'CommentBySE'
,'Auto-Approved By SE' SE_Status
	FROM Quote_ref qref JOIN [dbo].[tt_GAL6TA_XPRICE] xpc ON qref.Item_code=xpc.ICAT
	WHERE 
		Status IN ('Sent For Approval')
		--AND RequestedBy_Flag='CUSTOMER' 
		AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
		AND (CP_number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select BranchCode from LoginInfo where EngineerId=@assigned_salesengineer_id)) 
			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select BranchCode from LoginInfo where EngineerId=@assigned_salesengineer_id) )) 
		AND UPPER(Assigned_To)=UPPER(@role)
	--	AND GETDATE()> DATEADD(DD,1, Requested_date)
	AND (qref.Agreement_Price is not  null AND qref.Agreement_Price<>'' AND Convert(float,qref.Agreement_Price)>0.0)
		UNION 
		SELECT ID
,Ref_number
,Item_code
,Item_Desc
,WHS
,Order_type
,Order_frequency
,Total_QTY
,QTY_perOrder
,List_Price
,Expected_price
,DC_rate
,Discount_Price
,Agreement_Price
,CONVERT(INT,CONVERT(decimal(38,2),Offer_Price)) 'Offer_Price'
,Cust_number
,Cust_Name
,Cust_SP
,Comp_Name
,Comp_Desc
,Comp_SP
,CP_number
,CP_Name
,MOQ
,Status
,Requested_date
,Approval_date
,Rejected_date
,Approved_Rejected_By
,Rejected_Reason
,RequestedBy
,RequestedBy_Flag
,ReRequested_Date
,ReRequested_By
,Assigned_To
,Accept_Visible_Flag
,Multiple_Order_Flag
,CASE WHEN  qref.New_OfferPrice IS NULL OR  qref.New_OfferPrice=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.New_OfferPrice,0))) END New_OfferPrice
,CASE WHEN  qref.Recommended_Price IS NULL OR  qref.Recommended_Price=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.Recommended_Price,0))) END Recommended_Price
,StatusChange_Count
,Quotation_no
,Order_Validity
,Approved_OrderQty
,Approved_OrderFreq,  Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy, xpc.ICS 'StockCode'
		, xpc.Price25 AverageSellingPrice, (Select IVTV FROM tt_GALF6TA_II WHERE ICAT=qref.item_code) UnitPrice, 
	dbo.fn_getStock(qref.Item_code) Stock 
	,CASE WHEN qref.Order_Validity is null OR qref.Order_Validity='' 
		THEN  (select CONVERT(INT,VALUE) from TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY')
		ELSE qref.Order_Validity END 'OrderValidity'
		,dbo.fn_getGP(qref.Ref_number,qref.Item_code) GP
,'' TotOrderValue
, '' CP
, (Select top 1 StatusChange_Comment from tbl_QuoteStatusLog where Ref_number= qref.Ref_number AND Item_code=qref.Item_code AND Status='Approved By SE' order by ID desc) 'CommentBySE'
,'Approved By SE' SE_Status
	FROM Quote_ref qref JOIN [dbo].[tt_GAL6TA_XPRICE] xpc ON qref.Item_code=xpc.ICAT
	WHERE 
		Status IN ('Approved By SE','Request For ReApproval')
		--AND RequestedBy_Flag='CUSTOMER' 
		AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
		AND (CP_number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select BranchCode from LoginInfo where EngineerId=@assigned_salesengineer_id)) 
			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select BranchCode from LoginInfo where EngineerId=@assigned_salesengineer_id) )) 
		AND UPPER(Assigned_To)=UPPER(@role)

else if(@role='SE')
	SELECT ID
,Ref_number
,Item_code
,Item_Desc
,WHS
,Order_type
,Order_frequency
,Total_QTY
,QTY_perOrder
,List_Price
,Expected_price
,DC_rate
,Discount_Price
,Agreement_Price
,CONVERT(INT,CONVERT(decimal(38,2),Offer_Price)) 'Offer_Price'
,Cust_number
,Cust_Name
,Cust_SP
,Comp_Name
,Comp_Desc
,Comp_SP
,CP_number
,CP_Name
,MOQ
,Status
,Requested_date
,Approval_date
,Rejected_date
,Approved_Rejected_By
,Rejected_Reason
,RequestedBy
,RequestedBy_Flag
,ReRequested_Date
,ReRequested_By
,Assigned_To
,Accept_Visible_Flag
,Multiple_Order_Flag
,CASE WHEN  qref.New_OfferPrice IS NULL OR  qref.New_OfferPrice=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.New_OfferPrice,0))) END New_OfferPrice
,CASE WHEN  qref.Recommended_Price IS NULL OR  qref.Recommended_Price=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.Recommended_Price,0))) END Recommended_Price
,StatusChange_Count
,Quotation_no
,Order_Validity
,Approved_OrderQty
,Approved_OrderFreq, Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy, xpc.ICS 'StockCode'
	, xpc.Price25 AverageSellingPrice, (Select IVTV FROM tt_GALF6TA_II WHERE ICAT=qref.item_code) UnitPrice, 
	dbo.fn_getStock(qref.Item_code) Stock 
	,CASE WHEN qref.Order_Validity is null OR qref.Order_Validity='' 
		THEN  (select CONVERT(INT,VALUE) from TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY')
		ELSE qref.Order_Validity END 'OrderValidity'
		,dbo.fn_getGP(qref.Ref_number,qref.Item_code) GP
,'' TotOrderValue
, '' CP
	FROM Quote_ref qref JOIN [dbo].[tt_GAL6TA_XPRICE] xpc ON qref.Item_code=xpc.ICAT
	WHERE 
		Status IN ('Sent For Approval')
		--AND RequestedBy_Flag='CUSTOMER' 
		AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
		AND (CP_number IN (Select customer_number from tt_customer_master WHERE assigned_salesengineer_id=@assigned_salesengineer_id)
			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE assigned_salesengineer_id=@assigned_salesengineer_id) )
		AND UPPER(Assigned_To)=UPPER(@role)
		AND GETDATE()<= DATEADD(DD,1, Requested_date)
	--UNION
	--SELECT *, Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy FROM Quote_ref 
	--WHERE 
	--	Status IN ('Escalated')
	--	AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
	--	AND (CP_number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select BranchCode from LoginInfo where EngineerId=@assigned_salesengineer_id)) 
	--		OR Cust_Number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select BranchCode from LoginInfo where EngineerId=@assigned_salesengineer_id) )) 
	--	AND UPPER(Assigned_To)=UPPER(@role)
	ELSE IF (@role='TM')
--SELECT *, Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy FROM Quote_ref 
--	WHERE 
--		Status IN ('Sent For Approval')
--		AND RequestedBy_Flag='CUSTOMER' 
--		AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
--		AND (CP_number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select region_code from tt_territory_region_relation where territory_engineer_id=@assigned_salesengineer_id)) 
--			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select region_code from tt_territory_region_relation where territory_engineer_id=@assigned_salesengineer_id) )) 
--		AND UPPER(Assigned_To)=UPPER('BM')
--	UNION
	SELECT ID
,Ref_number
,Item_code
,Item_Desc
,WHS
,Order_type
,Order_frequency
,Total_QTY
,QTY_perOrder
,List_Price
,Expected_price
,DC_rate
,Discount_Price
,Agreement_Price
,CONVERT(INT,CONVERT(decimal(38,2),Offer_Price)) 'Offer_Price'
,Cust_number
,Cust_Name
,Cust_SP
,Comp_Name
,Comp_Desc
,Comp_SP
,CP_number
,CP_Name
,MOQ
,Status
,Requested_date
,Approval_date
,Rejected_date
,Approved_Rejected_By
,Rejected_Reason
,RequestedBy
,RequestedBy_Flag
,ReRequested_Date
,ReRequested_By
,Assigned_To
,Accept_Visible_Flag
,Multiple_Order_Flag
,CASE WHEN  qref.New_OfferPrice IS NULL OR  qref.New_OfferPrice=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.New_OfferPrice,0))) END New_OfferPrice
,CASE WHEN  qref.Recommended_Price IS NULL OR  qref.Recommended_Price=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.Recommended_Price,0))) END Recommended_Price
,StatusChange_Count
,Quotation_no
,Order_Validity
,Approved_OrderQty
,Approved_OrderFreq,  Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy, xpc.ICS 'StockCode'
	, xpc.Price25 AverageSellingPrice, (Select IVTV FROM tt_GALF6TA_II WHERE ICAT=qref.item_code) UnitPrice, 
	dbo.fn_getStock(qref.Item_code) Stock 
	,CASE WHEN qref.Order_Validity is null OR qref.Order_Validity='' 
		THEN  (select CONVERT(INT,VALUE) from TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY')
		ELSE qref.Order_Validity END 'OrderValidity'
		,dbo.fn_getGP(qref.Ref_number,qref.Item_code) GP
,'' TotOrderValue
, '' CP
, '' 'CommentBySE'
,'Auto-Approved By SE' SE_Status
	FROM Quote_ref qref JOIN [dbo].[tt_GAL6TA_XPRICE] xpc ON qref.Item_code=xpc.ICAT
	WHERE 
		Status IN ('Sent For Approval')
		--AND RequestedBy_Flag='CUSTOMER' 
		AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
		AND (CP_number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select region_code from tt_territory_region_relation where territory_engineer_id=@assigned_salesengineer_id)) 
			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select region_code from tt_territory_region_relation where territory_engineer_id=@assigned_salesengineer_id) )) 
		AND UPPER(Assigned_To)=UPPER('BM')
	--	AND GETDATE()> DATEADD(DD,1, Requested_date)
	AND (qref.Agreement_Price is not  null AND qref.Agreement_Price<>'' AND Convert(float,qref.Agreement_Price)>0.0)
		UNION 
		SELECT ID
,Ref_number
,Item_code
,Item_Desc
,WHS
,Order_type
,Order_frequency
,Total_QTY
,QTY_perOrder
,List_Price
,Expected_price
,DC_rate
,Discount_Price
,Agreement_Price
,CONVERT(INT,CONVERT(decimal(38,2),Offer_Price)) 'Offer_Price'
,Cust_number
,Cust_Name
,Cust_SP
,Comp_Name
,Comp_Desc
,Comp_SP
,CP_number
,CP_Name
,MOQ
,Status
,Requested_date
,Approval_date
,Rejected_date
,Approved_Rejected_By
,Rejected_Reason
,RequestedBy
,RequestedBy_Flag
,ReRequested_Date
,ReRequested_By
,Assigned_To
,Accept_Visible_Flag
,Multiple_Order_Flag
,CASE WHEN  qref.New_OfferPrice IS NULL OR  qref.New_OfferPrice=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.New_OfferPrice,0))) END New_OfferPrice
,CASE WHEN  qref.Recommended_Price IS NULL OR  qref.Recommended_Price=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.Recommended_Price,0))) END Recommended_Price
,StatusChange_Count
,Quotation_no
,Order_Validity
,Approved_OrderQty
,Approved_OrderFreq,  Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy, xpc.ICS 'StockCode'
		, xpc.Price25 AverageSellingPrice, (Select IVTV FROM tt_GALF6TA_II WHERE ICAT=qref.item_code) UnitPrice, 
	dbo.fn_getStock(qref.Item_code) Stock 
	,CASE WHEN qref.Order_Validity is null OR qref.Order_Validity='' 
		THEN  (select CONVERT(INT,VALUE) from TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY')
		ELSE qref.Order_Validity END 'OrderValidity'
		,dbo.fn_getGP(qref.Ref_number,qref.Item_code) GP
,'' TotOrderValue
, '' CP
, (Select top 1 StatusChange_Comment from tbl_QuoteStatusLog where Ref_number= qref.Ref_number AND Item_code=qref.Item_code AND Status='Approved By SE' order by ID desc) 'CommentBySE'
,'Approved By SE' SE_Status
	FROM Quote_ref qref JOIN [dbo].[tt_GAL6TA_XPRICE] xpc ON qref.Item_code=xpc.ICAT
	WHERE 
		Status IN ('Approved By SE')
		--AND RequestedBy_Flag='CUSTOMER' 
		AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
		AND (CP_number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select region_code from tt_territory_region_relation where territory_engineer_id=@assigned_salesengineer_id)) 
			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select region_code from tt_territory_region_relation where territory_engineer_id=@assigned_salesengineer_id) )) 
		AND UPPER(Assigned_To)=UPPER('BM')



		

END






