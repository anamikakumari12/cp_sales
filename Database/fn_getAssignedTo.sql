USE [Taegutec_Sales_Budget]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_getAgreementPrice]    Script Date: 9/5/2019 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[fn_getAssignedTo](@ID VARCHAR(100))
RETURNS VARCHAR(200) AS
BEGIN

	DECLARE @ret VARCHAR(200)
	DECLARE @Status VARCHAR(MAX), @AgreementPrice VARCHAR(100), @Expected_Price VARCHAR(100)
	SELECT @Status=Status, @AgreementPrice=Agreement_Price, @Expected_Price=Expected_price from Quote_ref WHERE ID=@ID
	IF(@Status='Request For ReApproval' OR @Status='Escalated')
	BEGIN
		SET @ret='HO'
	END
	ELSE IF(@Status='Sent For Approval')
	BEGIN
		IF(@AgreementPrice IS NULL OR @AgreementPrice='')
			SET @ret='DESK'
		ELSE
			IF(@AgreementPrice>@Expected_Price)
				SET @ret='HO'
			ELSE
			SET @ret='DESK'
	END
	ELSE
	BEGIN
		SET @ret=NULL
	END
	RETURN @ret
END


