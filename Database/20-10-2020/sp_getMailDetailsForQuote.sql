USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_getMailDetailsForQuote]    Script Date: 21-10-2020 12:09:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_getMailDetailsForQuote]
(
@Module VARCHAR(100),
@Component VARCHAR(100),
@RefNumber VARCHAR(100),
@item VARCHAR(100)=null
)
AS
BEGIN

DECLARE @Requestor VARCHAR(10), @RequestedFlag VARCHAR(20), @RequestorMailId VARCHAR(100), @Customer VARCHAR(10), @Cust_name VARCHAR(MAX), @Status VARCHAR(100), @item_desc VARCHAR(100), @SpecialPrice VARCHAR(100), @QTY VARCHAR(100), @validity VARCHAR(200), @StatusName VARCHAR(200), @EndCust VARCHAR(MAX)
DECLARE @TO VARCHAR(MAX), @CC VARCHAR(MAX), @Subject VARCHAR(MAX), @Body VARCHAR(MAX)

DECLARE @BRANCH VARCHAR(100),@BMFLAG VARCHAR(10),@HOFLAG VARCHAR(10),@DeskFLAG VARCHAR(10),@SEFLAG VARCHAR(10)

SELECT TOP 1 @Requestor=RequestedBy, @RequestedFlag=RequestedBy_Flag FROM Quote_ref WHERE Ref_number=@RefNumber


IF(@RequestedFlag='CUSTOMER')
BEGIN
	SELECT @Customer=Cust_number, @Cust_name=Cust_Name FROM Quote_ref WHERE Ref_number=@RefNumber
	SELECT @RequestorMailId=EmailId FROM LoginInfo WHERE EngineerId=@Requestor
END
ELSE IF (@RequestedFlag='DISTRIBUTOR')
BEGIN

	SELECT @RequestorMailId=Email_Id  FROM tbl_LoginInfoForDistributor WHERE Id=@Requestor
	SELECT @Customer=CP_number, @Cust_name=CP_Name FROM Quote_ref WHERE Ref_number=@RefNumber
END
--SELECT @Customer 'Requestor'
IF(@RequestorMailId IS NOT NULL OR @RequestorMailId<>'')
BEGIN
	IF(@Module ='Quote' AND (@Component='SubmitQuote' OR @Component='ReSubmitQuote'))
	BEGIN
		SELECT @SEFLAG=SE, @BMFLAG=BM, @HOFLAG=HO,@DeskFLAG=Desk FROM MailConfiguration where Module =@Module AND Component=@Component

		SELECT @BRANCH=BranchCode FROM LoginInfo WHERE EngineerId =(SELECT assigned_salesengineer_id FROM tt_customer_master WHERE customer_number=@Customer)
		 IF (@RequestedFlag='DISTRIBUTOR')
		 BEGIN
		SELECT  @TO=STUFF(
					(   SELECT ';' + CONVERT(VARCHAR(MAX), EmailId) 
						FROM
						(
						SELECT EmailId FROM LoginInfo WHERE RoleId ='SE' AND LoginStatus=1 AND BranchCode=@BRANCH AND @SEFLAG='YES' AND EngineerId IN (Select assigned_salesengineer_id from tt_customer_master Where customer_number=@Customer)
						UNION
						SELECT EmailIds EmailId FROM MailConfiguration WHERE Module =@Module AND Component=@Component
						) t
					FOR xml path('')
                   )
                   , 1
                   , 1
                   , '')

		SELECT  @CC=STUFF(
					(   SELECT ';' + CONVERT(VARCHAR(MAX), EmailId) 
						FROM
						(
						SELECT @RequestorMailId EmailId
						UNION
						SELECT EmailId FROM LoginInfo WHERE RoleId ='BM' AND LoginStatus=1 AND BranchCode=@BRANCH AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE EngineerId IN (
						SELECT territory_engineer_id FROM tt_territory_region_relation WHERE region_code=@BRANCH) AND LoginStatus=1 AND @BMFLAG='YES'
						UNION 
						--SELECT EmailId FROM LoginInfo WHERE RoleId ='HO' AND LoginStatus=1 AND @HOFLAG='YES'
						--UNION
						SELECT Email_id from tbl_DeskLogin WHERE Status=1 AND @DeskFLAG='YES' AND Email_id NOT IN ('satish@taegutec-india.com')) t
					FOR xml path('')
                   )
                   , 1
                   , 1
                   , '')
		END
		ELSE
		 BEGIN
		SELECT  @TO=STUFF(
					(   SELECT ';' + CONVERT(VARCHAR(MAX), EmailId) 
						FROM
						(
						SELECT EmailId FROM LoginInfo WHERE RoleId ='BM' AND LoginStatus=1 AND BranchCode=@BRANCH AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE EngineerId IN (
						SELECT territory_engineer_id FROM tt_territory_region_relation WHERE region_code=@BRANCH) AND LoginStatus=1 AND @BMFLAG='YES'
						UNION
						SELECT EmailIds EmailId FROM MailConfiguration WHERE Module =@Module AND Component=@Component
						) t
					FOR xml path('')
                   )
                   , 1
                   , 1
                   , '')

		SELECT  @CC=STUFF(
					(   SELECT ';' + CONVERT(VARCHAR(MAX), EmailId) 
						FROM
						(
						SELECT @RequestorMailId EmailId
						UNION
						
						--SELECT EmailId FROM LoginInfo WHERE RoleId ='HO' AND LoginStatus=1 AND @HOFLAG='YES'
						--UNION
						SELECT Email_id from tbl_DeskLogin WHERE Status=1 AND @DeskFLAG='YES' AND Email_id NOT IN ('satish@taegutec-india.com')) t
					FOR xml path('')
                   )
                   , 1
                   , 1
                   , '')
		END
		--SELECT  @TO 'TO'
		IF(@Component='SubmitQuote')
			SET @Status='Requested'
		ELSE SET @Status='Requested for ReApproval'
		--SELECT @CC=@RequestorMailId
		
		Select @StatusName=dbo.fn_getStatusName(@Status)

print @StatusName
		SELECT @Subject='Quote is submitted with reference number : '+@RefNumber

		--SELECT @Body='Hi,<br/> The quote request is submitted with reference number '+@RefNumber+' for Customer/Channel Partner : '+@Cust_name+'. Please update the status as required.<br/><br/> Do not reply to this mail.'

		SELECT @Body='Hi,<br/>Quote request is submitted with below information: <br/><br/>'+
		'<table style="border: 1px solid black;"><tr><td style="border: 1px solid black;">Customer/CP Name</td><td style="border: 1px solid black;">'+@Cust_name+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">RFQ Number</td><td style="border: 1px solid black;">'+@RefNumber+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Status</td><td style="border: 1px solid black;">'+@StatusName+'</td></tr>'+
		'</table><br/><br/> Do not reply to this mail.'

	END
	ELSE IF (@Module ='Quote' AND @Component='StatusChange')
	BEGIN
		IF EXISTS(SELECT * FROM Quote_ref WHERE Ref_number=@RefNumber AND Item_code=@item AND Status='Sent For Approval')
		BEGIN
		SELECT @TO='',@CC='',@Subject='',@Body=''
		END
		ELSE
		BEGIN
			SELECT @BMFLAG=BM, @HOFLAG=HO,@DeskFLAG=Desk FROM MailConfiguration where Module =@Module AND Component=@Component
			SELECT @BRANCH=BranchCode FROM LoginInfo WHERE EngineerId =(SELECT assigned_salesengineer_id FROM tt_customer_master WHERE customer_number=@Customer)
			SELECT @Status=Status FROM Quote_ref WHERE Ref_number=@RefNumber AND Item_code=@item
			Select @StatusName=dbo.fn_getStatusName(@Status)

print @StatusName
			IF(@Status='Escalated')
			BEGIN

			SELECT  @TO=STUFF(
					(   SELECT ';' + CONVERT(VARCHAR(MAX), EmailId) 
						FROM
						(
						SELECT EmailIds EmailId FROM MailConfiguration WHERE Module =@Module AND Component=@Component
						UNION
						SELECT EmailId FROM LoginInfo WHERE RoleId ='BM' AND LoginStatus=1 AND BranchCode=@BRANCH AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE EngineerId =(SELECT assigned_salesengineer_id FROM tt_customer_master WHERE customer_number=@Customer)
						UNION 
						SELECT EmailId FROM LoginInfo WHERE EngineerId IN (
						SELECT territory_engineer_id FROM tt_territory_region_relation WHERE region_code=@BRANCH) AND LoginStatus=1 AND @BMFLAG='YES'
						--UNION 
						--SELECT EmailIds EmailId FROM MailConfiguration WHERE Module =@Module AND Component='Escalated'
						--UNION
						--SELECT EmailId FROM LoginInfo WHERE RoleId ='HO' AND LoginStatus=1 AND @HOFLAG='YES'
						--UNION
						--SELECT Email_id from tbl_DeskLogin WHERE Status=1 AND @DeskFLAG='YES'
						) t
					FOR xml path('')
                   )
                   , 1
                   , 1
                   , '')
			
			
			SELECT @CC=@RequestorMailId
			
			SELECT @Subject='Quote escalated for reference number : '+@RefNumber +' Item code : '+@item
			
			Declare @EscReason VARCHAR(MAX), @ReqPrice VARCHAR(100)
			Select top 1 @EscReason=StatusChange_Comment, @EndCust=End_cust_name from tbl_QuoteStatusLog where Ref_Number=@RefNumber and Item_code=@item and Status='Escalated' order by ID desc
			SELECT @item_desc=Item_Desc,
			@SpecialPrice=New_OfferPrice,
			@ReqPrice=Expected_price FROM Quote_ref WHERE Ref_number=@RefNumber AND Item_code=@item

			SELECT @Body='Hi,<br/>RFQ Status Change with below information: <br/><br/>'+
		'<table style="border: 1px solid black;"><tr><td style="border: 1px solid black;">Customer/CP Name</td><td style="border: 1px solid black;">'+@Cust_name+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">RFQ Number</td><td style="border: 1px solid black;">'+@RefNumber+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Status</td><td style="border: 1px solid black;">'+@StatusName+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">End Customer</td><td style="border: 1px solid black;">'+@EndCust+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Item Desc</td><td style="border: 1px solid black;">'+@item_desc+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Offer Price</td><td style="border: 1px solid black;">'+@SpecialPrice+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Request Price</td><td style="border: 1px solid black;">'+@ReqPrice+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Reason For Escalation</td><td style="border: 1px solid black;">'+@EscReason+'</td></tr>'+
		'</table><br/><br/> Do not reply to this mail.'	
			END
			ELSE IF(@Status='Escalated By BM')
			BEGIN

			SELECT  @TO=STUFF(
					(   SELECT ';' + CONVERT(VARCHAR(MAX), EmailId) 
						FROM
						(
						SELECT EmailIds EmailId FROM MailConfiguration WHERE Module =@Module AND Component=@Component
						UNION
						--SELECT EmailId FROM LoginInfo WHERE RoleId ='BM' AND LoginStatus=1 AND BranchCode=@BRANCH AND @BMFLAG='YES'
						--UNION 
						--SELECT EmailId FROM LoginInfo WHERE EngineerId IN (
						--SELECT territory_engineer_id FROM tt_territory_region_relation WHERE region_code=@BRANCH) AND LoginStatus=1 AND @BMFLAG='YES'
						--UNION 
						SELECT EmailIds EmailId FROM MailConfiguration WHERE Module =@Module AND Component='Escalated'
						UNION
						--SELECT EmailId FROM LoginInfo WHERE RoleId ='HO' AND LoginStatus=1 AND @HOFLAG='YES'
						--UNION
						SELECT Email_id from tbl_DeskLogin WHERE Status=1 AND @DeskFLAG='YES' AND Email_id NOT IN ('satish@taegutec-india.com')) t
					FOR xml path('')
                   )
                   , 1
                   , 1
                   , '')
			
			
			SELECT @CC=@RequestorMailId
			
			SELECT @Subject='Quote escalated for reference number : '+@RefNumber +' Item code : '+@item
			SELECT @Body='Hi,<br/>RFQ Status Change with below information: <br/><br/>'+
		'<table style="border: 1px solid black;"><tr><td style="border: 1px solid black;">Customer/CP Name</td><td style="border: 1px solid black;">'+@Cust_name+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">RFQ Number</td><td style="border: 1px solid black;">'+@RefNumber+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Status</td><td style="border: 1px solid black;">'+@StatusName+'</td></tr>'+
		'</table><br/><br/> Do not reply to this mail.'
			END
			ELSE
			BEGIN
			SELECT  @CC=STUFF(
					(   SELECT ';' + CONVERT(VARCHAR(MAX), EmailId) 
						FROM
						(
						SELECT EmailIds EmailId FROM MailConfiguration WHERE Module =@Module AND Component=@Component
						UNION
						SELECT EmailId FROM LoginInfo WHERE RoleId ='BM' AND LoginStatus=1 AND BranchCode=@BRANCH AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE EngineerId =(SELECT assigned_salesengineer_id FROM tt_customer_master WHERE customer_number=@Customer)
						UNION 
						SELECT EmailId FROM LoginInfo WHERE EngineerId IN (
						SELECT territory_engineer_id FROM tt_territory_region_relation WHERE region_code=@BRANCH) AND LoginStatus=1 AND @BMFLAG='YES'
						UNION 
						--SELECT EmailId FROM LoginInfo WHERE RoleId ='HO' AND LoginStatus=1 AND @HOFLAG='YES'
						--UNION
						SELECT Email_id from tbl_DeskLogin WHERE Status=1 AND @DeskFLAG='YES' AND Email_id NOT IN ('satish@taegutec-india.com')) t
					FOR xml path('')
                   )
                   , 1
                   , 1
                   , '')
			
			
			SELECT @TO=@RequestorMailId
			SELECT @Subject='Quote approval for reference number : '+@RefNumber
			SELECT @Body='Hi,<br/>RFQ Status Change with below information: <br/><br/>'+
		'<table style="border: 1px solid black;"><tr><td style="border: 1px solid black;">Customer/CP Name</td><td style="border: 1px solid black;">'+@Cust_name+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">RFQ Number</td><td style="border: 1px solid black;">'+@RefNumber+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Status</td><td style="border: 1px solid black;">'+@StatusName+'</td></tr>'+
		'</table><br/><br/> Do not reply to this mail.'
			END
			--SELECT @Body='Hi,<br/> The requested quote with reference number '+@RefNumber+' for Customer/Channel Partner : '+@Cust_name+' is completed. Please place order for approved quotes and have a look for rejected quotes.<br/><br/> Do not reply to this mail.'
			
		END
	END
	ELSE IF(@Module ='Quote' AND @Component='PlaceOrder')
	BEGIN
		SELECT @BMFLAG=BM, @HOFLAG=HO,@DeskFLAG=Desk FROM MailConfiguration where Module =@Module AND Component=@Component

		SELECT @BRANCH=BranchCode FROM LoginInfo WHERE EngineerId =(SELECT assigned_salesengineer_id FROM tt_customer_master WHERE customer_number=@Customer)
		SELECT  @TO=STUFF(
					(   SELECT ';' + CONVERT(VARCHAR(MAX), EmailId) 
						FROM
						(
						SELECT EmailIds EmailId FROM MailConfiguration WHERE Module =@Module AND Component=@Component
						UNION
						SELECT EmailId FROM LoginInfo WHERE RoleId ='BM' AND LoginStatus=1 AND BranchCode=@BRANCH AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE EngineerId IN (
						SELECT territory_engineer_id FROM tt_territory_region_relation WHERE region_code=@BRANCH) AND LoginStatus=1 AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE RoleId ='HO' AND LoginStatus=1 AND @HOFLAG='YES'
						UNION
						SELECT Email_id from tbl_DeskLogin WHERE Status=1 AND @DeskFLAG='YES' AND Email_id NOT IN ('satish@taegutec-india.com')) t
					FOR xml path('')
                   )
                   , 1
                   , 1
                   , '')
		
		SELECT @CC=@RequestorMailId

		SELECT @Subject='Order placed for reference number : '+@RefNumber+' and item number :'+@item

		SELECT @Body='Hi,<br/> The order is placed for reference number '+@RefNumber+'  and item number :'+@item+'. <br/><br/> Do not reply to this mail.'

	END

	ELSE IF (@Module ='Quote' AND @Component='AddInPA')
	BEGIN
	SELECT @Status=Status FROM Quote_ref WHERE Ref_number=@RefNumber AND Item_code=@item
	Select @StatusName=dbo.fn_getStatusName(@Status)

print @StatusName
		IF(@Status='Accepted')
		BEGIN
			SELECT @BMFLAG=BM, @HOFLAG=HO,@DeskFLAG=Desk FROM MailConfiguration where Module =@Module AND Component=@Component
			SELECT @BRANCH=BranchCode FROM LoginInfo WHERE EngineerId =(SELECT assigned_salesengineer_id FROM tt_customer_master WHERE customer_number=@Customer)
			SELECT  @TO=STUFF(
					(   SELECT ';' + CONVERT(VARCHAR(MAX), EmailId) 
						FROM
						(
						SELECT EmailIds EmailId FROM MailConfiguration WHERE Module =@Module AND Component=@Component
						--UNION
						--SELECT EmailId FROM LoginInfo WHERE RoleId ='BM' AND LoginStatus=1 AND BranchCode=@BRANCH AND @BMFLAG='YES'
						--UNION 
						--SELECT EmailId FROM LoginInfo WHERE EngineerId IN (
						--SELECT territory_engineer_id FROM tt_territory_region_relation WHERE region_code=@BRANCH) AND LoginStatus=1 AND @BMFLAG='YES'
						--UNION 
						--SELECT EmailId FROM LoginInfo WHERE RoleId ='HO' AND LoginStatus=1 AND @HOFLAG='YES'
						UNION
						SELECT Email_id from tbl_DeskLogin WHERE Status=1 AND @DeskFLAG='YES' AND Email_id NOT IN ('satish@taegutec-india.com')) t
					FOR xml path('')
                   )
                   , 1
                   , 1
                   , '')
			
			SELECT @Status=Status, 
			@validity=CONCAT(CONVERT(VARCHAR,Approval_date, 104),' to ', CONVERT(VARCHAR,DATEADD(DD,Order_Validity,Approval_date), 104)), 
			@item_desc=Item_Desc,
			@SpecialPrice=New_OfferPrice,
			@QTY=Approved_OrderQty FROM Quote_ref WHERE Ref_number=@RefNumber AND Item_code=@item
			--SELECT  FROM Quote_ref WHERE Ref_number=@RefNumber AND Item_code=@item
			--SELECT  FROM Quote_ref WHERE Ref_number=@RefNumber AND Item_code=@item
			--SELECT FROM Quote_ref WHERE Ref_number=@RefNumber AND Item_code=@item
			Select @StatusName=dbo.fn_getStatusName(@Status)

print @StatusName
			SELECT @CC=@RequestorMailId 

			SELECT @Subject='Quote accepted for reference number : '+@RefNumber
			SELECT @Body='Hi,<br/>'
			IF EXISTS(select * from tbl_QuoteStatusLog where Ref_Number=@RefNumber and Item_code=@item and Status='Escalated & Approved')
			SELECT @Body+='Kindly add the escalated price into PA: <br/><br/>'
			ELSE
			SELECT @Body+='Kindly add the item into PA: <br/><br/>'
			--SELECT @Body='Hi,<br/> The requested quote with reference number '+@RefNumber+' for Customer/Channel Partner : '+@Cust_name+' is completed. Please place order for approved quotes and have a look for rejected quotes.<br/><br/> Do not reply to this mail.'
			
		SELECT @Body+='<table style="border: 1px solid black;"><tr><td style="border: 1px solid black;">Customer/CP Name</td><td style="border: 1px solid black;">'+@Cust_name+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">RFQ Number</td><td style="border: 1px solid black;">'+@RefNumber+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Status</td><td style="border: 1px solid black;">'+@StatusName+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Item</td><td style="border: 1px solid black;">'+@item_desc+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Quantity</td><td style="border: 1px solid black;">'+@QTY+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Special Price</td><td style="border: 1px solid black;">'+CONVERT(VARCHAR(100),ROUND(Convert(FLOAT,@SpecialPrice),0))+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Validity</td><td style="border: 1px solid black;">'+@validity+'</td></tr>'+
		'</table><br/><br/> Do not reply to this mail.'
		END
		ELSE IF(@Status='Added In PA')
		BEGIN
			SELECT @BMFLAG=BM, @HOFLAG=HO,@DeskFLAG=Desk FROM MailConfiguration where Module =@Module AND Component=@Component
			SELECT @BRANCH=BranchCode FROM LoginInfo WHERE EngineerId =(SELECT assigned_salesengineer_id FROM tt_customer_master WHERE customer_number=@Customer)
			SELECT  @TO=@RequestorMailId
			--SELECT CONVERT(VARCHAR,Approval_date, 104)+' to '+CONVERT(VARCHAR,DATEADD(DD,Order_Validity,Approval_date), 104) FROM Quote_ref WHERE Ref_number=@RefNumber AND Item_code=@item
			SELECT @Status=Status, 
			@validity=CONVERT(VARCHAR,Approval_date, 104)+' to '+CONVERT(VARCHAR,DATEADD(DD,Order_Validity,Approval_date), 104), 
			@item_desc=Item_Desc,
			@SpecialPrice=New_OfferPrice,
			@QTY=Approved_OrderQty FROM Quote_ref WHERE Ref_number=@RefNumber AND Item_code=@item
			Select @StatusName=dbo.fn_getStatusName(@Status)

print @StatusName
			SELECT @CC='anamika@knstek.com'
			print @validity
			SELECT @Subject='Quote added in PA for reference number : '+@RefNumber + ' Item_code : '+@item
			SELECT @Body='Hi,<br/> The requested quote with reference number '+@RefNumber+ ' and Item_code : '+@item+' for Customer/Channel Partner : '+@Cust_name+' is added in PA. <br/>'
			SELECT @Body+='<table style="border: 1px solid black;"><tr><td style="border: 1px solid black;">Customer/CP Name</td><td style="border: 1px solid black;">'+@Cust_name+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">RFQ Number</td><td style="border: 1px solid black;">'+@RefNumber+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Status</td><td style="border: 1px solid black;">'+@StatusName+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Item</td><td style="border: 1px solid black;">'+@item_desc+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Quantity</td><td style="border: 1px solid black;">'+@QTY+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Special Price</td><td style="border: 1px solid black;">'+CONVERT(VARCHAR(100),ROUND(Convert(FLOAT,@SpecialPrice),0))+'</td></tr>'+
		'<tr><td style="border: 1px solid black;">Validity</td><td style="border: 1px solid black;">'+@validity+'</td></tr>'+
		'</table><br/><br/> Do not reply to this mail.'
		END
		ELSE
		BEGIN
		SELECT @TO='',@CC='',@Subject='',@Body=''
		END
	END
END
SELECT @TO='teja@knstek.com'
SELECT @CC='anamika@knstek.com'
Select @TO 'To', @CC 'CC', @Subject 'Subject', @Body 'Message'

END



--exec sp_getMailDetailsForQuote 'Quote','AddInPA','RN_1119_2020-08-25_1397','5514739'

--select * from Quote_ref WHERE St

--Hi,<br/>Kindly add the item into PA: <br/><br/><table style="border: 1px solid black;"><tr><td style="border: 1px solid black;">Customer/CP Name</td><td style="border: 1px solid black;">HARIOM AGENCIES</td></tr><tr><td style="border: 1px solid black;">RFQ Number</td><td style="border: 1px solid black;">RN_1119_2020-08-25_1397</td></tr><tr><td style="border: 1px solid black;">Status</td><td style="border: 1px solid black;">Accepted</td></tr><tr><td style="border: 1px solid black;">Item</td><td style="border: 1px solid black;">CCMT 09T308 MT     TT5100</td></tr><tr><td style="border: 1px solid black;">Quantity</td><td style="border: 1px solid black;">300</td></tr><tr><td style="border: 1px solid black;">Special Price</td><td style="border: 1px solid black;">140</td></tr><tr><td style="border: 1px solid black;">Validity</td><td style="border: 1px solid black;">27.08.2020 to </td></tr></table><br/><br/> Do not reply to this mail.