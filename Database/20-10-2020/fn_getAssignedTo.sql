USE [Taegutec_Sales_Budget]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_getAssignedTo]    Script Date: 20-10-2020 20:31:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[fn_getAssignedTo](@ID VARCHAR(100))
RETURNS VARCHAR(200) AS
BEGIN

	DECLARE @ret VARCHAR(200)
	DECLARE @Status VARCHAR(MAX), @AgreementPrice VARCHAR(100), @Expected_Price VARCHAR(100), @flag VARCHAR(100)
	SELECT @Status=Status, @AgreementPrice=Agreement_Price, @Expected_Price=Expected_price, @flag=RequestedBy_Flag from Quote_ref WHERE ID=@ID
	IF(@Status='Escalated By BM' OR @Status='Approved By BM')
	BEGIN
		SET @ret='HO'
	END
	ELSE IF(@Status='Sent For Approval')
	BEGIN
		if(@flag='DISTRIBUTOR')
		BEGIN
			IF(@AgreementPrice IS NULL OR @AgreementPrice='' OR @AgreementPrice='0.0')
				SET @ret='DESK'
			ELSE
				--IF(@AgreementPrice>@Expected_Price)
				--	SET @ret='SE'
				--ELSE
				SET @ret='BM'
		END
		ELSE 
			SET @ret='BM'
	END
	ELSE IF(@Status='Request For ReApproval' OR @Status='Escalated')
	BEGIN
		SET @ret='BM'
	END
	ELSE IF(@Status='Approved By SE')
	BEGIN
		IF(@AgreementPrice IS NULL OR @AgreementPrice='' OR @AgreementPrice='0.0')
				SET @ret='DESK'
			ELSE
				SET @ret='BM'
	END
	ELSE
	BEGIN
		SET @ret=NULL
	END
	RETURN @ret
END


