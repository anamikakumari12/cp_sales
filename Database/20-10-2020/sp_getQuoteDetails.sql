USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_getQuoteDetails]    Script Date: 28-10-2020 13:38:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_getQuoteDetails null,null,null,null,null,null,null,'10/20/2020','10/20/2020','CHE'
ALTER PROCEDURE [dbo].[sp_getQuoteDetails]
(
@ID INT=null,
@item VARCHAR(100)=null,
@RefNumber VARCHAR(100)=null,
@Status VARCHAR(100)=null,
@flag VARCHAR(100)=null,
@customer_number VARCHAR(MAX)=NULL,
@RequestedBy VARCHAR(100)=NULL,
@StartDate VARCHAR(100)=null,
@EndDate VARCHAR(100)=null,
@Branch VARCHAR(100)=null
)
As
BEGIN

IF(@Status='' OR @Status='ALL')
SET @Status=null

DECLARE @RFQstatus TABLE(Status VARCHAR(100))

IF(@Status IS NOT NULL)
BEGIN
	IF(UPPER(@Status)='OPEN')
	INSERT INTO @RFQstatus(Status)
	VALUES('Sent For Approval'),('Escalated'),('Accepted')
	ELSE
	INSERT INTO @RFQstatus(Status)
	VALUES('Approved'),('Escalated & Approved'),('Rejected'),('Added In PA')
	
END

if(@ID=0)
SET @ID=null
DECLARE @DAYS INT
SELECT @DAYS=VALUE FROM TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY'
SELECT 
q.ID
,q.Ref_number
,q.Item_code
,REPLACE(REPLACE(REPLACE(q.Item_Desc, CHAR(9), ''), CHAR(10), ''), CHAR(13), '') Item_Desc
,q.WHS
,q.Order_type
,q.Order_frequency
,q.Total_QTY
,q.QTY_perOrder
,q.List_Price
,q.Expected_price
,q.DC_rate
,LTRIM(RTRIM(q.Cust_number)) Cust_number
,(SELECT  tc.[FixedString]  FROM   dbo.TrimChars(q.Cust_Name, NCHAR(0x09) + NCHAR(0x20) + NCHAR(0x0D) + NCHAR(0x0A)) tc) Cust_Name  
,q.Cust_SP
,REPLACE(REPLACE(REPLACE(q.Comp_Name, CHAR(9), ''), CHAR(10), ''), CHAR(13), '') Comp_Name
,REPLACE(REPLACE(REPLACE(q.Comp_Desc, CHAR(9), ''), CHAR(10), ''), CHAR(13), '') Comp_Desc
,q.Comp_SP
,q.CP_number
,q.CP_Name
,CASE WHEN t.Availablility='Valid' OR t.Availablility='' OR t.Availablility is null THEN q.Status ELSE 'Expired' END Status
--,q.Status Status1
,FORMAT(q.Requested_date,'MM/dd/yyyy hh:mm:ss') Requested_date
,q.Approval_date
,q.Rejected_date
,q.Approved_Rejected_By
--,(SELECT  tc.[FixedString]  FROM   dbo.TrimChars(q.Rejected_Reason, NCHAR(0x09) + NCHAR(0x20) + NCHAR(0x0D) + NCHAR(0x0A)) tc) Rejected_Reason  
, REPLACE(REPLACE(REPLACE(q.Rejected_Reason, CHAR(9), ''), CHAR(10), ''), CHAR(13), '') Rejected_Reason
,q.MOQ
,ROUND(q.Offer_Price, 0) Offer_Price
,q.RequestedBy
,q.RequestedBy_Flag
, t.Availablility
, dbo.fn_GetOrderFlag(q.ID) 'Order_Flag'
,q.Assigned_To
,q.Accept_Visible_Flag
,q.Multiple_Order_Flag
,ROUND(q.New_OfferPrice,0) New_OfferPrice
,q.Recommended_Price
,CASE WHEN q.Order_Validity is null OR q.Order_Validity='' 
		THEN  (select CONVERT(INT,VALUE) from TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY')
		ELSE q.Order_Validity END 'Order_Validity'
, xpc.ICS 'StockCode', ROUND(xpc.Price25,0) AverageSellingPrice
, ROUND(ii.IVTV,0) UnitPrice
--, (Select IVTV FROM tt_GALF6TA_II WHERE ICAT=q.item_code) UnitPrice
, 0 Stock 
--, ROUND(dbo.fn_getStock(q.Item_code),0) Stock 
--,dbo.fn_getGP(q.Ref_number,q.Item_code) GP
,CASE WHEN CONVERT(float,ISNULL(q.Offer_Price,0))=0 THEN 0 ELSE ROUND((CONVERT(float,ISNULL(q.Offer_Price,0))-CONVERT(float,ISNULL(ii.IVTV,0)))*100/CONVERT(float,ISNULL(q.Offer_Price,0)),0) END GP
,'' TotOrderValue
,q.Approved_OrderQty
,(Select REPLACE(REPLACE(REPLACE(StatusChange_Comment, CHAR(9), ''), CHAR(10), ''), CHAR(13), '') from tbl_QuoteStatusLog WHERE ID=(Select MAX(ID) from tbl_QuoteStatusLog WHERE Quote_ID=q.ID )) 'StatusChange_Comment'
,dbo.fn_getStatusName(q.Status) 'StatusName'
FROM Quote_ref q JOIN [dbo].[tt_GAL6TA_XPRICE] xpc ON q.Item_code=xpc.ICAT
JOIN tt_GALF6TA_II ii ON ii.ICAT=q.item_code
LEFT JOIN (select ID, Approval_date, CASE WHEN DATEDIFF(dd,Approval_date,Getdate())>Order_Validity THEN 'Invalid' ELSE 'Valid' END as 'Availablility' from Quote_ref where Approval_date is not null AND Approval_date<>'' AND Status='Approved') t
ON 
q.ID=t.ID
 WHERE q.Item_code =ISNULL(@item, q.Item_code) AND q.Ref_number=ISNULL(@RefNumber,q.Ref_Number) AND q.ID=ISNULL(@ID,q.ID)
 AND (Status= ISNULL(@Status, Status) OR Status IN (Select Status from @RFQstatus))
 AND ((UPPER(@flag)='CUSTOMER' AND q.Cust_Name=ISNULL(@customer_number,q.Cust_number)) OR (UPPER(@flag)='DISTRIBUTOR' AND q.CP_number=ISNULL(@customer_number,q.CP_number)) OR @flag is null)
 AND CONVERT(DATE,Requested_date) >=ISNULL(@StartDate,Requested_date) 
 AND CONVERT(DATE,Requested_date)<= ISNULL(@EndDate,Requested_date)
 AND (@Branch is null OR (q.Cust_number IN (Select customer_number from tt_customer_master where Customer_region=@Branch))OR (q.CP_number IN (Select customer_number from tt_customer_master where Customer_region=@Branch)))
END





