USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_RequestForReApproval]    Script Date: 9/4/2019 3:49:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_quoteStatusChange]
(
@ID				INT,
@RefNumber		VARCHAR(200),
@item			VARCHAR(100),
@ModifiedByFlag VARCHAR(100),
@ModifiedBy		VARCHAR(100),
@Status			VARCHAR(100),
@Reason			VARCHAR(MAX)=null,
@MOQ			VARCHAR(100)=null,
@OfferPrice		VARCHAR(100)=null,
@error_code		INT OUTPUT,
@error_msg		VARCHAR(MAX) OUTPUT
)
AS
BEGIN
	IF(@ID=0 OR @ID is null)
	BEGIN
	SET @error_code=100
	SET @error_msg='Error: ID is required.'
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@ID)
		BEGIN
			IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item)
			BEGIN
				
				UPDATE Quote_ref SET Status=@Status
					, Approval_date= CASE  WHEN @Status='Approved' THEN GETDATE() ELSE Approval_date END
					, Rejected_date= CASE  WHEN @Status='Rejected' THEN GETDATE() ELSE Rejected_date END
					, Rejected_Reason=CASE  WHEN @Status='Rejected' THEN @Reason ELSE Rejected_Reason END
					, Approved_Rejected_By=CASE  WHEN @Status='Approved' OR @Status='Rejected' THEN @ModifiedBy ELSE Approved_Rejected_By END
					, MOQ=ISNULL(@MOQ,MOQ)
					,Offer_Price=ISNULL(@OfferPrice,Offer_Price)
					,Assigned_To=ISNULL(dbo.fn_getAssignedTo(@ID), Assigned_To)
				WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item
				
				

								
				IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@ID AND Status=@STATUS AND Ref_number=@RefNumber AND Item_code=@item)
				BEGIN
					
					INSERT INTO tbl_QuoteStatusLog(Quote_ID,Ref_Number,Item_code,Status,StatusChangeBy_Flag,StatusChangeBy_Id,StatusChangeDate,StatusChange_Comment)
					SELECT @ID, @RefNumber, @item,@Status,@ModifiedByFlag, @ModifiedBy,GETDATE(), @Reason  FROM Quote_ref
					 WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item

					SET @error_code=200
					SET @error_msg='Quote request status is modified successfully.'
				END
				ELSE
				BEGIN
					SET @error_code=103
					SET @error_msg='Error: Error in updating data.'
				END
			END											
			ELSE
			BEGIN
				SET @error_code=102
				SET @error_msg='Error: Reference Number and Item code are not available in Quote table.'
			END
		END
		ELSE
		BEGIN
			SET @error_code=101
			SET @error_msg='Error: ID is not available in Quote table.'
		END
	END

END