﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DistributorSystemBO
{
    public class CommonFunctions
    {
        public static void StaticErrorLog(Exception ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;


            StreamWriter log;
            string ErrorLogFile = ConfigurationManager.AppSettings["ErrorLogFile"].ToString();
            string ErrorFile = ConfigurationManager.AppSettings["ErrorFile"].ToString();
            if (!File.Exists(ErrorFile))
            {
                log = new StreamWriter(ErrorLogFile, true);
            }
            else
            {
                log = File.AppendText(ErrorLogFile);
            }

            // Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(message);
            // Close the stream:
            log.Close();
        }
        public void ErrorLog(Exception ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;


            StreamWriter log;
            string ErrorLogFile = ConfigurationManager.AppSettings["ErrorLogFile"].ToString();
            string ErrorFile = ConfigurationManager.AppSettings["ErrorFile"].ToString();
            if (!File.Exists(ErrorFile))
            {
                log = new StreamWriter(ErrorLogFile, true);
            }
            else
            {
                log = File.AppendText(ErrorLogFile);
            }

            // Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(message);
            // Close the stream:
            log.Close();
        }

        public void TestLog(string a)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: Test Log");
            message += Environment.NewLine;
            message += string.Format("Message Value: {0}", a);
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;


            StreamWriter log;
            string ErrorLogFile = ConfigurationManager.AppSettings["ErrorLogFile"].ToString();
            string ErrorFile = ConfigurationManager.AppSettings["ErrorFile"].ToString();
            if (!File.Exists(ErrorFile))
            {
                log = new StreamWriter(ErrorLogFile, true);
            }
            else
            {
                log = File.AppendText(ErrorLogFile);
            }

            // Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(message);
            // Close the stream:
            log.Close();
        }



        public void SendMail(EmailDetails objEmail)
        {
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            MailMessage email;
            SmtpClient smtpc;
            try
            {
                email = new MailMessage();
                if (objEmail.toMailId.Contains(";"))
                {
                    List<string> names = objEmail.toMailId.Split(';').ToList<string>();
                    for (int i = 0; i < names.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(names[i].TrimStart().TrimEnd()))
                            email.To.Add(names[i]);
                    }
                }
                else
                {
                    email.To.Add(objEmail.toMailId);
                }

                // email.To.Add(objEmail.toMailId); //Destination Recipient e-mail address.
                if (!string.IsNullOrEmpty(objEmail.ccMailId))
                {
                    if (!string.IsNullOrEmpty(objEmail.ccMailId))
                    {
                        if (objEmail.ccMailId.Contains(";"))
                        {
                            List<string> names = objEmail.ccMailId.Split(';').ToList<string>();
                            for (int i = 0; i < names.Count; i++)
                            {
                                if (!string.IsNullOrEmpty(names[i].TrimStart().TrimEnd()))
                                    email.CC.Add(names[i]);
                            }
                        }
                        else
                        {
                            email.CC.Add(objEmail.ccMailId);
                        }
                    }
                }
                    //email.CC.Add(objEmail.ccMailId);
                email.Subject = objEmail.subject;
                email.Body = objEmail.body;
                email.IsBodyHtml = true;
                if (!string.IsNullOrEmpty(objEmail.attachment))
                    email.Attachments.Add(new System.Net.Mail.Attachment(objEmail.attachment));
                smtpc = new SmtpClient();
                smtpc.Send(email);
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            finally
            {
                email = new MailMessage();
            }
        }

        public static async Task SendGridMail(EmailDetails email)
        {
            try
            {
                var sendGridClient = new SendGridClient("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");

                var sendGridMessage = new SendGridMessage()
                {
                    From = new EmailAddress("ttilalerts@taegutec-india.com", "TT Alerts"),
                    Subject = email.subject,
                    HtmlContent = email.body
                };
                //sendGridMessage.AddTo(new EmailAddress(email.toMailId));
                List<EmailAddress> objList = new List<EmailAddress>();
                if (!string.IsNullOrEmpty(email.toMailId))
                {
                    if (email.toMailId.Contains(";"))
                    {
                        List<string> names = email.toMailId.Split(';').ToList<string>();
                        for (int i = 0; i < names.Count; i++)
                        {
                            if (!string.IsNullOrEmpty(names[i].TrimStart().TrimEnd()))
                                //email.CC.Add(names[i]);
                                objList.Add(new EmailAddress(names[i]));
                        }
                    }
                    else
                    {
                        objList.Add(new EmailAddress(email.toMailId));
                    }
                    sendGridMessage.AddTos(objList);
                }
                objList = new List<EmailAddress>();
                if (!string.IsNullOrEmpty(email.ccMailId))
                {
                    if (email.ccMailId.Contains(";"))
                    {
                        List<string> names = email.ccMailId.Split(';').ToList<string>();
                        for (int i = 0; i < names.Count; i++)
                        {
                            if (!string.IsNullOrEmpty(names[i].TrimStart().TrimEnd()))
                                //email.CC.Add(names[i]);
                                objList.Add(new EmailAddress(names[i]));
                        }
                    }
                    else
                    {
                        objList.Add(new EmailAddress(email.ccMailId));
                    }
                    sendGridMessage.AddCcs(objList);
                }
                if (!string.IsNullOrEmpty(email.attachment))
                {
                    byte[] byteData = Encoding.ASCII.GetBytes(File.ReadAllText(email.attachment));
                    sendGridMessage.AddAttachment(new SendGrid.Helpers.Mail.Attachment()
                    {
                        Content = Convert.ToBase64String(byteData),
                        Filename = Path.GetFileName(email.attachment),
                        Type = "application/pdf",
                        Disposition = "attachment"
                    }
                        );
                }
                var response = await sendGridClient.SendEmailAsync(sendGridMessage).ConfigureAwait(false);
                // var response =  sendGridClient.SendEmailAsync(sendGridMessage).ConfigureAwait(false);
                //if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                //{
                //    //Console.WriteLine("Email sent");
                //}
            }
            catch (Exception ex)
            {
                StaticErrorLog(ex);
            }
        }
        public DataTable getConfiguration()
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand("sp_getConfiguration", sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
               ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

    }
}
