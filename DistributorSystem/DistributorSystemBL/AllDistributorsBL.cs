﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DistributorSystemDAL;

namespace DistributorSystemBL
{
    
    public class AllDistributorsBL
    {
        AllDistributorsDAL objAllDistDAL = new AllDistributorsDAL();
        public DataTable getAllDistributorsBL()
        {
            return objAllDistDAL.getAllDistributorsDAL();
        }
    }
}
