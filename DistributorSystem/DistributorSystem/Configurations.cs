﻿using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using System;
using System.Configuration;

namespace DistributorSystem
{
    public class Configurations
    {
        public static readonly string AuthorityUrl = ConfigurationManager.AppSettings["authorityUrl"];
        public static readonly string ResourceUrl = ConfigurationManager.AppSettings["resourceUrl"];
        public static readonly string ApiUrl = ConfigurationManager.AppSettings["apiUrl"];
        public static readonly string ApplicationSecret = ConfigurationManager.AppSettings["applicationSecret"];
        public static readonly string ApplicationIdText = ConfigurationManager.AppSettings["applicationId"];
        // Application
        public static readonly Guid ApplicationId = new Guid(Convert.ToString(ConfigurationManager.AppSettings["applicationId"]));

        // Power BI workspace
        public static readonly Guid WorkspaceId = new Guid(Convert.ToString(ConfigurationManager.AppSettings["workspaceId"]));

        // Master App user account
        public static readonly string MasterAppUsername = ConfigurationManager.AppSettings["pbiUsername"];
        public static readonly string MasterAppPassword = ConfigurationManager.AppSettings["pbiPassword"];
        
    }
}