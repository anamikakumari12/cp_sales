﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonthlySales.aspx.cs" Inherits="DistributorSystem.MonthlySales" MasterPageFile="~/MasterPage.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript"  class="init">
    $(document).ready(function () {
        var head_content = $('#MainContent_grdMonthlySales tr:first').html();
        $('#MainContent_grdMonthlySales').prepend('<thead></thead>')
        $('#MainContent_grdMonthlySales thead').html('<tr>' + head_content + '</tr>');
        $('#MainContent_grdMonthlySales tbody tr:first').hide();
        //$('#MainContent_grdMonthlySales').dataTable({
        //    "dom": 'T<"clear">lfrtip',
        //    "tableTools": {
        //        "sSwfPath": "swf/copy_csv_xls_pdf.swf"
        //    }
        //});
        $('#MainContent_grdMonthlySales').dataTable();
        //$('#MainContent_grdEntry').addClass('nowrap').dataTable({
        //    responsive: true,
        //    columnDefs: [
        //    { targets:[-1,-3], className:'dt-body-right'
        //    }]
        //});
    });
</script>    <style>
        
        .mn_margin
        {
            margin:10px 0 0 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="col-md-12 mn_margin">
    <asp:Panel ID="Panel1" runat="server" CssClass="filter_panel">
        <%--<div class="col-md-4">
            <asp:Label runat="server" ID="lblYear" Text="Year"></asp:Label>
        </div>
        <div class="col-md-4">
            <asp:TextBox runat="server" ID="txtYear"></asp:TextBox>
        </div>--%>
        <div class="col-md-4">
            <%--<asp:Button runat="server" ID="btnEntry" Text="Entry" OnClick="btnEntry_Click" />--%>
             <asp:Button runat="server" CssClass="btnSubmit" ID="btnSave" Text="Save" OnClick="btnSave_Click"/>
        </div>
    </asp:Panel>
        <asp:Panel runat="server" ID="pnlData">
            <asp:GridView ID="grdMonthlySales" CssClass="display compact" runat="server" AutoGenerateColumns="false" OnRowDataBound="grdMonthlySales_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="Customer Number">
                        <ItemTemplate>
                            <asp:Label ID="lblCustomerNumber" runat="server" Text='<%#Bind("customernumber") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer Name">
                        <ItemTemplate>
                            <asp:Label ID="lblCustomerName" runat="server" Text='<%#Bind("customername") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:TextBox ID="txtAmount" runat="server" Text='<%#Bind("month1") %>'></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblmonth1Header" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblmonth1" runat="server" Text='<%#Bind("month2") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblmonth2Header" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblmonth2" runat="server" Text='<%#Bind("month3") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </div>
</asp:Content>
