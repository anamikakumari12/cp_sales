USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_getStatusLog]    Script Date: 20-10-2020 20:28:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Anamika Kumari
-- Create date: 02-09-2020
-- Description:	To get status log for selected  item
-- =============================================
ALTER PROCEDURE [dbo].[sp_getStatusLog]
(
@RefNumber VARCHAR(MAX),
@item VARCHAR(100)
)
AS
BEGIN

Select Ref_Number, Item_code, StatusChangeBy_Id,
Case When StatusChangeBy_Flag='Desk' Then (Select CONCAT(First_Name,' ',Last_Name) from tbl_DeskLogin where ID=StatusChangeBy_Id)
 When StatusChangeBy_Flag='DISTRIBUTOR' or StatusChangeBy_Flag is null Then (Select Distributor_name from tbl_LoginInfoForDistributor where Id=StatusChangeBy_Id)
ELSE (Select EngineerName from LoginInfo where EngineerId=StatusChangeBy_Id) END AS 'StatusChangeBy_Name',
 StatusChange_Comment, StatusChangeDate, dbo.fn_getStatusName(Status) Status INTO #TEMP
from tbl_QuoteStatusLog 
 where Ref_Number=@RefNumber and Item_code=@item order by ID desc


 DECLARE @Status VARCHAR(200)

 Select Top 1 @Status=Status from tbl_QuoteStatusLog 
 where Ref_Number=@RefNumber and Item_code=@item order by ID desc

 IF @Status IN ('Escalated','Approved By BM','Approved By SE','Escalated By BM','Sent For Approval')
 INSERT INTO #TEMP
 Select Ref_Number, Item_code, '' StatusChangeBy_Id, '' StatusChangeBy_Name,
 '' StatusChange_Comment, GETDATE() StatusChangeDate, 
 Case When Assigned_To='DESK' Then 'Pending At Desk'
 When Assigned_To='BM' Then 'Pending At BM'
 When Assigned_To='HO' Then 'Pending At HO'
 When Assigned_To='SE' Then 'Pending At SE'
ELSE '' END AS  Status 
from Quote_ref 
 where Ref_Number=@RefNumber and Item_code=@item order by ID desc

 SELECT distinct * FROM #TEMP order by StatusChangeDate desc
END