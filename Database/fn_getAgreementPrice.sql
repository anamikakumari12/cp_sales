ALTER FUNCTION fn_getAgreementPrice(@Cust_Number VARCHAR(100), @CP_Number VARCHAR(100), @Item_number VARCHAR(100), @ListPrice VARCHAR(100))
RETURNS numeric(22,2) AS
BEGIN

	DECLARE @ret numeric(22,2) ;
	DECLARE @DGRP VARCHAR(10), @IDGRP VARCHAR(10), @DLNO VARCHAR(100), @Discount FLOAT
	SELECT @DGRP=Discount_Group FROM tt_customer_discountgroup_mapping WHERE Customer_number=ISNULL(@CP_Number,@Cust_Number)
	SELECT @IDGRP=IDISC FROM tt_GALF6TA_II WHERE ICAT=@Item_number
	SELECT @DLNO = CASE @DGRP WHEN 'D1' THEN '1354' WHEN 'D2' THEN '1355' WHEN 'D3' THEN '1356' WHEN 'D4' THEN '1357' WHEN 'D5' THEN '1358' ELSE '' END
	IF(@DLNO='' OR @DLNO is null OR @DLNO=null)
		SET @ret=0.0
	ELSE
		SELECT @Discount=DLDIS FROM tt_GALF6TA_DL dl
			JOIN tt_GALF6TA_II ii ON ii.IDISC=dl.DLKEY
			WHERE dl.DLNO IN (SELECT DCNO FROM tt_GALF6TA_DC WHERE DCTEFT>CONCAT(Year(GETDATE()),RIGHT('0' + RTRIM(MONTH(GETDATE())), 2),DAY(GETDATE())))
			AND ii.ICAT=@Item_number
			AND dl.DLNO=@DLNO
			SET @ret=CONVERT(float,@ListPrice)-(CONVERT(float,@ListPrice)*@Discount)
	RETURN @ret
END


GO
