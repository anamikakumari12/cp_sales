﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuoteRequest.aspx.cs" Inherits="DistributorSystem.QuoteRequest" MasterPageFile="~/MasterPage.Master" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/0.10.0/lodash.min.js"></script>
    <script src="js/bundle.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.full.js"></script>

    <link href="css/Tabs.css" rel="stylesheet" />
    <style>
      
        table.dataTable.no-footer
        {
            margin-left:0!important;
        }
        .dataTables_scrollHeadInner
        {
            width:100%!important;
        }
        .modal
        {
            position: fixed;
            top: 0;
            left: 0;
            background-color: black;
            z-index: 99;
            opacity: 0.8;
            filter: alpha(opacity=80);
            -moz-opacity: 0.8;
            min-height: 100%;
            width: 100%;
        }

        .loading
        {
            font-family: Arial;
            font-size: 10pt;
            border: 5px solid #67CFF5;
            width: 200px;
            height: 100px;
            display: none;
            position: fixed;
            background-color: White;
            z-index: 999;
        }

        .disabled
        {
            background-color: #e4e4e4!important;
            border: 1px solid #aaa!important;
            border-radius: 4px!important;
            cursor: default!important;
            float: left!important;
            /* margin-right: 5px; */
            /* margin-top: 5px; */
            padding: 0 5px!important;
        }

        .dataTables_wrapper
        {
            min-height: 300px;
        }

        .order
        {
            padding-left: 10px;
            padding-right: 10px;
        }

        .divorder
        {
            border: 1px solid #316f8f;
            height: 90px;
            padding: 10px;
        }

        .ddl
        {
            background-color: #fff;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            height: 28px;
        }

        .required:after
        {
            content: " *";
            color: red;
        }

        .loader_div
        {
            position: absolute;
            top: 0;
            bottom: 0%;
            left: 0;
            right: 0%;
            z-index: 99;
            opacity: 0.7;
            display: none;
            background: lightgrey url('../../../tt_dist/images/loader.gif') center center no-repeat;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            debugger;
            //$(".ep").blur(function () {
                $('#MainContent_grdPriceRequest').on('blur', 'tbody tr td .ep', function () {
                debugger;
                var id = $(this).get(0).id;
                var data = sessionStorage.getItem("APValidity");
                data = JSON.parse(data);
                if (!(data.d.item == null || data.d.item == "" || data.d.item == undefined)) {
                    var message = "The agreement price for item " + data.d.item_desc + " with quantity " + data.d.quantity + " is already available with price " + data.d.AP + ". The price is valid till " + data.d.Valid_to + ". If you want to proceed with request, you should enter lesser requested price than agreement price. Do you want to continue?";
                    if (window.confirm(message)) {
                        // They clicked Yes
                    }
                    else {
                        var hdnItem_id = id.replace("txtTargetPrice", "hdnItem");
                        var item_id = id.replace("txtTargetPrice", "ddlitem");
                        var WHS_id = id.replace("txtTargetPrice", "txtWHS");
                        var LP_id = id.replace("txtTargetPrice", "txtDLP");
                        var AP_id = id.replace("txtTargetPrice", "txtAP");
                        $('#' + WHS_id).val("");
                        $('#' + LP_id).val("");
                        $('#' + AP_id).val("");
                        $('#' + hdnItem_id).val("");
                        $("#" + item_id).val("0");
                        $("#" + item_id).select2({ text: "--Select--", "minimumInputLength": 3, });
                        $('#' + id).val("");
                        $('#' + id.replace("txtTargetPrice", "txtDCRate")).val("");
                    }
                }
            });

            //$(".qty").focus(function () {
            //    console.log('in');
            //}).blur(function () {
            //    debugger;
            //    console.log('out');
            //    var qty_value = $(this).val();
            //    var qty_id = $(this).get(0).id;
            //    var item_value = $("#" + qty_id.replace('txtTotQTY', 'hdnItem')).val();
            //    $.ajax({
            //        url: 'QuoteRequest.aspx/LoadAgreementPrice',
            //        method: 'post',
            //        datatype: 'json',
            //        data: '{"item":"' + item_value + '", "qty":"' + qty_value + '"}',
            //        contentType: "application/json; charset=utf-8",
            //        success: function (data) {
            //            console.log(data);
            //            console.log(JSON.stringify(data));
            //            sessionStorage.setItem('APValidity', JSON.stringify(data));
            //            $('#' + qty_id.replace('txtTotQTY', 'txtAP')).val(data.d.AP);
            //            //var id = qty_id;
            //            //var data = sessionStorage.getItem("APValidity");
            //            //data = JSON.parse(data);
            //            //if (!(data.d.item == null || data.d.item == "" || data.d.item == undefined)) {
            //            //    var message = "The agreement price for item " + data.d.item_desc + " with quantity " + data.d.quantity + " is already available with price " + data.d.AP + ". The price is valid till " + data.d.Valid_to + ". If you want to proceed with request, you should enter lesser requested price than agreement price. Do you want to continue?";
            //            //    if (window.confirm(message)) {
            //            //        // They clicked Yes
            //            //    }
            //            //    else {
            //            //        var hdnItem_id = id.replace("txtTotQTY", "hdnItem");
            //            //        var item_id = id.replace("txtTotQTY", "ddlitem");
            //            //        var WHS_id = id.replace("txtTotQTY", "txtWHS");
            //            //        var LP_id = id.replace("txtTotQTY", "txtDLP");
            //            //        var AP_id = id.replace("txtTotQTY", "txtAP");
            //            //        $('#' + WHS_id).val("");
            //            //        $('#' + LP_id).val("");
            //            //        $('#' + AP_id).val("");
            //            //        $('#' + hdnItem_id).val("");
            //            //        $("#" + item_id).val("0");
            //            //        $("#" + item_id).select2({ text: "--Select--", "minimumInputLength": 3, });
            //            //        $('#' + id).val("");
            //            //        $('#' + id.replace("txtTotQTY", "txtDCRate")).val("");
            //            //    }
            //            //}
            //        },
            //        error: function (xhr, ajaxOptions, thrownError) {
            //            alert(xhr.responseText);
            //        }
            //    });
            //});
           
        $('#MainContent_grdPriceRequest').on('focusout', 'tbody tr td .qty', function () {
                debugger;
                console.log('out');

                var qty_value = $(this).val();
                var qty_id = $(this).get(0).id;
                var item_value = $("#" + qty_id.replace('txtTotQTY', 'hdnItem')).val();
                $.ajax({
                    url: 'QuoteRequest.aspx/LoadAgreementPrice',
                    method: 'post',
                    datatype: 'json',
                    data: '{"item":"' + item_value + '", "qty":"' + qty_value + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        console.log(JSON.stringify(data));
                        sessionStorage.setItem('APValidity', JSON.stringify(data));
                        $('#' + qty_id.replace('txtTotQTY', 'txtAP')).val(data.d.AP);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });


            });

            $(".add-row").click(function (evt, obj) {

                add_row(evt, obj);
            });

            $('.ddl_item').on("change", function (e) {
                console.log("Start Item change : " + Date.now());
                var item_value = $(this).val();
                var item_id = $(this).get(0).id;
                $("#" + item_id.replace('ddlitem', 'hdnItem')).val(item_value);
                $.ajax({
                    url: 'QuoteRequest.aspx/LoadItemDetails',
                    method: 'post',
                    datatype: 'json',
                    data: '{"item":"' + item_value + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log("Start Load Item Detail : "+Date.now());
                        BindItemDetails(data, item_id);
                        console.log("Start Load Item Detail : " + Date.now());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });

            });

            $('.delbtn').on('click', function (evt) {

                if (Deletepopup(evt, this)) {
                    debugger;
                    var $row = jQuery(this).closest('tr');
                    $row.remove();
                    $('#MainContent_grdPriceRequest').dataTable();
                    if (parseInt($("[id*=grdPriceRequest] tr").length) <= 3) {
                        add_new_row(0);
                    }
                    //var $columns = $row.find('td');

                    //$columns.addClass('row-highlight');
                    //var values = "";

                    //jQuery.each($columns, function (i, item) {
                    //    values = values + 'td' + (i + 1) + ':' + item.innerHTML + '<br/>';
                    //    alert(values);
                    //});
                    //console.log(values);
                }
            });

            $('#MainContent_btnSave').on('click', function (evt, obj)
            {
                debugger;
                jQuery(".loader_div").show();
                if (validateFields(evt, obj)) {
                    var totaltrCount = $("[id*=grdPriceRequest] tr").length;
                    var trCount = $("[id*=grdPriceRequest] td").closest("tr").length;
                    var param;
                    var paramList = [];
                    var values = "";
                    var item_id;
                    var i;
                    for (rowCount = 0; rowCount < trCount; rowCount++) {
                        item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(rowCount).id;

                        i = item_id.substring(item_id.lastIndexOf("_")+1, item_id.length)
                        param = {
                            Ref_number: $('#MainContent_txtRef').val(),
                            Item_code: $('#MainContent_grdPriceRequest_ddlitem_' + i).val(),
                            Item_Desc: $('#MainContent_grdPriceRequest_ddlitem_' + i + '  option:selected').text(),
                            WHS: $('#MainContent_grdPriceRequest_txtWHS_' + i).val(),
                            Order_type: $('#MainContent_grdPriceRequest_ddlOrder_' + i).val(),
                            Order_freq:'',
                                //$('#MainContent_grdPriceRequest_ddlFrequency_' + i).val(),
                            Total_QTY: $('#MainContent_grdPriceRequest_txtTotQTY_' + i).val(),
                            QTY_perOrder: $('#MainContent_grdPriceRequest_txtQTYPO_' + i).val(),
                            List_Price: $('#MainContent_grdPriceRequest_txtDLP_' + i).val(),
                            Expected_price: $('#MainContent_grdPriceRequest_txtTargetPrice_' + i).val(),
                            DC_rate: $('#MainContent_grdPriceRequest_txtDCRate_' + i).val(),
                            Cust_number: $('#MainContent_grdPriceRequest_hdnCustomers_' + i).val(),
                            Cust_SP: $('#MainContent_grdPriceRequest_txtCustSP_' + i).val(),
                            Comp_Name: '',
                            Comp_Desc: '',
                            Comp_SP: ''
                            //Comp_Name: $('#MainContent_grdPriceRequest_ddlCompanyName_' + i).val(),
                            //Comp_Desc: $('#MainContent_grdPriceRequest_txtDescription_' + i).val(),
                            //Comp_SP: $('#MainContent_grdPriceRequest_txtCompanySP_' + i).val()
                        }
                        paramList.push(param);
                    }
                    var dataParam = {
                        obj: paramList
                    }
                    $.ajax({
                        url: 'QuoteRequest.aspx/GetItemDetailsForTable',
                        method: 'post',
                        datatype: 'json',
                        data: JSON.stringify(dataParam),
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            alert(data.d.ErrorMsg);
                            location.reload(true);
                            jQuery(".loader_div").hide();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.responseText);
                            jQuery(".loader_div").hide();
                        }
                    });
                }
                else {
                    alert("Please check error message.");
                    jQuery(".loader_div").hide();
                }

                
            });

            $('#MainContent_btnDraft').on('click', function (evt, obj) {
                debugger;
                jQuery(".loader_div").show();
                if (validateFields(evt, obj)) {
                    var totaltrCount = $("[id*=grdPriceRequest] tr").length;
                    var trCount = $("[id*=grdPriceRequest] td").closest("tr").length;
                    var param;
                    var paramList = [];
                    var values = "";
                    var item_id;
                    var i;
                    for (rowCount = 0; rowCount < trCount; rowCount++) {
                        item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(rowCount).id;

                        i = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)
                        param = {
                            Ref_number: $('#MainContent_txtRef').val(),
                            Item_code: $('#MainContent_grdPriceRequest_ddlitem_' + i).val(),
                            Item_Desc: $('#MainContent_grdPriceRequest_ddlitem_' + i + '  option:selected').text(),
                            WHS: $('#MainContent_grdPriceRequest_txtWHS_' + i).val(),
                            Order_type: $('#MainContent_grdPriceRequest_ddlOrder_' + i).val(),
                            Order_freq: '',
                            //$('#MainContent_grdPriceRequest_ddlFrequency_' + i).val(),
                            Total_QTY: $('#MainContent_grdPriceRequest_txtTotQTY_' + i).val(),
                            QTY_perOrder: $('#MainContent_grdPriceRequest_txtQTYPO_' + i).val(),
                            List_Price: $('#MainContent_grdPriceRequest_txtDLP_' + i).val(),
                            Expected_price: $('#MainContent_grdPriceRequest_txtTargetPrice_' + i).val(),
                            DC_rate: $('#MainContent_grdPriceRequest_txtDCRate_' + i).val(),
                            Cust_number: $('#MainContent_grdPriceRequest_hdnCustomers_' + i).val(),
                            Cust_SP: $('#MainContent_grdPriceRequest_txtCustSP_' + i).val(),
                            Comp_Name: '',
                            Comp_Desc: '',
                            Comp_SP: ''
                            //Comp_Name: $('#MainContent_grdPriceRequest_ddlCompanyName_' + i).val(),
                            //Comp_Desc: $('#MainContent_grdPriceRequest_txtDescription_' + i).val(),
                            //Comp_SP: $('#MainContent_grdPriceRequest_txtCompanySP_' + i).val()
                        }
                        paramList.push(param);
                    }
                    var dataParam = {
                        obj: paramList
                    }
                    $.ajax({
                        url: 'QuoteRequest.aspx/SaveAsDraft',
                        method: 'post',
                        datatype: 'json',
                        data: JSON.stringify(dataParam),
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            alert(data.d.ErrorMsg);
                            location.reload(true);
                            jQuery(".loader_div").hide();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.responseText);
                            jQuery(".loader_div").hide();
                        }
                    });
                }
                else {
                    alert("Please check error message.");
                    jQuery(".loader_div").hide();
                }


            });

            $('.lnk_item').on('click', function (evt) {
                
                var item_id = $(this).get(0).id.replace('lnk', 'lbl');
                var item_value = $('#' + item_id).text();

                $.ajax({
                    url: 'QuoteRequest.aspx/LoadDetailsFromlink',
                    method: 'post',
                    datatype: 'json',
                    data: '{"item":"' + item_value + '", "type":"F"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        LoadFrequentlyRecentlyItem(data);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });
            });

            $('.lnk_Ritem').on('click', function (evt) {
                
                var item_id = $(this).get(0).id.replace('lnk', 'lbl');
                var item_value = $('#' + item_id).text();

                $.ajax({
                    url: 'QuoteRequest.aspx/LoadDetailsFromlink',
                    method: 'post',
                    datatype: 'json',
                    data: '{"item":"' + item_value + '", "type":"R"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        LoadFrequentlyRecentlyItem(data);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });
            });
            LoadDropdowns();
            LoadTable();
        });


            
        function LaodAP(event, obj)
        {
            //debugger;
            //var qty_value = $('#' + obj.id).val();
            //var qty_id = obj.id;
            //var item_value = $("#" + qty_id.replace('txtTotQTY', 'hdnItem')).val();
            //$.ajax({
            //    url: 'QuoteRequest.aspx/LoadAgreementPrice',
            //    method: 'post',
            //    datatype: 'json',
            //    data: '{"item":"' + item_value + '", "qty":"' + qty_value + '"}',
            //    contentType: "application/json; charset=utf-8",
            //    success: function (data) {
            //        console.log(data);
            //        console.log(JSON.stringify(data));
            //        sessionStorage.setItem('APValidity', JSON.stringify(data));
            //        $('#' + qty_id.replace('txtTotQTY', 'txtAP')).val(data.d.AP);
            //    },
            //    error: function (xhr, ajaxOptions, thrownError) {
            //        alert(xhr.responseText);
            //    }
            //});
        }

        function ddl_item_change(e) {

            var item_value = e.value;
            var item_id = e.id;
            $("#" + item_id.replace('ddlitem', 'hdnItem')).val(item_value);
            $.ajax({
                url: 'QuoteRequest.aspx/LoadItemDetails',
                method: 'post',
                datatype: 'json',
                data: '{"item":"' + item_value + '"}',
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    BindItemDetails(data, item_id);

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                }
            });

        }

        function delete_row(evt, obj) {
            debugger;
            if (Deletepopup(evt, obj)) {
                var $row = jQuery(obj).closest('tr');
                $row.remove();
                if (parseInt($("[id*=grdPriceRequest] tr").length )<= 3) {
                    add_new_row(0);
                }
                //var $columns = $row.find('td');

                //$columns.addClass('row-highlight');
                //var values = "";

                //jQuery.each($columns, function (i, item) {
                //    values = values + 'td' + (i + 1) + ':' + item.innerHTML + '<br/>';
                //    alert(values);
                //});
                //console.log(values);
            }
        }
        function add_row(evt, obj) {
            console.log("Start AddRow : " + Date.now());
            if (validateFields(evt, obj)) {
                console.log("After Validate : " + Date.now());
                var totaltrCount = $("[id*=grdPriceRequest] tr").length;
                var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;

                var item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(rowCount - 1).id;

                var trCount = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)
                trCount++;
                console.log("mid AddRow : " + Date.now());
                add_new_row(trCount);
                console.log("end AddRow : " + Date.now());
            }
        }
        function add_new_row(trCount)
        {
           
            markup = '<tr role="row" class="even">				<td>' +
                   '         <input type="hidden" id="MainContent_grdPriceRequest_hdnItem_' + trCount + '">' +
                  '          <input type="hidden" id="MainContent_grdPriceRequest_hdnStockCode_' + trCount + '">' +
                 '           <select id="MainContent_grdPriceRequest_ddlitem_' + trCount + '" onchange="ddl_item_change(this);" class="ddl_item" style="width:200px!important;"></select>' +
                '        </td><td>' +
               '             <input type="text" readonly="readonly" id="MainContent_grdPriceRequest_txtWHS_' + trCount + '" class="ddl" style="width: 70px;">' +
              '          </td><td>' +
             '               <select id="MainContent_grdPriceRequest_ddlOrder_' + trCount + '" class="ddl" onchange="OrderChange(this);">' +
            '	<option value="onetime">One Time</option>' +
            '	<option value="schedule">Schedule</option>' +
            '</select>' +
              '          </td><td>' +
            // '               <select id="MainContent_grdPriceRequest_ddlFrequency_' + trCount + '" class="ddl disabled" disabled="">' +
            //'	<option value="weekly">Weekly</option>' +
            //'	<option value="fortnightly">Fortnightly</option>' +
            //'	<option value="monthly">Monthly</option>' +
            //'</select>' +
            //            '</td><td>' +
                         '   <input type="text" id="MainContent_grdPriceRequest_txtTotQTY_' + trCount + '" class="ddl qty" onkeyup="LaodAP(event, this);" onkeypress="return isNumberKey(event,this);" style="width: 80px;">' +
                        '</td><td>' +
                        '    <input type="text" id="MainContent_grdPriceRequest_txtQTYPO_' + trCount + '" class="ddl disabled" onkeypress="return isNumberKey(event,this);" style="width: 80px;" disabled="">' +
                        '</td><td>' +
                         '   <input type="text" readonly="readonly" id="MainContent_grdPriceRequest_txtDLP_' + trCount + '" class="ddl disabled" style="width: 80px;">' +
                        '</td><td>' +
                         '   <input type="text" readonly="readonly" id="MainContent_grdPriceRequest_txtAP_' + trCount + '" class="ddl disabled" style="width: 80px;">' +
                        '</td><td>' +
                         '   <input type="text" id="MainContent_grdPriceRequest_txtTargetPrice_' + trCount + '" class="ddl ep" onkeypress="return isNumberKey(event,this);" onkeyup="calDCrate(event,this);" style="width: 80px;">' +
                        '    <input type="hidden" id="MainContent_grdPriceRequest_hdnAgreementPrice_' + trCount + '">' +
                       '     <span id="MainContent_grdPriceRequest_lblPriceError_' + trCount + '" style="color: red;"></span>' +
                      '  </td><td>' +
                     '       <input type="text" id="MainContent_grdPriceRequest_txtDCRate_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" onkeyup="calExpectedPrice(event,this);" style="width: 80px;">' +
                    '    </td><td>' +
                   '         <select id="MainContent_grdPriceRequest_ddlCustName_' + trCount + '" class="ddl" style="width: 200px;" ></select>' +
                  '          <input type="hidden" id="MainContent_grdPriceRequest_hdnCustomers_' + trCount + '">' +
                 '           <span id="MainContent_grdPriceRequest_lblCustError_' + trCount + '" style="color: red;"></span>' +
                '        </td><td>' +
               '             <input type="text" id="MainContent_grdPriceRequest_txtCustSP_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" style="width: 80px;">' +
              '          </td><td>' +
            // '               <select id="MainContent_grdPriceRequest_ddlCompanyName_' + trCount + '" class="ddl" style="width: 200px;">' +
            //'</select>' +
            //            '</td><td>' +
            //             '   <input type="text" id="MainContent_grdPriceRequest_txtDescription_' + trCount + '" class="ddl" style="width: 200px;">' +
            //            '</td><td>' +
            //             '   <input type="text" id="MainContent_grdPriceRequest_txtCompanySP_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" style="width: 80px;">' +
            //            '</td><td>' +
                          '  <input type="image" id="MainContent_grdPriceRequest_imgbtnAdd_' + trCount + '" onclick="add_row();" class="add-row" src="images/add-icon.jpeg" onclick="return false;" style="width: 25px;">' +
                         '   <input type="image" id="MainContent_grdPriceRequest_imgbtnDel_' + trCount + '" class="delbtn" src="images/delete.png" onclick="delete_row(event,this);" onclick="return false;"  style="width: 25px;">' +
                        '</td>' +
        '</tr>";'
            tableBody = $("table tbody");
            tableBody.append(markup);
            debugger;
            //  LoadDropdowns();
            LoadDropdownForNextRow(trCount);
            console.log("Start AddNewRow : " + Date.now());
        }

        function LoadDropdownForNextRow(i)
        {
            jQuery(".loader_div").show();
            var msg = sessionStorage.getItem("itemdata");
            BindItemSelect(JSON.parse(msg), i);
            msg = sessionStorage.getItem("customerdata");
            BindCustomerSelect(JSON.parse(msg), i);

            var ordertype = $("#MainContent_grdPriceRequest_ddlOrder_" + i).val();
            if (ordertype == "schedule") {
                $("#MainContent_grdPriceRequest_txtQTYPO_" + i).removeClass('disabled');
                $("#MainContent_grdPriceRequest_txtQTYPO_" + i).prop("disabled", false);
            }
            else {
                $("#MainContent_grdPriceRequest_txtQTYPO_" + i).addClass('disabled');
                $("#MainContent_grdPriceRequest_txtQTYPO_" + i).prop("disabled", true);
            }
            jQuery(".loader_div").hide();
        }

        function LoadFrequentlyRecentlyItem(data) {
            
            var msg = data.d;
            var totaltrCount = $("[id*=grdPriceRequest] tr").length;
            var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;

            var item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(rowCount-1).id;

            var trCount = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)
            trCount++;
            markup = '<tr role="row" class="even">				<td>' +
                   '         <input type="hidden" id="MainContent_grdPriceRequest_hdnItem_' + trCount + '" value="' + msg.Item_code + '" >' +
                  '          <input type="hidden" id="MainContent_grdPriceRequest_hdnStockCode_' + trCount + '">' +
                 '           <select id="MainContent_grdPriceRequest_ddlitem_' + trCount + '" onchange="ddl_item_change(this);" class="ddl_item" style="width:200px!important;"></select>' +
                '        </td><td>' +
               '             <input type="text" value="' + msg.WHS + '" readonly="readonly" id="MainContent_grdPriceRequest_txtWHS_' + trCount + '" class="ddl" style="width: 70px;">' +
              '          </td><td>' +
             '               <select id="MainContent_grdPriceRequest_ddlOrder_' + trCount + '" class="ddl" onchange="OrderChange(this);">' +
            '	<option value="onetime">One Time</option>' +
            '	<option value="schedule">Schedule</option>' +
            '</select>' +
              '          </td><td>' +
            // '               <select id="MainContent_grdPriceRequest_ddlFrequency_' + trCount + '" class="ddl disabled" disabled="">' +
            //'	<option value="weekly">Weekly</option>' +
            //'	<option value="fortnightly">Fortnightly</option>' +
            //'	<option value="monthly">Monthly</option>' +
            //'</select>' +
            //            '</td><td>' +
                         '   <input type="text" value="' + msg.Total_QTY + '" id="MainContent_grdPriceRequest_txtTotQTY_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" style="width: 80px;">' +
                        '</td><td>' +
                        '    <input type="text" value="' + msg.QTY_perOrder + '" id="MainContent_grdPriceRequest_txtQTYPO_' + trCount + '" class="ddl disabled" onkeypress="return isNumberKey(event,this);" style="width: 80px;" disabled="">' +
                        '</td><td>' +
                         '   <input type="text" value="' + msg.List_Price + '" readonly="readonly" id="MainContent_grdPriceRequest_txtDLP_' + trCount + '" class="ddl disabled" style="width: 80px;">' +
                        '</td><td>' +
                         '   <input type="text" readonly="readonly" id="MainContent_grdPriceRequest_txtAP_' + trCount + '" class="ddl disabled" style="width: 80px;">' +
                        '</td><td>' +
                         '   <input type="text" value="' + msg.Expected_price + '" id="MainContent_grdPriceRequest_txtTargetPrice_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" onkeyup="calDCrate(event,this);" style="width: 80px;">' +
                        '    <input type="hidden" id="MainContent_grdPriceRequest_hdnAgreementPrice_' + trCount + '">' +
                       '     <span id="MainContent_grdPriceRequest_lblPriceError_' + trCount + '" style="color: red;"></span>' +
                      '  </td><td>' +
                     '       <input type="text" value="' + msg.DC_rate + '" id="MainContent_grdPriceRequest_txtDCRate_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" onkeyup="calExpectedPrice(event,this);" style="width: 80px;">' +
                    '    </td><td>' +
                   '         <select id="MainContent_grdPriceRequest_ddlCustName_' + trCount + '" class="ddl" style="width: 200px;" ></select>' +
                  '          <input type="hidden" value="' + msg.Cust_number + '" id="MainContent_grdPriceRequest_hdnCustomers_' + trCount + '">' +
                 '           <span id="MainContent_grdPriceRequest_lblCustError_' + trCount + '" style="color: red;"></span>' +
                '        </td><td>' +
               '             <input type="text" value="' + msg.Cust_SP + '" id="MainContent_grdPriceRequest_txtCustSP_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" style="width: 80px;">' +
              '          </td><td>' +
            // '               <select id="MainContent_grdPriceRequest_ddlCompanyName_' + trCount + '" class="ddl" style="width: 200px;">' +
            //'</select>' +
            //            '</td><td>' +
            //             '   <input type="text" id="MainContent_grdPriceRequest_txtDescription_' + trCount + '" class="ddl" style="width: 200px;">' +
            //            '</td><td>' +
            //             '   <input type="text" id="MainContent_grdPriceRequest_txtCompanySP_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" style="width: 80px;">' +
            //            '</td><td>' +
                          '  <input type="image" id="MainContent_grdPriceRequest_imgbtnAdd_' + trCount + '" onclick="add_row();" class="add-row" src="images/add-icon.jpeg" onclick="return false;" style="width: 25px;">' +
                         '   <input type="image" id="MainContent_grdPriceRequest_imgbtnDel_' + trCount + '" class="delbtn" src="images/delete.png" onclick="Deletepopup(event,this);" onclick="return false;" style="width: 25px;">' +
                        '</td>' +
        '</tr>";'
            tableBody = $("table tbody");
            tableBody.append(markup);
            //LoadDropdowns();
            LoadDropdownForNextRow(trCount);
        }

        function BindItemDetails(data, id) {

            $('#' + id.replace('ddlitem', 'txtWHS')).val(data.d.WHS);
            $('#' + id.replace('ddlitem', 'txtDLP')).val(data.d.LP);
            $('#' + id.replace('ddlitem', 'hdnStockCode')).val(data.d.stockCode);
            ShowMOQCondition(data.d.item, data.d.stockCode, id);
        }

        function BindItems(msg) {
            
            var totalRowCount = $("[id*=grdPriceRequest] tr").length;   
            var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;
            var item_id;
            var i;
            for (var trCount = 0; trCount < rowCount; trCount++) {
                item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(trCount).id;
                i = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)
                BindItemSelect(msg.d,i);
            }

        }
        function BindItemSelect(msg,i)
        {
            if (!$("#MainContent_grdPriceRequest_ddlitem_" + i).data('select2')) {
                $("#MainContent_grdPriceRequest_ddlitem_" + i).select2({
                    "width": "200px", "minimumInputLength": 3, data: msg, placeholder: "Select an item"
                });
                var str = $("#MainContent_grdPriceRequest_hdnItem_" + i).val();
                if (!IsnullOrEmpty(str)) {
                    $("#MainContent_grdPriceRequest_ddlitem_" + i).val(str);
                    $("#MainContent_grdPriceRequest_ddlitem_" + i).select2({  matcher: matchStart, text: str, "minimumInputLength": 3, });
                }
            }
        }
        function matchStart(params, data) {
            debugger;
            // If there are no search terms, return all of the data
            if ($.trim(params.term) === '') {
                return data;
            }

            // Skip if there is no 'children' property
            if (typeof data.children === 'undefined') {
                return null;
            }

            // `data.children` contains the actual options that we are matching against
            var filteredChildren = [];
            $.each(data.children, function (idx, child) {
                if (child.text.toUpperCase().indexOf(params.term.toUpperCase()) == 0) {
                    filteredChildren.push(child);
                }
            });

            // If we matched any of the timezone group's children, then set the matched children on the group
            // and return the group object
            if (filteredChildren.length) {
                var modifiedData = $.extend({}, data, true);
                modifiedData.children = filteredChildren;

                // You can return modified objects from here
                // This includes matching the `children` how you want in nested data sets
                return modifiedData;
            }

            // Return `null` if the term should not be displayed
            return null;
        }
        function ShowMOQCondition(item, stock, ddlidesc_id) {
            debugger;
            if (stock != "1" && stock != "6") {
                //alert(item + " may involve MOQ, do you want to proceed?");
                if (window.confirm(item + " may involve MOQ, do you want to proceed?")) {
                    // They clicked Yes
                }
                else {
                    //var item_id = ddlidesc_id.replace("ddlitem", "ddlitem");
                    var WHS_id = ddlidesc_id.replace("ddlitem", "txtWHS");
                    var LP_id = ddlidesc_id.replace("ddlitem", "txtDLP");
                    var AP_id = ddlidesc_id.replace("ddlitem", "txtAP");
                    $('#' + WHS_id).val("");
                    $('#' + LP_id).val("");
                    $('#' + AP_id).val("");
                    $("#" + ddlidesc_id).val("0");
                    $("#" + ddlidesc_id).select2({ text: "--Select--", "minimumInputLength": 3, });

                }
            }
        }

        function LoadPage() {
            LoadDropdowns();
            LoadTable();
        }
        function LoadTable() {

            var head_content = $('#MainContent_grdPriceRequest tr:first').html();
            $('#MainContent_grdPriceRequest').prepend('<thead></thead>')
            $('#MainContent_grdPriceRequest thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdPriceRequest tbody tr:first').hide();
            var table = $('#MainContent_grdPriceRequest').dataTable({
                "ordering": false,
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'lBfrtip',
                //dom: 'Bfrtip',
                buttons: [
                    //'copy', 'csv',
                    //'excel', 'pdf', 'print'
                ],
                //"scrollY": 200,
                "scrollX": true
            });
        }
        function LoadDropdowns() {
            jQuery(".loader_div").show();
            console.log("LoadItem start : " + Date.now());
            $.ajax({
                url: 'QuoteRequest.aspx/LoadItems',
                method: 'post',
                datatype: 'json',
                data: '',
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    sessionStorage.setItem("itemdata", JSON.stringify(data.d));
                    BindItems(data);
                    jQuery(".loader_div").hide();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                    jQuery(".loader_div").hide();
                }
            });

            $.ajax({
                url: 'QuoteRequest.aspx/LoadCustomers',
                method: 'post',
                datatype: 'json',
                data: '',
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    sessionStorage.setItem("customerdata", JSON.stringify(msg.d));
                    var totalRowCount = $("[id*=grdPriceRequest] tr").length;
                    var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;
                    var item_id;
                    var i;
                    for (var trCount = 0; trCount < rowCount; trCount++) {
                        item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(trCount).id;
                        i = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)
                        BindCustomerSelect(msg.d, i);

                        var ordertype = $("#MainContent_grdPriceRequest_ddlOrder_" + i).val();
                        if (ordertype == "schedule") {
                            $("#MainContent_grdPriceRequest_txtQTYPO_" + i).removeClass('disabled');
                            $("#MainContent_grdPriceRequest_txtQTYPO_" + i).prop("disabled", false);
                        }
                        else {
                            $("#MainContent_grdPriceRequest_txtQTYPO_" + i).addClass('disabled');
                            $("#MainContent_grdPriceRequest_txtQTYPO_" + i).prop("disabled", true);
                        }
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                }
            });
            //$.ajax({
            //    url: 'QuoteRequest.aspx/LoadCompetitors',
            //    method: 'post',
            //    datatype: 'json',
            //    data: '',
            //    contentType: "application/json; charset=utf-8",
            //    success: function (msg) {
            //        var totalRowCount = $("[id*=grdPriceRequest] tr").length;
            //        var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;
            //        var item_id;
            //        var i;
            //        for (var trCount = 0; trCount < rowCount; trCount++) {
            //            item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(trCount).id;
            //            i = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)
            //            if($("#MainContent_grdPriceRequest_ddlCompanyName_" + i)!=undefined)
            //                $("#MainContent_grdPriceRequest_ddlCompanyName_" + i).select2({ data: msg.d });
            //        }
            //    },
            //    error: function (xhr, ajaxOptions, thrownError) {
            //        alert(xhr.responseText);
            //    }
            //});
            //var totalRowCount = $("[id*=grdPriceRequest] tr").length;
            //var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;
            //var item_id;
            //var i;
            //for (var trCount = 0; trCount < rowCount; trCount++) {
            //    item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(trCount).id;
            //    i = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)
            //    var ordertype = $("#MainContent_grdPriceRequest_ddlOrder_" + i).val();
            //    if (ordertype == "schedule") {
            //        $("#MainContent_grdPriceRequest_txtQTYPO_" + i).removeClass('disabled');
            //        $("#MainContent_grdPriceRequest_txtQTYPO_" + i).prop("disabled", false);
            //    }
            //    else {
            //        $("#MainContent_grdPriceRequest_txtQTYPO_" + i).addClass('disabled');
            //        $("#MainContent_grdPriceRequest_txtQTYPO_" + i).prop("disabled", true);
            //    }
            //}
        }

        function BindCustomerSelect(msg,i)
        {
            $("#MainContent_grdPriceRequest_ddlCustName_" + i).select2({ "multiple": "true", data: msg });
            var str = $("#MainContent_grdPriceRequest_hdnCustomers_" + i).val();
            if (!IsnullOrEmpty(str)) {
                $("#MainContent_grdPriceRequest_ddlCustName_" + i).val(str.split(","));
                $("#MainContent_grdPriceRequest_ddlCustName_" + i).select2({ text: str });
            }
        }

        document.addEventListener("DOMContentLoaded", function () {

            var tabs = document.querySelectorAll('.tabbed li');
            var switchers = document.querySelectorAll('.switcher-box a');
            var skinable = document.getElementById('skinable');

            for (var i = 0, len = tabs.length; i < len; i++) {
                tabs[i].addEventListener("click", function () {
                    if (this.classList.contains('active')) {

                        return;
                    }
                    var parent = this.parentNode,
                        innerTabs = parent.querySelectorAll('li');

                    for (var index = 0, iLen = innerTabs.length; index < iLen; index++) {
                        innerTabs[index].classList.remove('active');
                    }

                    this.classList.add('active');
                });
            }

            for (var i = 0, len = switchers.length; i < len; i++) {
                switchers[i].addEventListener("click", function () {
                    if (this.classList.contains('active'))
                        return;

                    var parent = this.parentNode,
                        innerSwitchers = parent.querySelectorAll('a'),
                        skinName = this.getAttribute('skin');

                    for (var index = 0, iLen = innerSwitchers.length; index < iLen; index++) {
                        innerSwitchers[index].classList.remove('active');
                    }

                    this.classList.add('active');
                    skinable.className = 'tabbed round ' + skinName;
                });
            }
        });

        function tabchange(e) {

            if (e.id == "MainContent_freqList") {
                $('#MainContent_divFrequent').css("display", "block");
                $('#MainContent_divRecent').css("display", "none");
                $('#MainContent_divRecent').removeClass("active");
                $('#MainContent_divFrequent').addClass("active");
            }
            else {
                $('#MainContent_divFrequent').css("display", "none");
                $('#MainContent_divRecent').css("display", "block");
                $('#MainContent_divRecent').addClass("active");
                $('#MainContent_divFrequent').removeClass("active");
            }

        }

        function isNumberKey(evt, obj) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                     // Number of digits right of decimal point.
                     (match[1] ? match[1].length : 0)
                     // Adjust for scientific notation.
                     - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;
                if (charCode == 46) return false;
            }
            else {
                if (value.length > 10) {
                    if (charCode == 46) return true;
                    else return false;
                }
            }
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function calculateYearlyQty(evt, obj) {
            var value = obj.value;
            if (value != undefined && value != '') {
                var mqty_id = obj.id;
                var aqty_id = mqty_id.replace("txtMQTY", "txtAQTY");
                var actual_id = "#" + aqty_id;
                $(actual_id).val(value * 12);
            }
        }
        function calculateMonthlyQty(evt, obj) {
            var value = obj.value;
            if (value != undefined && value != '') {
                var mqty_id = obj.id;
                var aqty_id = mqty_id.replace("txtAQTY", "txtMQTY");
                var actual_id = "#" + aqty_id;
                $(actual_id).val(value / 12);
            }
        }
        function calDCrate(evt, obj) {
            
            var value = obj.value;
            var ep_id = obj.id;
            var lp_id = ep_id.replace("txtTargetPrice", "txtDLP");
            var dc_id = ep_id.replace("txtTargetPrice", "txtDCRate");
            if (value != undefined && value != '') {
                var actual_lp_id = "#" + lp_id;
                var actual_dc_id = "#" + dc_id;
                var listprice = $(actual_lp_id).val();
                var rate = (listprice - value) * 100 / listprice;
                $(actual_dc_id).val(parseFloat(rate).toFixed(2));

                $('#' + dc_id).attr("disabled", "true");
            }
            else {
                $('#' + dc_id).val('');
                $('#' + dc_id).removeAttr("disabled");
            }
        }

        function calExpectedPrice(evt, obj) {

            var value = obj.value;
            var dc_id = obj.id;
            var lp_id = dc_id.replace("txtDCRate", "txtDLP");
            var ep_id = dc_id.replace("txtDCRate", "txtTargetPrice");
            if (value != undefined && value != '') {
                var actual_lp_id = "#" + lp_id;
                var actual_ep_id = "#" + ep_id;
                var listprice = $(actual_lp_id).val();
                var targetprice = listprice * (1 - (value / 100));
                $(actual_ep_id).val(parseFloat(targetprice).toFixed(2));
                $('#' + ep_id).attr("disabled", "true");
            }
            else {
                $('#' + ep_id).val('');
                $('#' + ep_id).removeAttr("disabled");
            }
        }
        function Deletepopup(evt, obj) {
            var id = obj.id;
            var item_id = id.replace("imgbtnDel", "ddlitem");
            var item = $("#" + item_id + " option:selected").text().replace(/\s+/g, " ");
            if (!confirm("Do you want to delete " + item + "?")) { return false; }
            else
                return true;
        }
        function validateFields(evt, obj) {
            
            var table = $('#MainContent_grdPriceRequest');
            var errFlag = 0;
            var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;
            var i;
            var item_id;
            for (var trCount = 0; trCount < rowCount; trCount++) {
                item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(trCount).id;

                i = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)
                
                if (IsnullOrEmpty($("#MainContent_grdPriceRequest_ddlitem_" + i).val())) {
                    $("#MainContent_grdPriceRequest_ddlitem_" + i).css("border", "1px solid red");

                    $('.select2-container').css({ "border": "1px solid red;" });

                    errFlag++;
                }
                else {
                    $('.select2-container').css({ "border": "1px solid #aaa;" });
                }
                //if (IsnullOrEmpty($("#MainContent_grdPriceRequest_ddlitemdesc_" + i).val())) {
                //    $("#MainContent_grdPriceRequest_ddlitemdesc_" + i).css("border", "1px solid red");
                //    errFlag++;
                //}
                //else {
                //    $("#MainContent_grdPriceRequest_ddlitemdesc_" + i).css("border", "");
                //}
                //if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtTotQTY_" + i).val())) {
                //    $("#MainContent_grdPriceRequest_txtTotQTY_" + i).css("border", "1px solid red");
                //    errFlag++;
                //}
                //else {
                //    $("#MainContent_grdPriceRequest_txtTotQTY_" + i).css("border", "");
                //}

                var ordertype = $("#MainContent_grdPriceRequest_ddlOrder_" + i).val();
                if (ordertype == "schedule") {
                    if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtQTYPO_" + i).val())) {
                        $("#MainContent_grdPriceRequest_txtQTYPO_" + i).css("border", "1px solid red");
                        errFlag++;
                    }
                    else {
                        $("#MainContent_grdPriceRequest_txtQTYPO_" + i).css("border", "");
                    }
                }

                if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtTargetPrice_" + i).val())) {
                    $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "1px solid red");
                    errFlag++;
                }
                else {
                    $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "");
                    var tp = $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).val();
                    var ap = $("#MainContent_grdPriceRequest_hdnAgreementPrice_" + i).val();
                    if (!IsnullOrEmpty(ap) && ap > 0) {
                        if (tp >= ap) {
                            $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "1px solid red");
                            errFlag++;
                            $("#MainContent_grdPriceRequest_lblPriceError_" + i).text("Expected price should be less than the agreement price.");

                        }
                        else {
                            $("#MainContent_grdPriceRequest_lblPriceError_" + i).text("");
                            $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "");
                        }
                    }
                    else {
                        $("#MainContent_grdPriceRequest_lblPriceError_" + i).text("");
                        $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "");
                    }
                }
                if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtDCRate_" + i).val())) {
                    $("#MainContent_grdPriceRequest_txtDCRate_" + i).css("border", "1px solid red");
                    errFlag++;
                }
                else {
                    $("#MainContent_grdPriceRequest_txtDCRate_" + i).css("border", "");
                }
                //if (IsnullOrEmpty($("#MainContent_grdPriceRequest_ddlCustName_" + i).val())) {
                //    //$("#MainContent_grdPriceRequest_ddlCustName_" + i).css("border", "1px solid red");
                //    //$("#MainContent_grdPriceRequest_lblCustError_" + i).text("Required");
                //    //errFlag++;
                //}
                //else {
                //    $("#MainContent_grdPriceRequest_ddlCustName_" + i).css("border", "");
                //    var cust = $("#MainContent_grdPriceRequest_ddlCustName_" + i).val();
                //    $("#MainContent_grdPriceRequest_lblCustError_" + i).text("");
                //    $("#MainContent_grdPriceRequest_hdnCustomers_" + i).val(cust.toString());
                //}
            }
            if (errFlag > 0) {
                return false;
            }
            else {
                return true;
            }
        }
        function IsnullOrEmpty(val) {
            if (val != '' && val != undefined && val != '--Select--')
                return false;
            else
                return true;
        }
        function OrderChange(e) {

            var ordertype = e.options[e.selectedIndex].value;
            var id = e.id;
            //var feq_id = id.replace("ddlOrder", "ddlFrequency");
            var qty_id = id.replace("ddlOrder", "txtQTYPO");
            if (ordertype == "schedule") {
                //$('#' + feq_id).removeClass('disabled');
                //$('#' + feq_id).prop("disabled", false);
                $('#' + qty_id).removeClass('disabled');
                $('#' + qty_id).prop("disabled", false);
            }
            else {
                //$('#' + feq_id).addClass('disabled');
                //$('#' + feq_id).prop("disabled", true);
                $('#' + qty_id).addClass('disabled');
                $('#' + qty_id).prop("disabled", true);
            }
        }
        function ShowProgress() {

            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loader_div" class="loader_div"></div>
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Quote</a>
                        </li>
                        <li class="current">Request For Quote</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mn_margin">
        <asp:ScriptManager ID="SM1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>

        <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">

            <ContentTemplate>
                <asp:Panel ID="panelref" runat="server" CssClass="filter_panel">
                    <div class="col-md-12 nopadding">
                        <div class="col-md-1">
                            <asp:Label ID="lblRef" runat="server" Text="Reference"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <div class="controls">
                                <asp:TextBox ID="txtRef" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <asp:Button runat="server" ID="btnSave" CssClass="btnSubmit" Text="Submit" OnClientClick="return false;" />
                            <asp:Button runat="server" ID="btnDraft" CssClass="btnSubmit" Text="Save As Draft" OnClientClick="return false;" />
                        </div>
                    </div>
                </asp:Panel>
                <asp:Label runat="server" ID="lblmessage"></asp:Label>

                <asp:GridView ID="grdPriceRequest" CssClass="display compact" runat="server" AutoGenerateColumns="false">


                    <Columns>

                        <asp:TemplateField HeaderText="Item Code" HeaderStyle-Width="200px">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hdnItem" Value='<%#Bind("ItemNumber") %>' />
                                <asp:HiddenField runat="server" ID="hdnStockCode" />
                                <asp:DropDownList CssClass="ddl_item" ID="ddlitem" runat="server" Style="width: 200px!important;"></asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderWHS" Text="WHS" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtWHS" ReadOnly="true" Text='<%#Bind("WHS") %>' CssClass="ddl" runat="server" Style="width: 70px;"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderOrder" Text="Order Type" runat="server" CssClass="required"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlOrder" runat="server" CssClass="ddl" onchange="OrderChange(this);">
                                    <asp:ListItem Text="One Time" Value="onetime"></asp:ListItem>
                                    <asp:ListItem Text="Schedule" Value="schedule"></asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <%-- <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderOrder" Text="Order Frequency" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlFrequency" runat="server" CssClass="ddl">
                                    <asp:ListItem Text="Weekly" Value="weekly"></asp:ListItem>
                                    <asp:ListItem Text="Fortnightly" Value="fortnightly"></asp:ListItem>
                                    <asp:ListItem Text="Monthly" Value="monthly"></asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderAQTY" Text="Total QTY" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtTotQTY" Text='<%#Bind("Total_QTY") %>' onkeypress="return isNumberKey(event,this);" CssClass="ddl qty" runat="server" Style="width: 80px;"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderQTYPO" Text="QTY Per Order" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtQTYPO" Text='<%#Bind("QTYPO") %>' onkeypress="return isNumberKey(event,this);" CssClass="ddl" runat="server" Style="width: 80px;"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderDLP" Text="List Price" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtDLP" ReadOnly="true" Text='<%#Bind("DLP") %>' CssClass="ddl disabled" runat="server" Style="width: 80px;"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderAP" Text="Agreement Price" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtAP" ReadOnly="true" CssClass="ddl disabled" runat="server" Style="width: 80px;"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderTargetPrice" Text="Expected Price" runat="server" CssClass="required"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtTargetPrice" Text='<%#Bind("TargetPrice") %>' onkeypress="return isNumberKey(event,this);" onkeyup="calDCrate(event,this);" CssClass="ddl ep" runat="server" Style="width: 80px;"></asp:TextBox>
                                <asp:HiddenField ID="hdnAgreementPrice" runat="server" />
                                <asp:Label ID="lblPriceError" runat="server" Style="color: red;" Text=""></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderDCRate" Text="Discount Rate(%)" runat="server" CssClass="required"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtDCRate" Text='<%#Bind("DCRate") %>' onkeypress="return isNumberKey(event,this);" onkeyup="calExpectedPrice(event,this);" CssClass="ddl" runat="server" Style="width: 80px;"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderCustName" Text="Cust Name" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlCustName" runat="server" CssClass="ddl" Style="width: 200px;">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnCustomers" runat="server" Value='<%#Bind("CustName") %>' />
                                <asp:Label ID="lblCustError" runat="server" Style="color: red;" Text=""></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderCustSP" Text="Sales Price" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtCustSP" Text='<%#Bind("CustSP") %>' CssClass="ddl" Style="width: 80px;" onkeypress="return isNumberKey(event,this);" runat="server"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                      <%--  <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderCompanyName" Text="Competitor Company" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlCompanyName" runat="server" CssClass="ddl" Style="width: 200px;">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderDescription" Text="Competitor Product" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtDescription" Text='<%#Bind("Description") %>' Style="width: 200px;" CssClass="ddl" runat="server"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderCompanySP" Text="Competitor Sales Price" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtCompanySP" Text='<%#Bind("CompanySP") %>' onkeypress="return isNumberKey(event,this);" CssClass="ddl" Style="width: 80px;" runat="server"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label Text="Actions" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:ImageButton OnClientClick="return false;" CssClass="add-row" runat="server" Style="width: 25px;" ID="imgbtnAdd" ImageUrl="images/add-icon.jpeg" />
                                <%--<asp:ImageButton OnClientClick="return validateFields(event,this);" CssClass="add-row" CommandName="AddItem" CommandArgument='<%# Eval("ItemNumber") %>' runat="server" Style="width: 25px;" ID="imgbtnAdd" ImageUrl="images/add-icon.jpeg" />--%>
                                <%--<asp:ImageButton CommandName="Delete" OnClientClick="return Deletepopup(event,this);" runat="server" Style="width: 25px;" ID="imgbtnDel" ImageUrl="images/delete.png" CssClass="delbtn" />--%>
                                <asp:ImageButton OnClientClick="return false;" runat="server" Style="width: 25px;" ID="imgbtnDel" ImageUrl="images/delete.png" CssClass="delbtn" />

                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <div id="divOrders" runat="server" style="display: none;">
                    <div class="tabbed skin-turquoise round" id="skinable" style="margin-bottom: 10px;">
                        <ul>
                            <li id="freqList" runat="server" class="active" onclick="tabchange(this);">Frequently Inquired</li>
                            <li id="recentList" runat="server" onclick="tabchange(this);">Recently Inquired</li>
                        </ul>
                    </div>

                    <div class="col-md-12 nopad" id="divFrequent" runat="server" style="display: block">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 order" id="divItem1" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkItem1" runat="server" CssClass="lnk_item" OnClientClick="return false;">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item :<strong>
                                            <asp:Label ID="lblItem1" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4>
                                            <strong>
                                                <asp:Label ID="lblDesc1" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div></div>
                        <div class="col-md-2 order" id="divItem2" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkItem2" runat="server" CssClass="lnk_item" OnClientClick="return false;">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblItem2" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4>
                                            <strong>
                                                <asp:Label ID="lblDesc2" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-2 order" id="divItem3" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkItem3" runat="server" CssClass="lnk_item" OnClientClick="return false;">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblItem3" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4>
                                            <strong>
                                                <asp:Label ID="lblDesc3" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-2 order" id="divItem4" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkItem4" runat="server" CssClass="lnk_item" OnClientClick="return false;">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblItem4" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4>
                                            <strong>
                                                <asp:Label ID="lblDesc4" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-2 order" id="divItem5" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkItem5" runat="server" CssClass="lnk_item" OnClientClick="return false;">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblItem5" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4>
                                            <strong>
                                                <asp:Label ID="lblDesc5" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-1"></div>
                    </div>

                    <div class="col-md-12 nopad" id="divRecent" runat="server" style="display: none">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 order" id="divOItem1" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkOItem1" runat="server" CssClass="lnk_Ritem" OnClientClick="return false;">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblOItem1" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4>
                                            <strong>
                                                <asp:Label ID="lblODesc1" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div></div>
                        <div class="col-md-2 order" id="divOItem2" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkOItem2" runat="server" CssClass="lnk_Ritem" OnClientClick="return false;">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblOItem2" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4>
                                            <strong>
                                                <asp:Label ID="lblODesc2" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-2 order" id="divOItem3" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkOItem3" runat="server" CssClass="lnk_Ritem" OnClientClick="return false;">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblOItem3" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4><strong>
                                            <asp:Label ID="lblODesc3" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-2 order" id="divOItem4" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkOItem4" runat="server" CssClass="lnk_Ritem" OnClientClick="return false;">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblOItem4" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4><strong>
                                            <asp:Label ID="lblODesc4" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-2 order" id="divOItem5" style="cursor: pointer;">
                            <asp:LinkButton ID="lnkOItem5" runat="server" CssClass="lnk_Ritem" OnClientClick="return false;">
                                <div class="divorder">
                                    <div class="text-info mt-3">
                                        <h5>Item : <strong>
                                            <asp:Label ID="lblOItem5" runat="server"></asp:Label></strong></h5>
                                    </div>
                                    <div class="text-info mt-2">
                                        <h4><strong>
                                            <asp:Label ID="lblODesc5" runat="server"></asp:Label></strong></h4>
                                    </div>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>

            </ContentTemplate>
            <%--<Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>--%>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="updateProgress" runat="server">
            <ProgressTemplate>
                <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                    <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
        <%--    <img src="loader.gif" alt="" />--%>
            <img src="images/loader.gif" alt=""  />
        </div>
    </div>



</asp:Content>

