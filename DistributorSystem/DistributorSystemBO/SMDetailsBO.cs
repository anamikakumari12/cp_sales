﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DistributorSystemBO
{
    public class SMDetailsBO
    {
        public int ID  { get; set; }
        public string CP_Number { get; set; }
        public string CP_Name { get; set; }
        public string Customer_Number { get; set; }
        public string Customer_Name { get; set; }
        public string SM_NAME { get; set; }
        public string SM_MAIL_ID { get; set; }
        public int ERR_CODE { get; set; }
        public string ERR_MESSAGE { get; set; }
    }
}
