﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Disclaimer.aspx.cs" Inherits="DistributorSystem.Disclaimer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Disclaimer ::-Distributor System-::</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <style>
        .form-box {
            max-width: 70%;
            border: #e9cd40 solid 5px;
            padding: 10px;
            color: white;
            
    border-radius: 10px;
        }
        .btn-login {
            width: 10%;
            align-items: center;
            margin-left: 45%;
        }
    </style>
</head>
<body class="mn_background">
    <div class="form-box">
        
        <form id="Form1" action="#" runat="server">
            <p style="margin-left:15px;">Dear All,</p>
              <div><div class="col-md-1"><p style="margin: 0;float: right;">1.</p></div><div  class="col-md-11">New section 206C (1H) introduced in Income Tax Act wef 1st Oct 2020.</div></div>
              <div><div class="col-md-1"><p style="margin: 0;float: right;">2.</p></div><div class="col-md-11">If a seller has turnover over Rs. 10 crore in previous financial year (2019-20), then it is liable to collect 0.075% (please see point 5 below on TCS rate) on sale value exceeding Rs. 50 lakhs for each buyer.</div></div>
              <div><div class="col-md-1"><p style="margin: 0;float: right;">3.</p></div><div class="col-md-11">The buyer is responsible to make payment of TCS charged in the Invoice and can claim income tax credit at the end of the financial year.</div></div>
              <div><div class="col-md-1"><p style="margin: 0;float: right;">4.</p></div><div class="col-md-11">If the buyer does not quote his PAN/AADHAR number to the supplier, TCS will be collected @1%, instead of 0.075% applicable.</div></div>
              <div><div class="col-md-1"><p style="margin: 0;float: right;">5.</p></div><div class="col-md-11">Please note that full rate of TCS is 0.1%. However, the government has reduced the TCS rate to 0.075% till 31.3.2021.</div></div>
              <div><div class="col-md-1"><p style="margin: 0;float: right;">6.</p></div><div class="col-md-11">The tax is to be collected on Basic Sale Value + GST. Similarly, the value of purchase of each buyer to arrive at threshold of Rs. 50 lakhs will be Basic Sale Value+GST.</div></div>
              <div><div class="col-md-1"><p style="margin: 0;float: right;">7.</p></div><div class="col-md-11">In case of Credit Notes, the TCS amount will also get reduced. If a Credit Note is issued for a sales invoice where TCS was not applicable, the CRN will not have any TCS reversal.</div></div>
              <div><div class="col-md-1"><p style="margin: 0;float: right;">8.</p></div><div class="col-md-11">We will be charging TCS only for those CP’s/Customers whose Basic Sale Value+GST exceeds Rs.50 lakhs for period 1.4.2020 to 30.9.2020. We will have a review mechanism in place to ensure that we begin charging TCS for those customers who cross this threshold of Rs. 50 lakhs.</div></div>
              <div><div class="col-md-1"><p style="margin: 0;float: right;">9.</p></div><div class="col-md-11">TaeguTec India has made necessary changes in its Invoicing program to take care of this requirement.</div></div>
            
          <%--  1. New section 206C (1H) introduced in Income Tax Act wef 1st Oct 2020.<br />
            2. If a seller has turnover over Rs. 10 crore in previous financial year (2019-20), then it is liable to collect 0.075% (please see point 5 below on TCS rate) on sale value exceeding Rs. 50 lakhs for each buyer.<br />
            3. The buyer is responsible to make payment of TCS charged in the Invoice and can claim income tax credit at the end of the financial year.<br />
            4. If the buyer does not quote his PAN/AADHAR number to the supplier, TCS will be collected @1%, instead of 0.075% applicable.<br />
            5. Please note that full rate of TCS is 0.1%. However, the government has reduced the TCS rate to 0.075% till 31.3.2021.<br />
            6. The tax is to be collected on Basic Sale Value + GST. Similarly, the value of purchase of each buyer to arrive at threshold of Rs. 50 lakhs will be Basic Sale Value+GST.<br />
            7. In case of Credit Notes, the TCS amount will also get reduced. If a Credit Note is issued for a sales invoice where TCS was not applicable, the CRN will not have any TCS reversal.<br />
            8. We will be charging TCS only for those CP’s/Customers whose Basic Sale Value+GST exceeds Rs.50 lakhs for period 1.4.2020 to 30.9.2020. We will have a review mechanism in place to ensure that we begin charging TCS for those customers who cross this threshold of Rs. 50 lakhs.<br />
            9. TaeguTec India has made necessary changes in its Invoicing program to take care of this requirement.<br />
            <br />--%>
            <br />
            <p  style="margin-left:15px;">This is for your kind information and necessary action.</p>

  <br />
            <br />

            <p  style="margin-left:15px;"><b>Any clarification please speak to TT Logistics Team</b></p>
            <asp:Button ValidationGroup="VGLogin" runat="server" ID="btnOk" CssClass="btn_Login" Text="OK" OnClick="btnOk_Click" style="width: 10%;margin-left: 45%;" />
        </form>

    </div>

    <div class="footer mn_footer">
        <p>&copy; 2017 Distributor System. All Rights Reserved | Designed by <a href="#" target="_blank">KNS Technologies</a></p>
    </div>
</body>
</html>
