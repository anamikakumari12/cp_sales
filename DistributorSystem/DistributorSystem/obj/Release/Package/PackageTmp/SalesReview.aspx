﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SalesReview.aspx.cs" Inherits="DistributorSystem.SalesReview" MasterPageFile="~/MasterPage.Master" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

   <style>
        body {
            font-family: 90%/1.45em "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif;
        }

        .mn_margin {
            margin: 10px 0 0 0;
        }

        .mn_bott {
            margin: 0px 0 10px 0;
        }

        .btn-default.btn-on-2.active {
            background-color: #006681;
            color: white;
        }

        .btn-default.btn-off-2.active {
            background-color: #006681;
            color: white;
        }

        #MainContent_Panel1 {
            margin: 0 0 12px 0;
            width: 100%;
            float: left;
        }
         .amcharts-chart-div a{
            display:none!important
        }
       #ChartSalesReview
       {
           height:500px;
           width:100%;
       }
    </style>

     <script type="text/javascript" class="init">
         $(document).ready(function () {
             var head_contentvalues = $('#MainContent_grdSalesReviewValues tr:first').html();
             $('#MainContent_grdSalesReviewValues').prepend('<thead></thead>')
             $('#MainContent_grdSalesReviewValues thead').html('<tr>' + head_contentvalues + '</tr>');
             $('#MainContent_grdSalesReviewValues tbody tr:first').hide();
             $('#MainContent_grdSalesReviewValues').DataTable({
                 "paging": false,
                 "ordering": false,
                 "searching": false,
                 "bInfo": false
             });           
         });

         function LoadChartSalesReview() {
             var tmp = null;
             $.ajax({
                 type: "POST",
                 url: 'SalesReview.aspx/LoadChartSalesReview',
                 data: "",
                 contentType: "application/json; charset=utf-8",
                 dataType: "json",
                 success: function (msg) {
                     console.log("msg : " + msg);
                     tmp = msg.d;
                     console.log("tmp1 : " + tmp);
                     var d = new Date();
                     var chart = AmCharts.makeChart("ChartSalesReview", {
                         "type": "serial",
                         "theme": "light",
                         "dataProvider": tmp,
                         "valueAxes": [{
                             "gridColor": "#FFFFFF",
                             "gridAlpha": 0.2,
                             "dashLength": 0
                         }],
                         "gridAboveGraphs": true,
                         "startDuration": 1,
                         "graphs": [{
                             "title": "YTD TARGET " + d.getFullYear(),
                             "balloonText": "[[title]]: <b>[[value]]</b>",
                             "bullet": "round",
                             "bulletSize": 10,
                             "bulletBorderColor": "#ffffff",
                             "bulletBorderAlpha": 1,
                             "bulletBorderThickness": 2,
                             "valueField": "YTD TARGET " + d.getFullYear()
                         },
                         {
                             "title": "YTD SALE " + d.getFullYear(),
                             "balloonText": "[[title]]: <b>[[value]]</b>",
                             "bullet": "round",
                             "bulletSize": 10,
                             "bulletBorderColor": "#ffffff",
                             "bulletBorderAlpha": 1,
                             "bulletBorderThickness": 2,
                             "valueField": "YTD SALE "+d.getFullYear()
                         }],
                         "chartCursor": {
                             "categoryBalloonEnabled": false,
                             "cursorAlpha": 0,
                             "zoomable": false
                         },
                         "categoryField": "HEADING",
                         "categoryAxis": {
                             "gridPosition": "start",
                             "gridAlpha": 0
                         },
                         "legend": {}
                     });
                     chart.dataProvider = AmCharts.parseJSON(tmp);
                     chart.validateData();
                 },
                 error: function (e) {
                     //console(e);
                 }
             });
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>
     <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Review</a>
                        </li>
                        <li class="current">Sales Review</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <%--<asp:UpdatePanel runat="server">
        <ContentTemplate>--%>

    <div class="col-md-12 mn_margin">
        <asp:Panel ID="Panel1" runat="server" CssClass="filter_panel">
            <div class="col-md-2" style="width: 115px; margin: 6px 0 0; padding: 0 0 0 10px;">
                <asp:Label runat="server" ID="lblcustname" Text="Customer Name"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="ddlCustomerList" OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" Width="150px" Height="30px" runat="server">
                </asp:DropDownList>
            </div>
             <div class="col-md-2" style="width: 115px; margin: 6px 0 0; padding: 0 0 0 10px;">
                <asp:Label runat="server" ID="lblSalesMan" Text="Sales Man"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="ddlSalesman" OnSelectedIndexChanged="ddlSalesman_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" Width="150px" Height="30px" runat="server">
                </asp:DropDownList>
            </div>
        </asp:Panel>
        <div>
            <asp:Label runat="server" ID="lblResult"></asp:Label>
        </div>
        <asp:Panel ID="panelvalues" runat="server">
            <div id="Divvalues" class="pull-right">
                <div class="btn-group">
                    <%-- data-toggle="buttons">--%>
                    <asp:Button ID="Btn_Units" runat="server" class="btn btn-default btn-off-2 btn-sm" Text="Val In Units" OnClick="Btn_Units_Click"/>
                    <asp:Button ID="Btn_Thousand" runat="server" class="btn btn-default btn-off-2 btn-sm active" Text="Val In '000" OnClick="Btn_Thousand_Click" />
                    <asp:Button ID="Btn_Lakhs" runat="server" class="btn btn-default btn-off-2 btn-sm" Text="Val In Lakhs" OnClick="Btn_Lakhs_Click" />
                </div>
            </div>
        </asp:Panel>
    </div>

    <asp:Panel runat="server" ID="pnlData">
        <div runat="server" id="divgridchartValues" visible="false">
            <asp:GridView ID="grdSalesReviewValues" runat="server" ViewStateMode="Enabled" CssClass="display compact" AutoGenerateColumns="False" ShowHeader="false">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblHeading"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("HEADING") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblHeading" Text="JAN"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lbljan" Text='<%# Eval("JAN") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblHeading" Text="FEB"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblfeb" Text='<%# Eval("FEB") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblHeading" Text="MAR"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblmar" Text='<%# Eval("MAR") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblHeading" Text="APR"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblapr" Text='<%# Eval("APR") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblHeading" Text="MAY"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblmay" Text='<%# Eval("MAY") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblHeading" Text="JUN"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lbljun" Text='<%# Eval("JUN") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblHeading" Text="JUL"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lbljul" Text='<%# Eval("JUL") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblHeading" Text="AUG"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblAug" Text='<%# Eval("AUG") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblHeading" Text="SEP"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblsep" Text='<%# Eval("SEP") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblHeading" Text="OCT"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lbloct" Text='<%# Eval("OCT") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblHeading" Text="NOV"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblnov" Text='<%# Eval("NOV") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblHeading" Text="DEC"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lbldec" Text='<%# Eval("DEC") %>'></asp:Label>
<%--<asp:Label runat="server" ID="lblFlag" Text='<%# Eval("flag") %>' Visible="false"></asp:Label>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

            <div style="float: left; padding: 10px; width: 100%;" class="col-md-12">
                <div id="ChartSalesReview"></div>
               <%-- <asp:Chart ID="ChartSalesReview" runat="server" Width="1307px" Visible="False" ViewStateMode="Enabled" Style="max-width: 100% !important" BackColor="#E1E1E1">
                    <Titles>
                        <asp:Title Text="Summary of Sales Budget Monthly "></asp:Title>
                    </Titles>
                    <Legends>
                        <asp:Legend Alignment="Center" Docking="Top" IsTextAutoFit="true" Name="Legend2" LegendStyle="Row" />
                    </Legends>
                    <Series>
                        <asp:Series Name="YTD SALE" ShadowOffset="1" Enabled="True" LabelForeColor="White" LabelAngle="-90"
                            CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column">
                        </asp:Series>
                        <asp:Series Name="YTD PLAN" ShadowOffset="1" ChartType="Line" LabelBorderWidth="1"></asp:Series>
                        <asp:Series Name="Series4" IsValueShownAsLabel="True" ShadowOffset="1" ChartType="Point"></asp:Series>
                        <asp:Series Name="YTD SALE PREVIOUS YEAR" ShadowOffset="1" ChartType="Line"></asp:Series>
                        <asp:Series Name="Series5" IsValueShownAsLabel="True" ShadowOffset="1" ChartType="Point"></asp:Series>
                    </Series>

                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1" BackColor="#ACD1E9">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
                                <MajorGrid LineWidth="0" />
                                <LabelStyle Font="Verdana, 8.25pt" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>--%>

            </div>

        </div>
    </asp:Panel>

    
       <%-- </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlCustomerList" EventName="SelectedIndexChanged" />
            <asp:PostBackTrigger ControlID="Btn_Units"/>
            <asp:PostBackTrigger ControlID="Btn_Thousand"/>
            <asp:PostBackTrigger ControlID="Btn_Lakhs"/>
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>
