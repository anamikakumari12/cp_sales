USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_getAgreementPrice]    Script Date: 09-11-2020 17:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec sp_getAgreementPrice null,'1624','5613753'
ALTER PROCEDURE [dbo].[sp_getAgreementPrice](@CustNumber VARCHAR(100)=null, @CP_Number VARCHAR(100)=null, @item VARCHAR(100))
AS
BEGIN
	
	IF(@CP_Number is not null AND @CP_Number<>'')
	SET @CustNumber=@CP_Number
	SELECT dl.DLKEY,ii.IDSCO,dl.DLVUL AgreementPrice,DLBRF,dl.DLTEFF, dl.DLTEFT
--ii.ICS, ii.IC1,dl.DLKEY,ii.IDSCO,cc.CRGN, cc.CGRP,cc.CTYP,dn.DNCUST, cc.CNME1,dn.DNDNO, dl.DLBRF, dl.DLVUL,dc.DCDSC,dc.DCTEFF, dc.DCTEFT, dn.DNTUPD, dn.DNUSRU,cc.CWHS, dl.DLTEFF, dl.DLTEFT, ii.IVTV
FROM 
	tt_GALF6TA_II ii JOIN tt_GALF6TA_DL dl ON ii.ICAT=dl.DLKEY
	JOIN tt_GALF6TA_DN dn ON dn.DNDNO=dl.DLNO
	JOIN tt_GALF6TA_DC dc ON dc.DCNO=dn.DNDNO
	JOIN tt_customer_discountgroup_mapping cc ON cc.CUST=dn.DNCUST
WHERE ii.ICS IN (1,3,6,8)
	AND ii.IC14='Y'
	AND CTYP IN ('C','D')
	AND DNCUST=@CustNumber
	AND DLKEY=@item
	--AND CONVERT(FLOAT,DLBRF)<=@Quantity
	--AND CONVERT(FLOAT,DLBRF)>0.00
	AND DLTEFT>=CONVERT(INT, CONCAT(YEAR(GETDATE()),RIGHT('0' + RTRIM(MONTH(GETDATE())), 2), RIGHT('0' + RTRIM(DAY(GETDATE())), 2)))
	ORDER BY DLBRF

END

