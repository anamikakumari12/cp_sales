﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SalesDashboard.aspx.cs" Inherits="DistributorSystem.SalesDashboard" MasterPageFile="~/MasterPage.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/radar.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    <script type="text/javascript">
        function LoadCharts() {
            LoadChartHistoricalSales();
            LoadChartConsolidatedSalesView();
            LoadChartSalesByCustomer();
            LoadChartSalesByMonth();
        }
        function LoadChartHistoricalSales() {
            var tmp = null;
            $.ajax({
                type: "POST",
                url: 'SalesDashboard.aspx/LoadChartHistoricalSales',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];
                    var chart = AmCharts.makeChart("ChartHistoricalSales", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "gridColor": "#FFFFFF",
                            "gridAlpha": 0,
                            "dashLength": 0
                        }],
                        "gridAboveGraphs": true,
                        "startDuration": 1,
                        "graphs": [{
                            "type": "column",
                            "title": "value",
                            "balloonText": "[[title]]: <b>[[value]]</b>",
                            "bullet": "round",
                            "bulletSize": 10,
                            "bulletBorderColor": "#ffffff",
                            "bulletBorderAlpha": 1,
                            "bulletBorderThickness": 2,
                            "fillAlphas": 0.8,
                            "lineAlpha": 0.2,
                            "fillColorsField": "color",
                            "valueField": "value"
                        }],
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "Targets",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "gridAlpha": 0
                        },
                        "legend": {}
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartConsolidatedSalesView() {
            var tmp = null;
            $.ajax({
                type: "POST",
                url: 'SalesDashboard.aspx/LoadChartConsolidatedSalesView',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];
                    var chart = AmCharts.makeChart("ChartConsolidatedSalesView", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "gridColor": "#FFFFFF",
                            "gridAlpha": 0,
                            "dashLength": 0
                        }],
                        "gridAboveGraphs": true,
                        "startDuration": 1,
                        "graphs": [{
                            "type": "column",
                            "title": "value",
                            "balloonText": "[[title]]: <b>[[value]]</b>",
                            "bullet": "round",
                            "bulletSize": 10,
                            "bulletBorderColor": "#ffffff",
                            "bulletBorderAlpha": 1,
                            "bulletBorderThickness": 2,
                            "fillAlphas": 0.8,
                            "lineAlpha": 0.2,
                            "fillColorsField": "color",
                            "valueField": "value"
                        }],
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "Targets",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "gridAlpha": 0
                        },
                        "legend": {}
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartSalesByCustomer() {
            var tmp = null;
            $.ajax({
                type: "POST",
                url: 'SalesDashboard.aspx/LoadChartSalesByCustomer',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];
                    var chart = AmCharts.makeChart("ChartSalesByCustomer", {
                        "type": "pie",
                        "theme": "light",
                        "dataProvider": tmp,
                        "valueField": "value",
                        "titleField": "Customer_Name",
                        "balloon": {
                            "fixedPosition": true
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartSalesByMonth() {
            var tmp = null;
            $.ajax({
                type: "POST",
                url: 'SalesDashboard.aspx/LoadChartSalesByMonth',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];
                    var chart = AmCharts.makeChart("ChartSalesByMonth", {
                        "type": "pie",
                        "theme": "light",
                        "dataProvider": tmp,
                        "valueField": "value",
                        "titleField": "Month",
                        "balloon": {
                            "fixedPosition": true
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
    </script>
    <style>
        #ChartHistoricalSales
        {
            height:500px;
            width:50%;
        }
        #ChartConsolidatedSalesView{
            height:500px;
            width:50%;
        }
        #ChartSalesByCustomer{
            height:500px;
            width:50%;
        }
        #ChartSalesByMonth{
            height:500px;
            width:50%;
        }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-md-12 mn_margin">
        <div>
        <div class="col-md-6" id="ChartHistoricalSales"></div>
        <div class="col-md-6" id="ChartConsolidatedSalesView"></div></div>
        <div>
        <div class="col-md-6" id="ChartSalesByCustomer"></div>
        <div class="col-md-6" id="ChartSalesByMonth"></div></div>
        </div>
    </asp:Content>
