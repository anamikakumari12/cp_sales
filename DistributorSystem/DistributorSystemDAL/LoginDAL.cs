﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DistributorSystemBO;

namespace DistributorSystemDAL
{
    public class LoginDAL
    {

        CommonFunctions objCom = new CommonFunctions();
        public LoginBO authLoginDAL(LoginBO objLoginBO)
        {
            LoginBO objOutPut = new LoginBO();
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection con = new SqlConnection(connstring);
            SqlDataAdapter sqlda;
            DataTable dtoutput = new DataTable();
            try
            {
                con.Open();
                SqlCommand command = new SqlCommand(ResourceFileDAL.sp_getLoginForDistributors, con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(ResourceFileDAL.UserID, SqlDbType.VarChar, 500).Value = objLoginBO.Username;
                command.Parameters.Add(ResourceFileDAL.Password, SqlDbType.VarChar, 1000).Value = objLoginBO.Password;
                sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dtoutput);

                if (dtoutput.Rows.Count > 0)
                {
                    objOutPut.EngineerId = Convert.ToString(dtoutput.Rows[0]["Id"]);
                    objOutPut.UserFullName = Convert.ToString(dtoutput.Rows[0]["Employee_name"]);
                    objOutPut.LoginMailID = Convert.ToString(dtoutput.Rows[0]["Email_id"]);
                    objOutPut.Distributor_number = Convert.ToString(dtoutput.Rows[0]["Distributor_number"]);
                    objOutPut.ErrorMessege = Convert.ToString(dtoutput.Rows[0]["error_msg"]);
                    objOutPut.ErrorNum = Convert.ToInt32(dtoutput.Rows[0]["error_code"]);
                    objOutPut.Distributor_name = Convert.ToString(dtoutput.Rows[0]["Distributor_name"]);
                    objOutPut.Password = Convert.ToString(dtoutput.Rows[0]["Password"]);
                    objOutPut.Role = Convert.ToString(dtoutput.Rows[0]["Role"]);
                    objOutPut.Quote_Flag = Convert.ToInt32(dtoutput.Rows[0]["Quote_Flag"]);
                    objOutPut.Price_Flag = Convert.ToInt32(dtoutput.Rows[0]["Price_Flag"]);
                    objOutPut.cter = Convert.ToString(dtoutput.Rows[0]["cter"]);
                }
                
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
                objOutPut.ErrorMessege = ex.Message;
                objOutPut.ErrorNum = 1;
            }
            finally
            {
                con.Close();
            }
            return objOutPut;
        }

        public LoginBO ChangePasswordDAL(LoginBO objLoginBO)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda;
            DataTable dtoutput = new DataTable();
            LoginBO objOutPut = new LoginBO();
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(ResourceFileDAL.ResetUserPasswordForDistributor, connection);
                command.CommandType = CommandType.StoredProcedure;


                command.Parameters.Add(ResourceFileDAL.UserID, SqlDbType.VarChar, 100).Value = objLoginBO.Username;
                command.Parameters.Add(ResourceFileDAL.Password, SqlDbType.VarChar, 1000).Value = objLoginBO.Password;
                command.Parameters.Add(ResourceFileDAL.Generated_Password, SqlDbType.VarChar, 1000).Value = objLoginBO.genrated_password;
                sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dtoutput);

                if (dtoutput.Rows.Count > 0)
                {
                    objOutPut.UserFullName = Convert.ToString(dtoutput.Rows[0]["Employee_name"]);
                    objOutPut.LoginMailID = Convert.ToString(dtoutput.Rows[0]["Email_id"]);
                    objOutPut.Distributor_number = Convert.ToString(dtoutput.Rows[0]["Distributor_number"]);
                    objOutPut.ErrorMessege = Convert.ToString(dtoutput.Rows[0]["error_msg"]);
                    objOutPut.ErrorNum = Convert.ToInt32(dtoutput.Rows[0]["error_code"]);
                    objOutPut.Distributor_name = Convert.ToString(dtoutput.Rows[0]["Distributor_name"]);
                    objOutPut.Password = Convert.ToString(dtoutput.Rows[0]["Password"]);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return objOutPut;
        }

        public DataTable GetAllMenusDAL(LoginBO objLoginBO)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda;
            DataTable dtoutput = new DataTable();
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(ResourceFileDAL.sp_getMenus, connection);
                command.CommandType = CommandType.StoredProcedure;


                command.Parameters.Add(ResourceFileDAL.Distributor_number, SqlDbType.VarChar, 100).Value = objLoginBO.Username;
                sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dtoutput);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtoutput;
        }
    }
}
