﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DistributorSystemDAL;
using DistributorSystemBO;

namespace DistributorSystemBL
{
    public class QuoteBL
    {
        QuoteDAL objDAL = new QuoteDAL();
        public DataTable GetItemDescBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetItemDescDAL(objQuoteBO);
        }

        public DataTable GetFrequentItemsBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetFrequentItemsDAL(objQuoteBO);
        }

        public DataTable GetCustomerListBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetCustomerListDAL(objQuoteBO);
        }

        public DataTable GetRecentItemsBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetRecentItemsDAL(objQuoteBO);
        }

        public DataTable GetItemDetailsBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetItemDetailsDAL(objQuoteBO);
        }

        public string DeleteQuotes(string itemcode)
        {
            return objDAL.DeleteQuotesDAL(itemcode);
        }
        public QuoteBO SaveQuotesBL(DataTable dt)
        {
            return objDAL.SaveQuotesDAL(dt);
        }

        public DataTable GetQuoteDetailsBL(QuoteBO objquoteBO)
        {
            return objDAL.GetQuoteDetailsDAL(objquoteBO);
        }
        public DataTable GetQuoteSummaryBL(QuoteBO objquoteBO)
        {
            return objDAL.GetQuoteSummaryDAL(objquoteBO);
        }

        public DataTable GetCompetitorsBL()
        {
            return objDAL.GetCompetitorsDAL();
        }

        public QuoteBO PlaceOrderBL(QuoteBO objBO)
        {
            return objDAL.PlaceOrderDAL(objBO);
        }

        public QuoteBO RequestForReApprovalBL(DataTable dtQuote)
        {
            return objDAL.RequestForReApprovalDAL(dtQuote);
        }

        public DataTable getQuoteFormatBL(QuoteBO objQuoteBO)
        {
            return objDAL.getQuoteFormatDAL(objQuoteBO);
        }

        public DataTable getQuotePOFormatBL(QuoteBO objQuoteBO)
        {
            return objDAL.getQuotePOFormatDAL(objQuoteBO);
        }

        public DataTable GetAgreementPriceBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetAgreementPriceDAL(objQuoteBO);
        }

        public QuoteBO DraftQuotesBL(DataTable dt)
        {
            return objDAL.DraftQuotesDAL(dt);
        }

        public DataTable GetDraftedQuoteBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetDraftedQuoteDAL(objQuoteBO);
        }

        public DataTable GetQuoteStatusLogBL(QuoteBO objBO)
        {
            return objDAL.GetQuoteStatusLogDAL(objBO);
        }

        public DataTable GetAPListBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetAPListDAL(objQuoteBO);
        }
    }
}
