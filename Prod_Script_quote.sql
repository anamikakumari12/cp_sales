USE [Taegutec_Sales_Budget]
GO
/****** Object:  Table [dbo].[tt_Competitor]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP TABLE [dbo].[tt_Competitor]
GO
/****** Object:  Table [dbo].[tbl_QuoteStatusLog]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP TABLE [dbo].[tbl_QuoteStatusLog]
GO
/****** Object:  Table [dbo].[tbl_QuoteOrder]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP TABLE [dbo].[tbl_QuoteOrder]
GO
/****** Object:  Table [dbo].[tbl_DeskLogin]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP TABLE [dbo].[tbl_DeskLogin]
GO
/****** Object:  Table [dbo].[TBL_CONFIGURATION]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP TABLE [dbo].[TBL_CONFIGURATION]
GO
/****** Object:  Table [dbo].[Quote_ref]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP TABLE [dbo].[Quote_ref]
GO
/****** Object:  Table [dbo].[MailConfiguration]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP TABLE [dbo].[MailConfiguration]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetOrderFlag]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP FUNCTION [dbo].[fn_GetOrderFlag]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetOrderCount]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP FUNCTION [dbo].[fn_GetOrderCount]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_getMultiCustomers]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP FUNCTION [dbo].[fn_getMultiCustomers]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_getDiscountPrice]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP FUNCTION [dbo].[fn_getDiscountPrice]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_getAssignedTo]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP FUNCTION [dbo].[fn_getAssignedTo]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_getAgreementPrice]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP FUNCTION [dbo].[fn_getAgreementPrice]
GO
/****** Object:  StoredProcedure [dbo].[sp_saveQuotes]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_saveQuotes]
GO
/****** Object:  StoredProcedure [dbo].[sp_quoteStatusChange]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_quoteStatusChange]
GO
/****** Object:  StoredProcedure [dbo].[sp_PlaceOrderForQuote]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_PlaceOrderForQuote]
GO
/****** Object:  StoredProcedure [dbo].[sp_getRecentlyOrderedItem]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_getRecentlyOrderedItem]
GO
/****** Object:  StoredProcedure [dbo].[sp_getQuoteSummaryDesk]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_getQuoteSummaryDesk]
GO
/****** Object:  StoredProcedure [dbo].[sp_getQuoteSummary]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_getQuoteSummary]
GO
/****** Object:  StoredProcedure [dbo].[sp_getQuotePO]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_getQuotePO]
GO
/****** Object:  StoredProcedure [dbo].[sp_getQuoteFormat]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_getQuoteFormat]
GO
/****** Object:  StoredProcedure [dbo].[sp_getQuoteDetails]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_getQuoteDetails]
GO
/****** Object:  StoredProcedure [dbo].[sp_getPendingQuotes]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_getPendingQuotes]
GO
/****** Object:  StoredProcedure [dbo].[sp_getMailDetailsForQuote]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_getMailDetailsForQuote]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetItemDetailsForQuote]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_GetItemDetailsForQuote]
GO
/****** Object:  StoredProcedure [dbo].[sp_getItemDesc]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_getItemDesc]
GO
/****** Object:  StoredProcedure [dbo].[sp_getFrequentlyOrderedItem]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_getFrequentlyOrderedItem]
GO
/****** Object:  StoredProcedure [dbo].[sp_getEscalatedQuotes]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_getEscalatedQuotes]
GO
/****** Object:  StoredProcedure [dbo].[sp_getCustomers_Quote]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_getCustomers_Quote]
GO
/****** Object:  StoredProcedure [dbo].[sp_getCompetitorsForQuote]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP PROCEDURE [dbo].[sp_getCompetitorsForQuote]
GO
/****** Object:  UserDefinedTableType [dbo].[QuoteTableType]    Script Date: 11/14/2019 12:27:13 AM ******/
DROP TYPE [dbo].[QuoteTableType]
GO
/****** Object:  StoredProcedure [dbo].[sp_getLoginForDistributors]    Script Date: 11/14/2019 1:18:31 AM ******/
DROP PROCEDURE [dbo].[sp_getLoginForDistributors]
GO
/****** Object:  StoredProcedure [dbo].[sp_deskLogin]    Script Date: 11/14/2019 1:18:31 AM ******/
DROP PROCEDURE [dbo].[sp_deskLogin]
GO
/****** Object:  UserDefinedTableType [dbo].[QuoteTableType]    Script Date: 11/14/2019 12:27:13 AM ******/
CREATE TYPE [dbo].[QuoteTableType] AS TABLE(
	[Ref_number] [varchar](100) NULL,
	[Item_code] [varchar](100) NULL,
	[Item_Desc] [varchar](max) NULL,
	[WHS] [varchar](100) NULL,
	[Order_type] [varchar](100) NULL,
	[Order_frequency] [varchar](max) NULL,
	[Total_Qty] [varchar](100) NULL,
	[QTY_perOrder] [varchar](100) NULL,
	[List_Price] [varchar](100) NULL,
	[Expected_price] [varchar](100) NULL,
	[DC_rate] [varchar](100) NULL,
	[Cust_number] [varchar](100) NULL,
	[Cust_Name] [varchar](max) NULL,
	[Cust_SP] [varchar](100) NULL,
	[Comp_Name] [varchar](max) NULL,
	[Comp_Desc] [varchar](max) NULL,
	[Comp_SP] [varchar](100) NULL,
	[CP_number] [varchar](100) NULL,
	[CP_Name] [varchar](100) NULL,
	[Status] [varchar](100) NULL,
	[RequestedBy] [varchar](100) NULL,
	[RequestedBy_Flag] [varchar](100) NULL
)
GO
/****** Object:  StoredProcedure [dbo].[sp_getCompetitorsForQuote]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_getCompetitorsForQuote]
AS
BEGIN
	SELECT * FROM tt_Competitor
END

GO
/****** Object:  StoredProcedure [dbo].[sp_getCustomers_Quote]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec sp_getCustomers_Quote null,'HO',null,'HYD','TTA',null,null
CREATE PROCEDURE [dbo].[sp_getCustomers_Quote] 
(@assigned_salesengineer_id varchar(10)=null,
 @roleId varchar(50),
 @custtype varchar(10)=null,
 @branchcode varchar(10)=null,
 @cter varchar(10)=null,
 @customer_class varchar(10)=null,
 @territory_engineer_id varchar(10)=null
 )
AS
BEGIN
	if(@roleId = 'SE')
	BEGIN

		Select customer_number, customer_short_name from  tt_customer_master 
		WHERE ( @assigned_salesengineer_id='ALL' OR @assigned_salesengineer_id IS NULL OR assigned_salesengineer_id=@assigned_salesengineer_id)
		--AND customer_type=ISNULL(@custtype,customer_type)
		AND customer_type='C'
		AND @customer_class IS NULL OR (customer_class=@customer_class)
		order by customer_short_name

	RETURN
	END
	else if(@roleId = 'BM')
	BEGIN
		Select customer_number, customer_short_name from  tt_customer_master 
		 WHERE ( @assigned_salesengineer_id='ALL'OR @assigned_salesengineer_id IS NULL OR assigned_salesengineer_id=@assigned_salesengineer_id)
		AND ( @branchcode='ALL' OR Customer_region=@branchcode)
		AND customer_type=ISNULL(@custtype,customer_type)
		--AND customer_type='C'
		AND @customer_class IS NULL OR (customer_class=@customer_class)
		order by customer_short_name
	RETURN
	END
	else if(@roleId = 'HO')
	BEGIN 
		Select customer_number, customer_short_name from  tt_customer_master 
		WHERE 
		( @assigned_salesengineer_id='ALL'OR @assigned_salesengineer_id IS NULL OR assigned_salesengineer_id=@assigned_salesengineer_id)
	    AND ( @branchcode='ALL' OR Customer_region=@branchcode)
		AND customer_type=ISNULL(@custtype,customer_type) 
		AND cter =ISNULL(@cter,cter)
		AND @customer_class IS NULL OR (customer_class=@customer_class)
		
		order by customer_short_name
	RETURN
	END
	else if(@roleId = 'TM')
	BEGIN
		select customer_number, customer_short_name from tt_customer_master where territory_engineer_id=@territory_engineer_id
		AND ( @assigned_salesengineer_id='ALL'OR @assigned_salesengineer_id IS NULL OR assigned_salesengineer_id=@assigned_salesengineer_id)
		AND ( @branchcode='ALL' OR @branchcode IS NULL OR Customer_region=@branchcode)
		AND cter =ISNULL(@cter,cter)
		AND @customer_class IS NULL OR (customer_class=@customer_class)
	RETURN
	END
	
	
RETURN
END









GO
/****** Object:  StoredProcedure [dbo].[sp_getEscalatedQuotes]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_getEscalatedQuotes]
(
@StartDate VARCHAR(100)=null,
@EndDate VARCHAR(100)=null,
@role VARCHAR(10),
@cter VARCHAR(10)=null,
@assigned_salesengineer_id VARCHAR(10)=null
)
AS
BEGIN

IF(@StartDate='')
SET @StartDate = null
IF(@EndDate='')
SET @EndDate = null
--if(@role='Desk')
--	SELECT * FROM Quote_ref 
--	WHERE Status IN ('Escalated') AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
--	AND UPPER(Assigned_To)=UPPER(@role)
--else 
if(@role='HO')
	SELECT *, Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy 
	,(Select StatusChange_Comment from tbl_QuoteStatusLog WHERE ID=(Select MAX(ID) from tbl_QuoteStatusLog WHERE Quote_ID=t.ID )) 'StatusChange_Comment'
	FROM Quote_ref t
	WHERE 
		Status IN ('Escalated By BM') AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
		AND (CP_number IN (Select customer_number from tt_customer_master WHERE cter=ISNULL(@cter,'TTA')) 
			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE cter=ISNULL(@cter,'TTA'))) 
		AND UPPER(Assigned_To)=UPPER(@role)
else if(@role='BM')
	
	SELECT *, Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy
	,(Select StatusChange_Comment from tbl_QuoteStatusLog WHERE ID=(Select MAX(ID) from tbl_QuoteStatusLog WHERE Quote_ID=t.ID )) 'StatusChange_Comment'
	 FROM Quote_ref t
	WHERE 
		Status IN ('Escalated')
		AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
		AND (CP_number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select BranchCode from LoginInfo where EngineerId=@assigned_salesengineer_id)) 
			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select BranchCode from LoginInfo where EngineerId=@assigned_salesengineer_id) )) 
		AND UPPER(Assigned_To)=UPPER(@role)

ELSE IF (@role='TM')

	SELECT *, Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy
	,(Select StatusChange_Comment from tbl_QuoteStatusLog WHERE ID=(Select MAX(ID) from tbl_QuoteStatusLog WHERE Quote_ID=t.ID )) 'StatusChange_Comment'
	FROM Quote_ref t
	WHERE 
		Status IN ('Escalated')
		AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
		AND (CP_number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select region_code from tt_territory_region_relation where territory_engineer_id=@assigned_salesengineer_id)) 
			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select region_code from tt_territory_region_relation where territory_engineer_id=@assigned_salesengineer_id) )) 
		AND UPPER(Assigned_To)=UPPER('BM')

END




GO
/****** Object:  StoredProcedure [dbo].[sp_getFrequentlyOrderedItem]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec sp_getFrequentlyOrderedItem 'DISTRIBUTOR','1037'

CREATE PROCEDURE [dbo].[sp_getFrequentlyOrderedItem]
(
@flag VARCHAR(100),
@Requested_By VARCHAR(100)
)
AS
BEGIN
	IF(@flag='DISTRIBUTOR')
	BEGIN
	  SELECT t1.Item_Code 'item', t2.Item_desc 'item_desc', t2.Ref_number, t2.WHS, t2.Total_QTY, t2.Order_frequency,t2.QTY_perOrder, t2.Order_type, t2.List_Price,t2.Expected_price,t2.DC_rate, t2.Cust_number,t2.Cust_SP,t2.Comp_Name,t2.Comp_Desc, t2.Comp_SP FROM (
	SELECT  Item_Code, Item_Desc, MAX(ID) c_id FROM Quote_ref 
		WHERE Item_code IN (SELECT top 5 Item_code FROM (
			SELECT  Item_code, COUNT(*) c_count FROM Quote_ref WHERE CP_number=@Requested_By GROUP BY Item_code) t
		ORDER BY c_count DESC) 
		AND CP_number=@Requested_By
		GROUP BY Item_Code, Item_Desc) t1
	JOIN Quote_ref t2 ON t1.Item_Code=t2.Item_Code AND t1.c_id=t2.ID AND t2.CP_number=@Requested_By
	
	END
	ELSE
	BEGIN

	 SELECT t1.Item_Code 'item', t2.Item_desc 'item_desc', t2.Ref_number, t2.WHS, t2.Total_QTY, t2.Order_frequency,t2.QTY_perOrder, t2.Order_type, t2.List_Price,t2.Expected_price,t2.DC_rate, t2.Cust_number,t2.Cust_SP,t2.Comp_Name,t2.Comp_Desc, t2.Comp_SP FROM (
	SELECT  Item_Code, Item_Desc, MAX(ID) c_id FROM Quote_ref 
		WHERE Item_code IN (SELECT top 5 Item_code FROM (
			SELECT  Item_code, COUNT(*) c_count FROM Quote_ref WHERE Cust_number=@Requested_By GROUP BY Item_code) t
		ORDER BY c_count DESC) 
		AND Cust_number=@Requested_By
		GROUP BY Item_Code, Item_Desc) t1
	JOIN Quote_ref t2 ON t1.Item_Code=t2.Item_Code AND t1.c_id=t2.ID AND t2.Cust_number=@Requested_By

		
	END
END



GO
/****** Object:  StoredProcedure [dbo].[sp_getItemDesc]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_getItemDesc]
(
@CustNumber VARCHAR(100)
)
AS
BEGIN
DECLARE @cter VARCHAR(10)
SELECT @cter=cter FROM tt_customer_master WHERE  customer_number=@CustNumber
	IF(@cter='TTA')
	BEGIN
		SELECT  t1.ICAT item, t1.IDSCO item_desc FROM  [dbo].[tt_GAL6TA_XPRICE] t1 JOIN tt_GALF6TA_II t2 ON t1.ICAT=t2.ICAT
		where t1.PRICE2>0  AND t1.ICS in (1,3,6,8) AND t2.ISTS='S' AND t2.IC14='Y'
		--AND t1.ICAT IN (SELECT ii.ICAT FROM tt_GALF6TA_DL dl
		--	JOIN tt_GALF6TA_II ii ON ii.ICAT=dl.DLKEY
		--	WHERE dl.DLNO IN (SELECT DCNO FROM tt_GALF6TA_DC WHERE DCTEFT>CONCAT(Year(GETDATE()),RIGHT('0' + RTRIM(MONTH(GETDATE())), 2),DAY(GETDATE())))
		--	AND convert(float,DLDIS)>0.0)
		--AND t1.ICAT>5500000
		AND t1.ICAT<2300459
	END
	ELSE
	BEGIN
		SELECT t1.ICAT item, t1.IDSCO item_desc FROM  [dbo].[tt_GAL6TA_XPRICE] t1 JOIN tt_GALF6TA_II t2 ON t1.ICAT=t2.ICAT
		where t1.PRICE2>0  AND t1.ICS in (1,3,6,8) AND t2.ISTS='D' AND t2.IC14='Y'
		--AND t1.ICAT IN (SELECT ii.ICAT FROM tt_GALF6TA_DL dl
		--	JOIN tt_GALF6TA_II ii ON ii.ICAT=dl.DLKEY
		--	WHERE dl.DLNO IN (SELECT DCNO FROM tt_GALF6TA_DC WHERE DCTEFT>CONCAT(Year(GETDATE()),RIGHT('0' + RTRIM(MONTH(GETDATE())), 2),DAY(GETDATE())))
		--	AND convert(float,DLDIS)>0.0)
		--AND t1.ICAT>5500000
		AND t1.ICAT<2300459
	END
END





GO
/****** Object:  StoredProcedure [dbo].[sp_GetItemDetailsForQuote]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetItemDetailsForQuote](@item VARCHAR(100), @CustNumber VARCHAR(100))
AS
BEGIN
SELECT PRICE2 ListPrice, CASE WHEN IC1='P' THEN 'AA' WHEN IC1='M' THEN 'AE' ELSE 'DU' END WHS, dbo.fn_getAgreementPrice(null,@CustNumber,@item) 'AgreementPrice', ICS StockCode
FROM  [dbo].[tt_GAL6TA_XPRICE] WHERE ICAT= @item

END


GO
/****** Object:  StoredProcedure [dbo].[sp_getMailDetailsForQuote]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec sp_getMailDetailsForQuote 'Quote','StatusChange','RN_1493_2019-09-10_5','3102442'
CREATE PROCEDURE [dbo].[sp_getMailDetailsForQuote]
(
@Module VARCHAR(100),
@Component VARCHAR(100),
@RefNumber VARCHAR(100),
@item VARCHAR(100)=null
)
AS
BEGIN

DECLARE @Requestor VARCHAR(10), @RequestedFlag VARCHAR(20), @RequestorMailId VARCHAR(100), @Customer VARCHAR(10), @Cust_name VARCHAR(MAX)
DECLARE @TO VARCHAR(MAX), @CC VARCHAR(MAX), @Subject VARCHAR(MAX), @Body VARCHAR(MAX)

DECLARE @BRANCH VARCHAR(100),@BMFLAG VARCHAR(10),@HOFLAG VARCHAR(10),@DeskFLAG VARCHAR(10)

SELECT TOP 1 @Requestor=RequestedBy, @RequestedFlag=RequestedBy_Flag FROM Quote_ref WHERE Ref_number=@RefNumber



IF(@RequestedFlag='CUSTOMER')
BEGIN
	SELECT @Customer=Cust_number, @Cust_name=Cust_Name FROM Quote_ref WHERE Ref_number=@RefNumber
	SELECT @RequestorMailId=EmailId FROM LoginInfo WHERE EngineerId=@Requestor
END
ELSE IF (@RequestedFlag='DISTRIBUTOR')
BEGIN

	SELECT @RequestorMailId=Email_Id  FROM tbl_LoginInfoForDistributor WHERE Id=@Requestor
	SELECT @Customer=CP_number, @Cust_name=CP_Name FROM Quote_ref WHERE Ref_number=@RefNumber
END
--SELECT @Customer 'Requestor'
IF(@RequestorMailId IS NOT NULL OR @RequestorMailId<>'')
BEGIN
	IF(@Module ='Quote' AND (@Component='SubmitQuote' OR @Component='ReSubmitQuote'))
	BEGIN
		SELECT @BMFLAG=BM, @HOFLAG=HO,@DeskFLAG=Desk FROM MailConfiguration where Module =@Module AND Component=@Component

		SELECT @BRANCH=BranchCode FROM LoginInfo WHERE EngineerId =(SELECT assigned_salesengineer_id FROM tt_customer_master WHERE customer_number=@Customer)
		SELECT  @TO=STUFF(
					(   SELECT ';' + CONVERT(VARCHAR(MAX), EmailId) 
						FROM
						(
						SELECT EmailIds EmailId FROM MailConfiguration WHERE Module =@Module AND Component=@Component
						UNION
						SELECT EmailId FROM LoginInfo WHERE RoleId ='BM' AND LoginStatus=1 AND BranchCode=@BRANCH AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE EngineerId IN (
						SELECT territory_engineer_id FROM tt_territory_region_relation WHERE region_code=@BRANCH) AND LoginStatus=1 AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE RoleId ='HO' AND LoginStatus=1 AND @HOFLAG='YES'
						UNION
						SELECT Email_id from tbl_DeskLogin WHERE Status=1 AND @DeskFLAG='YES') t
					FOR xml path('')
                   )
                   , 1
                   , 1
                   , '')
		
		--SELECT  @TO 'TO'

		SELECT @CC=@RequestorMailId

		SELECT @Subject='Quote is submitted with reference number : '+@RefNumber

		SELECT @Body='Hi,<br/> The quote request is submitted with reference number '+@RefNumber+' for Customer/Channel Partner : '+@Cust_name+'. Please update the status as required.<br/><br/> Do not reply to this mail.'

	END
	ELSE IF (@Module ='Quote' AND @Component='StatusChange')
	BEGIN
		IF EXISTS(SELECT * FROM Quote_ref WHERE Ref_number=@RefNumber AND Status='Sent For Approval')
		BEGIN
		SELECT @TO='',@CC='',@Subject='',@Body=''
		END
		ELSE
		BEGIN
			SELECT @BMFLAG=BM, @HOFLAG=HO,@DeskFLAG=Desk FROM MailConfiguration where Module =@Module AND Component=@Component
			SELECT @BRANCH=BranchCode FROM LoginInfo WHERE EngineerId =(SELECT assigned_salesengineer_id FROM tt_customer_master WHERE customer_number=@Customer)
			SELECT  @CC=STUFF(
					(   SELECT ';' + CONVERT(VARCHAR(MAX), EmailId) 
						FROM
						(
						SELECT EmailIds EmailId FROM MailConfiguration WHERE Module =@Module AND Component=@Component
						UNION
						SELECT EmailId FROM LoginInfo WHERE RoleId ='BM' AND LoginStatus=1 AND BranchCode=@BRANCH AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE EngineerId IN (
						SELECT territory_engineer_id FROM tt_territory_region_relation WHERE region_code=@BRANCH) AND LoginStatus=1 AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE RoleId ='HO' AND LoginStatus=1 AND @HOFLAG='YES'
						UNION
						SELECT Email_id from tbl_DeskLogin WHERE Status=1 AND @DeskFLAG='YES') t
					FOR xml path('')
                   )
                   , 1
                   , 1
                   , '')
			
			SELECT @TO=@RequestorMailId

			SELECT @Subject='Quote approval for reference number : '+@RefNumber

			SELECT @Body='Hi,<br/> The requested quote with reference number '+@RefNumber+' for Customer/Channel Partner : '+@Cust_name+' is completed. Please place order for approved quotes and have a look for rejected quotes.<br/><br/> Do not reply to this mail.'
		END
	END
	ELSE IF(@Module ='Quote' AND @Component='PlaceOrder')
	BEGIN
		SELECT @BMFLAG=BM, @HOFLAG=HO,@DeskFLAG=Desk FROM MailConfiguration where Module =@Module AND Component=@Component

		SELECT @BRANCH=BranchCode FROM LoginInfo WHERE EngineerId =(SELECT assigned_salesengineer_id FROM tt_customer_master WHERE customer_number=@Customer)
		SELECT  @TO=STUFF(
					(   SELECT ';' + CONVERT(VARCHAR(MAX), EmailId) 
						FROM
						(
						SELECT EmailIds EmailId FROM MailConfiguration WHERE Module =@Module AND Component=@Component
						UNION
						SELECT EmailId FROM LoginInfo WHERE RoleId ='BM' AND LoginStatus=1 AND BranchCode=@BRANCH AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE EngineerId IN (
						SELECT territory_engineer_id FROM tt_territory_region_relation WHERE region_code=@BRANCH) AND LoginStatus=1 AND @BMFLAG='YES'
						UNION 
						SELECT EmailId FROM LoginInfo WHERE RoleId ='HO' AND LoginStatus=1 AND @HOFLAG='YES'
						UNION
						SELECT Email_id from tbl_DeskLogin WHERE Status=1 AND @DeskFLAG='YES') t
					FOR xml path('')
                   )
                   , 1
                   , 1
                   , '')
		
		SELECT @CC=@RequestorMailId

		SELECT @Subject='Order placed for reference number : '+@RefNumber+' and item number :'+@item

		SELECT @Body='Hi,<br/> The order is placed for reference number '+@RefNumber+'  and item number :'+@item+'. <br/><br/> Do not reply to this mail.'

	END
END
SELECT @TO='anamika@knstek.com'
SELECT @CC='teja@knstek.com'
Select @TO 'To', @CC 'CC', @Subject 'Subject', @Body 'Message'

END





GO
/****** Object:  StoredProcedure [dbo].[sp_getPendingQuotes]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec sp_getPendingQuotes null,null,'BM','TTA','11'
CREATE PROCEDURE [dbo].[sp_getPendingQuotes]
(
@StartDate VARCHAR(100)=null,
@EndDate VARCHAR(100)=null,
@role VARCHAR(10),
@cter VARCHAR(10)=null,
@assigned_salesengineer_id VARCHAR(10)=null
)
AS
BEGIN

IF(@StartDate='')
SET @StartDate = null
IF(@EndDate='')
SET @EndDate = null
if(@role='Desk')
	SELECT * FROM Quote_ref 
	WHERE Status IN ('Sent For Approval') AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
	AND UPPER(Assigned_To)=UPPER(@role)
else if(@role='HO')
	SELECT *, Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy FROM Quote_ref 
	WHERE 
		Status IN ('Request For ReApproval','Approved By BM') AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
		AND (CP_number IN (Select customer_number from tt_customer_master WHERE cter=ISNULL(@cter,'TTA')) 
			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE cter=ISNULL(@cter,'TTA'))) 
		AND UPPER(Assigned_To)=UPPER(@role)
else if(@role='BM')
	SELECT *, Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy FROM Quote_ref 
	WHERE 
		Status IN ('Sent For Approval')
		--AND RequestedBy_Flag='CUSTOMER' 
		AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
		AND (CP_number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select BranchCode from LoginInfo where EngineerId=@assigned_salesengineer_id)) 
			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select BranchCode from LoginInfo where EngineerId=@assigned_salesengineer_id) )) 
		AND UPPER(Assigned_To)=UPPER(@role)
	--UNION
	--SELECT *, Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy FROM Quote_ref 
	--WHERE 
	--	Status IN ('Escalated')
	--	AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
	--	AND (CP_number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select BranchCode from LoginInfo where EngineerId=@assigned_salesengineer_id)) 
	--		OR Cust_Number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select BranchCode from LoginInfo where EngineerId=@assigned_salesengineer_id) )) 
	--	AND UPPER(Assigned_To)=UPPER(@role)

ELSE IF (@role='TM')
--SELECT *, Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy FROM Quote_ref 
--	WHERE 
--		Status IN ('Sent For Approval')
--		AND RequestedBy_Flag='CUSTOMER' 
--		AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
--		AND (CP_number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select region_code from tt_territory_region_relation where territory_engineer_id=@assigned_salesengineer_id)) 
--			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select region_code from tt_territory_region_relation where territory_engineer_id=@assigned_salesengineer_id) )) 
--		AND UPPER(Assigned_To)=UPPER('BM')
--	UNION
	SELECT *, Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy FROM Quote_ref 
	WHERE 
		Status IN ('Escalated')
		AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
		AND (CP_number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select region_code from tt_territory_region_relation where territory_engineer_id=@assigned_salesengineer_id)) 
			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select region_code from tt_territory_region_relation where territory_engineer_id=@assigned_salesengineer_id) )) 
		AND UPPER(Assigned_To)=UPPER('BM')

END




GO
/****** Object:  StoredProcedure [dbo].[sp_getQuoteDetails]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_getQuoteDetails]
(
@ID INT=null,
@item VARCHAR(100)=null,
@RefNumber VARCHAR(100)=null
)
As
BEGIN
if(@ID=0)
SET @ID=null
DECLARE @DAYS INT
SELECT @DAYS=VALUE FROM TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY'
SELECT 
q.ID
,q.Ref_number
,q.Item_code
,q.Item_Desc
,q.WHS
,q.Order_type
,q.Order_frequency
,q.Total_QTY
,q.QTY_perOrder
,q.List_Price
,q.Expected_price
,q.DC_rate
,q.Cust_number
,q.Cust_Name
,q.Cust_SP
,q.Comp_Name
,q.Comp_Desc
,q.Comp_SP
,q.CP_number
,q.CP_Name
,CASE WHEN t.Availablility='Valid' OR t.Availablility='' OR t.Availablility is null THEN q.Status ELSE 'Expired' END Status
--,q.Status Status1
,q.Requested_date
,q.Approval_date
,q.Rejected_date
,q.Approved_Rejected_By
,q.Rejected_Reason
,q.MOQ
,q.Offer_Price
,q.RequestedBy
,q.RequestedBy_Flag
, t.Availablility
, dbo.fn_GetOrderFlag(q.ID) 'Order_Flag'
,q.Assigned_To
,q.Accept_Visible_Flag
,q.Multiple_Order_Flag
,q.New_OfferPrice
,q.Recommended_Price
FROM Quote_ref q
LEFT JOIN (select ID, Approval_date, CASE WHEN DATEDIFF(dd,Approval_date,Getdate())>@DAYS THEN 'Invalid' ELSE 'Valid' END as 'Availablility' from Quote_ref where Approval_date is not null AND Approval_date<>'' AND Status='Approved') t
ON 
q.ID=t.ID
 WHERE q.Item_code =ISNULL(@item, q.Item_code) AND q.Ref_number=ISNULL(@RefNumber,q.Ref_Number) AND q.ID=ISNULL(@ID,q.ID)

END



GO
/****** Object:  StoredProcedure [dbo].[sp_getQuoteFormat]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_getQuoteFormat]
(
@RefNumber VARCHAR(MAX)
)
AS
BEGIN


SELECT  
Item_Desc 'Item', 
(select ICS from tt_GALF6TA_II WHERE ICAT=q.Item_code) Stock_Code, 
CASE WHEN  RequestedBy_Flag='DISTRIBUTOR' THEN CP_Name ELSE Cust_Name END Customer_Name,
'Address 1' Customer_Address,
(select VALUE from TBL_CONFIGURATION WHERE MODULE='TAEGUTEC ADDRESS') 'Taegutec_Address',
CASE WHEN  RequestedBy_Flag='DISTRIBUTOR' THEN CP_number ELSE Cust_number END Customer_Number,
Ref_number 'Quotation_No',
Requested_date 'Date',
Item_code 'Catalogue_No',
'' 'Tender_No',
'' 'Due_On',
(select VALUE from TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY') 'Valid_For',
Total_QTY 'Item_Qty',
CONVERT(decimal(38,2),Expected_price) 'Unit_Price',
CONVERT(decimal(38,2),(CONVERT(decimal(38,2),Expected_price)*Convert(float,Total_QTY))) 'Line_Value',
'' 'Delivery_Date',
Case WHEN  RequestedBy_Flag='DISTRIBUTOR' THEN dbo.fn_getMultiCustomers(q.Cust_number) ELSE Cust_Name END 'End_Customer'
 FROM Quote_ref q WHERE Ref_number= @RefNumber

END




GO
/****** Object:  StoredProcedure [dbo].[sp_getQuotePO]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_getQuotePO]
(
@RefNumber VARCHAR(MAX),
@item VARCHAR(100)
)
AS
BEGIN

DECLARE @PO_Id VARCHAR(100)
SELECT @PO_Id=Id FROM tbl_QuoteOrder q WHERE q.Ref_number= @RefNumber AND Item=@item 

SELECT  
Item_Desc 'Item', 
CASE WHEN  RequestedBy_Flag='DISTRIBUTOR' THEN CP_Name ELSE Cust_Name END Customer_Name,
'Address 1' Customer_Address,
(select VALUE from TBL_CONFIGURATION WHERE MODULE='TAEGUTEC ADDRESS') 'Taegutec_Address',
CASE WHEN  RequestedBy_Flag='DISTRIBUTOR' THEN CP_number ELSE Cust_number END Customer_Number,
PurchaseOrderComment,
qo.ID 'PO_No',
qo.Ref_number 'Quotation_No',
OrderPlace_Date 'Date',
Item_code 'Catalogue_No',
Total_QTY 'Item_Qty',
CONVERT(decimal(38,2),Expected_price) 'Unit_Price',
CONVERT(decimal(38,2),(CONVERT(decimal(38,2),Expected_price)*Convert(float,Total_QTY))) 'Line_Value',
OrderStartDate 'Schedule_Date',
q.WHS,
Case WHEN  RequestedBy_Flag='DISTRIBUTOR' THEN dbo.fn_getMultiCustomers(q.Cust_number) ELSE Cust_Name END 'End_Customer'
 FROM tbl_QuoteOrder qo JOIN Quote_ref q ON qo.Quote_Id=q.ID
 WHERE qo.ID=@PO_Id

END





GO
/****** Object:  StoredProcedure [dbo].[sp_getQuoteSummary]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec sp_getQuoteSummary '07/20/2019', '08/06/2019', '1741','77','CUSTOMER'

CREATE PROCEDURE [dbo].[sp_getQuoteSummary]
(
@StartDate VARCHAR(100)=null,
@EndDate VARCHAR(100)=null,
@CustNumber VARCHAR(100)=null,
@branchcode VARCHAR(100)=null,
@assigned_salesengineer_id VARCHAR(100)=null,
@cter VARCHAR(100)=null,
@customer_class VARCHAR(100)=null,
@flag VARCHAR(100)
)
As
BEGIN

if(@StartDate is null OR @StartDate='')
SET @StartDate=null
else
SET @StartDate=Cast(@StartDate as datetime)

Print @StartDate

if(@EndDate is null OR @EndDate='')
SET @EndDate=null
else
SET @EndDate=Cast(@EndDate as datetime)

Print @EndDate

CREATE TABLE #TEMP(Ref_number varchar(100),Cust_Name VARCHAR(MAX), Requested_date datetime, Status varchar(100))

	IF(@flag='DISTRIBUTOR')
		INSERT INTO #TEMP(Ref_number,Cust_Name,Requested_date, Status) 
		SELECT Ref_number,CP_Name,Requested_date, Status 
		FROM Quote_ref 
		WHERE CONVERT(DATE,Requested_date) >=ISNULL(@StartDate,Requested_date) 
			AND CONVERT(DATE,Requested_date)<= ISNULL(@EndDate,Requested_date)
			AND RequestedBy_Flag=@flag
			AND CP_number=@CustNumber
	ELSE
		INSERT INTO #TEMP(Ref_number,Cust_Name,Requested_date, Status)
		SELECT Ref_number, Cust_Name,Requested_date, Status
		FROM Quote_ref 
		WHERE CONVERT(DATE,Requested_date) >=ISNULL(@StartDate,Requested_date) 
			AND CONVERT(DATE,Requested_date)<= ISNULL(@EndDate,Requested_date)
			AND RequestedBy_Flag=@flag
			AND Cust_number=ISNULL(@CustNumber,Cust_number)
			AND Cust_number IN (Select customer_number from tt_customer_master 
							WHERE Customer_region=ISNULL(@branchcode, Customer_region)
							AND assigned_salesengineer_id =ISNULL(@assigned_salesengineer_id,assigned_salesengineer_id)
							AND cter=ISNULL(@cter,'TTA')
							AND customer_class=ISNULL(@customer_class,customer_class)
							)

SELECT distinct t1.Ref_number,t1.Cust_Name, t1.Requested_date,
 CASE
 WHEN t3.c_count=t4.c_count THEN 'Approved' 
 WHEN t3.c_count=t5.c_count THEN 'Rejected' 
 WHEN t3.c_count=t4.c_count+t5.c_count THEN 'Reviewed'
 WHEN t3.c_count>t4.c_count OR t3.c_count>t5.c_count THEN 'Partial Reviewed'
 ELSE 'Submitted' END 'Status'
--, CASE t2.Status WHEN 'Approved' THEN 'In-Process' WHEN 'Rejected' THEN 'In-Process' ELSE 'Sent For Approval' END 
FROM
(SELECT distinct  Ref_number,Cust_Name,Requested_date FROM #TEMP )t1
JOIN 
(SELECT distinct Ref_number,Cust_Name,Requested_date, Status FROM #TEMP ) t2 ON t1.Ref_number=t2.Ref_number AND t1.Cust_Name=t2.Cust_Name
JOIN 
(SELECT Ref_number, COUNT(*) c_count FROM (SELECT distinct Ref_number,Cust_Name,Requested_date, Status FROM #TEMP) t GROUP BY Ref_number ) t3 ON t1.Ref_number=t3.Ref_number
Left JOIN 
(SELECT Ref_number, COUNT(*) c_count FROM (SELECT distinct Ref_number,Cust_Name,Requested_date, Status FROM #TEMP WHERE Status='Approved') t GROUP BY Ref_number ) t4 ON t1.Ref_number=t4.Ref_number
Left JOIN 
(SELECT Ref_number, COUNT(*) c_count FROM (SELECT distinct Ref_number,Cust_Name,Requested_date, Status FROM #TEMP WHERE Status='Rejected') t GROUP BY Ref_number ) t5 ON t1.Ref_number=t5.Ref_number

DROP TABLE #TEMP



END







GO
/****** Object:  StoredProcedure [dbo].[sp_getQuoteSummaryDesk]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_getQuoteSummaryDesk '','',null

CREATE PROCEDURE [dbo].[sp_getQuoteSummaryDesk]
(
@StartDate VARCHAR(100)=null,
@EndDate VARCHAR(100)=null,
--@CustNumber VARCHAR(100)=null,
--@branchcode VARCHAR(100)=null,
--@assigned_salesengineer_id VARCHAR(100)=null,
--@cter VARCHAR(100)=null,
--@customer_class VARCHAR(100)=null,
@flag VARCHAR(100)=null
)
AS
BEGIN

if(@StartDate is null OR @StartDate='')
SET @StartDate=null
else
SET @StartDate=Cast(@StartDate as datetime)

Print @StartDate

if(@EndDate is null OR @EndDate='')
SET @EndDate=null
else
SET @EndDate=Cast(@EndDate as datetime)

Print @EndDate

CREATE TABLE #TEMP(Ref_number varchar(100),cust_type VARCHAR(100), CustCP_Num VARCHAR(100), CustCP_Name VARCHAR(MAX), Order_value FLOAT, Requested_date datetime, Status varchar(100))

	IF(@flag='DISTRIBUTOR')
		INSERT INTO #TEMP(Ref_number,cust_type,CustCP_Num,CustCP_Name,Order_value ,Requested_date, Status) 
		SELECT Ref_number,@flag,CP_Number,CP_Name, CONVERT(FLOAT,QTY_perOrder)*CONVERT(FLOAT,Expected_price),Requested_date, Status 
		FROM Quote_ref 
		WHERE CONVERT(DATE,Requested_date) >=ISNULL(@StartDate,Requested_date) 
			AND CONVERT(DATE,Requested_date)<= ISNULL(@EndDate,Requested_date)
			AND RequestedBy_Flag=@flag
	ELSE IF(@flag='CUSTOMER')
		INSERT INTO #TEMP(Ref_number,cust_type,CustCP_Num,CustCP_Name,Order_value,Requested_date, Status)
		SELECT Ref_number,@flag, Cust_number, Cust_Name,CONVERT(FLOAT,QTY_perOrder)*CONVERT(FLOAT,Expected_price),Requested_date, Status
		FROM Quote_ref 
		WHERE CONVERT(DATE,Requested_date) >=ISNULL(@StartDate,Requested_date) 
			AND CONVERT(DATE,Requested_date)<= ISNULL(@EndDate,Requested_date)
			AND RequestedBy_Flag=@flag
	ELSE
		INSERT INTO #TEMP(Ref_number,cust_type,CustCP_Num,CustCP_Name,Order_value,Requested_date, Status)
		SELECT Ref_number,'DISTRIBUTOR', CP_Number,CP_Name, CONVERT(FLOAT,QTY_perOrder)*CONVERT(FLOAT,Expected_price),Requested_date, Status
		FROM Quote_ref 
		WHERE CONVERT(DATE,Requested_date) >=ISNULL(@StartDate,Requested_date) 
			AND CONVERT(DATE,Requested_date)<= ISNULL(@EndDate,Requested_date)
			AND RequestedBy_Flag='DISTRIBUTOR'
		UNION
		SELECT Ref_number, 'CUSTOMER', Cust_number, Cust_Name, CONVERT(FLOAT,QTY_perOrder)*CONVERT(FLOAT,Expected_price),Requested_date, Status
		FROM Quote_ref 
		WHERE CONVERT(DATE,Requested_date) >=ISNULL(@StartDate,Requested_date) 
			AND CONVERT(DATE,Requested_date)<= ISNULL(@EndDate,Requested_date)
			AND RequestedBy_Flag='CUSTOMER'

SELECT distinct t1.Ref_number,t1.cust_type,t1.CustCP_Num,t1.CustCP_Name,CAST(t1.Order_value as decimal(10,2)) Order_value, t1.Requested_date,
 CASE
 WHEN t3.c_count=t4.c_count THEN 'Approved' 
 WHEN t3.c_count=t5.c_count THEN 'Rejected' 
 WHEN t3.c_count=t4.c_count+t5.c_count THEN 'Reviewed'
 WHEN t3.c_count>t4.c_count OR t3.c_count>t5.c_count THEN 'Partial Reviewed'
 ELSE 'Submitted' END 'Status'
--, CASE t2.Status WHEN 'Approved' THEN 'In-Process' WHEN 'Rejected' THEN 'In-Process' ELSE 'Sent For Approval' END 
FROM
(SELECT Ref_number,cust_type,CustCP_Num,CustCP_Name,SUM(CONVERT(FLOAT,Order_value)) Order_value,Requested_date FROM #TEMP GROUP BY Ref_number,cust_type,CustCP_Num,CustCP_Name,Requested_date )t1
JOIN 
(SELECT distinct Ref_number,cust_type,CustCP_Num,CustCP_Name,Requested_date, Status FROM #TEMP ) t2 ON t1.Ref_number=t2.Ref_number AND t1.CustCP_Num=t2.CustCP_Num
JOIN 
(SELECT Ref_number, COUNT(*) c_count FROM (SELECT distinct Ref_number,cust_type,CustCP_Num,CustCP_Name,Requested_date, Status FROM #TEMP) t GROUP BY Ref_number ) t3 ON t1.Ref_number=t3.Ref_number
Left JOIN 
(SELECT Ref_number, COUNT(*) c_count FROM (SELECT distinct Ref_number,cust_type,CustCP_Num,CustCP_Name,Requested_date, Status FROM #TEMP WHERE Status='Approved') t GROUP BY Ref_number ) t4 ON t1.Ref_number=t4.Ref_number
Left JOIN 
(SELECT Ref_number, COUNT(*) c_count FROM (SELECT distinct Ref_number,cust_type,CustCP_Num,CustCP_Name,Requested_date, Status FROM #TEMP WHERE Status='Rejected') t GROUP BY Ref_number ) t5 ON t1.Ref_number=t5.Ref_number

DROP TABLE #TEMP
END

GO
/****** Object:  StoredProcedure [dbo].[sp_getRecentlyOrderedItem]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_getRecentlyOrderedItem]
(
@flag VARCHAR(100),
@Requested_By VARCHAR(100)
)
AS
BEGIN
	IF(@flag='DISTRIBUTOR')
	BEGIN
		select  top 5 Item_code item, item_desc, Ref_number, WHS, Total_QTY, Order_frequency,QTY_perOrder, Order_type, List_Price,Expected_price,DC_rate, Cust_number,Cust_SP,Comp_Name,Comp_Desc, Comp_SP
		from Quote_ref 
		WHERE CP_number=@Requested_By
		order by Requested_date desc 
	END
	ELSE
	BEGIN
		select  top 5 Item_code item, item_desc, Ref_number, WHS, Total_QTY, Order_frequency,QTY_perOrder, Order_type, List_Price,Expected_price,DC_rate, Cust_number,Cust_SP,Comp_Name,Comp_Desc, Comp_SP
		from Quote_ref
		WHERE Cust_number=@Requested_By
		 order by Requested_date desc 
	END

END

GO
/****** Object:  StoredProcedure [dbo].[sp_PlaceOrderForQuote]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_PlaceOrderForQuote]
(
@QuoteId	INT,
@RefNumber	VARCHAR(200),
@item		VARCHAR(100),
@OrderType	VARCHAR(100),
@ScheduleType	VARCHAR(100),
@Quantity		VARCHAR(100),
@OrderByID	VARCHAR(100),
@OrderByName	VARCHAR(100),
@CustNumber		VARCHAR(100),
@CustName		VARCHAR(MAX),
@flag		VARCHAR(100),
@OrderStartDate		VARCHAR(100),
@Reason				VARCHAR(MAX),
@error_code		INT OUTPUT,
@error_msg		VARCHAR(MAX) OUTPUT
)
AS
BEGIN
	IF(@QuoteId=0 OR @QuoteId is null)
	BEGIN
	SET @error_code=100
	SET @error_msg='Error: Quote ID is required.'
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@QuoteId)
		BEGIN
			IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@QuoteId AND Ref_number=@RefNumber AND Item_code=@item)
			BEGIN
				
				INSERT INTO tbl_quoteOrder(Quote_Id,Ref_number, Item, Order_Type, Schedule_Type,OrderStartDate,QuantityPerOrder, OrderBy_ID, OrderBY_Name, Order_Flag, CustName, CustNumber, OrderPlace_Date,Order_Status, PurchaseOrderComment)
				VALUES(@QuoteId,@RefNumber,@item,@OrderType,@ScheduleType, Convert(Date,@OrderStartDate),@Quantity, @OrderByID,@OrderByName,@flag,@CustName, @CustNumber, GETDATE(),'Placed', @Reason)
				IF EXISTS(SELECT * FROM tbl_quoteOrder WHERE Quote_Id=@QuoteId AND Ref_number=@RefNumber AND Item=@item)
				BEGIN
					SET @error_code=200
					SET @error_msg='Order is placed successfully.'
				END
				ELSE
				BEGIN
					SET @error_code=103
					SET @error_msg='Error: Error in saving data.'
				END
			END											
			ELSE
			BEGIN
				SET @error_code=102
				SET @error_msg='Error: Reference Number and Item code are not available in Quote table.'
			END
		END
		ELSE
		BEGIN
			SET @error_code=101
			SET @error_msg='Error: Quote ID is not available in Quote table.'
		END
	END

END



GO
/****** Object:  StoredProcedure [dbo].[sp_quoteStatusChange]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_quoteStatusChange]
(
@ID				INT,
@RefNumber		VARCHAR(200),
@item			VARCHAR(100),
@flag			VARCHAR(100),
@changeby		VARCHAR(100),
@Status			VARCHAR(100),
@Reason			VARCHAR(MAX)=null,
@MOQ			VARCHAR(100)=null,
@OfferPrice		VARCHAR(100)=null,
@MultiOrderFlag	INT=null,
@RecommendedPrice VARCHAR(100)=null,
@error_code		INT OUTPUT,
@error_msg		VARCHAR(MAX) OUTPUT
)
AS
BEGIN
	DECLARE @OrderType VARCHAR(100)
	IF(@Status='Approved' AND (@MOQ is null OR @MOQ=''))
	BEGIN
		SELECT @OrderType= Order_type FROM Quote_ref WHERE ID=@ID
		IF(@OrderType='onetime')
		BEGIN
			SELECT @MOQ= Total_QTY FROM Quote_ref WHERE ID=@ID
		END
		ELSE
		BEGIN
			SELECT @MOQ= QTY_perOrder FROM Quote_ref WHERE ID=@ID
		END
	END

	IF(@MultiOrderFlag=0)
	BEGIN
		SET @MultiOrderFlag=null
	END

	Print @MultiOrderFlag

	IF(@ID=0 OR @ID is null)
	BEGIN
	SET @error_code=100
	SET @error_msg='Error: ID is required.'
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@ID)
		BEGIN
			IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item)
			BEGIN
				UPDATE Quote_ref SET Status=@Status
					, Approval_date= CASE  WHEN @Status='Approved' THEN GETDATE() ELSE Approval_date END
					, Rejected_date= CASE  WHEN @Status='Rejected' THEN GETDATE() ELSE Rejected_date END
					, Rejected_Reason=CASE  WHEN @Status='Rejected' THEN @Reason ELSE Rejected_Reason END
					, Approved_Rejected_By=CASE  WHEN @Status='Approved' OR @Status='Rejected' THEN @changeby ELSE Approved_Rejected_By END
					, MOQ=ISNULL(@MOQ,MOQ)
					,New_OfferPrice=ISNULL(@OfferPrice,Offer_Price)
					,Multiple_Order_Flag=ISNULL(@MultiOrderFlag,Multiple_Order_Flag)
					,Recommended_Price=@RecommendedPrice
				WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item

				UPDATE Quote_ref SET Assigned_To=ISNULL(dbo.fn_getAssignedTo(@ID), Assigned_To)
				WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item
								
				IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@ID AND Status=@STATUS AND Ref_number=@RefNumber AND Item_code=@item)
				BEGIN
					
					INSERT INTO tbl_QuoteStatusLog(Quote_ID,Ref_Number,Item_code,Status,StatusChangeBy_Flag,StatusChangeBy_Id,StatusChangeDate,StatusChange_Comment)
					SELECT @ID, @RefNumber, @item,@Status,@flag, @changeby,GETDATE(), @Reason  FROM Quote_ref
					 WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item

					SET @error_code=200
					SET @error_msg='Quote request status is modified successfully.'
				END
				ELSE
				BEGIN
					SET @error_code=103
					SET @error_msg='Error: Error in updating data.'
				END
			END											
			ELSE
			BEGIN
				SET @error_code=102
				SET @error_msg='Error: Reference Number and Item code are not available in Quote table.'
			END
		END
		ELSE
		BEGIN
			SET @error_code=101
			SET @error_msg='Error: ID is not available in Quote table.'
		END
	END

END
GO
/****** Object:  StoredProcedure [dbo].[sp_saveQuotes]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_saveQuotes](@tblQuote  [QuoteTableType] Readonly,@error_code INT OUTPUT, @error_msg VARCHAR(MAX) OUTPUT, @RefNumber VARCHAR(100) OUTPUT)
AS
BEGIN

set @error_code=100
set @error_msg='Initializing'




--DECLARE @RefNum VARCHAR(100)
DECLARE @CPNUM VARCHAR(100)
DECLARE @ID VARCHAR(50)
DECLARE @FLAG VARCHAR(100)
DECLARE @CUST VARCHAR(100)
select @ID=right(Ref_number, charindex('_', reverse(Ref_number) + '_') - 1)
 From Quote_ref WHERE ID=(select MAX(ID) FROM Quote_ref)
 if(@ID is null OR @ID='')
	SET @ID=1
ELSE
	SET @ID=CONVERT(INT,@ID)+1
--Select @ID
SELECT top 1 @RefNumber=Ref_number, @CPNUM=CP_number, @FLAG=RequestedBy_Flag, @CUST=Cust_number FROM @tblQuote 
	IF(@FLAG='DISTRIBUTOR')
	BEGIN
		IF(@RefNumber is null OR @RefNumber='')
		SET @RefNumber='RN_'+@CPNUM+'_'+CONVERT(VARCHAR(20),CONVERT(Date,GETDATE()))+'_'+@ID
	END
	ELSE
	BEGIN
		IF(@RefNumber is null OR @RefNumber='')
		SET @RefNumber='RN_'+@CUST+'_'+CONVERT(VARCHAR(20),CONVERT(Date,GETDATE()))+'_'+@ID
	END

INSERT INTO Quote_ref 
(
Ref_number
,Item_code 
,Item_Desc 
,WHS			
,Order_type	
,Order_frequency
,Total_QTY
,QTY_perOrder
,List_Price 	
,Expected_price	
,DC_rate	
,Cust_number
,Cust_Name	
,Cust_SP	
,Comp_Name	
,Comp_Desc	
,Comp_SP	
,CP_number	
,CP_Name	
,Status
,Requested_DATE	
,RequestedBy
,RequestedBy_Flag
,Discount_Price
,Agreement_Price
)
SELECT @RefNumber
,Item_code 
,Item_Desc 
,WHS			
,Order_type	
,Order_frequency
,Total_QTY
,QTY_perOrder
,List_Price 	
,Expected_price	
,DC_rate	
,Cust_number
,Cust_Name	
,Cust_SP	
,Comp_Name	
,Comp_Desc	
,Comp_SP	
,CP_number	
,CP_Name	
,Status
,GETDATE()
,RequestedBy
,RequestedBy_Flag
,dbo.fn_getDiscountPrice(Cust_number,CP_number,Item_code, List_Price)
,dbo.fn_getAgreementPrice(Cust_number,CP_number,Item_code)
FROM @tblQuote

INSERT INTO tbl_QuoteStatusLog(Quote_ID,Ref_Number,Item_code,Status,StatusChangeBy_Flag,StatusChangeBy_Id,StatusChangeDate)
SELECT ID, Ref_number, Item_code,Status,RequestedBy_Flag, RequestedBy,GETDATE()  FROM Quote_ref WHERE Ref_number=@RefNumber

UPDATE Quote_ref SET Offer_Price=Case WHEN Discount_Price IS NULL OR Discount_Price='' OR Discount_Price='0.0' THEN NULL 
								ELSE CASE WHEN CONVERT(float,Expected_Price)>CONVERT(float,Discount_Price) THEN Expected_Price ELSE Discount_Price END
								END
WHERE Ref_number=@RefNumber

UPDATE Quote_ref SET Assigned_To='DESK'
WHERE Ref_number=@RefNumber

--UPDATE Quote_ref SET Assigned_To=dbo.fn_getAssignedTo(ID)
--WHERE Ref_number=@RefNumber

UPDATE Quote_ref SET Accept_Visible_Flag = CASE WHEN ISNULL(Discount_Price,0.0)='0.0' THEN 0 ELSE 1 END
WHERE Ref_number=@RefNumber


set @error_code=0
set @error_msg='Success'

END



GO
/****** Object:  UserDefinedFunction [dbo].[fn_getAgreementPrice]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_getAgreementPrice](@Cust_Number VARCHAR(100), @CP_Number VARCHAR(100), @Item_number VARCHAR(100))
RETURNS numeric(22,2) AS
BEGIN
	DECLARE @ret numeric(22,2) ;
	IF(@CP_Number is not null AND @CP_Number<>'')
	SET @Cust_Number=@CP_Number
	SELECT DISTINCT 
@ret=dl.DLVUL
--ii.ICS, ii.IC1,dl.DLKEY,ii.IDSCO,cc.CRGN, cc.CGRP,cc.CTYP,dn.DNCUST, cc.CNME1,dn.DNDNO, dl.DLBRF, dl.DLVUL,dc.DCDSC,dc.DCTEFF, dc.DCTEFT, dn.DNTUPD, dn.DNUSRU,cc.CWHS, dl.DLTEFF, dl.DLTEFT, ii.IVTV
FROM 
	tt_GALF6TA_II ii JOIN tt_GALF6TA_DL dl ON ii.ICAT=dl.DLKEY
	JOIN tt_GALF6TA_DN dn ON dn.DNDNO=dl.DLNO
	JOIN tt_GALF6TA_DC dc ON dc.DCNO=dn.DNDNO
	JOIN tt_customer_discountgroup_mapping cc ON cc.CUST=dn.DNCUST
WHERE ii.ICS IN (1,3,6,8)
	AND ii.IC14='Y'
	AND CTYP IN ('C','D')
	AND DNCUST=@Cust_Number
	AND DLKEY=@Item_number
			 
	RETURN @ret
END







GO
/****** Object:  UserDefinedFunction [dbo].[fn_getAssignedTo]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_getAssignedTo](@ID VARCHAR(100))
RETURNS VARCHAR(200) AS
BEGIN

	DECLARE @ret VARCHAR(200)
	DECLARE @Status VARCHAR(MAX), @AgreementPrice VARCHAR(100), @Expected_Price VARCHAR(100), @flag VARCHAR(100)
	SELECT @Status=Status, @AgreementPrice=Agreement_Price, @Expected_Price=Expected_price, @flag=RequestedBy_Flag from Quote_ref WHERE ID=@ID
	IF(@Status='Request For ReApproval' OR @Status='Escalated By BM' OR @Status='Approved By BM')
	BEGIN
		SET @ret='HO'
	END
	ELSE IF(@Status='Sent For Approval')
	BEGIN
		if(@flag='DISTRIBUTOR')
		BEGIN
			IF(@AgreementPrice IS NULL OR @AgreementPrice='' OR @AgreementPrice='0.0')
				SET @ret='DESK'
			ELSE
				IF(@AgreementPrice>@Expected_Price)
					SET @ret='BM'
				ELSE
				SET @ret='DESK'
		END
		ELSE 
			SET @ret='BM'
	END
	ELSE IF(@Status='Escalated')
	BEGIN
		SET @ret='BM'
	END
	ELSE
	BEGIN
		SET @ret=NULL
	END
	RETURN @ret
END



GO
/****** Object:  UserDefinedFunction [dbo].[fn_getDiscountPrice]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_getDiscountPrice](@Cust_Number VARCHAR(100), @CP_Number VARCHAR(100), @Item_number VARCHAR(100), @ListPrice VARCHAR(100))
RETURNS numeric(22,2) AS
BEGIN

	DECLARE @ret numeric(22,2) ;
	IF(@CP_Number='')
	 SET @CP_Number=null
	DECLARE @DGRP VARCHAR(10), @IDGRP VARCHAR(10), @DLNO VARCHAR(100), @Discount FLOAT
	SELECT @DGRP=CDISG FROM tt_customer_discountgroup_mapping WHERE CUST=ISNULL(@CP_Number,@Cust_Number)
	SELECT @IDGRP=IDISC FROM tt_GALF6TA_II WHERE ICAT=@Item_number
	SELECT @DLNO = CASE @DGRP WHEN 'D1' THEN '1354' WHEN 'D2' THEN '1355' WHEN 'D3' THEN '1356' WHEN 'D4' THEN '1357' WHEN 'D5' THEN '1358' WHEN 'TEST' THEN '1353' ELSE '' END
	IF(@DLNO='' OR @DLNO is null OR @DLNO=null)
		SET @ret=0.0
	ELSE
		SELECT @Discount=DLDIS FROM tt_GALF6TA_DL dl
			JOIN tt_GALF6TA_II ii ON ii.IDISC=dl.DLKEY
			WHERE dl.DLNO IN (SELECT DCNO FROM tt_GALF6TA_DC WHERE DCTEFT>CONCAT(Year(GETDATE()),RIGHT('0' + RTRIM(MONTH(GETDATE())), 2),DAY(GETDATE())))
			AND ii.ICAT=@Item_number
			AND dl.DLNO=@DLNO
			AND ii.IDISC=@IDGRP
			SET @ret=CONVERT(float,@ListPrice)-(CONVERT(float,@ListPrice)*@Discount)
	RETURN @ret
END



GO
/****** Object:  UserDefinedFunction [dbo].[fn_getMultiCustomers]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Function [dbo].[fn_getMultiCustomers]
(
@customer_numbers VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
DECLARE @output VARCHAR(MAX)
SELECT @output=coalesce(@output + ',', '') +  convert(varchar(MAX),CNME1) 
	FROM DBVW_DistibutorsCustomers 
	  WHERE  CRPID IN (Select Item from dbo.SplitString(@customer_numbers,','))
return @output

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetOrderCount]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetOrderCount]
(
	@ID INT
)
RETURNS INT
AS
BEGIN
	DECLARE @ret INT
	DECLARE @ExpiryDays INT, @MultiOrderFlag INT=NULL, @OrderBeforeExpire INT, @OrderDateDiff INT, @ExpiryFlag INT
	SELECT @MultiOrderFlag=ISNULL(Multiple_Order_Flag,0) FROM Quote_ref WHERE ID=@ID
	SELECT @ExpiryDays=VALUE FROM TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY'
	SELECT @OrderDateDiff=DATEDIFF(DAY,(SELECT Approval_date FROM Quote_ref WHERE ID=@ID),(SELECT MAX(OrderPlace_Date) FROM tbl_QuoteOrder WHERE Quote_Id=@ID))
	SELECT @OrderBeforeExpire=COUNT(*) FROM tbl_QuoteOrder 
		WHERE 
			Quote_Id=@ID 
			AND @OrderDateDiff<@ExpiryDays
	SELECT @ExpiryFlag=CASE WHEN DATEDIFF(DAY,(SELECT Approval_date FROM Quote_ref WHERE ID=@ID), GETDATE())>@ExpiryDays THEN 1 ELSE 0 END
	IF(@ExpiryFlag>0)
		SET @ret=0
	ELSE IF(@MultiOrderFlag>0 AND @OrderBeforeExpire>0)
		SET @ret=0
	ELSE IF(@MultiOrderFlag>0 AND @OrderBeforeExpire=0)
		SET @ret=1
	ELSE IF(@MultiOrderFlag=0 AND @OrderBeforeExpire>0)
		SET @ret=0
	ELSE IF(@MultiOrderFlag=0 AND @OrderBeforeExpire=0)
		SET @ret=1

	RETURN @ret

END


GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetOrderFlag]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetOrderFlag]
(
	@ID INT
)
RETURNS INT
AS
BEGIN
	DECLARE @ret INT
	DECLARE @ExpiryDays INT, @MultiOrderFlag INT=NULL, @OrderBeforeExpire INT, @OrderDateDiff INT, @ExpiryFlag INT
	SELECT @MultiOrderFlag=ISNULL(Multiple_Order_Flag,0) FROM Quote_ref WHERE ID=@ID
	SELECT @ExpiryDays=VALUE FROM TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY'
	SELECT @OrderDateDiff=DATEDIFF(DAY,(SELECT Approval_date FROM Quote_ref WHERE ID=@ID),(SELECT MAX(OrderPlace_Date) FROM tbl_QuoteOrder WHERE Quote_Id=@ID))
	SELECT @OrderBeforeExpire=COUNT(*) FROM tbl_QuoteOrder 
		WHERE 
			Quote_Id=@ID 
			AND @OrderDateDiff<@ExpiryDays
	SELECT @ExpiryFlag=CASE WHEN DATEDIFF(DAY,(SELECT Approval_date FROM Quote_ref WHERE ID=@ID), GETDATE())>@ExpiryDays THEN 1 ELSE 0 END
	IF(@ExpiryFlag>0)
		SET @ret=0
	ELSE IF(@MultiOrderFlag>0)
		SET @ret=1
	ELSE IF(@MultiOrderFlag=0 AND @OrderBeforeExpire>0)
		SET @ret=0
	ELSE IF(@MultiOrderFlag=0 AND @OrderBeforeExpire=0)
		SET @ret=1

	RETURN @ret

END


GO
/****** Object:  Table [dbo].[MailConfiguration]    Script Date: 11/14/2019 12:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MailConfiguration](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Module] [varchar](100) NULL,
	[Component] [varchar](100) NULL,
	[BM] [varchar](10) NULL,
	[HO] [varchar](10) NULL,
	[Desk] [varchar](10) NULL,
	[EmailIds] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Quote_ref]    Script Date: 11/14/2019 12:27:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Quote_ref](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Ref_number] [varchar](100) NULL,
	[Item_code] [varchar](100) NULL,
	[Item_Desc] [varchar](max) NULL,
	[WHS] [varchar](100) NULL,
	[Order_type] [varchar](100) NULL,
	[Order_frequency] [varchar](max) NULL,
	[Total_QTY] [varchar](100) NULL,
	[QTY_perOrder] [varchar](100) NULL,
	[List_Price] [varchar](100) NULL,
	[Expected_price] [varchar](100) NULL,
	[DC_rate] [varchar](100) NULL,
	[Discount_Price] [varchar](100) NULL,
	[Agreement_Price] [varchar](100) NULL,
	[Offer_Price] [varchar](100) NULL,
	[Cust_number] [varchar](100) NULL,
	[Cust_Name] [varchar](max) NULL,
	[Cust_SP] [varchar](100) NULL,
	[Comp_Name] [varchar](max) NULL,
	[Comp_Desc] [varchar](max) NULL,
	[Comp_SP] [varchar](100) NULL,
	[CP_number] [varchar](100) NULL,
	[CP_Name] [varchar](100) NULL,
	[MOQ] [varchar](100) NULL,
	[Status] [varchar](100) NULL,
	[Requested_date] [datetime] NULL,
	[Approval_date] [datetime] NULL,
	[Rejected_date] [datetime] NULL,
	[Approved_Rejected_By] [varchar](max) NULL,
	[Rejected_Reason] [varchar](max) NULL,
	[RequestedBy] [varchar](20) NULL,
	[RequestedBy_Flag] [varchar](100) NULL,
	[ReRequested_Date] [datetime] NULL,
	[ReRequested_By] [varchar](100) NULL,
	[Assigned_To] [varchar](200) NULL,
	[Accept_Visible_Flag] [int] NULL,
	[Multiple_Order_Flag] [int] NULL,
	[New_OfferPrice] [varchar](100) NULL,
	[Recommended_Price] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CONFIGURATION]    Script Date: 11/14/2019 12:27:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CONFIGURATION](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MODULE] [varchar](200) NULL,
	[VALUE] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_DeskLogin]    Script Date: 11/14/2019 12:27:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DeskLogin](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[First_Name] [varchar](100) NULL,
	[Last_Name] [varchar](100) NULL,
	[Email_id] [varchar](max) NULL,
	[Password] [varchar](max) NULL,
	[Status] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_QuoteOrder]    Script Date: 11/14/2019 12:27:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_QuoteOrder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Quote_Id] [int] NULL,
	[Ref_number] [varchar](200) NULL,
	[Item] [varchar](100) NULL,
	[Order_Type] [varchar](100) NULL,
	[Schedule_Type] [varchar](100) NULL,
	[OrderStartDate] [varchar](100) NULL,
	[QuantityPerOrder] [varchar](100) NULL,
	[OrderBy_ID] [varchar](100) NULL,
	[OrderBY_Name] [varchar](max) NULL,
	[OrderPlace_Date] [datetime] NULL,
	[Order_Status] [varchar](100) NULL,
	[Order_Flag] [varchar](100) NULL,
	[CustName] [varchar](max) NULL,
	[CustNumber] [varchar](100) NULL,
	[PurchaseOrderComment] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_QuoteStatusLog]    Script Date: 11/14/2019 12:27:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_QuoteStatusLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Quote_ID] [int] NULL,
	[Ref_Number] [varchar](200) NULL,
	[Item_code] [varchar](100) NULL,
	[Status] [varchar](200) NULL,
	[StatusChangeBy_Flag] [varchar](max) NULL,
	[StatusChangeBy_Id] [varchar](100) NULL,
	[StatusChangeDate] [datetime] NULL,
	[StatusChange_Comment] [varchar](max) NULL,
	[Price_added] [varchar](1000) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tt_Competitor]    Script Date: 11/14/2019 12:27:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tt_Competitor](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Competitor_Name] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

/****** Object:  StoredProcedure [dbo].[sp_deskLogin]    Script Date: 11/14/2019 1:18:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_deskLogin]
(
@email VARCHAR(MAX),
@password VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @Error_code INT
	DECLARE @Error_msg VARCHAR(MAX)
	IF EXISTS(Select * from tbl_DeskLogin WHERE Email_id=@email)
	BEGIN
		IF EXISTS(Select * from tbl_DeskLogin WHERE Email_id=@email AND Password=@password )
		BEGIN
			IF EXISTS(Select * from tbl_DeskLogin WHERE Email_id=@email AND Password=@password AND Status='1')
			BEGIN
				SET @Error_code=0
				SET @Error_msg='Success'
				SELECT ID,CONCAT(First_Name,' ',Last_name) Full_name, @Error_code 'Error_code', @Error_msg 'Error_msg' FROM tbl_DeskLogin WHERE Email_id=@email AND Password=@password AND Status='1'
			END
			ELSE
			BEGIN
				SET @Error_code=4
				SET @Error_msg='Your account is deactivated. Please contact administrator.'
				SELECT '' ID,'' Full_name, @Error_code 'Error_code', @Error_msg 'Error_msg'
			END
		END
		ELSE
		BEGIN
			SET @Error_code=3
			SET @Error_msg='Incorrect Password. Please enter correct password and try again.'
			SELECT '' ID,'' Full_name, @Error_code 'Error_code', @Error_msg 'Error_msg'
		END
	END
	ELSE
	BEGIN
		SET @Error_code=2
		SET @Error_msg='Either you don''t have account or you have entered wrong email id.'
		SELECT '' ID,'' Full_name, @Error_code 'Error_code', @Error_msg 'Error_msg'
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_getLoginForDistributors]    Script Date: 11/14/2019 1:18:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Anamika
-- ALTER date: 03/10/2017
-- Description:	Login Authentication for Distributor
-- =============================================
CREATE PROCEDURE [dbo].[sp_getLoginForDistributors]
(@UserId varchar(100),
 @Password varchar(MAX))
 AS
 BEGIN
 SET NOCOUNT ON;
 DECLARE @error_msg VARCHAR(MAX), @error_code INT
	IF EXISTS (SELECT * FROM tbl_LoginInfoForDistributor Where Status = 1 AND Distributor_number = @UserId)
	BEGIN
	print '1'
	IF EXISTS (SELECT * FROM tbl_LoginInfoForDistributor Where Password=@Password AND Distributor_number = @UserId)
		SELECT *,'' error_msg, 0 error_code
		from tbl_LoginInfoForDistributor

		where Distributor_number = @UserId AND Password=@Password
	ELSE
	BEGIN 
	SET @error_msg = 'User name and password do not match.' SET @error_code = 1
	SELECT '' Id,'' Distributor_number, '' Distributor_name ,'' Employee_name ,'' Email_id,'' Password, '' Role, @error_msg error_msg, @error_code error_code
	END

	END

	ELSE IF EXISTS (SELECT * FROM tbl_LoginInfoForDistributor Where Status = 0 AND Distributor_number = @UserId)
	BEGIN SET @error_msg = 'Your account is blocked' SET @error_code = 2
	SELECT '' Id,'' Distributor_number, '' Distributor_name ,'' Employee_name ,'' Email_id,'' Password, '' Role, @error_msg error_msg, @error_code error_code
	END

	ELSE
	BEGIN SET @error_msg = 'You do not have account in Sales budget Application.' SET @error_code = 2
	SELECT '' Id,'' Distributor_number, '' Distributor_name ,'' Employee_name ,'' Email_id,'' Password, '' Role, @error_msg error_msg, @error_code error_code
	END
 END







GO