USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_quoteStatusChange]    Script Date: 10-11-2020 16:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DROP PROCEDURE sp_quoteStatusChange
ALTER PROCEDURE [dbo].[sp_quoteStatusChange]
(
@tblQuote  [QuoteStatusChangeTableType] Readonly
)
AS
BEGIN

INSERT INTO tt_QuoteStatusChangeInterface
Select * from @tblQuote

	DECLARE @table TABLE(Ref_Number VARCHAR(200), Item VARCHAR(200), Quotation_no VARCHAR(200), MailTo VARCHAR(MAX), MAilCC VARCHAR(MAX), Subject VARCHAR(MAX), Body VARCHAR(MAX), error_code	INT, error_msg	VARCHAR(MAX), PDF_Flag INT)
	CREATE TABLE #tbl_mail(MailTo VARCHAR(MAX), MAilCC VARCHAR(MAX), Subject VARCHAR(MAX), Body VARCHAR(MAX) )
	DECLARE @OrderType VARCHAR(100), @Status_Count VARCHAR(10), @RefNumber VARCHAR(200), @item VARCHAR(100), @Status VARCHAR(100),
	@MOQ [varchar](100) ,
	@OfferPrice [varchar](100) ,
	@MultiOrderFlag [int] ,
	@RecommendedPrice [varchar](100),
	@ID int,
	@flag [varchar](100) ,
	@changeby [varchar](100) ,
	@Reason [varchar](max) ,
	@OrderFreq [varchar](100) ,
	@OrderQty int,
	@OrderValidity int,
	--@ExpiryDate [DateTime] ,
	@Quotation_no VARCHAR(200),
	@Comp_name VARCHAR(MAX),
	@Comp_desc VARCHAR(MAX),
	@Comp_SP VARCHAR(MAX),
	@End_cust_name VARCHAR(MAX),
	@End_cust_num VARCHAR(100),
	@Status_Count1 VARCHAR(10)


	CREATE TABLE #tbl_ref(RefNumber VARCHAR(200), Ref_Count INT )
	TRUNCATE TABLE #tbl_ref
	INSERT INTO #tbl_ref
	Select RefNumber, Count(*) Ref_Count  FROM @tblQuote GROUP BY RefNumber
	
	

	DECLARE ref_cursor CURSOR FOR
		SELECT RefNumber
		FROM #tbl_ref;

OPEN ref_cursor;
FETCH NEXT FROM ref_cursor INTO @RefNumber;

WHILE @@FETCH_STATUS = 0
   BEGIN
      
   SELECT @Status_Count1=dbo.fn_getStatusChange_Count(@RefNumber,(select COUNT(*) from @tblQuote where RefNumber=@RefNumber))
   DECLARE item_cursor CURSOR FOR
		SELECT item
		FROM @tblQuote WHERE RefNumber=@RefNumber
	OPEN item_cursor;
	FETCH NEXT FROM item_cursor INTO @item;

	WHILE @@FETCH_STATUS = 0
	BEGIN


	  SELECT @Status=Status, @MOQ=MOQ, 
	  @OfferPrice=OfferPrice, @MultiOrderFlag=MultiOrderFlag, 
	  @RecommendedPrice=RecommendedPrice, @ID=ID, @flag=flag,
	  @changeby=changeby, @Reason=Reason,
	  --, @ExpiryDate=expiry_date 
	  @OrderFreq =OrderFreq,
	@OrderQty =OrderQty,
	@OrderValidity =validity,
	@Comp_name=Comp_name,
	@Comp_desc=Comp_desc,
	@Comp_SP=Comp_SP,
	@End_cust_name=End_Cust_name,
	@End_cust_num=End_Cust_num
	  FROM @tblQuote WHERE RefNumber=@RefNumber AND item=@item
      SELECT @Status_Count=StatusChange_Count FROM Quote_ref WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item

	  IF(@Status_Count IS NULL OR @Status_Count=null OR @Status_Count='')
	  BEGIN
	  SET @Status_Count=@Status_Count1
	  END
	 
	  if(@flag='HO' OR @flag='DESK')
	  BEGIN
	  
		
		--SELECT @Status_Count
		IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item AND Quotation_no IS NOT NULL AND Quotation_no !='')
		BEGIN
		SELECT @Quotation_no= Quotation_no FROM Quote_ref WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item AND Quotation_no IS NOT NULL AND Quotation_no !=''
		--SELECT @Quotation_no
		END
		ELSE
		BEGIN
		SET @Quotation_no=CONCAT(@RefNumber,'/', @Status_Count)
		 END
	  END
	 -- ELSE if(@flag='CP')
	 -- BEGIN
	 -- IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item AND Quotation_no IS NOT NULL AND Quotation_no !='')
		--BEGIN
		--SELECT @Quotation_no= Quotation_no FROM Quote_ref WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item AND Quotation_no IS NOT NULL AND Quotation_no !=''
		--SET @Status_Count= SUBSTRING(@Quotation_no,CHARINDEX('/',@Quotation_no)+1,LEN(@Quotation_no)-CHARINDEX('/',@Quotation_no))
		--END
		--ELSE
		--BEGIN
		--SET @Quotation_no=''
		--SET @Status_Count=''
		--END
	 -- END
	  ELSE
	  BEGIN
		IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item AND Quotation_no IS NOT NULL AND Quotation_no !='')
		BEGIN
		SELECT @Quotation_no= Quotation_no FROM Quote_ref WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item AND Quotation_no IS NOT NULL AND Quotation_no !=''
		SET @Status_Count= right(@Quotation_no, charindex('/', reverse(@Quotation_no) + '/') - 1)
		--SET @Status_Count= SUBSTRING(@Quotation_no,CHARINDEX('/',@Quotation_no)+1,LEN(@Quotation_no)-CHARINDEX('/',@Quotation_no))
		END
		ELSE
		BEGIN
		SET @Quotation_no=''
		SET @Status_Count=''
		END
	  END
	IF((@Status='Approved' OR @Status='Escalated & Approved') AND (@MOQ is null OR @MOQ=''))
	BEGIN
		SELECT @OrderType= Order_type FROM Quote_ref WHERE ID=@ID
		IF(@OrderType='onetime')
		BEGIN
			SELECT @MOQ= Total_QTY FROM Quote_ref WHERE ID=@ID
		END
		ELSE
		BEGIN
			SELECT @MOQ= QTY_perOrder FROM Quote_ref WHERE ID=@ID
		END
	END
	IF(@MultiOrderFlag=0)
	BEGIN
		SET @MultiOrderFlag=null
	END

	Print @MultiOrderFlag

	IF(@ID=0 OR @ID is null)
	BEGIN
		INSERT INTO @table(Ref_Number,Quotation_no, error_code,error_msg)
			VALUES(@RefNumber,CONCAT(@RefNumber,'/', @Status_Count),100,'Error: ID is required.')

	END
	ELSE
	BEGIN
		IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@ID)
		BEGIN
			IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item)
			BEGIN
			IF(@Status='Accepted' OR @Status='Added In PA')
			BEGIN						 
				UPDATE Quote_ref SET Status=@Status
				WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item
			END
			ELSE
			BEGIN
				UPDATE Quote_ref SET Status=@Status
					, Approval_date= CASE  WHEN (@Status='Approved' OR @Status='Escalated & Approved') THEN GETDATE() ELSE Approval_date END
					, Rejected_date= CASE  WHEN @Status='Rejected' THEN GETDATE() ELSE Rejected_date END
					, Rejected_Reason=CASE  WHEN @Status='Rejected' THEN @Reason ELSE Rejected_Reason END
					, Approved_Rejected_By=CASE  WHEN @Status='Approved' OR @Status='Rejected' OR @Status='Escalated & Approved' THEN @changeby ELSE Approved_Rejected_By END
					, MOQ=ISNULL(@MOQ,MOQ)
					,New_OfferPrice=CASE  WHEN (@Status='Approved' OR @Status='Escalated & Approved') THEN ISNULL(@OfferPrice,Offer_Price) ELSE New_OfferPrice END
					,Multiple_Order_Flag=ISNULL(@MultiOrderFlag,Multiple_Order_Flag)
					,Recommended_Price=ISNULL(@RecommendedPrice,Recommended_Price)
					,StatusChange_Count=ISNULL(@Status_Count,StatusChange_Count)
					,Quotation_no=ISNULL(@Quotation_no,Quotation_no)
					--,ExpiryDate=ISNULL(@ExpiryDate,ExpiryDate)
					,Order_Validity=ISNULL(@OrderValidity,Order_Validity)
					,Approved_OrderQty=ISNULL(@OrderQty,Approved_OrderQty)
					,Approved_OrderFreq=ISNULL(@OrderFreq,Approved_OrderFreq)
				WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item

				If(@Status='Escalated')
				BEGIN
					UPDATE Quote_ref SET Comp_Name=@Comp_name
					, Comp_Desc=@Comp_desc
					, Comp_SP=@Comp_SP
					WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item

					UPDATE Quote_ref SET Total_QTY=@OrderQty
					WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item AND Order_type='onetime'
					
					UPDATE Quote_ref SET QTY_perOrder=@OrderQty
					WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item AND Order_type='schedule'
				END
				UPDATE Quote_ref SET Assigned_To=ISNULL(dbo.fn_getAssignedTo(@ID), Assigned_To)
				WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item
				
				END

				IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@ID AND Status=@STATUS AND Ref_number=@RefNumber AND Item_code=@item)
				BEGIN
					--IF(@Status_Count='C')
					--SET @pdf_flag=1
					INSERT INTO tbl_QuoteStatusLog(Quote_ID,Ref_Number,Item_code,Status,StatusChangeBy_Flag,StatusChangeBy_Id,StatusChangeDate,StatusChange_Comment,Price_added, End_cust_num, End_cust_name)
					SELECT @ID, @RefNumber, @item,@Status,@flag, @changeby,GETDATE(), REPLACE(REPLACE(@Reason, CHAR(13), ''), CHAR(10), ''),CONVERT(VARCHAR(1000),New_OfferPrice), @End_cust_name, @End_cust_num FROM Quote_ref
					 WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item

					 IF(@STATUS='Accepted')
					 INSERT INTO @table(Ref_Number,Item, Quotation_no, error_code,error_msg)
					 VALUES(@RefNumber,@item,@Quotation_no,200,'Approved Price will be added in PA.')
					 ELSE IF(@STATUS='Added In PA')
					 INSERT INTO @table(Ref_Number,Item, Quotation_no, error_code,error_msg)
					 VALUES(@RefNumber,@item,@Quotation_no,200,'Item is added in PA with special price.')
					 ELSE
					 INSERT INTO @table(Ref_Number,Item, Quotation_no, error_code,error_msg)
					 VALUES(@RefNumber,@item,@Quotation_no,200,'Quote request status is modified successfully.')

				END
				ELSE
				BEGIN
				
					INSERT INTO @table(Ref_Number,Item, Quotation_no, error_code,error_msg)
					 VALUES(@RefNumber,@item,CONCAT(@RefNumber,'/', @Status_Count),103,'Error: Error in updating data.')
				END
			END											
			ELSE
			BEGIN
				INSERT INTO @table(Ref_Number,Item, Quotation_no, error_code,error_msg)
					 VALUES(@RefNumber,@item, CONCAT(@RefNumber,'/', @Status_Count),102,'Error: Reference Number and Item code are not available in Quote table.')
			END
		END
		ELSE
		BEGIN
			INSERT INTO @table(Ref_Number,Item, Quotation_no, error_code,error_msg)
					 VALUES(@RefNumber,@item,CONCAT(@RefNumber,'/', @Status_Count),101,'Error: ID is not available in Quote table.')
		END
	END

	FETCH NEXT FROM item_cursor INTO @item;
	END;
	

	TRUNCATE TABLE #tbl_mail
	If(@Status='Accepted' OR @Status='Added In PA')
		INSERT INTO #tbl_mail(MailTo,MAilCC,Subject,Body)
		exec sp_getMailDetailsForQuote 'Quote','AddInPA',@RefNumber,@item
	ELSE

		INSERT INTO #tbl_mail(MailTo,MAilCC,Subject,Body)
		exec sp_getMailDetailsForQuote 'Quote','StatusChange',@RefNumber,@item

	Update @table SET  MailTo=t.MailTo,MAilCC=t.MAilCC,Subject=t.Subject,Body=t.Body, PDF_Flag=1
	FROM #tbl_mail t WHERE Ref_Number=@RefNumber

	
	FETCH NEXT FROM ref_cursor INTO @RefNumber;
	CLOSE item_cursor;
	DEALLOCATE item_cursor;

END
CLOSE ref_cursor;
DEALLOCATE ref_cursor;

Select * from @table

DROP TABLE #tbl_ref
DROP TABLE #tbl_mail

END

