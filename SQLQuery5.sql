USE [Taegutec_Sales_Budget]
GO

CREATE TABLE Quote_ref 
(
ID INT IDENTITY(1,1),
Ref_number VARCHAR(100),
Item_code VARCHAR(100),
Item_Desc VARCHAR(MAX),
WHS			VARCHAR(100),
Monthly_Qty	VARCHAR(100),
Yearly_QTY	VARCHAR(100),
QTY_perOrder 	VARCHAR(100),
Order_type		VARCHAR(100),
List_Price 	VARCHAR(100),
Expected_price	VARCHAR(100),
DC_rate	VARCHAR(100),
Cust_number	VARCHAR(100),
Cust_Name	VARCHAR(MAX),
Cust_SP	VARCHAR(100),
Comp_Name	VARCHAR(MAX),
Comp_Desc	VARCHAR(MAX),
Comp_SP	VARCHAR(100),
CP_number	VARCHAR(100),
CP_Name	VARCHAR(100),
Status	VARCHAR(100),
Requested_date DATETIME,
Approval_date DATETIME,
Rejected_date DATETIME
)


GO

/****** Object:  UserDefinedTableType [dbo].[BudgetSummary]    Script Date: 7/19/2019 11:41:05 AM ******/
CREATE TYPE [dbo].[QuoteTableType] AS TABLE(
	Ref_number VARCHAR(100),
Item_code VARCHAR(100),
Item_Desc VARCHAR(MAX),
WHS			VARCHAR(100),
Monthly_Qty	VARCHAR(100),
Yearly_QTY	VARCHAR(100),
QTY_perOrder 	VARCHAR(100),
Order_type		VARCHAR(100),
List_Price 	VARCHAR(100),
Expected_price	VARCHAR(100),
DC_rate	VARCHAR(100),
Cust_number	VARCHAR(100),
Cust_Name	VARCHAR(MAX),
Cust_SP	VARCHAR(100),
Comp_Name	VARCHAR(MAX),
Comp_Desc	VARCHAR(MAX),
Comp_SP	VARCHAR(100),
CP_number	VARCHAR(100),
CP_Name	VARCHAR(100),
Status	VARCHAR(100)
)
GO





ALTER PROCEDURE sp_saveQuotes(@tblQuote  [QuoteTableType] Readonly,@error_code INT OUTPUT, @error_msg VARCHAR(MAX) OUTPUT)
AS
BEGIN

set @error_code=100
set @error_msg='Initializing'

INSERT INTO Quote_ref 
(
Ref_number
,Item_code 
,Item_Desc 
,WHS			
,Monthly_Qty	
,Yearly_QTY	
,QTY_perOrder
,Order_type	
,List_Price 	
,Expected_price	
,DC_rate	
,Cust_number
,Cust_Name	
,Cust_SP	
,Comp_Name	
,Comp_Desc	
,Comp_SP	
,CP_number	
,CP_Name	
,Status
,Requested_DATE	
)
SELECT Ref_number
,Item_code 
,Item_Desc 
,WHS			
,Monthly_Qty	
,Yearly_QTY	
,QTY_perOrder
,Order_type	
,List_Price 	
,Expected_price	
,DC_rate	
,Cust_number
,Cust_Name	
,Cust_SP	
,Comp_Name	
,Comp_Desc	
,Comp_SP	
,CP_number	
,CP_Name	
,Status
,GETDATE()	FROM @tblQuote


set @error_code=0
set @error_msg='Success'

END