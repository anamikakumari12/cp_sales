USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_getEscalatedQuotes]    Script Date: 21-10-2020 15:45:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec sp_getEscalatedQuotes null,null,'BM','TTA','71'
ALTER PROCEDURE [dbo].[sp_getEscalatedQuotes]
(
@StartDate VARCHAR(100)=null,
@EndDate VARCHAR(100)=null,
@role VARCHAR(10),
@cter VARCHAR(10)=null,
@assigned_salesengineer_id VARCHAR(10)=null
)
AS
BEGIN

IF(@StartDate='')
SET @StartDate = null
IF(@EndDate='')
SET @EndDate = null
--if(@role='Desk')
--	SELECT * FROM Quote_ref 
--	WHERE Status IN ('Escalated') AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
--	AND UPPER(Assigned_To)=UPPER(@role)
--else 
if(@role='HO')
	SELECT qref.ID
,qref.Ref_number
,qref.Item_code
,qref.Item_Desc
,qref.WHS
,qref.Order_type
,qref.Order_frequency
,qref.Total_QTY
,qref.QTY_perOrder
,CONVERT(INT,CONVERT(decimal(38,2),qref.List_Price)) 'List_Price'
,qref.Expected_price
,qref.DC_rate
,qref.Discount_Price
,qref.Agreement_Price
,CONVERT(INT,CONVERT(decimal(38,2),qref.Offer_Price)) 'Offer_Price'
,qref.Cust_number
,qref.Cust_Name
,qref.Cust_SP
,qref.Comp_Name
,qref.Comp_Desc
,qref.Comp_SP
,qref.CP_number
,qref.CP_Name
,qref.MOQ
,qref.Status
,qref.Requested_date
,qref.Approval_date
,qref.Rejected_date
,qref.Approved_Rejected_By
,qref.Rejected_Reason
,qref.RequestedBy
,qref.RequestedBy_Flag
,qref.ReRequested_Date
,qref.ReRequested_By
,qref.Assigned_To
,qref.Accept_Visible_Flag
,qref.Multiple_Order_Flag
,CASE WHEN  qref.New_OfferPrice IS NULL OR  qref.New_OfferPrice=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.New_OfferPrice,0))) END New_OfferPrice
,CASE WHEN  qref.Recommended_Price IS NULL OR  qref.Recommended_Price=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.Recommended_Price,0))) END Recommended_Price
,qref.StatusChange_Count
,qref.Quotation_no
,qref.Order_Validity
,qref.Approved_OrderQty
,qref.Approved_OrderFreq,
	 Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy 
	,(Select StatusChange_Comment from tbl_QuoteStatusLog WHERE ID=(Select MAX(ID) from tbl_QuoteStatusLog WHERE Quote_ID=qref.ID )) 'StatusChange_Comment'
	,(Select End_Cust_name from tbl_QuoteStatusLog WHERE ID=(Select MAX(ID) from tbl_QuoteStatusLog WHERE Quote_ID=qref.ID and Status='Escalated' )) 'End_Customer'
	, xpc.ICS 'StockCode', 	CONVERT(INT,xpc.Price25) AverageSellingPrice, (Select CONVERT(INT,CONVERT(decimal(38,2), IVTV)) FROM tt_GALF6TA_II WHERE ICAT=qref.item_code) UnitPrice, 
	 CONVERT(INT,dbo.fn_getStock(qref.Item_code)) Stock 
	,CASE WHEN qref.Order_Validity is null OR qref.Order_Validity='' 
		THEN  (select CONVERT(INT,VALUE) from TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY')
		ELSE qref.Order_Validity END 'OrderValidity'
		,dbo.fn_getGP(qref.Ref_number,qref.Item_code) GP
,'' TotOrderValue
,'' CP
	FROM Quote_ref qref JOIN [dbo].[tt_GAL6TA_XPRICE] xpc ON qref.Item_code=xpc.ICAT
	WHERE 
		Status IN ('Escalated By BM') AND 
		--Status IN ('Escalated') AND 
		CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
		AND (CP_number IN (Select customer_number from tt_customer_master WHERE cter=ISNULL(@cter,'TTA')) 
			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE cter=ISNULL(@cter,'TTA'))) 
		AND UPPER(Assigned_To)=UPPER(@role)
else if(@role='BM')
	
	SELECT qref.ID
,qref.Ref_number
,qref.Item_code
,qref.Item_Desc
,qref.WHS
,qref.Order_type
,qref.Order_frequency
,qref.Total_QTY
,qref.QTY_perOrder
,CONVERT(INT,CONVERT(decimal(38,2),qref.List_Price)) 'List_Price'
,qref.Expected_price
,qref.DC_rate
,qref.Discount_Price
,qref.Agreement_Price
,CONVERT(INT,CONVERT(decimal(38,2),qref.Offer_Price)) 'Offer_Price'
,qref.Cust_number
,qref.Cust_Name
,qref.Cust_SP
,qref.Comp_Name
,qref.Comp_Desc
,qref.Comp_SP
,qref.CP_number
,qref.CP_Name
,qref.MOQ
,qref.Status
,qref.Requested_date
,qref.Approval_date
,qref.Rejected_date
,qref.Approved_Rejected_By
,qref.Rejected_Reason
,qref.RequestedBy
,qref.RequestedBy_Flag
,qref.ReRequested_Date
,qref.ReRequested_By
,qref.Assigned_To
,qref.Accept_Visible_Flag
,qref.Multiple_Order_Flag
,CASE WHEN  qref.New_OfferPrice IS NULL OR  qref.New_OfferPrice=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.New_OfferPrice,0))) END New_OfferPrice
,CASE WHEN  qref.Recommended_Price IS NULL OR  qref.Recommended_Price=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.Recommended_Price,0))) END Recommended_Price
,qref.StatusChange_Count
,qref.Quotation_no
,qref.Order_Validity
,qref.Approved_OrderQty
,qref.Approved_OrderFreq, Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy 
	,(Select StatusChange_Comment from tbl_QuoteStatusLog WHERE ID=(Select MAX(ID) from tbl_QuoteStatusLog WHERE Quote_ID=qref.ID )) 'StatusChange_Comment'
	,(Select End_Cust_name from tbl_QuoteStatusLog WHERE ID=(Select MAX(ID) from tbl_QuoteStatusLog WHERE Quote_ID=qref.ID and Status='Escalated')) 'End_Customer'
	, xpc.ICS 'StockCode'	, CONVERT(INT,xpc.Price25) AverageSellingPrice, (Select CONVERT(INT,CONVERT(decimal(38,2), IVTV)) FROM tt_GALF6TA_II WHERE ICAT=qref.item_code) UnitPrice, 
	 CONVERT(INT,dbo.fn_getStock(qref.Item_code)) Stock 
	,CASE WHEN qref.Order_Validity is null OR qref.Order_Validity='' 
		THEN  (select CONVERT(INT,VALUE) from TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY')
		ELSE qref.Order_Validity END 'OrderValidity'
		,dbo.fn_getGP(qref.Ref_number,qref.Item_code) GP
,'' TotOrderValue
,'' CP
	FROM Quote_ref qref JOIN [dbo].[tt_GAL6TA_XPRICE] xpc ON qref.Item_code=xpc.ICAT
	WHERE 
		Status IN ('Escalated')
		AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
		AND (CP_number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select BranchCode from LoginInfo where EngineerId=@assigned_salesengineer_id)) 
			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select BranchCode from LoginInfo where EngineerId=@assigned_salesengineer_id) )) 
		AND UPPER(Assigned_To)=UPPER(@role)
		AND qref.List_Price Is not null and qref.List_Price<>''
ELSE IF (@role='TM')

	SELECT qref.ID
,qref.Ref_number
,qref.Item_code
,qref.Item_Desc
,qref.WHS
,qref.Order_type
,qref.Order_frequency
,qref.Total_QTY
,qref.QTY_perOrder
,CONVERT(INT,CONVERT(decimal(38,2),qref.List_Price)) 'List_Price'
,qref.Expected_price
,qref.DC_rate
,qref.Discount_Price
,qref.Agreement_Price
,CONVERT(INT,CONVERT(decimal(38,2),qref.Offer_Price)) 'Offer_Price'
,qref.Cust_number
,qref.Cust_Name
,qref.Cust_SP
,qref.Comp_Name
,qref.Comp_Desc
,qref.Comp_SP
,qref.CP_number
,qref.CP_Name
,qref.MOQ
,qref.Status
,qref.Requested_date
,qref.Approval_date
,qref.Rejected_date
,qref.Approved_Rejected_By
,qref.Rejected_Reason
,qref.RequestedBy
,qref.RequestedBy_Flag
,qref.ReRequested_Date
,qref.ReRequested_By
,qref.Assigned_To
,qref.Accept_Visible_Flag
,qref.Multiple_Order_Flag
,CASE WHEN  qref.New_OfferPrice IS NULL OR  qref.New_OfferPrice=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.New_OfferPrice,0))) END New_OfferPrice
,CASE WHEN  qref.Recommended_Price IS NULL OR  qref.Recommended_Price=''  THEN '' else CONVERT(VARCHAR,CONVERT(INT,CONVERT(decimal(38,2), qref.Recommended_Price,0))) END Recommended_Price
,qref.StatusChange_Count
,qref.Quotation_no
,qref.Order_Validity
,qref.Approved_OrderQty
,qref.Approved_OrderFreq, Case WHEN RequestedBy_Flag='DISTRIBUTOR' THEN CONCAT(CP_Name,'(', CP_number,')') ELSE CONCAT((Select EngineerName from LoginInfo WHERE EngineerId=RequestedBy),'(',RequestedBy,')') END RaisedBy 
	,(Select StatusChange_Comment from tbl_QuoteStatusLog WHERE ID=(Select MAX(ID) from tbl_QuoteStatusLog WHERE Quote_ID=qref.ID )) 'StatusChange_Comment'
	,(Select End_Cust_name from tbl_QuoteStatusLog WHERE ID=(Select MAX(ID) from tbl_QuoteStatusLog WHERE Quote_ID=qref.ID and Status='Escalated')) 'End_Customer'
	, xpc.ICS 'StockCode'	, CONVERT(INT,xpc.Price25) AverageSellingPrice, (Select CONVERT(INT,CONVERT(decimal(38,2), IVTV)) FROM tt_GALF6TA_II WHERE ICAT=qref.item_code) UnitPrice, 
	 CONVERT(INT,dbo.fn_getStock(qref.Item_code)) Stock 
	,CASE WHEN qref.Order_Validity is null OR qref.Order_Validity='' 
		THEN  (select CONVERT(INT,VALUE) from TBL_CONFIGURATION WHERE MODULE='QUOTE EXPIRY')
		ELSE qref.Order_Validity END 'OrderValidity'
		,dbo.fn_getGP(qref.Ref_number,qref.Item_code) GP
,'' TotOrderValue
,'' CP
	FROM Quote_ref qref JOIN [dbo].[tt_GAL6TA_XPRICE] xpc ON qref.Item_code=xpc.ICAT
	WHERE 
		Status IN ('Escalated')
		AND CONVERT(DATE,Requested_date) >=ISNULL(CONVERT(DATE,@StartDate),Requested_date) AND CONVERT(DATE,Requested_date)<= ISNULL(CONVERT(DATE,@EndDate),Requested_date)
		AND (CP_number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select region_code from tt_territory_region_relation where territory_engineer_id=@assigned_salesengineer_id)) 
			OR Cust_Number IN (Select customer_number from tt_customer_master WHERE Customer_region =(select region_code from tt_territory_region_relation where territory_engineer_id=@assigned_salesengineer_id) )) 
		AND UPPER(Assigned_To)=UPPER('BM')

END





