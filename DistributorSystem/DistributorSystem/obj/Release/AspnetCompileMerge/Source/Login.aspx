﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="DistributorSystem.Login"  %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
 <head id="Head1" runat="server">
<title>Login ::-Distributor System-::</title>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
     <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
<link href="css/style.css" rel='stylesheet' type='text/css' />

 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.js"> </script>
<script src="js/modernizr.custom.js"></script>
     <script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(window).load(function () {
        $('.form-group-1 input').on('focus blur', function (e) {
            $(this).parents('.form-group-1').toggleClass('active', (e.type === 'focus' || this.value.length > 0));
        }).trigger('blur');
    });
    function validateControls() {
        var err_flag = 0;
        if ($('#MainContent_txtusername').val() == "") {
            err_flag = 1;
        }
        if (err_flag == 0) {
            $('#MainContent_lblError').text('');
            return true;

        }
        else {
            $('#MainContent_lblError').text('Please enter username.');
            return false;
        }
    }
    function message() {
        return "Username is required.";
    }
</script>
     <style type="text/css">
         .login-form {
  overflow: hidden;
  position: relative;
  padding: 40px;
}
         .logo_heading
         {
             color: #008244;
             font-size: 19.54px;
             margin: 16px 0 0;
             font-weight: 800;
             background: #f3f3f3;
                padding: 15px 0px;
                text-align: center;
         }
     </style>
</head>
<body class="mn_background">
    <div class="form-box">
        <p class="logo_heading">
            <%--Distributor System</p>--%>
        <img src="images/distribution-logo.png" style="width: 350px;" /></p>
        <form id="Form1" action="#" class="login-form" runat="server">
        <div class="form-group-1">
          <label class="label-control">
            <span class="label-text">UserName</span>
          </label>
            <asp:TextBox runat="server" ID="txtusername" CssClass="form-control-1"></asp:TextBox>
            <asp:RequiredFieldValidator ValidationGroup="VGLogin" ControlToValidate="txtusername" Display="None" runat="server" ID="rfvUsername" SetFocusOnError="true" ErrorMessage="Enter Username" ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ValidationGroup="VGRequest" ControlToValidate="txtusername" Display="None" runat="server" ID="rfvforrequest" SetFocusOnError="true" ErrorMessage="Enter Username" ForeColor="Red"></asp:RequiredFieldValidator>
            
        </div>
        <div class="form-group-1">
          <label class="label-control">
            <span class="label-text">Password</span>
          </label> 
             <asp:TextBox runat="server" ID="txtpassword" TextMode="Password" CssClass="form-control-1"></asp:TextBox>
                <asp:RequiredFieldValidator ValidationGroup="VGLogin" Display="None"  ControlToValidate="txtpassword" runat="server" ID="rfvpwd" SetFocusOnError="true" ErrorMessage="Password is required." ForeColor="Red"></asp:RequiredFieldValidator>
                            
        </div>
        <div class="form-group-1">
            <asp:ValidationSummary id="valSum" runat="server"  ValidationGroup="VGLogin" ForeColor="Red" />
            <asp:ValidationSummary id="ValidationSummary1" runat="server"  ForeColor="Red" ValidationGroup="VGRequest" />
        <asp:LinkButton ValidationGroup="VGRequest" Text="Request Password" ID="lnkRequest" runat="server"></asp:LinkButton>
        </div>
        <asp:Button ValidationGroup="VGLogin" runat="server" ID="btnLogin" CssClass="btn_Login" Text="Sign In" OnClick="btnLogin_Click"  />
    </form>
 
    </div>        
     </div>
	
		<div class="footer mn_footer">
		   <p>&copy; 2017 Distributor System. All Rights Reserved | Designed by <a href="#" target="_blank">KNS Technologies</a></p>
		</div>
</body>
</html>

