﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBO;
using DistributorSystemBL;
using System.Data;
using System.Web.Services;


namespace DistributorSystem
{
    public partial class ItemSubmittedOnSale : System.Web.UI.Page
    {
        #region Global Declaration
        SaleBO objSaleBO;
        SaleBL objSaleBL;
        CommonFunctions objCom = new CommonFunctions();
        public static int gridLoadedStatus;
        #endregion
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }

                if (!IsPostBack)
                {
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void BindGrid()
        {
            DataTable dtOutput = new DataTable();
            try
            {
                objSaleBO = new SaleBO();
                objSaleBL = new SaleBL();
                objSaleBO.cust_num = Convert.ToInt32(Session["DistributorNumber"]);
                dtOutput = objSaleBL.GetItemsSubmittedonSaleBL(objSaleBO);
                if(dtOutput !=null)
                {
                    if(dtOutput.Rows.Count>0)
                    {

                        grdItem.DataSource = dtOutput;
                        grdItem.DataBind();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "BindGridView();", true);
                    }
                    else
                    {
                        grdItem.DataSource = null;
                        grdItem.DataBind();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "$('#divError').attr(\"style\", \"display: block; \"); $('#divMsg').attr(\"style\", \"display:none;\"); ", true);

                    }
                }
                else
                {
                    grdItem.DataSource = null;
                    grdItem.DataBind();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "$('#divError').attr(\"style\", \"display: block; \"); $('#divMsg').attr(\"style\", \"display:none;\"); ", true);

                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
    }
}