﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BudgetEntry.aspx.cs" Inherits="DistributorSystem.BudgetEntry" MasterPageFile="~/MasterPage.Master" %>


<asp:Content ContentPlaceHolderID="head" runat="server">
    

<script type="text/javascript"  class="init">

    $(document).ready(function () {

        var head_content = $('#MainContent_grdEntry tr:first').html();
        $('#MainContent_grdEntry').prepend('<thead></thead>')
        $('#MainContent_grdEntry thead').html('<tr>' + head_content + '</tr>');
        $('#MainContent_grdEntry tbody tr:first').hide();

        //$('#MainContent_grdEntry').addClass('nowrap').dataTable({
        //    responsive: true,
        //    columnDefs: [
        //    { targets:[-1,-3], className:'dt-body-right'
        //    }]
        //});
        $('#MainContent_grdEntry').DataTable();
        //$('#MainContent_grdEntry').dataTable({
        //    "dom": 'T<"clear">lfrtip',
        //    "tableTools": {
        //        "sSwfPath": "swf/copy_csv_xls_pdf.swf"
        //    }
        //});
        
    });
</script>    <style>
        body
        {
            font-family:90%/1.45em "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif
        }
        .mn_margin
        {
            margin:10px 0 0 0;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
     <div class="col-md-12 mn_margin">
    <asp:Panel ID="Panel1" runat="server" CssClass="filter_panel">
        <div class="col-md-1">
            <asp:Label runat="server" ID="lblYear" Text="Year"></asp:Label>
        </div>
        <div class="col-md-2">
            <asp:TextBox runat="server" ID="txtYear"></asp:TextBox>
        </div>
        <div class="col-md-4">
            <asp:Button runat="server" ID="btnEntry" CssClass="btnSubmit" Text="Entry" OnClick="btnEntry_Click" />
             <asp:Button runat="server" ID="btnSave" CssClass="btnSubmit" Text="Save" OnClick="btnSave_Click" />
        </div>
    </asp:Panel>
        <asp:Panel runat="server" ID="pnlData" >
            <asp:GridView ID="grdEntry" CssClass="display compact" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Customer Number">
                        <ItemTemplate>
                            <asp:Label ID="lblCustomerNumber" runat="server" Text='<%#Bind("customernumber") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer Name">
                        <ItemTemplate>
                            <asp:Label ID="lblCustomerName" runat="server" Text='<%#Bind("customername") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </div>
</asp:Content>

