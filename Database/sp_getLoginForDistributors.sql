USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_getLoginForDistributors]    Script Date: 8/6/2019 9:41:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Anamika
-- ALTER date: 03/10/2017
-- Description:	Login Authentication for Distributor
-- =============================================
ALTER PROCEDURE [dbo].[sp_getLoginForDistributors]
(@UserId varchar(100),
 @Password varchar(MAX))
 AS
 BEGIN
 SET NOCOUNT ON;
 DECLARE @error_msg VARCHAR(MAX), @error_code INT
	IF EXISTS (SELECT * FROM tbl_LoginInfoForDistributor Where Status = 1 AND Distributor_number = @UserId)
	BEGIN
	print '1'
	IF EXISTS (SELECT * FROM tbl_LoginInfoForDistributor Where Password=@Password AND Distributor_number = @UserId)
		SELECT *,'' error_msg, 0 error_code
		from tbl_LoginInfoForDistributor

		where Distributor_number = @UserId AND Password=@Password
	ELSE
	BEGIN 
	SET @error_msg = 'User name and password do not match.' SET @error_code = 1
	SELECT '' Id,'' Distributor_number, '' Distributor_name ,'' Employee_name ,'' Email_id,'' Password, '' Role, @error_msg error_msg, @error_code error_code
	END

	END

	ELSE IF EXISTS (SELECT * FROM tbl_LoginInfoForDistributor Where Status = 0 AND Distributor_number = @UserId)
	BEGIN SET @error_msg = 'Your account is blocked' SET @error_code = 2
	SELECT '' Id,'' Distributor_number, '' Distributor_name ,'' Employee_name ,'' Email_id,'' Password, '' Role, @error_msg error_msg, @error_code error_code
	END

	ELSE
	BEGIN SET @error_msg = 'You do not have account in Sales budget Application.' SET @error_code = 2
	SELECT '' Id,'' Distributor_number, '' Distributor_name ,'' Employee_name ,'' Email_id,'' Password, '' Role, @error_msg error_msg, @error_code error_code
	END
 END





