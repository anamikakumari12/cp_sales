﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ItemSubmittedOnSale.aspx.cs" Inherits="DistributorSystem.ItemSubmittedOnSale" MasterPageFile="~/MasterPage.Master" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
    <script src="Scripts/jquery.modal.min.js"></script>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <script type="text/javascript" class="init">

        $(document).ready(function () {
        });



        function BindGridView() {
            var head_content = $('#MainContent_grdItem tr:first').html();
            $('#MainContent_grdItem').prepend('<thead></thead>')
            $('#MainContent_grdItem thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdItem tbody tr:first').hide();
            //$('#MainContent_grdItem').append('<tfoot><tr> <th colspan="2" style="text-align:right">Total:</th><th style="text-align: right">' +<%= Session["YearlyAmount"] %> +'</th><th style="text-align: right">' +<%= Session["last1yearAmount"] %> +'</th><th style="text-align: right">' +<%= Session["last2yearAmount"] %> +'</th></tr></tfoot>');
            $('#MainContent_grdItem').DataTable(
                {
                    "paging": false
                    // lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    //        "columnDefs": [
                    //{
                    //    "targets": [4],
                    //    "visible": false,
                    //    "searchable": false
                    //}]
                });
            $('#divMsg').attr("style", "display:block;");
            $('#divError').attr("style", "display:none;");
        }
        function isNumberKey(evt, obj) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            //if (value.length > 0) {
            //    sessionStorage.setItem("refresh_flag", "1");
            //}
            //else {
            //    sessionStorage.setItem("refresh_flag", "0");
            //}
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                     // Number of digits right of decimal point.
                     (match[1] ? match[1].length : 0)
                     // Adjust for scientific notation.
                     - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;

                if (charCode == 46) return false;
            }
            else {
                if (value.length > 11) {
                    if (charCode == 46) {
                        return true;
                    }
                    else return false;
                }

            }
            if (charCode == 46) {
                return true;
            }
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            else {
                return true;
            }
        }

    </script>
    <style>
        #MainContent_pnlData {
            margin-left: 10%;
            width: 80%;
        }

        .mn_popup {
            width: 60%;
            align-content: center;
            margin: 15%;
        }

        body {
            font-family: 90%/1.45em "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif;
        }

        table {
            border-color: white !important;
        }

        .mn_margin {
            margin: 10px 0 0 0;
        }
        /*th { white-space: nowrap; }*/
        .result {
            margin-left: 45%;
            font-weight: bold;
        }

        .mn_bott {
            margin: 0px 0 10px 0;
        }

        .btn-default.btn-on-2.active {
            background-color: #006681;
            color: white;
        }

        .btn-default.btn-off-2.active {
            background-color: #006681;
            color: white;
        }

        .btn-group {
            margin-bottom: 5px;
            margin-top: 5px;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="scriptmanager"></asp:ScriptManager>
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Flash Sale</a>
                        </li>
                        <li class="current">Item Submitted</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mn_margin">
        <asp:Panel ID="Panel1" runat="server" CssClass="filter_panel">
            <div class="col-md-12 nopadding" id="divMsg" style="display: none;">
                THIS IS A PROVISIONAL ACCEPTANCE . FINAL CONFIMATION WILL BE SENT AFTER VERFICATION
            </div>
            <div class="col-md-12 nopadding" id="divError">
                NO ITEM REQUESTED FOR ORDER IN FIRE SALE.
            </div>
        </asp:Panel>
        <asp:Label runat="server" CssClass="result" ID="lblResult"></asp:Label>

        <asp:Panel runat="server" ID="pnlData">
            <asp:GridView ID="grdItem" CssClass="display compact" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Item Code">
                        <ItemTemplate>
                            <asp:Label ID="lblitemCode" runat="server" Text='<%#Bind("item_code") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item Desc">
                        <ItemTemplate>
                            <asp:Label ID="lblitemDesc" runat="server" Text='<%#Bind("Item_desc") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Quantity Requested For Order">
                        <ItemTemplate>
                            <div style="text-align: right">
                                <asp:Label ID="lblqty" Style="text-align: right" runat="server" Text='<%#Bind("Qty_ordered") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sale Price">
                        <ItemTemplate>
                            <div style="text-align: right">
                                <asp:Label ID="lblsale_price" Style="text-align: right" runat="server" Text='<%#Bind("Sale_price") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Order Requested On">
                        <ItemTemplate>
                            <div style="text-align: right">
                                <asp:Label ID="lblorderOn" Style="text-align: right" runat="server" Text='<%#Bind("order_placed_on") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>

        </asp:Panel>
    </div>
</asp:Content>


