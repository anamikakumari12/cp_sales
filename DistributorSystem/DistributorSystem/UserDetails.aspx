﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserDetails.aspx.cs" Inherits="DistributorSystem.UserDetails" MasterPageFile="~/AdminMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--    <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>--%>
    <%--<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>--%>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
    <script type="text/javascript" class="init">

        $(document).ready(function () {

            var head_content = $('#MainContent_grdUserDetails tr:first').html();
            $('#MainContent_grdUserDetails').prepend('<thead></thead>')
            $('#MainContent_grdUserDetails thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdUserDetails tbody tr:first').hide();
            $('#MainContent_grdUserDetails').DataTable(
                {
                });

        });
        function bindGridView() {
            debugger;
            var head_content = $('#MainContent_grdUserDetails tr:first').html();
            $('#MainContent_grdUserDetails').prepend('<thead></thead>')
            $('#MainContent_grdUserDetails thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdUserDetails tbody tr:first').hide();
            $('#MainContent_grdUserDetails').DataTable(
                {
                });
        }

        function isNumberKey(evt, obj) {

            debugger;
            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                     // Number of digits right of decimal point.
                     (match[1] ? match[1].length : 0)
                     // Adjust for scientific notation.
                     - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;

                if (charCode == 46) return false;
            }
            else {
                if (value.length > 2) {
                    if (charCode == 46) return true;
                    else return false;
                }

            }
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function openwindow() {
            $("#dialogwindow").dialog("open");
        }

        function checkPasswordMatch() {
            var pwd = $('#MainContent_txtpwd').val();
            var repwd = $('#MainContent_txtrepwd').val();
            if (pwd == repwd) {
                $('#MainContent_lblpwd').text('');
                return true;

            }
            else {
                $('#MainContent_lblpwd').text('Password does not match.');
                return false;
            }
        }

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        function validateControls() {
            //debugger
            var err_flag = 0;

            if ($('#MainContent_txtpwd').val() == "") {
                $('#MainContent_txtpwd').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtpwd').css('border-color', '');
            }

            if ($('#MainContent_txtrepwd').val() == "") {
                $('#MainContent_txtrepwd').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtrepwd').css('border-color', '');
            }

            if ($('#MainContent_txtEmailId').val() == "") {
                $('#MainContent_txtEmailId').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                if (!validateEmail($('#MainContent_txtEmailId').val())) {
                    $('#MainContent_txtEmailId').css('border-color', 'red');

                    err_flag = 1;
                }
                else
                    $('#MainContent_txtEmailId').css('border-color', '');
            }


            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return checkPasswordMatch();
            }

            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields/valid data.');
                return false;
            }
        }
    </script>
    <style>
        .mn_popup
        {
            width: 60%;
            align-content: center;
            margin: 15%;
        }

        body
        {
            font-family: 90%/1.45em "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif;
        }

        table
        {
            border-color: white!important;
        }

        .mn_margin
        {
            margin: 10px 0 0 0;
        }
        /*th { white-space: nowrap; }*/
        .result
        {
            margin-left: 45%;
            font-weight: bold;
        }

        #pnlData
        {
            width: 70%;
        }

        .control
        {
            padding: 9px;
            border-bottom: solid 1px #ddd;
            /*background-color: #eaeaea;*/
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="scriptmanager"></asp:ScriptManager>
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Users</a>
                        </li>
                        <li class="current">User Management</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>

            <div class="col-md-12 filter_panel">
                  <asp:Label ID="Label1" style="font-weight: bolder;" runat="server">User Details : </asp:Label>
               <%-- <div class="col-md-3">
                    <asp:Button runat="server" ID="btnEdit" CssClass="btnSubmit" OnClick="btnEdit_Click" Text="Edit" />

                    <asp:Button runat="server" ID="btnSave" CssClass="btnSubmit" OnClick="btnSave_Click" Text="Save" />
                </div>--%>
            </div>
            <div class="col-md-8 mn_margin">
              

                <asp:Panel runat="server" ID="pnlData">
                    <asp:GridView ID="grdUserDetails" CssClass="display compact" runat="server" AutoGenerateColumns="false" OnRowCommand="grdUserDetails_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="User Id">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerNumber" runat="server" Text='<%#Bind("Distributor_number") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerName" runat="server" Text='<%#Bind("Distributor_name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email Id">
                                <ItemTemplate>
                                    <asp:Label ID="txtEmail" runat="server" Text='<%#Bind("Email_id") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Bind("Status") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User Role">
                                <ItemTemplate>
                                    <asp:Label ID="lblRole" runat="server" Text='<%#Bind("Role") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ToolTip="Edit" Width="20px" ImageUrl="images/edit-icon.png" ID="imgAction" CommandArgument='<%# Bind("Distributor_number") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </div>
            <div class="col-md-4 mn_margin" style="margin-top: 4%;">

                <asp:Panel runat="server" ID="pnlEdit">
                    <div class="col-md-12 filter_panel">
                        <asp:Label runat="server" style="font-weight: bolder;" Text="User Details Update"></asp:Label>
                    </div>
                    <div class="col-md-12 nopad">
                        <div class="col-md-12 control">
                            <div class="col-md-6">
                                <asp:Label runat="server" Text="User Id : "></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:Label ID="lblDistributorNumber" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-12 control">
                            <div class="col-md-6">
                                <asp:Label runat="server" Text="User Name : "></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:Label runat="server" ID="lblDistributorName"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-12 control">
                            <div class="col-md-6">
                                <asp:Label runat="server" ID="Label3" Text="Email Id : "></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtEmailId"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-12 control">
                            <div class="col-md-6">
                                <asp:Label runat="server" Text="Password : "></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtpwd"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-12 control">
                            <div class="col-md-6">
                                <asp:Label runat="server" Text="Re-Type Password : "></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtrepwd"></asp:TextBox>
                                <asp:Label runat="server" ID="lblpwd" ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-12 control">
                            <div class="col-md-6">
                                <asp:Label runat="server" ID="Label4" Text="Status : "></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:CheckBox runat="server" ID="chkStatus" Text="Active" />
                            </div>
                        </div>
                        <div class="col-md-12 control">
                            <div class="col-md-6">
                                <asp:Label runat="server" ID="Label5" Text="Role : "></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:CheckBox runat="server" ID="chkRole" Text="Admin" />
                            </div>
                        </div>
                        <div class="col-md-12" style="padding:9px;">
                            <div class="col-md-6">
                            <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label> </div>
                            <div class="col-md-6">
                            <asp:Button runat="server" ID="btnUpdate" Text="Update" CssClass="btnSubmit" OnClick="btnUpdate_Click" OnClientClick="return validateControls();" />
                                </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
          </ContentTemplate>
      <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
            <asp:PostBackTrigger ControlID="grdUserDetails"/>
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>
