﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DistributorSystemBO
{
    public class ReviewBO
    {
        public int year { get; set; }
        public int error_code { get; set; }
        public string error_msg { get; set; }
        public string Distributor_name { get; set; }
        public string Distributor_number { get; set; }
        public string Flag { get; set; }
        public string Cter { get; set; }
        public int Valuein { get; set; }
        public string Branchcode { get; set; }
        public string Salesengineer { get; set; }
        public string Item_familyname { get; set; }
        public string Item_subfamilyname { get; set; }
        public string Customer_type { get; set; }
        public string Productgroup { get; set; }
        public string Item_code { get; set; }
        public string customer_number { get; set; }
        public string Item_familyID { get; set; }
    }
}
