﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DistributorSystemBO
{
    public class FileUploadBO
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string UploadDate { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string doctype { get; set; }
        public string salesName { get; set; }
        public int insertflag { get; set; }
        public int file_id { get; set; }
        public int err_code { get; set; }
        public string err_msg { get; set; }
    }
}
