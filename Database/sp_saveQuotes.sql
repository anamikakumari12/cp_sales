USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_saveQuotes]    Script Date: 9/9/2019 10:52:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_saveQuotes](@tblQuote  [QuoteTableType] Readonly,@error_code INT OUTPUT, @error_msg VARCHAR(MAX) OUTPUT, @RefNumber VARCHAR(100) OUTPUT)
AS
BEGIN

set @error_code=100
set @error_msg='Initializing'

--DECLARE @RefNum VARCHAR(100)
DECLARE @CPNUM VARCHAR(100)
DECLARE @ID VARCHAR(50)
DECLARE @FLAG VARCHAR(100)
DECLARE @CUST VARCHAR(100)
select @ID=right(Ref_number, charindex('_', reverse(Ref_number) + '_') - 1)
 From Quote_ref WHERE ID=(select MAX(ID) FROM Quote_ref)
 if(@ID is null OR @ID='')
	SET @ID=1
ELSE
	SET @ID=CONVERT(INT,@ID)+1
SELECT top 1 @RefNumber=Ref_number, @CPNUM=CP_number, @FLAG=RequestedBy_Flag, @CUST=Cust_number FROM @tblQuote 
	IF(@FLAG='DISTRIBUTOR')
	BEGIN
		IF(@RefNumber is null OR @RefNumber='')
		SET @RefNumber='RN_'+@CPNUM+'_'+CONVERT(VARCHAR(20),CONVERT(Date,GETDATE()))+'_'+@ID
	END
	ELSE
	BEGIN
		IF(@RefNumber is null OR @RefNumber='')
		SET @RefNumber='RN_'+@CUST+'_'+CONVERT(VARCHAR(20),CONVERT(Date,GETDATE()))+'_'+@ID
	END

INSERT INTO Quote_ref 
(
Ref_number
,Item_code 
,Item_Desc 
,WHS			
,Order_type	
,Order_frequency
,Total_QTY
,QTY_perOrder
,List_Price 	
,Expected_price	
,DC_rate	
,Cust_number
,Cust_Name	
,Cust_SP	
,Comp_Name	
,Comp_Desc	
,Comp_SP	
,CP_number	
,CP_Name	
,Status
,Requested_DATE	
,RequestedBy
,RequestedBy_Flag
,Discount_Price
,Agreement_Price
)
SELECT @RefNumber
,Item_code 
,Item_Desc 
,WHS			
,Order_type	
,Order_frequency
,Total_QTY
,QTY_perOrder
,List_Price 	
,Expected_price	
,DC_rate	
,Cust_number
,Cust_Name	
,Cust_SP	
,Comp_Name	
,Comp_Desc	
,Comp_SP	
,CP_number	
,CP_Name	
,Status
,GETDATE()
,RequestedBy
,RequestedBy_Flag
,dbo.fn_getDiscountPrice(Cust_number,CP_number,Item_code, List_Price)
,dbo.fn_getAgreementPrice(Cust_number,CP_number,Item_code)
FROM @tblQuote

INSERT INTO tbl_QuoteStatusLog(Quote_ID,Ref_Number,Item_code,Status,StatusChangeBy_Flag,StatusChangeBy_Id,StatusChangeDate)
SELECT ID, Ref_number, Item_code,Status,RequestedBy_Flag, RequestedBy,GETDATE()  FROM Quote_ref WHERE Ref_number=@RefNumber

UPDATE Quote_ref SET Offer_Price=Case WHEN Expected_Price>ISNULL(Discount_Price,0) THEN Expected_Price ELSE Discount_Price END
WHERE Ref_number=@RefNumber

UPDATE Quote_ref SET Assigned_To=dbo.fn_getAssignedTo(ID)
WHERE Ref_number=@RefNumber

UPDATE Quote_ref SET Accept_Visible_Flag = CASE WHEN ISNULL(Discount_Price,0.0)='0.0' THEN 0 ELSE 1 END
WHERE Ref_number=@RefNumber


set @error_code=0
set @error_msg='Success'

END

