﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBL;
using DistributorSystemBO;
//using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;

namespace DistributorSystem
{
    public partial class QuoteRequest : System.Web.UI.Page
    {
        #region GlobalDeclaration
        CommonFunctions objCom = new CommonFunctions();
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }

                if (!IsPostBack)
                {
                    QuoteBL objquoteBL = new QuoteBL();
                    QuoteBO objQuoteBO = new QuoteBO();
                    objQuoteBO.CP_Number = Convert.ToString(Session["DistributorNumber"]);
                    DataTable dt = new DataTable();
                    dt = objquoteBL.GetItemDescBL(objQuoteBO);
                    Session["ItemTable"] = dt;
                    dt = new DataTable();

                    dt = objquoteBL.GetCustomerListBL(objQuoteBO);
                    Session["dtCustForQuote"] = dt;
                    dt = new DataTable();
                    dt = objquoteBL.GetCompetitorsBL();
                    Session["dtCompetitors"] = dt;

                    BindGrid(objQuoteBO);
                    BindFrequentItems();
                    BindRecentItems();
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        #endregion

        #region Methods
        public static void CreateWorkbook(System.Data.DataTable dt, String path)
        {
            int rowindex = 0;
            int columnindex = 0;

            Microsoft.Office.Interop.Excel.Application wapp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Worksheet wsheet;
            Microsoft.Office.Interop.Excel.Workbook wbook;

            wapp.Visible = false;

            wbook = wapp.Workbooks.Add(true);
            wsheet = (Microsoft.Office.Interop.Excel.Worksheet)wbook.ActiveSheet;


            try
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    wsheet.Cells[1, i + 1] = dt.Columns[i].ColumnName;

                }

                foreach (DataRow row in dt.Rows)
                {
                    rowindex++;
                    columnindex = 0;
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnindex++;
                        wsheet.Cells[rowindex + 1, columnindex] = row[col.ColumnName];
                    }
                }
            }
            catch (Exception ex)
            {
                String err = ex.Message;
            }
            wapp.UserControl = true;

            wbook.SaveAs(path, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing,
            false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
            Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            wbook.Close(null, null, null);
        }

        private void BindRecentItems()
        {
            try
            {
                QuoteBL objquoteBL = new QuoteBL();
                DataTable dtRecitem = new DataTable();
                QuoteBO objQuoteBO = new QuoteBO();
                objQuoteBO.CP_Number = Convert.ToString(Session["DistributorNumber"]);
                dtRecitem = objquoteBL.GetRecentItemsBL(objQuoteBO);
                if (dtRecitem != null)
                {
                    Session["dtRecitem"] = dtRecitem;
                    if (dtRecitem.Rows.Count > 0)
                    {
                        recentList.Style.Add("display", "block");
                        divOrders.Style.Add("display", "block");
                        Label itemControl;
                        Label descControl;
                        for (int i = 0; i < dtRecitem.Rows.Count; i++)
                        {
                            itemControl = (Label)divRecent.Parent.FindControl("lblOItem" + (i + 1));
                            descControl = (Label)divRecent.Parent.FindControl("lblODesc" + (i + 1));
                            if (itemControl != null)
                            {
                                itemControl.Text = Convert.ToString(dtRecitem.Rows[i]["item"]);
                                descControl.Text = Convert.ToString(dtRecitem.Rows[i]["item_desc"]);
                            }
                        }
                    }
                    else
                    {
                        recentList.Style.Add("display", "none");
                    }
                }
                else
                {
                    recentList.Style.Add("display", "none");
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void BindFrequentItems()
        {
            try
            {
                QuoteBL objquoteBL = new QuoteBL();
                DataTable dtFreqitem = new DataTable();
                QuoteBO objQuoteBO = new QuoteBO();
                objQuoteBO.CP_Number = Convert.ToString(Session["DistributorNumber"]);
                dtFreqitem = objquoteBL.GetFrequentItemsBL(objQuoteBO);
                if (dtFreqitem != null)
                {
                    if (dtFreqitem.Rows.Count > 0)
                    {
                        Session["dtFreqitem"] = dtFreqitem;
                        freqList.Style.Add("display", "block");
                        divOrders.Style.Add("display", "block");
                        Label itemControl;
                        Label descControl;
                        for (int i = 0; i < dtFreqitem.Rows.Count; i++)
                        {
                            itemControl = (Label)divFrequent.Parent.FindControl("lblItem" + (i + 1));
                            descControl = (Label)divFrequent.Parent.FindControl("lblDesc" + (i + 1));
                            if (itemControl != null)
                            {
                                itemControl.Text = Convert.ToString(dtFreqitem.Rows[i]["item"]);
                                descControl.Text = Convert.ToString(dtFreqitem.Rows[i]["item_desc"]);
                            }
                        }
                    }
                    else
                    {
                        divOrders.Style.Add("display", "none");
                        freqList.Style.Add("display", "none");
                    }
                }
                else
                {
                    divOrders.Style.Add("display", "none");
                    freqList.Style.Add("display", "none");
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void BindGrid(QuoteBO objQuoteBO)
        {
            try
            {
                DataSet ds = GetData(objQuoteBO);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        grdPriceRequest.DataSource = ds;
                        grdPriceRequest.DataBind();
                    }
                    else
                    {  //adding empty row
                        ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                        grdPriceRequest.DataSource = ds;
                        grdPriceRequest.DataBind();
                        //grdPriceRequest.Rows[0].Visible = false;
                    }
                    ViewState["CurrentTable"] = ds.Tables[0];
                }
                else
                {
                    ViewState["CurrentTable"] = null;
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private DataSet GetData(QuoteBO objQuoteBO)
        {
            DataSet ds = new DataSet();
            try
            {
                QuoteBL objQuoteBL = new QuoteBL();
                DataTable dtDraft = new DataTable();
                dtDraft = objQuoteBL.GetDraftedQuoteBL(objQuoteBO);
                if (dtDraft != null)
                {
                    if (dtDraft.Rows.Count > 0)
                    {
                        ds.Tables.Add(dtDraft);
                    }
                    else
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("Id", typeof(Int32));
                        dt.Columns.Add(new DataColumn("ItemNumber", typeof(string)));
                        dt.Columns.Add(new DataColumn("ItemDescription", typeof(string)));
                        dt.Columns.Add(new DataColumn("WHS", typeof(string)));
                        dt.Columns.Add(new DataColumn("Order_Type", typeof(string)));
                        dt.Columns.Add(new DataColumn("Order_Freq", typeof(string)));
                        dt.Columns.Add(new DataColumn("Total_QTY", typeof(string)));
                        dt.Columns.Add(new DataColumn("QTYPO", typeof(string)));
                        dt.Columns.Add(new DataColumn("DLP", typeof(string)));
                        dt.Columns.Add(new DataColumn("TargetPrice", typeof(double)));
                        dt.Columns.Add(new DataColumn("DCRate", typeof(string)));
                        dt.Columns.Add(new DataColumn("CustName", typeof(string)));
                        dt.Columns.Add(new DataColumn("CustSP", typeof(string)));
                        dt.Columns.Add(new DataColumn("CompanyName", typeof(string)));
                        dt.Columns.Add(new DataColumn("Description", typeof(string)));
                        dt.Columns.Add(new DataColumn("CompanySP", typeof(string)));
                        ds.Tables.Add(dt);
                    }
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Id", typeof(Int32));
                    dt.Columns.Add(new DataColumn("ItemNumber", typeof(string)));
                    dt.Columns.Add(new DataColumn("ItemDescription", typeof(string)));
                    dt.Columns.Add(new DataColumn("WHS", typeof(string)));
                    dt.Columns.Add(new DataColumn("Order_Type", typeof(string)));
                    dt.Columns.Add(new DataColumn("Order_Freq", typeof(string)));
                    dt.Columns.Add(new DataColumn("Total_QTY", typeof(string)));
                    dt.Columns.Add(new DataColumn("QTYPO", typeof(string)));
                    dt.Columns.Add(new DataColumn("DLP", typeof(string)));
                    dt.Columns.Add(new DataColumn("TargetPrice", typeof(double)));
                    dt.Columns.Add(new DataColumn("DCRate", typeof(string)));
                    dt.Columns.Add(new DataColumn("CustName", typeof(string)));
                    dt.Columns.Add(new DataColumn("CustSP", typeof(string)));
                    dt.Columns.Add(new DataColumn("CompanyName", typeof(string)));
                    dt.Columns.Add(new DataColumn("Description", typeof(string)));
                    dt.Columns.Add(new DataColumn("CompanySP", typeof(string)));
                    ds.Tables.Add(dt);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return ds;
        }

        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        //if (Convert.ToString(table.Columns[j].ColumnName) == "SALES_MTD_VALUE")
                        //{
                        //    if (j < table.Columns.Count - 1)
                        //    {
                        //        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + table.Rows[i][j].ToString() + ",");
                        //    }
                        //    else if (j == table.Columns.Count - 1)
                        //    {
                        //        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":"  + table.Rows[i][j].ToString() );
                        //    }
                        //}
                        //else
                        //{ 
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString().Replace('"', ' ') + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString().Replace('"', ' ') + "\"");
                        }
                        //}
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }


        public static void LoadDatatocsv(DataTable dt)
        {
            try
            {

                string filepath = ConfigurationManager.AppSettings["QuoteRequestLog"] + Convert.ToString("_" + Convert.ToString(HttpContext.Current.Session["DistributorNumber"]) + "_" + Convert.ToString(DateTime.Now.ToString("yyyyMMddTHHmmss")));
                StringBuilder sb = new StringBuilder();

                string[] columnNames = dt.Columns.Cast<DataColumn>().
                                                  Select(column => column.ColumnName).
                                                  ToArray();
                sb.AppendLine(string.Join(",", columnNames));

                foreach (DataRow row in dt.Rows)
                {
                    string[] fields = row.ItemArray.Select(field => field.ToString()).
                                                    ToArray();
                    sb.AppendLine(string.Join(",", fields));
                }

                File.WriteAllText(filepath + ".csv", sb.ToString());
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        #endregion

        #region WebMethods

        [WebMethod]
        public static List<ItemClass> LoadDetailedGrid()
        {
            List<ItemClass> lists = new List<ItemClass>();
            try
            {
                DataTable dt = (DataTable)HttpContext.Current.Session["ItemTable"];
                foreach (DataRow dtrow in dt.Rows)
                {
                    ItemClass items = new ItemClass();
                    //items.name = Convert.ToString(dtrow["item"]);
                    //items.item_desc = Convert.ToString(dtrow["item_desc"]);

                    lists.Add(items);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
            }
            return lists;
        }

        [WebMethod]
        public static List<itemddl> LoadItems()
        {
            List<itemddl> lists = new List<itemddl>();
            try
            {
                DataTable dt = (DataTable)HttpContext.Current.Session["ItemTable"];
                itemddl items;
                items = new itemddl();
                items.id = "0";
                items.text = "--Select--";
                lists.Add(items);
                foreach (DataRow dtrow in dt.Rows)
                {
                    items = new itemddl();
                    items.id = Convert.ToString(dtrow["item"]);
                    items.text = string.Concat(Convert.ToString(dtrow["item_desc"]), "(", Convert.ToString(dtrow["item"]), ")");

                    lists.Add(items);
                }


            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
            }
            return lists;
        }

        [WebMethod]
        public static ItemClass LoadItemDetails(string item)
        {
            ItemClass lists = new ItemClass();
            QuoteBL objquoteBL = new QuoteBL();
            DataTable dt = new DataTable();
            QuoteBO objQuoteBO = new QuoteBO();
            try
            {
                //DataTable dt = (DataTable)HttpContext.Current.Session["ItemTable"];
                objQuoteBO.Item_Number = Convert.ToString(item);
                objQuoteBO.CP_Number = Convert.ToString(HttpContext.Current.Session["DistributorNumber"]);
                dt = objquoteBL.GetItemDetailsBL(objQuoteBO);
                if (dt.Rows.Count > 0)
                {
                    lists.item = item;
                    lists.WHS = Convert.ToString(dt.Rows[0]["WHS"]);
                    lists.LP = Convert.ToString(dt.Rows[0]["ListPrice"]);
                    lists.AP = Convert.ToString(dt.Rows[0]["AgreementPrice"]);
                    lists.stockCode = Convert.ToInt32(dt.Rows[0]["StockCode"]);
                    lists.item = Convert.ToString(dt.Rows[0]["item"]);
                    lists.item_desc = Convert.ToString(dt.Rows[0]["item_desc"]);
                    lists.quantity=Convert.ToString(dt.Rows[0]["BreakQty"]);
                    lists.IPACK = Convert.ToString(dt.Rows[0]["IPACK"]);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
            }
            return lists;
        }

        [WebMethod]
        public static string DeleteQuote(string item)
        {
            QuoteBL objquoteBL = new QuoteBL();
            DataTable dt = new DataTable();
            string res = "";
            try
            {
                string errCode = objquoteBL.DeleteQuotes(item);
                if (errCode == "0")
                {
                    res = "Success";
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
            }
            return res;
        }

        [WebMethod]
        public static List<itemddl> LoadCustomers()
        {
            List<itemddl> lists = new List<itemddl>();
            try
            {
                DataTable dt = (DataTable)HttpContext.Current.Session["dtCustForQuote"];
                foreach (DataRow dtrow in dt.Rows)
                {
                    itemddl items = new itemddl();
                    items.id = Convert.ToString(dtrow["customernumber"]);
                    items.text = Convert.ToString(dtrow["customername"]);
                    lists.Add(items);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
            }
            return lists;
        }

        [WebMethod]
        public static List<itemddl> LoadCompetitors()
        {
            List<itemddl> lists = new List<itemddl>();
            itemddl items;
            try
            {
                DataTable dt = (DataTable)HttpContext.Current.Session["dtCompetitors"];
                items = new itemddl();
                items.id = "0";
                items.text = "--Select--";
                lists.Add(items);
                foreach (DataRow dtrow in dt.Rows)
                {
                    items = new itemddl();
                    items.id = Convert.ToString(dtrow["Competitor_Name"]);
                    items.text = Convert.ToString(dtrow["Competitor_Name"]);
                    lists.Add(items);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
            }
            return lists;
        }

        [WebMethod]
        public static Output GetItemDetailsForTable(List<itemDetails> obj)
        {
            CommonFunctions objCom = new CommonFunctions();
            Output objOutput = new Output();
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["DistributorNumber"])))
                {
                    foreach (itemDetails item in obj)
                    {
                        item.CP_number = Convert.ToString(HttpContext.Current.Session["DistributorNumber"]);
                        item.CP_Name = Convert.ToString(HttpContext.Current.Session["DistributorName"]);
                        item.Status = "Sent For Approval";
                        item.RequestedBy = Convert.ToString(HttpContext.Current.Session["UserId"]);
                        item.RequestedBy_Flag = "DISTRIBUTOR";
                    }
                    DataTable dt = ToDataTable<itemDetails>(obj);
                    LoadDatatocsv(dt);
                    QuoteBL objQuoteBL = new QuoteBL();
                    QuoteBO objQuoteBO = new QuoteBO();
                    objQuoteBO = objQuoteBL.SaveQuotesBL(dt);

                    if (objQuoteBO.Err_code == 0)
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(objQuoteBO.to)))
                        {
                            //string attachment = GenerateQuoteFormat(objQuoteBO.Ref_Number);
                            EmailDetails objEmail = new EmailDetails();
                            objEmail.toMailId = objQuoteBO.to;
                            objEmail.ccMailId = objQuoteBO.cc;
                            objEmail.subject = objQuoteBO.subject;
                            objEmail.body = objQuoteBO.message;
                            //objEmail.attachment = attachment;
                            //objCom.SendMail(objEmail);
                            CommonFunctions.SendGridMail(objEmail).Wait();
                        }
                        objOutput.ErrorCode = 200;
                        objOutput.ErrorMsg = "Requests for quotes are submitted successfully.";
                    }
                    else
                    {
                        objOutput.ErrorCode = 201;
                        objOutput.ErrorMsg = "There is error in saving data. Please try again." + objQuoteBO.Err_msg;
                    }
                }
                else
                {
                    objOutput.ErrorCode = 201;
                    objOutput.ErrorMsg = "Session Time out.";
                }


            }
            catch (Exception ex)
            {
                objOutput.ErrorCode = 201;
                objOutput.ErrorMsg = "There is error in saving data. Please try again.";
                objCom.ErrorLog(ex);
            }
            return objOutput;
        }


        [WebMethod]
        public static Output SaveAsDraft(List<itemDetails> obj)
        {
            CommonFunctions objCom = new CommonFunctions();
            Output objOutput = new Output();
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["DistributorNumber"])))
                {
                    foreach (itemDetails item in obj)
                    {
                        item.CP_number = Convert.ToString(HttpContext.Current.Session["DistributorNumber"]);
                        item.CP_Name = Convert.ToString(HttpContext.Current.Session["DistributorName"]);
                        item.Status = "Sent For Approval";
                        item.RequestedBy = Convert.ToString(HttpContext.Current.Session["UserId"]);
                        item.RequestedBy_Flag = "DISTRIBUTOR";
                    }
                    DataTable dt = ToDataTable<itemDetails>(obj);
                    LoadDatatocsv(dt);
                    QuoteBL objQuoteBL = new QuoteBL();
                    QuoteBO objQuoteBO = new QuoteBO();
                    objQuoteBO = objQuoteBL.DraftQuotesBL(dt);

                    if (objQuoteBO.Err_code == 0)
                    {
                        
                        objOutput.ErrorCode = 200;
                        objOutput.ErrorMsg = "Requests for quotes are saved successfully.";
                    }
                    else
                    {
                        objOutput.ErrorCode = 201;
                        objOutput.ErrorMsg = "There is error in saving data. Please try again." + objQuoteBO.Err_msg;
                    }
                }
                else
                {
                    objOutput.ErrorCode = 201;
                    objOutput.ErrorMsg = "Session Time out.";
                }


            }
            catch (Exception ex)
            {
                objOutput.ErrorCode = 201;
                objOutput.ErrorMsg = "There is error in saving data. Please try again.";
                objCom.ErrorLog(ex);
            }
            return objOutput;
        }
        [WebMethod]
        public static itemDetails LoadDetailsFromlink(string item, string type)
        {
            itemDetails obj = new itemDetails();
            DataTable dtoutput = new DataTable();
            try
            {
                if (type == "F")
                {
                    dtoutput = (DataTable)HttpContext.Current.Session["dtFreqitem"];
                }
                else if (type == "R")
                {
                    dtoutput = (DataTable)HttpContext.Current.Session["dtRecitem"];
                }
                if (dtoutput.Rows.Count > 0)
                {
                    var rows = from row in dtoutput.AsEnumerable()
                               where row.Field<string>("item").Trim() == item
                               select row;
                    dtoutput = rows.CopyToDataTable();
                }
                if (dtoutput.Rows.Count > 0)
                {
                    obj.Item_code = Convert.ToString(dtoutput.Rows[0]["item"]);
                    obj.WHS = Convert.ToString(dtoutput.Rows[0]["WHS"]);
                    obj.Total_QTY = Convert.ToString(dtoutput.Rows[0]["Total_QTY"]);
                    obj.Order_freq = Convert.ToString(dtoutput.Rows[0]["Order_frequency"]);
                    obj.QTY_perOrder = Convert.ToString(dtoutput.Rows[0]["QTY_perOrder"]);
                    obj.Order_type = Convert.ToString(dtoutput.Rows[0]["Order_type"]);
                    obj.List_Price = Convert.ToString(dtoutput.Rows[0]["List_Price"]);
                    obj.Expected_price = Convert.ToString(dtoutput.Rows[0]["Expected_price"]);
                    obj.DC_rate = Convert.ToString(dtoutput.Rows[0]["DC_rate"]);
                    obj.Cust_number = Convert.ToString(dtoutput.Rows[0]["Cust_number"]);
                    obj.Cust_SP = Convert.ToString(dtoutput.Rows[0]["Cust_SP"]);
                    obj.Comp_Name = Convert.ToString(dtoutput.Rows[0]["Comp_Name"]);
                    obj.Comp_Desc = Convert.ToString(dtoutput.Rows[0]["Comp_Desc"]);
                    obj.Comp_SP = Convert.ToString(dtoutput.Rows[0]["Comp_SP"]);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
            }
            return obj;
        }

        [WebMethod]
        public static ItemClass LoadAgreementPrice(string item, string qty)
        {
            ItemClass lists = new ItemClass();
            QuoteBL objquoteBL = new QuoteBL();
            DataTable dt = new DataTable();
            QuoteBO objQuoteBO = new QuoteBO();
            try
            {
                objQuoteBO.quantity = Convert.ToString(qty);
                objQuoteBO.Item_Number = Convert.ToString(item);
                objQuoteBO.CP_Number = Convert.ToString(HttpContext.Current.Session["DistributorNumber"]);
                dt = objquoteBL.GetAgreementPriceBL(objQuoteBO);
                if (dt.Rows.Count > 0)
                {
                    lists.item = item;
                    lists.AP = Convert.ToString(dt.Rows[0]["AgreementPrice"]);
                    lists.item_desc = Convert.ToString(dt.Rows[0]["IDSCO"]);
                    lists.Valid_from = Convert.ToString(dt.Rows[0]["DLTEFF"]);
                    lists.Valid_to = Convert.ToString(dt.Rows[0]["DLTEFT"]);
                    lists.quantity = Convert.ToString(dt.Rows[0]["DLBRF"]);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
            }
            return lists;
        }

        [WebMethod]
        public static List<ItemClass> LoadAPList(string item)
        {
            ItemClass cls_item;
            List<ItemClass> lists = new List<ItemClass>();
            QuoteBL objquoteBL = new QuoteBL();
            DataTable dt = new DataTable();
            QuoteBO objQuoteBO = new QuoteBO();
            try
            {
                objQuoteBO.Item_Number = Convert.ToString(item);
                objQuoteBO.CP_Number = Convert.ToString(HttpContext.Current.Session["DistributorNumber"]);
                dt = objquoteBL.GetAPListBL(objQuoteBO);
                if (dt.Rows.Count > 0)
                {
                    for(int i=0; i<dt.Rows.Count; i++)
                    {
                        cls_item = new ItemClass();
                        cls_item.item = item;
                        cls_item.item_desc = Convert.ToString(dt.Rows[i]["IDSCO"]);
                        cls_item.AP = Convert.ToString(dt.Rows[0]["AgreementPrice"]);
                        cls_item.Valid_from = Convert.ToString(dt.Rows[0]["DLTEFF"]);
                        cls_item.Valid_to = Convert.ToString(dt.Rows[0]["DLTEFT"]);
                        cls_item.quantity = Convert.ToString(dt.Rows[0]["DLBRF"]);
                        lists.Add(cls_item);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
            }
            return lists;
        }

        #endregion

        #region CommentedCode

        #region reportGenerate

        //private string GenerateQuoteFormat(string ref_number)
        //{
        //    string file = string.Empty;
        //    try
        //    {
        //        string html = string.Empty;
        //        string filename = string.Empty;
        //        string filepath = string.Empty;
        //        QuoteBL objQuoteBL = new QuoteBL();
        //        QuoteBO objQuoteBO = new QuoteBO();
        //        DataTable dt = new DataTable();

        //        html = "<!DOCTYPE html><html><head><style>table td{border:solid 1px #ddd;padding:5px;}</style></head><body>";
        //        objQuoteBO.Ref_Number = ref_number;
        //        dt = objQuoteBL.getQuoteFormatBL(objQuoteBO);

        //        html += Getheading(dt);

        //        html += "</body></html>";
        //        filename = "Quote_" + Convert.ToString(ref_number) + ".pdf";
        //        filepath = ConfigurationManager.AppSettings["PDF_Folder"].ToString();
        //        convertPDF(html, filepath, filename);
        //        file = String.Concat(filepath, filename);
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //    return file;
        //}

        //private void convertPDF(string html, string filepath, string filename)
        //{
        //    try
        //    {
        //        //using (var stream = new MemoryStream())
        //        //{
        //        //    using (var document = new iTextSharp.text.Document())
        //        //    {
        //        //        PdfWriter writer = PdfWriter.GetInstance(document, stream);
        //        //        document.Open();
        //        //        using (var stringReader = new StringReader(html))
        //        //        {
        //        //            XMLWorkerHelper.GetInstance().ParseXHtml(
        //        //                writer, document, stringReader
        //        //            );
        //        //        }
        //        //        //HtmlConverter.ConvertToPdf(htmlStream, document);
        //        //    }
        //        //    File.WriteAllBytes(string.Concat(filepath, filename), stream.ToArray());
        //        //}
        //        using (FileStream fs = new FileStream(Path.Combine(filepath, "test.htm"), FileMode.Create))
        //        {
        //            using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
        //            {
        //                w.WriteLine(html);
        //            }
        //        }


        //        GeneratePdfFromHtml(filepath, filename, html);

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //private void GeneratePdfFromHtml(string filepath, string filename, string html)
        //{
        //    string outputFilename = Path.Combine(filepath, filename);
        //    string inputFilename = Path.Combine(filepath, "test.htm");

        //    using (var input = new FileStream(inputFilename, FileMode.Open))
        //    using (var output = new FileStream(outputFilename, FileMode.Create))
        //    {
        //        CreatePdf(filepath, filename, input, output, html);
        //    }
        //}

        //private void CreatePdf(string filepath, string filename, FileStream htmlInput, FileStream pdfOutput, string html)
        //{
        //    string imageURL;
        //    try
        //    {
        //        using (var document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 30, 30, 30, 30))
        //        {
        //            var writer = PdfWriter.GetInstance(document, pdfOutput);
        //            var worker = XMLWorkerHelper.GetInstance();
        //            TextReader tr = new StreamReader(htmlInput);
        //            document.Open();
        //            worker.ParseXHtml(writer, document, htmlInput, null, Encoding.UTF8);
        //            //worker.ParseXHtml(writer, document, new StringReader(html));
        //            document.Close();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //private string Getheading(DataTable dt)
        //{

        //    string output = string.Empty;
        //    StringBuilder strHTMLBuilder = new StringBuilder();
        //    string imageURL = string.Empty;
        //    string Heading = string.Empty;
        //    string taegutec_add = string.Empty;
        //    string Customer_Name = string.Empty;
        //    string Customer_Address = string.Empty;
        //    string Customer_Number = string.Empty;
        //    string Quotation_No = string.Empty;
        //    string Date = string.Empty;
        //    string Due_On = string.Empty;
        //    string Tender_No = string.Empty;
        //    string Valid_For = string.Empty;
        //    try
        //    {
        //        Heading = Convert.ToString(ConfigurationManager.AppSettings["QuoteFormat_Heading"]);
        //        imageURL = Convert.ToString(ConfigurationManager.AppSettings["Logo"]);
        //        if (dt != null)
        //        {
        //            if (dt.Rows.Count > 0)
        //            {
        //                Customer_Name = Convert.ToString(dt.Rows[0]["Customer_Name"]);
        //                Customer_Address = Convert.ToString(dt.Rows[0]["Customer_Address"]);
        //                Customer_Number = Convert.ToString(dt.Rows[0]["Customer_Number"]);
        //                Quotation_No = Convert.ToString(dt.Rows[0]["Quotation_No"]);
        //                Date = Convert.ToString(dt.Rows[0]["Date"]);
        //                Due_On = Convert.ToString(dt.Rows[0]["Due_On"]);
        //                Tender_No = Convert.ToString(dt.Rows[0]["Tender_No"]);
        //                Valid_For = Convert.ToString(dt.Rows[0]["Valid_For"]);
        //                taegutec_add = Convert.ToString(dt.Rows[0]["Taegutec_Address"]);


        //                // strHTMLBuilder.Append("<table style='border: 1px solid darkgray; font-family: Helvetica Neue, HelveticaNeue, Helvetica, Arial, sans-serif; border-collapse:collapse;'>");

        //                strHTMLBuilder.Append("<table style='border: 1px solid darkgray; font-family: Helvetica Neue, HelveticaNeue, Helvetica, Arial, sans-serif; border-collapse:collapse;'>");
        //                strHTMLBuilder.Append("<tr style=' background-color: #5faae6c7;'>");
        //                strHTMLBuilder.Append("<td colspan='9' style='text-align:center; font-size:30px; font-weight:bold; color:black;'>");
        //                strHTMLBuilder.Append(Heading);
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("</tr>");
        //                strHTMLBuilder.Append("<tr>");
        //                strHTMLBuilder.Append("<td colspan='4'><img style='float:left;width: 80%;' src='");
        //                strHTMLBuilder.Append(imageURL);
        //                strHTMLBuilder.Append("'/>");
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("<td colspan='5' style='font-weight:bold; font-size: 15px;'>");
        //                strHTMLBuilder.Append(Customer_Name);
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("</tr>");
        //                strHTMLBuilder.Append("<tr>");
        //                strHTMLBuilder.Append("<td colspan='4' style='font-weight:bold; font-size: 10px;'>");
        //                strHTMLBuilder.Append(taegutec_add);
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("<td colspan='5' style='font-weight:bold; font-size: 10px;'>");
        //                strHTMLBuilder.Append(Customer_Address);
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("</tr>");
        //                strHTMLBuilder.Append("<tr >");
        //                strHTMLBuilder.Append("<td colspan='4' >");
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("<td colspan='5'  style='font-weight:bold; font-size: 10px;'>");
        //                strHTMLBuilder.Append("REMARKS:");
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("</tr>");
        //                strHTMLBuilder.Append("<tr >");
        //                strHTMLBuilder.Append("<td colspan='4' style='padding:0px'>");
        //                strHTMLBuilder.Append("<table width='100%'>");
        //                strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>CUSTOMER NO : ");
        //                strHTMLBuilder.Append(Customer_Number);
        //                strHTMLBuilder.Append("</td></tr>");
        //                strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>QUOTATION NO : ");
        //                strHTMLBuilder.Append("");
        //                strHTMLBuilder.Append("</td></tr>");
        //                strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>RFQ NO : ");
        //                strHTMLBuilder.Append(Quotation_No);
        //                strHTMLBuilder.Append("</td></tr>");
        //                strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>DUE ON : ");
        //                strHTMLBuilder.Append(Due_On);
        //                strHTMLBuilder.Append("</td></tr>");
        //                strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>VALID FOR : ");
        //                strHTMLBuilder.Append(Valid_For);
        //                strHTMLBuilder.Append(" Days</td></tr>");
        //                strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>DATE : ");
        //                strHTMLBuilder.Append(Date);
        //                strHTMLBuilder.Append("</td></tr>");
        //                strHTMLBuilder.Append("</table>");
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("<td colspan='5'>");
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("</tr>");

        //                strHTMLBuilder.Append("<tr>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>SL<br/>NO</td>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>STOCK<br/>CODE</td>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>ITEM DESCRIPTION</td>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>CATALOGUE NO</td>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>ITEM QTY</td>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>UNIT<br/>PRICE</td>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>LINE<br/>VALUE</td>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>DELIVERY<br/>DATE</td>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>END<br/>CUSTOMER</td>");
        //                strHTMLBuilder.Append("</tr>");
        //                for (int i = 0; i < dt.Rows.Count; i++)
        //                {
        //                    strHTMLBuilder.Append("<tr>");
        //                    strHTMLBuilder.Append("<td  style='font-size: 10px; text-align:center;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(i + 1));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("<td style='font-size: 10px; text-align:center;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Stock_Code"]));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("<td style='font-size: 10px;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Item"]));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("<td style='font-size: 10px;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Catalogue_No"]));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("<td style='font-size: 10px;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Item_Qty"]));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("<td style='font-size: 10px; text-align:right;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Unit_Price"]));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("<td style='font-size: 10px; text-align:right;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Line_Value"]));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("<td style='font-size: 10px;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Delivery_Date"]));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("<td style='font-size: 10px;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["End_Customer"]));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("</tr>");
        //                }

        //                strHTMLBuilder.Append("</table>");
        //            }
        //        }

        //        output = strHTMLBuilder.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //    return output;
        //}
        #endregion

        //protected void ddlitem_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    QuoteBL objquoteBL = new QuoteBL();
        //    DataTable dt = new DataTable();
        //    QuoteBO objQuoteBO = new QuoteBO();
        //    try
        //    {
        //        DropDownList ddlitem = (DropDownList)sender;
        //        var row = ddlitem.NamingContainer;
        //        DropDownList ddlitemdesc = (DropDownList)row.FindControl("ddlitemdesc");
        //        ddlitemdesc.SelectedValue = ddlitem.SelectedValue;
        //        objQuoteBO.Item_Number = Convert.ToString(ddlitem.SelectedValue);
        //        objQuoteBO.CP_Number = Convert.ToString(Session["DistributorNumber"]);
        //        dt = objquoteBL.GetItemDetailsBL(objQuoteBO);
        //        TextBox txtWHS = (TextBox)row.FindControl("txtWHS");
        //        TextBox txtLP = (TextBox)row.FindControl("txtDLP");
        //        TextBox txtAP = (TextBox)row.FindControl("txtAP");
        //        HiddenField hdnAgreementPrice = (HiddenField)row.FindControl("hdnAgreementPrice");
        //        HiddenField hdnStockCode = (HiddenField)row.FindControl("hdnStockCode");

        //        if (dt != null)
        //        {
        //            if (dt.Rows.Count > 0)
        //            {
        //                txtWHS.Text = Convert.ToString(dt.Rows[0]["WHS"]);
        //                txtLP.Text = Convert.ToString(dt.Rows[0]["ListPrice"]);
        //                txtAP.Text = Convert.ToString(dt.Rows[0]["AgreementPrice"]);
        //                hdnAgreementPrice.Value = Convert.ToString(dt.Rows[0]["AgreementPrice"]);
        //                hdnStockCode.Value = Convert.ToString(dt.Rows[0]["StockCode"]);
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadPage1", "ShowMOQCondition('" + Convert.ToString(ddlitemdesc.SelectedItem) + "','" + Convert.ToString(dt.Rows[0]["StockCode"]) + "', '" + Convert.ToString(ddlitemdesc.ClientID) + "');", true);
        //            }
        //            else
        //            {
        //                txtWHS.Text = "";
        //                txtLP.Text = "";
        //                txtAP.Text ="";
        //                hdnAgreementPrice.Value = "";
        //                hdnStockCode.Value = "";
        //            }
        //            //////// comment by Bhagya --- else part forgot

        //        }
        //        else
        //        {
        //            txtWHS.Text = "";
        //            txtLP.Text = "";
        //            txtAP.Text = "";
        //            hdnAgreementPrice.Value = "";
        //            hdnStockCode.Value = "";
        //        }
        //        //////// comment by Bhagya --- else part forgot
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadPage", "LoadPage();", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlitemdesc_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DropDownList ddlitemdesc = (DropDownList)sender;
        //        var row = ddlitemdesc.NamingContainer;
        //        DropDownList ddlitem = (DropDownList)row.FindControl("ddlitem");

        //        ddlitem.SelectedValue = ddlitemdesc.SelectedValue;
        //        ddlitem_SelectedIndexChanged(sender, e);
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void grdPriceRequest_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {
        //        DataTable dt = (DataTable)Session["ItemTable"];
        //        DropDownList ddlitem = new DropDownList();
        //        if (e.Row.RowType == DataControlRowType.DataRow)
        //        {
        //            if (dt.Rows.Count > 0)
        //            {
        //                ddlitem = (DropDownList)e.Row.FindControl("ddlitem");
        //                ddlitem.DataSource = dt;
        //                ddlitem.DataTextField = dt.Columns["item"].ColumnName.ToString();
        //                ddlitem.DataValueField = dt.Columns["item"].ColumnName.ToString();
        //                ddlitem.DataBind();
        //                ddlitem.Items.Insert(0, new ListItem("--Select--", String.Empty));
        //                string selecteditem = DataBinder.Eval(e.Row.DataItem, "ItemNumber").ToString();
        //                if (!string.IsNullOrEmpty(selecteditem))
        //                    ddlitem.Items.FindByValue(selecteditem).Selected = true;
        //                //ddlitem.SelectedValue = selecteditem;
        //                else
        //                    ddlitem.SelectedIndex = 0;

        //                ddlitem = new DropDownList();
        //                ddlitem = (DropDownList)e.Row.FindControl("ddlitemdesc");
        //                ddlitem.DataSource = dt;
        //                ddlitem.DataTextField = dt.Columns["item_desc"].ColumnName.ToString();
        //                ddlitem.DataValueField = dt.Columns["item"].ColumnName.ToString();
        //                ddlitem.DataBind();
        //                ddlitem.Items.Insert(0, new ListItem("--Select--", String.Empty));
        //                selecteditem = DataBinder.Eval(e.Row.DataItem, "ItemNumber").ToString();
        //                if (!string.IsNullOrEmpty(selecteditem))
        //                    //ddlitem.SelectedValue = selecteditem;
        //                    ddlitem.Items.FindByValue(selecteditem).Selected = true;
        //                else
        //                    ddlitem.SelectedIndex = 0;

        //            }
        //            else
        //            {
        //                ddlitem = (DropDownList)e.Row.FindControl("ddlitem");
        //                ddlitem.DataSource = dt;
        //                ddlitem.DataBind();


        //                ddlitem = new DropDownList();
        //                ddlitem = (DropDownList)e.Row.FindControl("ddlitemdesc");
        //                ddlitem.DataSource = dt;
        //                ddlitem.DataBind();
        //            }

        //            DataTable dtCust = new DataTable();
        //            dtCust = (DataTable)Session["dtCustForQuote"];
        //            if (dtCust.Rows.Count > 0)
        //            {

        //                DropDownList ddlCust = new DropDownList();
        //                ddlCust = (DropDownList)e.Row.FindControl("ddlCustName");
        //                ddlCust.DataSource = dtCust;
        //                ddlCust.DataTextField = dtCust.Columns["customername"].ColumnName.ToString();
        //                ddlCust.DataValueField = dtCust.Columns["customernumber"].ColumnName.ToString();
        //                ddlCust.DataBind();
        //                ddlCust.Items.Insert(0, new ListItem("--Select--", String.Empty));
        //                //string selectedcust = DataBinder.Eval(e.Row.DataItem, "CustName").ToString();
        //                //if (!string.IsNullOrEmpty(selectedcust))
        //                //   // ddlCust.SelectedValue = selectedcust;
        //                //   ddlCust.Items.FindByValue(selectedcust).Selected = true;
        //                //else
        //                //    ddlCust.SelectedIndex = 0;
        //            }
        //            //// comment by Bhagya --- else part forgot, how to load the previous selection
        //            DataTable dtComp = new DataTable();
        //            dtComp = (DataTable)Session["dtCompetitors"];
        //            if (dtComp.Rows.Count > 0)
        //            {

        //                DropDownList ddlComp = new DropDownList();
        //                ddlComp = (DropDownList)e.Row.FindControl("ddlCompanyName");
        //                ddlComp.DataSource = dtComp;
        //                ddlComp.DataTextField = dtComp.Columns["Competitor_Name"].ColumnName.ToString();
        //                ddlComp.DataValueField = dtComp.Columns["Competitor_Name"].ColumnName.ToString();
        //                ddlComp.DataBind();
        //                ddlComp.Items.Insert(0, new ListItem("--Select--", String.Empty));
        //                string selected = DataBinder.Eval(e.Row.DataItem, "CompanyName").ToString();
        //                if (!string.IsNullOrEmpty(selected))
        //                    //ddlComp.SelectedValue = selected;
        //                    ddlComp.Items.FindByValue(selected).Selected = true;
        //                else
        //                    ddlComp.SelectedIndex = 0;
        //            }
        //            //// comment by Bhagya --- else part forgot
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void grdPriceRequest_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName == "AddItem")
        //        {
        //            imgbtnAddClick();
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadPage", "LoadPage()", true);
        //        }
        //        else if (e.CommandName == "Delete")
        //        {
        //            if (ViewState["CurrentTable"] != null)
        //            {
        //                DataTable dt = tblGridRow();
        //                GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
        //                //int index = Convert.ToInt32(e.CommandArgument);
        //                dt.Rows.RemoveAt(gvr.RowIndex);
        //                grdPriceRequest.DataSource = dt;
        //                grdPriceRequest.DataBind();
        //                SetPreviousData();
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadPage", "LoadPage()", true);
        //            }
        //            else
        //            {
        //                return;
        //            }

        //        }
        //        else
        //            return;
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }

        //}

        //protected void grdPriceRequest_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{

        //}

        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    DataTable dtCurrentTable = new DataTable();
        //    try
        //    {
        //        DataTable dt = new DataTable();
        //        dt.Columns.Add(new DataColumn("Ref_number", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Item_code", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Item_Desc", typeof(string)));
        //        dt.Columns.Add(new DataColumn("WHS", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Order_type", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Order_freq", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Total_QTY", typeof(string)));
        //        dt.Columns.Add(new DataColumn("QTY_perOrder", typeof(string)));
        //        dt.Columns.Add(new DataColumn("List_Price", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Expected_price", typeof(string)));
        //        dt.Columns.Add(new DataColumn("DC_rate", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Cust_number", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Cust_Name", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Cust_SP", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Comp_Name", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Comp_Desc", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Comp_SP", typeof(string)));
        //        dt.Columns.Add(new DataColumn("CP_number", typeof(string)));
        //        dt.Columns.Add(new DataColumn("CP_Name", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Status", typeof(string)));
        //        dt.Columns.Add(new DataColumn("RequestedBy", typeof(string)));
        //        dt.Columns.Add(new DataColumn("RequestedBy_Flag", typeof(string)));

        //        //dtCurrentTable = (DataTable)ViewState["CurrentTable"];
        //        DataRow drRow = null;
        //        if (ViewState["CurrentTable"] != null)
        //        {
        //            dtCurrentTable = (DataTable)ViewState["CurrentTable"];
        //            if (dtCurrentTable.Rows.Count > 0)
        //            {
        //                int rowIndex = 0;
        //                for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
        //                {
        //                    //extract the TextBox values
        //                    #region Load

        //                    DropDownList ddlitem = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[0].FindControl("ddlitem");
        //                    DropDownList ddlitemdesc = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[1].FindControl("ddlitemdesc");
        //                    TextBox txtWHS = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[2].FindControl("txtWHS");
        //                    DropDownList ddlOrder = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[3].FindControl("ddlOrder");
        //                    DropDownList ddlFrequency = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[4].FindControl("ddlFrequency");
        //                    TextBox txtTotQTY = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[5].FindControl("txtTotQTY");
        //                    TextBox txtQTYPO = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[6].FindControl("txtQTYPO");
        //                    TextBox txtDLP = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[7].FindControl("txtDLP");
        //                    TextBox txtTargetPrice = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[8].FindControl("txtTargetPrice");
        //                    TextBox txtDCRate = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[9].FindControl("txtDCRate");
        //                    HiddenField ddlCustName = (HiddenField)grdPriceRequest.Rows[rowIndex].Cells[10].FindControl("hdnCustomers");
        //                    TextBox txtCustSP = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[11].FindControl("txtCustSP");
        //                    DropDownList ddlCompanyName = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[12].FindControl("ddlCompanyName");
        //                    TextBox txtDescription = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[13].FindControl("txtDescription");
        //                    TextBox txtCompanySP = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[14].FindControl("txtCompanySP");
        //                    ImageButton imgbtnAdd = (ImageButton)grdPriceRequest.Rows[rowIndex].Cells[15].FindControl("imgbtnAdd");

        //                    #endregion
        //                    drRow = dt.NewRow();
        //                    #region GridToDatatable

        //                    drRow["Ref_number"] = Convert.ToString(txtRef.Text);
        //                    drRow["Item_code"] = Convert.ToString(ddlitem.SelectedValue);
        //                    drRow["Item_Desc"] = Convert.ToString(ddlitemdesc.SelectedItem);
        //                    drRow["WHS"] = Convert.ToString(txtWHS.Text);
        //                    drRow["Order_type"] = Convert.ToString(ddlOrder.SelectedValue);
        //                    drRow["Order_freq"] = Convert.ToString(ddlFrequency.SelectedValue);
        //                    drRow["Total_QTY"] = Convert.ToString(txtTotQTY.Text);
        //                    drRow["QTY_perOrder"] = Convert.ToString(txtQTYPO.Text);
        //                    drRow["List_Price"] = Convert.ToString(txtDLP.Text);
        //                    drRow["Expected_price"] = Convert.ToString(txtTargetPrice.Text);
        //                    drRow["DC_rate"] = Convert.ToString(txtDCRate.Text);
        //                    drRow["Cust_number"] = Convert.ToString(ddlCustName.Value);
        //                    drRow["Cust_Name"] = Convert.ToString(ddlCustName.Value);
        //                    drRow["Cust_SP"] = Convert.ToString(txtCustSP.Text);
        //                    drRow["Comp_Name"] = Convert.ToString(ddlCompanyName.SelectedValue);
        //                    drRow["Comp_Desc"] = Convert.ToString(txtDescription.Text);
        //                    drRow["Comp_SP"] = Convert.ToString(txtCompanySP.Text);
        //                    drRow["CP_number"] = Convert.ToString(Session["DistributorNumber"]);
        //                    drRow["CP_Name"] = Convert.ToString(Session["DistributorName"]);
        //                    drRow["Status"] = "Sent For Approval";
        //                    drRow["RequestedBy"] = Convert.ToString(Session["UserId"]);
        //                    drRow["RequestedBy_Flag"] = "DISTRIBUTOR";

        //                    dt.Rows.Add(drRow);
        //                    #endregion
        //                    rowIndex++;
        //                }
        //            }
        //        }

        //        if (dt.Rows.Count > 0)
        //        {
        //            try
        //            {
        //                string filepath = ConfigurationManager.AppSettings["QuoteRequestLog"] + Convert.ToString("_" + Convert.ToString(Session["DistributorNumber"]) + "_" + Convert.ToString(DateTime.Now.ToString("yyyyMMddTHHmmss")));
        //                StringBuilder sb = new StringBuilder();

        //                string[] columnNames = dt.Columns.Cast<DataColumn>().
        //                                                  Select(column => column.ColumnName).
        //                                                  ToArray();
        //                sb.AppendLine(string.Join(",", columnNames));

        //                foreach (DataRow row in dt.Rows)
        //                {
        //                    string[] fields = row.ItemArray.Select(field => field.ToString()).
        //                                                    ToArray();
        //                    sb.AppendLine(string.Join(",", fields));
        //                }

        //                File.WriteAllText(filepath + ".csv", sb.ToString());
        //                //CreateWorkbook(dt, filepath);
        //                //string attachment = "attachment; filename=QuoteRequest.xls";
        //                ////string filePath = string.Format("{0}/{1}", Server.MapPath("~/Content/UploadedFolder"), @"C:\Users\Vipin\Desktop\Sheets\MyXL6.xlsx");
        //                //Response.ClearContent();
        //                //Response.AddHeader("content-disposition", attachment);
        //                //Response.ContentType = "application/vnd.ms-excel";
        //                //string tab = "";
        //                //foreach (DataColumn dc in dt.Columns)
        //                //{
        //                //    Response.Write(tab + dc.ColumnName);
        //                //    tab = "\t";
        //                //}
        //                //Response.Write("\n");
        //                //int i;
        //                //foreach (DataRow dr in dt.Rows)
        //                //{
        //                //    tab = "";
        //                //    for (i = 0; i < dt.Columns.Count; i++)
        //                //    {
        //                //        Response.Write(tab + dr[i].ToString());
        //                //        tab = "\t";
        //                //    }
        //                //    Response.Write("\n");
        //                //}
        //                //Response.End();
        //            }
        //            catch (Exception ex)
        //            {
        //                objCom.ErrorLog(ex);
        //            }


        //            QuoteBL objQuoteBL = new QuoteBL();
        //            QuoteBO objQuoteBO = new QuoteBO();
        //            objQuoteBO = objQuoteBL.SaveQuotesBL(dt);

        //            if (objQuoteBO.Err_code == 0)
        //            {
        //                if (!String.IsNullOrEmpty(Convert.ToString(objQuoteBO.to)))
        //                {
        //                    //string attachment = GenerateQuoteFormat(objQuoteBO.Ref_Number);
        //                    EmailDetails objEmail = new EmailDetails();
        //                    objEmail.toMailId = objQuoteBO.to;
        //                    objEmail.ccMailId = objQuoteBO.cc;
        //                    objEmail.subject = objQuoteBO.subject;
        //                    objEmail.body = objQuoteBO.message;
        //                    //objEmail.attachment = attachment;
        //                    objCom.SendMail(objEmail);
        //                }
        //                dt = null;
        //                dtCurrentTable = null;
        //                ViewState["CurrentTable"] = null;
        //                //Page_Load(null, null);
        //                BindGrid();
        //                BindFrequentItems();
        //                BindRecentItems();
        //                lblmessage.Text = "Requests for quotes are submitted successfully.";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadPage", "LoadPage()", true);
        //            }
        //            else
        //            {
        //                lblmessage.Text = "There is error in saving data. Please try again." + objQuoteBO.Err_msg;
        //            }
        //        }
        //        else
        //        {
        //            lblmessage.Text = "There is no quote to save.";
        //        }
        //        //ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadPage", "LoadPage()", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        ////// comment by Bhagya --- call 2 javascript functions
        //protected void lnkItem1_Click(object sender, EventArgs e)
        //{
        //    CommonFunctions objCom = new CommonFunctions();
        //    try
        //    {
        //        string item_code = Convert.ToString(lblItem1.Text);
        //        LoadFrequentlyOrder(item_code);

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void lnkItem2_Click(object sender, EventArgs e)
        //{
        //    CommonFunctions objCom = new CommonFunctions();
        //    try
        //    {
        //        string item_code = Convert.ToString(lblItem2.Text);
        //        LoadFrequentlyOrder(item_code);

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void lnkItem3_Click(object sender, EventArgs e)
        //{
        //    CommonFunctions objCom = new CommonFunctions();
        //    try
        //    {
        //        string item_code = Convert.ToString(lblItem3.Text);
        //        LoadFrequentlyOrder(item_code);

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void lnkItem4_Click(object sender, EventArgs e)
        //{
        //    CommonFunctions objCom = new CommonFunctions();
        //    try
        //    {
        //        string item_code = Convert.ToString(lblItem4.Text);
        //        LoadFrequentlyOrder(item_code);

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void lnkItem5_Click(object sender, EventArgs e)
        //{
        //    CommonFunctions objCom = new CommonFunctions();
        //    try
        //    {
        //        string item_code = Convert.ToString(lblItem5.Text);
        //        LoadFrequentlyOrder(item_code);

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void lnkOItem1_Click(object sender, EventArgs e)
        //{
        //    CommonFunctions objCom = new CommonFunctions();
        //    try
        //    {
        //        string item_code = Convert.ToString(lblOItem1.Text);
        //        LoadRecentlyOrder(item_code);

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void lnkOItem2_Click(object sender, EventArgs e)
        //{
        //    CommonFunctions objCom = new CommonFunctions();
        //    try
        //    {
        //        string item_code = Convert.ToString(lblOItem2.Text);
        //        LoadRecentlyOrder(item_code);

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void lnkOItem3_Click(object sender, EventArgs e)
        //{
        //    CommonFunctions objCom = new CommonFunctions();
        //    try
        //    {
        //        string item_code = Convert.ToString(lblOItem3.Text);
        //        LoadRecentlyOrder(item_code);

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void lnkOItem4_Click(object sender, EventArgs e)
        //{
        //    CommonFunctions objCom = new CommonFunctions();
        //    try
        //    {
        //        string item_code = Convert.ToString(lblOItem4.Text);
        //        LoadRecentlyOrder(item_code);

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void lnkOItem5_Click(object sender, EventArgs e)
        //{
        //    CommonFunctions objCom = new CommonFunctions();
        //    try
        //    {
        //        string item_code = Convert.ToString(lblOItem5.Text);
        //        LoadRecentlyOrder(item_code);

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}


        //protected void imgbtnAddClick()
        //{
        //    int rowIndex = 0;
        //    try
        //    {
        //        if (ViewState["CurrentTable"] != null)
        //        {
        //            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
        //            DataRow drCurrentRow = null;
        //            if (dtCurrentTable.Rows.Count > 0)
        //            {
        //                for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
        //                {
        //                    //extract the TextBox values
        //                    DropDownList ddlitem = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[0].FindControl("ddlitem");
        //                    //DropDownList ddlitemdesc = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[1].FindControl("ddlitemdesc");
        //                    TextBox txtWHS = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[2].FindControl("txtWHS");
        //                    DropDownList ddlOrder = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[3].FindControl("ddlOrder");
        //                    DropDownList ddlFrequency = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[4].FindControl("ddlFrequency");
        //                    TextBox txtTotQTY = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[5].FindControl("txtTotQTY");
        //                    TextBox txtQTYPO = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[6].FindControl("txtQTYPO");
        //                    TextBox txtDLP = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[7].FindControl("txtDLP");
        //                    TextBox txtTargetPrice = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[8].FindControl("txtTargetPrice");
        //                    TextBox txtDCRate = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[9].FindControl("txtDCRate");
        //                    HiddenField ddlCustName = (HiddenField)grdPriceRequest.Rows[rowIndex].Cells[10].FindControl("hdnCustomers");
        //                    TextBox txtCustSP = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[11].FindControl("txtCustSP");
        //                    DropDownList ddlCompanyName = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[12].FindControl("ddlCompanyName");
        //                    TextBox txtDescription = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[13].FindControl("txtDescription");
        //                    TextBox txtCompanySP = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[14].FindControl("txtCompanySP");
        //                    ImageButton imgbtnAdd = (ImageButton)grdPriceRequest.Rows[rowIndex].Cells[15].FindControl("imgbtnAdd");

        //                    drCurrentRow = dtCurrentTable.NewRow();
        //                    //drCurrentRow["RowNumber"] = i + 1;
        //                    dtCurrentTable.Rows[i - 1]["ItemNumber"] = ddlitem.SelectedValue;
        //                    //dtCurrentTable.Rows[i - 1]["ItemDescription"] = ddlitemdesc.SelectedValue;
        //                    dtCurrentTable.Rows[i - 1]["WHS"] = txtWHS.Text;
        //                    dtCurrentTable.Rows[i - 1]["Order_type"] = Convert.ToString(ddlOrder.SelectedValue);
        //                    if (Convert.ToString(ddlOrder.SelectedValue) == "schedule")
        //                        dtCurrentTable.Rows[i - 1]["Order_freq"] = Convert.ToString(ddlFrequency.SelectedValue);
        //                    else
        //                        dtCurrentTable.Rows[i - 1]["Order_freq"] = "";
        //                    dtCurrentTable.Rows[i - 1]["Total_QTY"] = Convert.ToInt32(txtTotQTY.Text);
        //                    dtCurrentTable.Rows[i - 1]["QTYPO"] = Convert.ToString(txtQTYPO.Text);
        //                    dtCurrentTable.Rows[i - 1]["DLP"] = txtDLP.Text;
        //                    dtCurrentTable.Rows[i - 1]["TargetPrice"] = Convert.ToDouble(txtTargetPrice.Text);
        //                    dtCurrentTable.Rows[i - 1]["DCRate"] = txtDCRate.Text;
        //                    dtCurrentTable.Rows[i - 1]["CustName"] = ddlCustName.Value;
        //                    dtCurrentTable.Rows[i - 1]["CustSP"] = txtCustSP.Text;
        //                    dtCurrentTable.Rows[i - 1]["CompanyName"] = ddlCompanyName.SelectedValue;
        //                    dtCurrentTable.Rows[i - 1]["Description"] = txtDescription.Text;
        //                    dtCurrentTable.Rows[i - 1]["CompanySP"] = txtCompanySP.Text;
        //                    rowIndex++;
        //                }
        //                dtCurrentTable.Rows.Add(drCurrentRow);
        //                //// //// comment by Bhagya --- need explanation 
        //                ViewState["CurrentTable"] = dtCurrentTable;

        //                grdPriceRequest.DataSource = dtCurrentTable;
        //                grdPriceRequest.DataBind();
        //            }
        //        }
        //        else
        //        {
        //            Response.Write("ViewState is null");
        //        }
        //        //Set Previous Data on Postbacks

        //        SetPreviousData();
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //private void SetPreviousData()
        //{
        //    try
        //    {
        //        int rowIndex = 0;
        //        if (ViewState["CurrentTable"] != null)
        //        {
        //            DataTable dt = (DataTable)ViewState["CurrentTable"];
        //            if (dt.Rows.Count > 0)
        //            {
        //                for (int i = 0; i < dt.Rows.Count; i++)
        //                {
        //                    DropDownList ddlitem = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[0].FindControl("ddlitem");
        //                    //DropDownList ddlitemdesc = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[1].FindControl("ddlitemdesc");
        //                    TextBox txtWHS = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[2].FindControl("txtWHS");
        //                    DropDownList ddlOrder = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[3].FindControl("ddlOrder");
        //                    DropDownList ddlFrequency = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[4].FindControl("ddlFrequency");
        //                    TextBox txtTotQTY = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[5].FindControl("txtTotQTY");
        //                    TextBox txtQTYPO = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[6].FindControl("txtQTYPO");
        //                    TextBox txtDLP = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[7].FindControl("txtDLP");
        //                    TextBox txtTargetPrice = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[8].FindControl("txtTargetPrice");
        //                    TextBox txtDCRate = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[9].FindControl("txtDCRate");
        //                    HiddenField ddlCustName = (HiddenField)grdPriceRequest.Rows[rowIndex].Cells[10].FindControl("hdnCustomers");
        //                    TextBox txtCustSP = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[11].FindControl("txtCustSP");
        //                    DropDownList ddlCompanyName = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[12].FindControl("ddlCompanyName");
        //                    TextBox txtDescription = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[13].FindControl("txtDescription");
        //                    TextBox txtCompanySP = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[14].FindControl("txtCompanySP");

        //                    ddlitem.SelectedValue = Convert.ToString(dt.Rows[i]["ItemNumber"]);
        //                    //ddlitemdesc.SelectedValue = Convert.ToString(dt.Rows[i]["ItemDescription"]);
        //                    txtWHS.Text = Convert.ToString(dt.Rows[i]["WHS"]);
        //                    ddlOrder.SelectedValue = Convert.ToString(dt.Rows[i]["Order_type"]);
        //                    ddlFrequency.SelectedValue = Convert.ToString(dt.Rows[i]["Order_freq"]);
        //                    txtTotQTY.Text = Convert.ToString(dt.Rows[i]["Total_QTY"]);
        //                    txtQTYPO.Text = Convert.ToString(dt.Rows[i]["QTYPO"]);
        //                    txtDLP.Text = Convert.ToString(dt.Rows[i]["DLP"]);
        //                    txtTargetPrice.Text = Convert.ToString(dt.Rows[i]["TargetPrice"]);
        //                    txtDCRate.Text = Convert.ToString(dt.Rows[i]["DCRate"]);
        //                    ddlCustName.Value = Convert.ToString(dt.Rows[i]["CustName"]);
        //                    txtCustSP.Text = Convert.ToString(dt.Rows[i]["CustSP"]);
        //                    ddlCompanyName.SelectedValue = Convert.ToString(dt.Rows[i]["CompanyName"]);
        //                    txtDescription.Text = Convert.ToString(dt.Rows[i]["Description"]);
        //                    txtCompanySP.Text = Convert.ToString(dt.Rows[i]["CompanySP"]);

        //                    rowIndex++;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected DataTable tblGridRow()
        //{
        //    DataTable dtCurrentTable = new DataTable();
        //    try
        //    {
        //        if (ViewState["CurrentTable"] != null)
        //        {
        //            dtCurrentTable = (DataTable)ViewState["CurrentTable"];

        //            DataRow drCurrentRow = null;

        //            if (dtCurrentTable.Rows.Count > 0)
        //            {
        //                int rowIndex = 0;
        //                for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
        //                {
        //                    //extract the TextBox values
        //                    DropDownList ddlitem = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[0].FindControl("ddlitem");
        //                    DropDownList ddlitemdesc = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[1].FindControl("ddlitemdesc");
        //                    TextBox txtWHS = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[2].FindControl("txtWHS");
        //                    DropDownList ddlOrder = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[3].FindControl("ddlOrder");
        //                    DropDownList ddlFrequency = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[4].FindControl("ddlFrequency");
        //                    TextBox txtTotQTY = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[5].FindControl("txtTotQTY");
        //                    TextBox txtQTYPO = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[6].FindControl("txtQTYPO");
        //                    TextBox txtDLP = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[7].FindControl("txtDLP");
        //                    TextBox txtTargetPrice = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[8].FindControl("txtTargetPrice");
        //                    TextBox txtDCRate = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[9].FindControl("txtDCRate");
        //                    HiddenField ddlCustName = (HiddenField)grdPriceRequest.Rows[rowIndex].Cells[10].FindControl("hdnCustomers");
        //                    TextBox txtCustSP = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[11].FindControl("txtCustSP");
        //                    DropDownList ddlCompanyName = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[12].FindControl("ddlCompanyName");
        //                    TextBox txtDescription = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[13].FindControl("txtDescription");
        //                    TextBox txtCompanySP = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[14].FindControl("txtCompanySP");
        //                    ImageButton imgbtnAdd = (ImageButton)grdPriceRequest.Rows[rowIndex].Cells[15].FindControl("imgbtnAdd");

        //                    drCurrentRow = dtCurrentTable.NewRow();
        //                    //drCurrentRow["RowNumber"] = i + 1;
        //                    dtCurrentTable.Rows[i - 1]["ItemNumber"] = ddlitem.SelectedValue;
        //                    dtCurrentTable.Rows[i - 1]["ItemDescription"] = ddlitemdesc.SelectedValue;
        //                    dtCurrentTable.Rows[i - 1]["WHS"] = txtWHS.Text;
        //                    dtCurrentTable.Rows[i - 1]["Order_type"] = Convert.ToString(ddlOrder.SelectedValue);
        //                    if (Convert.ToString(ddlOrder.SelectedValue) == "schedule")
        //                        dtCurrentTable.Rows[i - 1]["Order_freq"] = Convert.ToString(ddlFrequency.SelectedValue);
        //                    else
        //                        dtCurrentTable.Rows[i - 1]["Order_freq"] = "";
        //                    dtCurrentTable.Rows[i - 1]["Total_QTY"] = Convert.ToInt32(txtTotQTY.Text);
        //                    dtCurrentTable.Rows[i - 1]["QTYPO"] = Convert.ToString(txtQTYPO.Text);
        //                    dtCurrentTable.Rows[i - 1]["DLP"] = txtDLP.Text;
        //                    dtCurrentTable.Rows[i - 1]["TargetPrice"] = Convert.ToDouble(txtTargetPrice.Text);
        //                    dtCurrentTable.Rows[i - 1]["DCRate"] = txtDCRate.Text;
        //                    dtCurrentTable.Rows[i - 1]["CustName"] = ddlCustName.Value;
        //                    dtCurrentTable.Rows[i - 1]["CustSP"] = txtCustSP.Text;
        //                    dtCurrentTable.Rows[i - 1]["CompanyName"] = ddlCompanyName.SelectedValue;
        //                    dtCurrentTable.Rows[i - 1]["Description"] = txtDescription.Text;
        //                    dtCurrentTable.Rows[i - 1]["CompanySP"] = txtCompanySP.Text;
        //                    rowIndex++;
        //                }
        //                ViewState["CurrentTable"] = dtCurrentTable;
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //    return dtCurrentTable;
        //}


        //private void LoadFrequentlyOrder(string item_code)
        //{
        //    CommonFunctions objCom = new CommonFunctions();
        //    DataTable dtoutput;
        //    try
        //    {
        //        dtoutput = (DataTable)HttpContext.Current.Session["dtFreqitem"];
        //        if (dtoutput.Rows.Count > 0)
        //        {
        //            var rows = from row in dtoutput.AsEnumerable()
        //                       where row.Field<string>("item").Trim() == item_code
        //                       select row;
        //            dtoutput = rows.CopyToDataTable();
        //        }
        //        LoadFrequentlyRecentlyOrder(dtoutput);

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadPage", "LoadPage()", true);
        //}

        //private void LoadRecentlyOrder(string item_code)
        //{
        //    CommonFunctions objCom = new CommonFunctions();
        //    DataTable dtoutput;
        //    try
        //    {
        //        dtoutput = (DataTable)HttpContext.Current.Session["dtRecitem"];
        //        if (dtoutput.Rows.Count > 0)
        //        {
        //            var rows = from row in dtoutput.AsEnumerable()
        //                       where row.Field<string>("item").Trim() == item_code
        //                       select row;
        //            dtoutput = rows.CopyToDataTable();
        //        }
        //        LoadFrequentlyRecentlyOrder(dtoutput);

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadPage", "LoadPage()", true);
        //}

        //protected void LoadFrequentlyRecentlyOrder(DataTable dtoutput)
        //{
        //    int rowIndex = 0;
        //    try
        //    {
        //        if (ViewState["CurrentTable"] != null)
        //        {
        //            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
        //            DataRow drCurrentRow = null;
        //            if (dtCurrentTable.Rows.Count > 0)
        //            {
        //                for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
        //                {
        //                    //extract the TextBox values
        //                    DropDownList ddlitem = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[0].FindControl("ddlitem");
        //                    DropDownList ddlitemdesc = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[1].FindControl("ddlitemdesc");
        //                    TextBox txtWHS = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[2].FindControl("txtWHS");
        //                    DropDownList ddlOrder = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[3].FindControl("ddlOrder");
        //                    DropDownList ddlFrequency = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[4].FindControl("ddlFrequency");
        //                    TextBox txtTotQTY = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[5].FindControl("txtTotQTY");
        //                    TextBox txtQTYPO = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[6].FindControl("txtQTYPO");
        //                    TextBox txtDLP = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[7].FindControl("txtDLP");
        //                    TextBox txtTargetPrice = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[8].FindControl("txtTargetPrice");
        //                    TextBox txtDCRate = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[9].FindControl("txtDCRate");
        //                    HiddenField ddlCustName = (HiddenField)grdPriceRequest.Rows[rowIndex].Cells[10].FindControl("hdnCustomers");
        //                    TextBox txtCustSP = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[11].FindControl("txtCustSP");
        //                    DropDownList ddlCompanyName = (DropDownList)grdPriceRequest.Rows[rowIndex].Cells[12].FindControl("ddlCompanyName");
        //                    TextBox txtDescription = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[13].FindControl("txtDescription");
        //                    TextBox txtCompanySP = (TextBox)grdPriceRequest.Rows[rowIndex].Cells[14].FindControl("txtCompanySP");
        //                    ImageButton imgbtnAdd = (ImageButton)grdPriceRequest.Rows[rowIndex].Cells[15].FindControl("imgbtnAdd");

        //                    if (!string.IsNullOrEmpty(Convert.ToString(ddlitem.SelectedValue)))
        //                    {
        //                        //drCurrentRow["RowNumber"] = i + 1;
        //                        dtCurrentTable.Rows[i - 1]["ItemNumber"] = ddlitem.SelectedValue;
        //                        dtCurrentTable.Rows[i - 1]["ItemDescription"] = ddlitemdesc.SelectedValue;
        //                        dtCurrentTable.Rows[i - 1]["WHS"] = txtWHS.Text;
        //                        dtCurrentTable.Rows[i - 1]["Order_type"] = Convert.ToString(ddlOrder.SelectedValue);
        //                        if (Convert.ToString(ddlOrder.SelectedValue) == "schedule")
        //                            dtCurrentTable.Rows[i - 1]["Order_freq"] = Convert.ToString(ddlFrequency.SelectedValue);
        //                        else
        //                            dtCurrentTable.Rows[i - 1]["Order_freq"] = "";
        //                        dtCurrentTable.Rows[i - 1]["Total_QTY"] = Convert.ToInt32(txtTotQTY.Text);
        //                        dtCurrentTable.Rows[i - 1]["QTYPO"] = Convert.ToString(txtQTYPO.Text);
        //                        dtCurrentTable.Rows[i - 1]["DLP"] = txtDLP.Text;
        //                        dtCurrentTable.Rows[i - 1]["TargetPrice"] = Convert.ToString(txtTargetPrice.Text);
        //                        dtCurrentTable.Rows[i - 1]["DCRate"] = txtDCRate.Text;
        //                        dtCurrentTable.Rows[i - 1]["CustName"] = ddlCustName.Value;
        //                        dtCurrentTable.Rows[i - 1]["CustSP"] = txtCustSP.Text;
        //                        dtCurrentTable.Rows[i - 1]["CompanyName"] = ddlCompanyName.SelectedValue;
        //                        dtCurrentTable.Rows[i - 1]["Description"] = txtDescription.Text;
        //                        dtCurrentTable.Rows[i - 1]["CompanySP"] = txtCompanySP.Text;
        //                    }

        //                    rowIndex++;
        //                }
        //                drCurrentRow = dtCurrentTable.NewRow();
        //                if (dtoutput.Rows.Count > 0)
        //                {
        //                    drCurrentRow["ItemNumber"] = Convert.ToString(dtoutput.Rows[0]["item"]);
        //                    drCurrentRow["ItemDescription"] = Convert.ToString(dtoutput.Rows[0]["item_desc"]);
        //                    drCurrentRow["WHS"] = Convert.ToString(dtoutput.Rows[0]["WHS"]);
        //                    drCurrentRow["Total_QTY"] = Convert.ToString(dtoutput.Rows[0]["Total_QTY"]);
        //                    drCurrentRow["Order_freq"] = Convert.ToString(dtoutput.Rows[0]["Order_frequency"]);
        //                    drCurrentRow["QTYPO"] = Convert.ToString(dtoutput.Rows[0]["QTY_perOrder"]);
        //                    drCurrentRow["Order_type"] = Convert.ToString(dtoutput.Rows[0]["Order_type"]);
        //                    drCurrentRow["DLP"] = Convert.ToString(dtoutput.Rows[0]["List_Price"]);
        //                    drCurrentRow["TargetPrice"] = Convert.ToString(dtoutput.Rows[0]["Expected_price"]);
        //                    drCurrentRow["DCRate"] = Convert.ToString(dtoutput.Rows[0]["DC_rate"]);
        //                    drCurrentRow["CustName"] = Convert.ToString(dtoutput.Rows[0]["Cust_number"]);
        //                    drCurrentRow["CustSP"] = Convert.ToString(dtoutput.Rows[0]["Cust_SP"]);
        //                    drCurrentRow["CompanyName"] = Convert.ToString(dtoutput.Rows[0]["Comp_Name"]);
        //                    drCurrentRow["Description"] = Convert.ToString(dtoutput.Rows[0]["Comp_Desc"]);
        //                    drCurrentRow["CompanySP"] = Convert.ToString(dtoutput.Rows[0]["Comp_SP"]);
        //                }
        //                dtCurrentTable.Rows.Add(drCurrentRow);
        //                ViewState["CurrentTable"] = dtCurrentTable;

        //                grdPriceRequest.DataSource = dtCurrentTable;
        //                grdPriceRequest.DataBind();
        //            }
        //        }
        //        else
        //        {
        //            Response.Write("ViewState is null");
        //        }
        //        //Set Previous Data on Postbacks

        //        SetPreviousData();
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        #endregion

    }
}