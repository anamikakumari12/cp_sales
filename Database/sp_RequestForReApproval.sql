USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_RequestForReApproval]    Script Date: 9/4/2019 3:49:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_RequestForReApproval]
(
@ID				INT,
@RefNumber		VARCHAR(200),
@item			VARCHAR(100),
@Requested_By	VARCHAR(100),
@STATUS			VARCHAR(100),
@error_code		INT OUTPUT,
@error_msg		VARCHAR(MAX) OUTPUT
)
AS
BEGIN
	IF(@ID=0 OR @ID is null)
	BEGIN
	SET @error_code=100
	SET @error_msg='Error: ID is required.'
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@ID)
		BEGIN
			IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item)
			BEGIN
				
				UPDATE Quote_ref
				--SET Status='Request For ReApproval',
				SET Status=@STATUS,
				ReRequested_Date=GETDATE(),
				ReRequested_By=@Requested_By
				--Approval_date=null
				WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item
				
				IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@ID AND Status=@STATUS AND Ref_number=@RefNumber AND Item_code=@item)
				BEGIN
					
					INSERT INTO tbl_QuoteStatusLog(Quote_ID,Ref_Number,Item_code,Status,StatusChangeBy_Flag,StatusChangeBy_Id,StatusChangeDate)
					SELECT @ID, @RefNumber, @item,@STATUS,RequestedBy_Flag, @Requested_By,GETDATE()  FROM Quote_ref
					 WHERE ID=@ID AND Ref_number=@RefNumber AND Item_code=@item

					SET @error_code=200
					SET @error_msg='Quote request is sent for approval successfully.'
				END
				ELSE
				BEGIN
					SET @error_code=103
					SET @error_msg='Error: Error in updating data.'
				END
			END											
			ELSE
			BEGIN
				SET @error_code=102
				SET @error_msg='Error: Reference Number and Item code are not available in Quote table.'
			END
		END
		ELSE
		BEGIN
			SET @error_code=101
			SET @error_msg='Error: ID is not available in Quote table.'
		END
	END

END