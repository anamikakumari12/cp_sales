USE [Taegutec_Sales_Budget]
GO

/****** Object:  UserDefinedTableType [dbo].[QuoteStatusChangeTableType]    Script Date: 21-10-2020 11:59:50 ******/
DROP TYPE [dbo].[QuoteStatusChangeTableType]
GO

/****** Object:  UserDefinedTableType [dbo].[QuoteStatusChangeTableType]    Script Date: 21-10-2020 11:59:50 ******/
CREATE TYPE [dbo].[QuoteStatusChangeTableType] AS TABLE(
	[ID] [int] NULL,
	[RefNumber] [varchar](200) NULL,
	[item] [varchar](100) NULL,
	[flag] [varchar](100) NULL,
	[changeby] [varchar](100) NULL,
	[Status] [varchar](100) NULL,
	[Reason] [varchar](max) NULL,
	[MOQ] [varchar](100) NULL,
	[OfferPrice] [varchar](100) NULL,
	[MultiOrderFlag] [int] NULL,
	[RecommendedPrice] [varchar](100) NULL,
	[validity] [int] NULL,
	[OrderQty] [int] NULL,
	[OrderFreq] [varchar](100) NULL,
	[Comp_name] [varchar](max) NULL,
	[Comp_desc] [varchar](max) NULL,
	[Comp_SP] [varchar](100) NULL,
	[End_cust_num] [varchar](100) NULL,
	[End_cust_name] [varchar](MAX) NULL
)
GO

ALTER TABLE tt_QuoteStatusChangeInterface ADD [End_cust_num] [varchar](MAX)
ALTER TABLE tt_QuoteStatusChangeInterface ADD [End_cust_name] [varchar](MAX)

ALTER TABLE tbl_QuoteStatusLog ADD [End_cust_num] [varchar](MAX)
ALTER TABLE tbl_QuoteStatusLog ADD [End_cust_name] [varchar](MAX)