﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DistributorSystemBO
{
    public class QuoteBO
    {
        public string CP_Number { get; set; }
        public string Item_Number { get; set; }
        public int Err_code { get; set; }
        public string Err_msg { get; set; }

        public string Ref_Number { get; set; }

        public string EndDate { get; set; }

        public string StartDate { get; set; }
        public int QuoteID { get; set; }
        public string OrderType { get; set; }
        public string scheduleType { get; set; }
        public string quantity { get; set; }
        public string orderbyId { get; set; }
        public string orderbyName { get; set; }
        public string orderbyFlag { get; set; }
        public string NumberOfOrder { get; set; }

        public string Cust_Number { get; set; }

        public string Cust_Name { get; set; }
        public string to { get; set; }
        public string cc { get; set; }
        public string subject { get; set; }
        public string message { get; set; }

        public string status { get; set; }

        public string OrderStartDate { get; set; }

        public int MultiOrderFlag { get; set; }

        public string Offer_Price { get; set; }

        public string MOQ { get; set; }

        public string Reason { get; set; }

        public string POComment { get; set; }
    }

    public class ItemClass
    {
        public string item { get; set; }
        public string item_desc { get; set; }
        public string AP { get; set; }
        public string WHS { get; set; }
        public string LP { get; set; }
        public int stockCode { get; set; }
        public string Valid_from { get; set; }
        public string Valid_to { get; set; }
        public string quantity { get; set; }
        public string IPACK { get; set; }
    }

    public class searchCls
    {
        public string q { get; set; }
        public string page { get; set; }
        public string per_page { get; set; }
    }
    public class itemddl
    {
        public string id { get; set; }
        public string text { get; set; }
    }
    public class itemDetails
    {
          public string Ref_number { get; set; }
          public string Item_code { get; set; }
          public string Item_Desc { get; set; }
          public string WHS { get; set; }
          public string Order_type { get; set; }
          public string Order_freq { get; set; }
          public string Total_QTY { get; set; }
          public string QTY_perOrder { get; set; }
          public string List_Price { get; set; }
          public string Expected_price { get; set; }
          public string DC_rate { get; set; }
          public string Cust_number { get; set; }
          public string Cust_Name { get; set; }
          public string Cust_SP { get; set; }
          public string Comp_Name { get; set; }
          public string Comp_Desc { get; set; }
          public string Comp_SP { get; set; }
          public string CP_number { get; set; }
          public string CP_Name { get; set; }
          public string Status { get; set; }
          public string RequestedBy { get; set; }
          public string RequestedBy_Flag { get; set; }

         //public string item { get; set; }
         //public string WHS { get; set; }
         //public string OrderType { get; set; }
         //public string Frequency { get; set; }
         //public string TotQTY { get; set; }
         //public string QTYPO { get; set; }
         //public string LP { get; set; }
         //public string AP { get; set; }
         //public string EP { get; set; }
         //public string Discount { get; set; }
         //public string CustName { get; set; }
         //public string SalePrice { get; set; }
         //public string Comp { get; set; }
         //public string CompProduct { get; set; }
         //public string CompSP { get; set; }
    }

    public class Output
    {
        public int ErrorCode { get; set; }
        public string ErrorMsg { get; set; }
    }

    public class QuoteStatusDB
    {
        public int ID { get; set; }
        public string RefNumber { get; set; }
        public string item { get; set; }
        public string flag { get; set; }
        public string changeby { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public string MOQ { get; set; }
        public string OfferPrice { get; set; }
        public int MultiOrderFlag { get; set; }
        public string RecommendedPrice { get; set; }
        public string Order_Validity { get; set; }
        public string OrderQty { get; set; }
        public string OrderFreq { get; set; }
        public string Comp_name { get; set; }
        public string Comp_desc { get; set; }
        public string Comp_SP { get; set; }
        public string End_cust_name { get; set; }
        public string End_cust_num { get; set; }
        public string Escalation_file { get; set; }
        public string BM_Approval_file { get; set; }
        public string HO_Approval_file { get; set; }
        public string Internal_Remarks { get; set; }
    }

}
