﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBL;
using DistributorSystemBO;

namespace DistributorSystem
{
    public partial class SalesDashboard : System.Web.UI.Page
    {
        #region Global Declaration
        CommonFunctions objCom = new CommonFunctions();
        #endregion
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }
                
                LoadDashboardData();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void LoadDashboardData()
        {
            DashboardBO objBO;
            DashboardBL objBL;
            DataSet dsoutput;
            string output = string.Empty;
            try
            {
                dsoutput = new DataSet();
                objBO = new DashboardBO();
                objBL = new DashboardBL();
                objBO.distributor_number = Convert.ToInt32(HttpContext.Current.Session["DistributorNumber"]);
                dsoutput = objBL.GetSalesDashboardDataBL(objBO);
                Session["SalesDashboardData"] = dsoutput;
                if (dsoutput != null)
                {
                    lblResult.Text = "";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "LoadChart", "LoadCharts()", true);
                }
                else
                {
                    lblResult.Text = "There are no data to show.";
                }
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "LoadChart", "LoadCharts()", true);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        [WebMethod]
        public static string LoadChartHistoricalSales()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            DataSet dsData;
            string output = string.Empty;
            try
            {
                dsData=new DataSet();
                dsData=(DataSet)HttpContext.Current.Session["SalesDashboardData"];
                dtoutput = new DataTable();
                if (dsData.Tables[0] != null)
                    if (dsData.Tables[0].Rows.Count > 0)
                        dtoutput = dsData.Tables[0];
                if (dtoutput.Rows.Count > 0)
                {
                    output = DataTableToJSONWithStringBuilder(dtoutput);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }
        [WebMethod]
        public static string LoadChartConsolidatedSalesView()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            DataSet dsData;
            string output = string.Empty;
            try
            {
                dsData = new DataSet();
                dsData = (DataSet)HttpContext.Current.Session["SalesDashboardData"];
                dtoutput = new DataTable();
                if (dsData.Tables[1] != null)
                    if (dsData.Tables[1].Rows.Count > 0)
                        dtoutput = dsData.Tables[1];
                if (dtoutput.Rows.Count > 0)
                {
                    output = DataTableToJSONWithStringBuilder(dtoutput);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }
        [WebMethod]
        public static string LoadChartSalesByCustomer()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            DataSet dsData;
            string output = string.Empty;
            try
            {
                dsData = new DataSet();
                dsData = (DataSet)HttpContext.Current.Session["SalesDashboardData"];
                dtoutput = new DataTable();
                if (dsData.Tables[2] != null)
                    if (dsData.Tables[2].Rows.Count > 0)
                        dtoutput = dsData.Tables[2];
                if (dtoutput.Rows.Count > 0)
                {
                    output = DataTableToJSONWithStringBuilder(dtoutput);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }
        [WebMethod]
        public static string LoadChartSalesByMonth()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            DataSet dsData;
            string output = string.Empty;
            try
            {
                dsData = new DataSet();
                dsData = (DataSet)HttpContext.Current.Session["SalesDashboardData"];
                dtoutput = new DataTable();
                if (dsData.Tables[3] != null)
                    if (dsData.Tables[3].Rows.Count > 0)
                        dtoutput = dsData.Tables[3];
                if (dtoutput.Rows.Count > 0)
                {
                    output = DataTableToJSONWithStringBuilder(dtoutput);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }

        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                        }
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }
    }
}