﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PuchaseBudgetSummary.aspx.cs" Inherits="DistributorSystem.PuchaseBudgetSummary" MasterPageFile="~/MasterPage.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        jQuery(".row_index").click(function () {

            var currentIndex = jQuery(this).data("index");

            jQuery(".subrowindex_" + currentIndex).each(function () {
                if (jQuery(this).css("display") != "none")
                    jQuery(this).slideUp();
                else
                    jQuery(this).slideDown();
            });
        });


        jQuery(document).on('click', '.parent_row_index td div img', function () {
            //debugger;
            var currentIndex = jQuery(this).closest(".parent_row_index").data("parent_row_index");
            var minimised = false;
            jQuery(".parent_row_index_" + currentIndex).each(function () {
                if (jQuery(this).data("toogle_status") == "MAXIMISED")
                    jQuery(this).find("td div img").click();
                var nextElement = jQuery(this).prev();
                if (jQuery(this).css("display") != "none") {
                    jQuery(this).slideUp();
                    if (jQuery(nextElement).hasClass("empty_row"))
                        jQuery(nextElement).slideUp();
                }
                else {
                    jQuery(this).slideDown();
                    if (jQuery(nextElement).hasClass("empty_row"))
                        jQuery(nextElement).slideDown();
                    minimised = true;
                }
            });


            if (minimised == true) {

                console.log(jQuery(this).attr("src"));

                console.log(jQuery(this).attr('id'));
                console.log(jQuery(this).attr("src"));
                jQuery(this).attr("src", "");
                jQuery(this).attr("src", "images/button_minus.gif");
                console.log(jQuery(this).attr("src"));

                //console.log("minimising");
                var anamika = jQuery(this).attr('id');
                gridviewScroll();
                //setTimeout(function () { console.log($('#' + anamika).attr('src')); gridviewScroll(9, anamika); }, 3000);

                console.log(jQuery(this).attr("src"));
            }
            else {
                /* $('#MainContent_grdviewAllValues').gridviewScroll({
                     enabled: false
                 });
                 console.log(jQuery(this).attr("src"));*/
                jQuery(this).attr("src", "images/button_plus.gif");
                gridviewScroll();
                //var anamika = jQuery(this).attr('id');
                //setTimeout(function () { console.log($('#' + anamika).attr('src')); gridviewScroll(9, anamika); }, 3000);*/
            }

            //if (localStorage.getItem("freezeSize")==9) {
            //    gridviewScroll(0);
            //}
            //else {
            //    gridviewScroll(0);
            //}

            //gridviewScroll(0);

        });
        jQuery(document).on('click', '.row_index_image', function () {
            debugger;
            var currentRowElement = jQuery(this).closest(".row_index");
            var currentIndex = jQuery(currentRowElement).data("index");
            var minimised = false;
            jQuery(".subrowindex_" + currentIndex).each(function () {
                if (jQuery(this).css("display") != "none") {
                    jQuery(this).slideUp();
                    minimised = true;
                }
                else {
                    jQuery(this).slideDown();
                    minimised = false;
                }
            });
            if (minimised) {
                jQuery(this).attr("src", "images/button_plus.gif");
                jQuery(currentRowElement).data("toogle_status", "MINIMISED");
            }
            else {
                jQuery(this).attr("src", "images/button_minus.gif");
                jQuery(currentRowElement).data("toogle_status", "MAXIMISED");
            }
            gridviewScroll();
        });
        jQuery(document).on('click', '#MainContent_expand', function () {
            debugger;
            console.log('outside');
            $("#MainContent_grdBudSummary").find("tr.parent_row_index td div img").each(function () {
                console.log('here 3');
                var currentIndex = jQuery(this).closest(".parent_row_index").data("parent_row_index");
                var minimised = false;
                jQuery(".parent_row_index_" + currentIndex).each(function () {
                    if (jQuery(this).data("toogle_status") == "MAXIMISED")
                        jQuery(this).find("td div img").click();
                    var nextElement = jQuery(this).prev();
                    if (jQuery(this).css("display") != "none") {
                    }
                    else {
                        jQuery(this).slideDown();
                        if (jQuery(nextElement).hasClass("empty_row"))
                            jQuery(nextElement).slideDown();
                        minimised = true;
                    }
                });
                jQuery(this).attr("src", "images/button_minus.gif");
                console.log('here 4');

            });
            $("#MainContent_grdBudSummary").find("tr td .row_index_image").each(function () {
                console.log('here 5');
                var currentRowElement = jQuery(this).closest(".row_index");
                var currentIndex = jQuery(currentRowElement).data("index");
                var minimised = false;
                jQuery(".subrowindex_" + currentIndex).each(function () {
                    if (jQuery(this).css("display") != "none") {
                        minimised = true;
                    }
                    else {
                        jQuery(this).slideDown();
                        minimised = false;
                    }
                });
                if (minimised) {
                }
                else {
                    jQuery(this).attr("src", "images/button_minus.gif");
                    jQuery(currentRowElement).data("toogle_status", "MAXIMISED");
                }
                console.log('here 6');
            });
            triggerPostGridLodedActions();
        });
        jQuery(document).on('click', '#MainContent_collapse', function () {
            debugger;
            $("#MainContent_grdBudSummary").find("tr.parent_row_index td div img").each(function () {
                console.log('here 3');
                var currentIndex = jQuery(this).closest(".parent_row_index").data("parent_row_index");
                var minimised = false;
                jQuery(".parent_row_index_" + currentIndex).each(function () {
                    if (jQuery(this).data("toogle_status") == "MAXIMISED")
                        jQuery(this).find("td div img").click();
                    var nextElement = jQuery(this).prev();
                    if (jQuery(this).css("display") != "none") {
                        jQuery(this).slideUp();
                        if (jQuery(nextElement).hasClass("empty_row"))
                            jQuery(nextElement).slideUp();
                    }

                });
                if (minimised) {
                    //jQuery(this).attr("src", "images/button_minus.gif");
                }
                else
                    jQuery(this).attr("src", "images/button_plus.gif");
                console.log('here 4');

            });
            $("#MainContent_grdBudSummary").find("tr td .row_index_image").each(function () {
                console.log('here 5');
                var currentRowElement = jQuery(this).closest(".row_index");
                var currentIndex = jQuery(currentRowElement).data("index");
                var minimised = false;
                jQuery(".subrowindex_" + currentIndex).each(function () {
                    if (jQuery(this).css("display") != "none") {
                        jQuery(this).slideUp();
                        minimised = true;
                    }
                    else {
                        //jQuery(this).slideDown();
                        minimised = false;
                    }
                });
                if (minimised) {
                    jQuery(this).attr("src", "images/button_plus.gif");
                    jQuery(currentRowElement).data("toogle_status", "MINIMISED");
                }
                else {
                    jQuery(this).attr("src", "images/button_plus.gif");
                    jQuery(currentRowElement).data("toogle_status", "MINIMISED");
                    //jQuery(this).attr("src", "images/button_minus.gif");
                    //jQuery(currentRowElement).data("toogle_status", "MAXIMISED");
                }
                console.log('here 6');
            });
            //triggerPostGridLodedActions();
        });
        function LoadCharts() {
            debugger;
            var tmp = null;
            $.ajax({
                type: "POST",
                url: 'PuchaseBudgetSummary.aspx/LoadSummaryTable',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("Success:");
                    tmp = msg.d;
                    LoadTable(tmp);
                },
                error: function (e) {
                    console.log("Error:");
                    console.log(e);
                }
            });
        }
        function LoadTable(tmp) {
            debugger;
            console.log(tmp);
            tmp = jQuery.parseJSON(tmp);
            //tmp = [{ "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "", "APPLICATION_DESC": "", "Quantity": "2", "Value": "15188" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "DRILL RUSH", "APPLICATION_DESC": "", "Quantity": "2", "Value": "15188" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "DRILL RUSH", "APPLICATION_DESC": "DRILL RUSH HEADS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "DRILL RUSH", "APPLICATION_DESC": "DRILL RUSH HEADS", "Quantity": "2", "Value": "15188" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "DRILL RUSH", "APPLICATION_DESC": "DRILL RUSH TOOLS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "REAMING", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "REAMING", "APPLICATION_DESC": "SOLID TS-REAM STD", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "S.CARB H-DRILL", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "S.CARB H-DRILL", "APPLICATION_DESC": "SC DRILLS WITH COOLANT", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "S.CARB H-DRILL", "APPLICATION_DESC": "SOLID CARBIDE SPECIAL DRILLS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "TOP DRILL/T-DRL", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "TOP DRILL/T-DRL", "APPLICATION_DESC": "DRILLING OTHER/SPECIAL INSERT", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "TOP DRILL/T-DRL", "APPLICATION_DESC": "T-DRILL INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "TOP DRILL/T-DRL", "APPLICATION_DESC": "T-DRILLS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "TOP DRILL/T-DRL", "APPLICATION_DESC": "TOP DRILL INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "TOP DRILL/T-DRL", "APPLICATION_DESC": "TOP DRILL TOOLS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "OTHER GROOVING", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "OTHER GROOVING", "APPLICATION_DESC": "QUADRUSH GROOVING INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "OTHER GROOVING", "APPLICATION_DESC": "QUADRUSH TOOLS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "OTHER GROOVING", "APPLICATION_DESC": "T-GROOVE SPC TOOLS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "T-CLAMP GROOVE", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "T-CLAMP GROOVE", "APPLICATION_DESC": "CG TOOLS FOR ALUMINUM", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "T-CLAMP GROOVE", "APPLICATION_DESC": "SPECIAL/OTHER GROOVING INS.", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "T-CLAMP GROOVE", "APPLICATION_DESC": "T-CLAMP GROOVE EXT.TLS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "T-CLAMP GROOVE", "APPLICATION_DESC": "T-CLAMP GROOVE INT.TLS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "T-CLAMP GROOVE", "APPLICATION_DESC": "T-CLAMP GRV IN.(NON ROUND SIZE", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "T-CLAMP GROOVE", "APPLICATION_DESC": "T-CLAMP INS FOR ALUMINUM", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "T-CLAMP GROOVE", "APPLICATION_DESC": "T-CLAMP PRESSED INS.( M TYPE)", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "T-CLAMP GROOVE", "APPLICATION_DESC": "T-CLAMP T/GRV INS.(ROUND SIZE)", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "T-CLAMP PARTING", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "T-CLAMP PARTING", "APPLICATION_DESC": "T-CLAMP PARTING OFF BLADES", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "T-CLAMP PARTING", "APPLICATION_DESC": "T-CLAMP PARTING OFF INS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "D&M - ROUGHING", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "D&M - ROUGHING", "APPLICATION_DESC": "CHASE 2 FEED INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "D&M - ROUGHING", "APPLICATION_DESC": "CHASE 2 FEED TOOLS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "D&M - ROUGHING", "APPLICATION_DESC": "CHASE FEED INSERT(SBMT 09,13)", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "DIE & MOLD PROD", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "DIE & MOLD PROD", "APPLICATION_DESC": "BUTTON INSERTS(5-20MM)", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "DIE & MOLD PROD", "APPLICATION_DESC": "CHASE BALL INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "DIE & MOLD PROD", "APPLICATION_DESC": "CHASE BALL QUAD INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "DIE & MOLD PROD", "APPLICATION_DESC": "CUTTER FOR BUTTON(5-20) INS.", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "DIE & MOLD PROD", "APPLICATION_DESC": "FINE BALL INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "DIE & MOLD PROD", "APPLICATION_DESC": "FINE BALL TOOLS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "DIE & MOLD PROD", "APPLICATION_DESC": "PLUNGER CUTTERS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "HEXA&HEPTA MILL", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "HEXA&HEPTA MILL", "APPLICATION_DESC": "CHASE 2 HEPTA 14 INS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "HEXA&HEPTA MILL", "APPLICATION_DESC": "CHASE 2 HEPTA(14MM) CUTTERS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "HEXA&HEPTA MILL", "APPLICATION_DESC": "HEXA2MILL INS.(05,10MM)", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "HEXA&HEPTA MILL", "APPLICATION_DESC": "HEXA2MILL INS.(05,10MM) CUTTER", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "HEXA&HEPTA MILL", "APPLICATION_DESC": "OTHER HEXAGON/HEPTA INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "HEXA&HEPTA MILL", "APPLICATION_DESC": "OTHER/SPC HEXA&HEPTA CUTTERS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "ISO MILL", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "ISO MILL", "APPLICATION_DESC": "LION SCREW CLAMPING  CUTTER", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "ISO MILL", "APPLICATION_DESC": "LION SCREW CLAMPING  INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "ISO MILL", "APPLICATION_DESC": "LION WEDGE CLAMPING  INSERTSS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "ISO MILL", "APPLICATION_DESC": "LS 2 QUAD/TRIO INS(DOUBLE SIDE", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "ISO MILL", "APPLICATION_DESC": "SPECIAL/OTHER ISO MILL INSERT", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "ISO MILL", "APPLICATION_DESC": "TS-THREAD INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(D)", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(D)", "APPLICATION_DESC": "CHASE 2 MILL 09,06MM INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(D)", "APPLICATION_DESC": "CHASE 2 MILL 11,16MM INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(D)", "APPLICATION_DESC": "CHASE 2 MILL CUTTERS(09,06MM)", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(D)", "APPLICATION_DESC": "CHASE 2 MILL CUTTERS(11,16MM)", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(D)", "APPLICATION_DESC": "CHASE 2 QUAD CUTTER", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(D)", "APPLICATION_DESC": "CHASE 2 QUAD INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(D)", "APPLICATION_DESC": "MILL 2 RUSH CUTTERS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(D)", "APPLICATION_DESC": "MILL 2 RUSH INS.(DOUBLE SIDE)", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "CHASE ALU CUTTERS 16MM", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "CHASE ALU INSERTS 16MM", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "CHASE OTHER/SPECIAL INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "CHASEMILL END MILLS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "CHASEMILL FACE MILLS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "CHASEMILL INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "CHASEMILL SPECIAL CUTTERS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "CHASEOCTO CUTTERS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "CHASEOCTO INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "CHASEQUAD  ENDMILL", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "CHASEQUAD FACE/FLUT/SLOT CTRS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "CHASEQUAD INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "MILL RUSH CUTTERS(TRIO)", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "MILL RUSH INSERTS(TRIO)", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "SOLID CARB MILL", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "SOLID CARB MILL", "APPLICATION_DESC": "SOLID CARBIDE - SPECIAL", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "SOLID CARB MILL", "APPLICATION_DESC": "SOLID CARBIDE BRAZED ENDMILLS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "SOLID CARB MILL", "APPLICATION_DESC": "SOLID-FLAT ENDMILL", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "SOLID CARB MILL", "APPLICATION_DESC": "SOLID-ROUGHER ENDMILL", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "TANGENTIAL MILL", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "TANGENTIAL MILL", "APPLICATION_DESC": "TANGENTIAL SPC/OTH CUTTERS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "TANGENTIAL MILL", "APPLICATION_DESC": "TANGENTIAL SPC/OTH INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "TANGENTIAL MILL", "APPLICATION_DESC": "TOPSLOT CUTTERS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "TANGENTIAL MILL", "APPLICATION_DESC": "TOPSLOT INSERT(03-26MM)", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "OTHER", "SUBFAMILY_NAME": "", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "OTHER", "SUBFAMILY_NAME": "SPARE PARTS", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "OTHER", "SUBFAMILY_NAME": "SPARE PARTS", "APPLICATION_DESC": "MILLING SPARE PARTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "OTHER", "SUBFAMILY_NAME": "SPARE PARTS", "APPLICATION_DESC": "SPARE PARTS FOR TOOLS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "OTHER", "SUBFAMILY_NAME": "SPARE PARTS", "APPLICATION_DESC": "TURING SPARE PARTS - CARBIDE", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TOOLING SYSTEMS", "SUBFAMILY_NAME": "", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TOOLING SYSTEMS", "SUBFAMILY_NAME": "MPT BORING TOOL", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TOOLING SYSTEMS", "SUBFAMILY_NAME": "MPT BORING TOOL", "APPLICATION_DESC": "MPT EXTENSION REDUCTION", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TOOLING SYSTEMS", "SUBFAMILY_NAME": "MPT BORING TOOL", "APPLICATION_DESC": "MPT HEAD", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TOOLING SYSTEMS", "SUBFAMILY_NAME": "MPT BORING TOOL", "APPLICATION_DESC": "MPT SHANKS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TOOLING SYSTEMS", "SUBFAMILY_NAME": "MPT BORING TOOL", "APPLICATION_DESC": "MPT TOOLS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TOOLING SYSTEMS", "SUBFAMILY_NAME": "TOOLING SYSTEMS", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TOOLING SYSTEMS", "SUBFAMILY_NAME": "TOOLING SYSTEMS", "APPLICATION_DESC": "COLLET HOLDERS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TOOLING SYSTEMS", "SUBFAMILY_NAME": "TOOLING SYSTEMS", "APPLICATION_DESC": "ENDMILL HOLDERS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TOOLING SYSTEMS", "SUBFAMILY_NAME": "TOOLING SYSTEMS", "APPLICATION_DESC": "SHELL MILL HOLDERS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TOOLING SYSTEMS", "SUBFAMILY_NAME": "TOOLING SYSTEMS", "APPLICATION_DESC": "SPRING COLLETS, KITS & SETS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TOOLING SYSTEMS", "SUBFAMILY_NAME": "TOOLING SYSTEMS", "APPLICATION_DESC": "T-HOLD/OTHER ACCESSORIES", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TOOLING SYSTEMS", "SUBFAMILY_NAME": "TOOLING SYSTEMS", "APPLICATION_DESC": "T-HOLD/PLUSTAD", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "", "APPLICATION_DESC": "", "Quantity": "2", "Value": "5474" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "MINIATURE", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "MINIATURE", "APPLICATION_DESC": "TOP MINI INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTHREAD", "APPLICATION_DESC": "", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTHREAD", "APPLICATION_DESC": "TAEGUTHREAD  INSERTS M TYPE", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTHREAD", "APPLICATION_DESC": "TAEGUTHREAD INSERTS-GROUND", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTHREAD", "APPLICATION_DESC": "TAEGUTHREAD TOOLS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "", "Quantity": "2", "Value": "5474" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "CERAMICS/SIN INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "CERMET INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "GOLDRUSH CVD INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "GOLDRUSH PVD INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "ISO TOOLS FOR ECO INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "RHINO(ECO) INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TAEGUTURN CVD INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TAEGUTURN HOLDERS EXTERNAL", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TAEGUTURN HOLDERS INTERNAL", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TAEGUTURN PVD INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TAEGUTURN SPECIAL INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TAEGUTURN T-TYPE HOLDERS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TAEGUTURN T-TYPE HOLDERS", "Quantity": "2", "Value": "5474" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TAEGUTURN UC INSERTS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TURN INSERTS FOR ALUMINUM", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TURNRUSH HOLDERS", "Quantity": "0", "Value": "0" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TURNRUSH INSERTS", "Quantity": "0", "Value": "0" }];
            var table = $('#example').DataTable({
                "data": tmp,
                "columns": [
                   {
                       "className": 'details-control',
                       "orderable": true,
                       "data": null,
                       "defaultContent": ''
                   },
                   { "data": "FAMILY_NAME" },
                   { "data": "Quantity" },
                   { "data": "Value" }
                ],
                "order": [[1, 'asc']],
                "rowGroup": "FAMILY_NAME"
            });

            // Add event listener for opening and closing details


        }

        function format(callback, id) {
            debugger;
            console.log("id: " + id.FAMILY_NAME);
            var param = id.FAMILY_NAME;
            $.ajax({
                type: "POST",
                url: 'PuchaseBudgetSummary.aspx/LoadSummarySubTable',
                data: "{'familyname' :'" + param + "'}",// passing the parameter 
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var data = JSON.parse(msg.d);
                    var tdata = subfamily(data);
                    callback($('<table id="table1" class="display" cellspacing="0" width="100%">' + tdata + '</table>')).show();
                    $('#table1').DataTable({
                        "data": data,
                        "columns": [
                           {
                               "className": 'details-control',
                               "orderable": true,
                               "data": null,
                               "defaultContent": ''
                           },
                           { "data": "SUBFAMILY_NAME" },
                           { "data": "SubQuantity" },
                           { "data": "SubValue" }
                        ],
                        "order": [[1, 'asc']],
                        "searching": false,
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": false,
                        "bInfo": false,
                        "bAutoWidth": false
                    });
                },
                error: function (e) {
                    console.log("Error1:");
                    console.log(e);
                }
            });
        }
        function subfamily(data) {
            debugger;
            var thead = '', tbody = '';
            for (var key in data[0]) {
                thead += '<th style="width: 60px;"></th><th>SUBFAMILY_NAME</th><th>Quantity</th><th>Value</th>';
            }

            // $.each(data, function (i, d) {
            for (var x = 0; x < data.length ; x++) {
                tbody += '<tr><td style="width:290px">' + data[x].SUBFAMILY_NAME + '</td><td style="width:210px">' + data[x].SubQuantity + '</td><td style="width:100px">' + data[x].SubValue + '</td><td style="width:100px">' +
              '</td></tr>';

            }
            //});
            //callback($('<table>' + thead + tbody + '</table>')).show();
            return tbody;
        }
        function formatapp(callback, id) {
            debugger;
            console.log("id: " + id.FAMILY_NAME);
            var param = id.FAMILY_NAME;
            $.ajax({
                type: "POST",
                url: 'PuchaseBudgetSummary.aspx/LoadSummarySubTable',
                data: "{'familyname' :'" + param + "'}",// passing the parameter 
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var data = JSON.parse(msg.d);
                    var tdata = subfamily(data);
                    callback($('<table id="table2" class="display" cellspacing="0" width="100%">' + tdata + '</table>')).show();
                    $('#table1').DataTable({
                        "data": data,
                        "columns": [
                           {
                               "className": 'details-control',
                               "orderable": true,
                               "data": null,
                               "defaultContent": ''
                           },
                           { "data": "SUBFAMILY_NAME" },
                           { "data": "SubQuantity" },
                           { "data": "SubValue" }
                        ],
                        "order": [[1, 'asc']],
                        "searching": false,
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": false,
                        "bInfo": false,
                        "bAutoWidth": false
                    });
                },
                error: function (e) {
                    console.log("Error1:");
                    console.log(e);
                }
            });
        }
        $(document).ready(function () {
            debugger;

            var head_content = $('#MainContent_grdBudSummary tr:first').html();
            $('#MainContent_grdBudSummary').prepend('<thead></thead>')
            $('#MainContent_grdBudSummary thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdBudSummary tbody tr:first').hide();

            $('#MainContent_grdBudSummary').DataTable({
                scrollY: '500px',
                scrollX: '100%',
                sScrollXInner: "100%",
                //paging: false,
                sorting: false,
                fixedColumns: {
                    leftColumns: 3

                }
            });

            //// Add event listener for opening and closing details
            //$('#example').on('click', 'td.details-control', function () {
            //    var table = $('#example').DataTable();
            //    var tr = $(this).closest('tr');
            //    var row = table.row(tr);
            //    var rowdata = (table.row(tr).data());
            //    if (row.child.isShown()) {
            //        // This row is already open - close it
            //        row.child.hide();
            //        tr.removeClass('shown');
            //    } else {
            //        console.log(table.row(tr).data());
            //        // Open this row
            //        format(row.child, table.row(tr).data());
            //        tr.addClass('shown');
            //    }
            //});
            //$('#example').on('click', ' td table td.details-control', function () {
            //    var table = $('#table1').DataTable();
            //    var tr = $(this).closest('tr');
            //    var row = table.row(tr);
            //    var rowdata = (table.row(tr).data());
            //    if (row.child.isShown()) {
            //        // This row is already open - close it
            //        row.child.hide();
            //        tr.removeClass('shown');
            //    } else {
            //        console.log(table.row(tr).data());
            //        // Open this row
            //        formatapp(row.child, table.row(tr).data());
            //        tr.addClass('shown');
            //    }
            //});
            //   //$('#example tbody').on('click', 'td.details-control', function () {
            //   //    var tr = $(this).closest('tr');
            //   //    var row = table.row(tr);

            //   //    if (row.child.isShown()) {
            //   //        // This row is already open - close it
            //   //        row.child.hide();
            //   //        tr.removeClass('shown');
            //   //    }
            //   //    else {
            //   //        // Open this row
            //   //        row.child(format(row.data())).show();
            //   //        tr.addClass('shown');
            //   //    }
            //   //});
        });
    </script>
    <style>
        td.details-control
        {
            background: url('../resources/details_open.png') no-repeat center center;
            cursor: pointer;
        }

        tr.shown td.details-control
        {
            background: url('../resources/details_close.png') no-repeat center center;
        }
        
        table.dataTable.no-footer
        {
            background:#fff;
        }
        .dataTables_wrapper
        {
            margin: 15px 0;
            width: 100%;
            float: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Summary</a>
                        </li>
                        <li class="current">Purchase Budget Summary</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div>
       <%-- <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th></th>
                    <th>FAMILY_NAME</th>
                    <th>Quantity</th>
                    <th>Value</th>
                </tr>
            </thead>
           
        </table>--%>
        <asp:GridView runat="server" ID="grdBudSummary" OnRowDataBound="grdBudSummary_RowDataBound" AutoGenerateColumns="false" CssClass="display compact">
            <Columns>
                <asp:TemplateField HeaderText="FamilyName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblFamily" Text='<%#Bind("FAMILY_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Sub-FamilyName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblSubFamily" Text='<%#Bind("SUBFAMILY_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Application">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblApp" Text='<%#Bind("APPLICATION_DESC") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_BUDGET_MTD_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear1" Text='<%#Bind("CY_BUDGET_MTD_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_BUDGET_MTD_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear2" Text='<%#Bind("CY_BUDGET_MTD_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_BUDGET_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear3" Text='<%#Bind("CY_BUDGET_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_BUDGET_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty1" Text='<%#Bind("CY_BUDGET_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="NY_BUDGET_MTD_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty2" Text='<%#Bind("NY_BUDGET_MTD_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="NY_BUDGET_MTD_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty3" Text='<%#Bind("NY_BUDGET_MTD_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LY_SALES_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear1" Text='<%#Bind("LY_SALES_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_SALES_PROJECTED_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear3" Text='<%#Bind("CY_SALES_PROJECTED_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="NY_BUDGET_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear1" Text='<%#Bind("NY_BUDGET_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="NY_BUDGET_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear2" Text='<%#Bind("NY_BUDGET_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LY_SALES_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear2" Text='<%#Bind("LY_SALES_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_SALES_PROJECTED_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty1" Text='<%#Bind("CY_SALES_PROJECTED_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 
             
                  <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="NY_INSERTS_PER_TOOL"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty2" Text='<%#Bind("NY_INSERTS_PER_TOOL") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_INSERTS_PER_TOOL"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty3" Text='<%#Bind("CY_INSERTS_PER_TOOL") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LY_INSERTS_PER_TOOL"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear1" Text='<%#Bind("LY_INSERTS_PER_TOOL") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
               <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="NY_INCREASE_IN_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear3" Text='<%#Bind("NY_INCREASE_IN_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_INCREASE_IN_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty1" Text='<%#Bind("CY_INCREASE_IN_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                  <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="NY_INCREASE_IN_QTY_PCT"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear1" Text='<%#Bind("NY_INCREASE_IN_QTY_PCT") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_INCREASE_IN_QTY_PCT"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear2" Text='<%#Bind("CY_INCREASE_IN_QTY_PCT") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="NY_INCREASE_IN_VALUE_PCT"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear3" Text='<%#Bind("NY_INCREASE_IN_VALUE_PCT") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_INCREASE_IN_VALUE_PCT"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty1" Text='<%#Bind("CY_INCREASE_IN_VALUE_PCT") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="NY_RUPEES_PER_PIECE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear1" Text='<%#Bind("NY_RUPEES_PER_PIECE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_RUPEES_PER_PIECE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear2" Text='<%#Bind("CY_RUPEES_PER_PIECE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LY_RUPEES_PER_PIECE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear3" Text='<%#Bind("LY_RUPEES_PER_PIECE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="NY_PRICE_CHANGE_PER_PIECE_PCT"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear3" Text='<%#Bind("NY_PRICE_CHANGE_PER_PIECE_PCT") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_PRICE_CHANGE_PER_PIECE_PCT"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty1" Text='<%#Bind("CY_PRICE_CHANGE_PER_PIECE_PCT") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LY_PRICE_CHANGE_PER_PIECE_PCT"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty2" Text='<%#Bind("LY_PRICE_CHANGE_PER_PIECE_PCT") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
               
                
            <%--     <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LLY_SALES_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear3" Text='<%#Bind("LLY_SALES_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LLY_SALES_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty1" Text='<%#Bind("LLY_SALES_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_SALES_MTD_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear3" Text='<%#Bind("CY_SALES_MTD_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_SALES_MTD_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty1" Text='<%#Bind("CY_SALES_MTD_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_SALES_YTD_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty2" Text='<%#Bind("CY_SALES_YTD_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_SALES_YTD_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty3" Text='<%#Bind("CY_SALES_YTD_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LY_SALES_MTD_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear1" Text='<%#Bind("LY_SALES_MTD_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LY_SALES_MTD_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear2" Text='<%#Bind("LY_SALES_MTD_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LY_SALES_YTD_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear3" Text='<%#Bind("LY_SALES_YTD_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LY_SALES_YTD_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty1" Text='<%#Bind("LY_SALES_YTD_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LLY_SALES_MTD_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty2" Text='<%#Bind("LLY_SALES_MTD_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LLY_SALES_MTD_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty3" Text='<%#Bind("LLY_SALES_MTD_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LLY_SALES_YTD_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear1" Text='<%#Bind("LLY_SALES_YTD_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LLY_SALES_YTD_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear2" Text='<%#Bind("LLY_SALES_YTD_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_SALES_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty2" Text='<%#Bind("CY_SALES_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_SALES_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty3" Text='<%#Bind("CY_SALES_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

               
               <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LLY_INSERTS_PER_TOOL"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear2" Text='<%#Bind("LLY_INSERTS_PER_TOOL") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LY_INCREASE_IN_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty2" Text='<%#Bind("LY_INCREASE_IN_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LLY_INCREASE_IN_QTY"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty3" Text='<%#Bind("LLY_INCREASE_IN_QTY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LY_INCREASE_IN_QTY_PCT"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear3" Text='<%#Bind("LY_INCREASE_IN_QTY_PCT") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LLY_INCREASE_IN_QTY_PCT"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty1" Text='<%#Bind("LLY_INCREASE_IN_QTY_PCT") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="NY_INCREASE_IN_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty2" Text='<%#Bind("NY_INCREASE_IN_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_INCREASE_IN_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty3" Text='<%#Bind("CY_INCREASE_IN_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LY_INCREASE_IN_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear1" Text='<%#Bind("LY_INCREASE_IN_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LLY_INCREASE_IN_VALUE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear2" Text='<%#Bind("LLY_INCREASE_IN_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LY_INCREASE_IN_VALUE_PCT"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty2" Text='<%#Bind("LY_INCREASE_IN_VALUE_PCT") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LLY_INCREASE_IN_VALUE_PCT"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty3" Text='<%#Bind("LLY_INCREASE_IN_VALUE_PCT") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LLY_RUPEES_PER_PIECE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty1" Text='<%#Bind("LLY_RUPEES_PER_PIECE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

  <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="NY_PRICE_CHANGE_PER_PIECE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty2" Text='<%#Bind("NY_PRICE_CHANGE_PER_PIECE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="CY_PRICE_CHANGE_PER_PIECE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty3" Text='<%#Bind("CY_PRICE_CHANGE_PER_PIECE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LY_PRICE_CHANGE_PER_PIECE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear1" Text='<%#Bind("LY_PRICE_CHANGE_PER_PIECE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LLY_PRICE_CHANGE_PER_PIECE"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear2" Text='<%#Bind("LLY_PRICE_CHANGE_PER_PIECE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="LLY_PRICE_CHANGE_PER_PIECE_PCT"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblQty3" Text='<%#Bind("LLY_PRICE_CHANGE_PER_PIECE_PCT") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                --%>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
