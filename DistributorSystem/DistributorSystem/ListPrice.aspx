﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListPrice.aspx.cs" Inherits="DistributorSystem.ListPrice" MasterPageFile="~/MasterPage.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var table1;
        $(document).ready(function () {
            debugger;
            //LoadTable();
        });
        function LoadTable() {
            var head_content = $('#MainContent_grdListPrice tr:first').html();
            $('#MainContent_grdListPrice').prepend('<thead></thead>')
            $('#MainContent_grdListPrice thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdListPrice tbody tr:first').hide();
            $('#MainContent_grdListPrice').DataTable(
                {
                    "info": false
                    //"paging": false,
                    //"sScrollY": ($(window).height() - 200),
                });

            var head_content1 = $('#MainContent_grdListPrice1 tr:first').html();
            $('#MainContent_grdListPrice1').prepend('<thead></thead>')
            $('#MainContent_grdListPrice1 thead').html('<tr>' + head_content1 + '</tr>');
            $('#MainContent_grdListPrice1 tbody tr:first').hide();
            $('#MainContent_grdListPrice1').DataTable(
                {
                    "info": false,
                    "paging": false,
                    "sScrollY": ($(window).height() - 200),
                });


        }

        function LoadSecondGrid() {
            var $RowSelected = $("#MainContent_grdListPrice1");

            if ($RowSelected.length > 0) {
                $("#selectedlist").show();
                $("#MainContent_txtsearch1").show();
                $("#MainContent_searchbtn1").show();
                $(".rightside_box").hide();
                $("#MainContent_Panel2").css({
                    "border-color": "#ddd",
                    "border-width": "1px",
                    "border-style": "solid"
                });

            }
        }


        //$(function () {
        //    $("#MainContent_grdListPrice [id*=chk_All]").click(function () {
        //        if ($(this).is(":checked")) {
        //            $("#MainContent_grdListPrice [id*=chk_multi]").attr("checked", "checked");
        //        } else {
        //            $("#MainContent_grdListPrice [id*=chk_multi]").removeAttr("checked");
        //        }
        //    });
        //    $("#MainContent_grdListPrice [id*=chk_multi]").click(function () {
        //        if ($("#MainContent_grdListPrice [id*=chk_multi]").length == $("#MainContent_grdListPrice [id*=chk_multi]:checked").length) {
        //            $("#MainContent_grdListPrice [id*=chk_All]").attr("checked", "checked");
        //        } else {
        //            $("#MainContent_grdListPrice [id*=chk_All]").removeAttr("checked");
        //        }
        //    });
        //});

        function selectAll(obj) {
            debugger;
            var id = obj.children[0].id.replace('chk_All', 'hdn_chk_All');
            if ($('#' + id).val() == "0")
                $('#' + id).val("1");
            else
                $('#' + id).val("0");
            var totaltrCount = $("[id*=MainContent_grdListPrice] tr").length;
            var trCount = totaltrCount;
            //sessionStorage.getItem("tot_Count");
            //$("[id*=grdPendingQuote] td").closest("tr").length;
            var multi_btn_flag = 0;
            var hdn_chk_id;
            var chk_id;
            if ($('#' + id).val() == "0") {
                for (i = 0; i < trCount; i++) {
                    hdn_chk_id = "MainContent_grdListPrice_Hdnchk_" + i;
                    chk_id = "MainContent_grdListPrice_chk_multi_" + i;
                    $("input[type='checkbox']").prop('checked', false);
                    //$("input[id=" + chk_id + "]").prop('checked', false);
                    $('#' + hdn_chk_id).val("0");

                }

            }
            else {
                for (i = 0; i < trCount; i++) {
                    hdn_chk_id = "MainContent_grdListPrice_Hdnchk_" + i;
                    chk_id = "MainContent_grdListPrice_chk_multi_" + i;
                    $("input[type='checkbox']").prop('checked', true);
                    //$("input[id=" + chk_id + "]").prop('checked', true);
                    $('#' + hdn_chk_id).val("1");

                }

            }
        }



        //$(document).on('click', '.chk_multi_add', function (e) {
        //    $(".loader_div").show();
        //});

        //$(document).on('click', '.chk1', function (e) {
        //    $(".loader_div").show();

        //});

        //$(document).on('click', '.chk_multi_All', function (e) {
        //    $(".loader_div").show();

        //});

        //$(document).on('click', '.chk_multi_All1', function (e) {
        //    $(".loader_div").show();

        //});

        //$(document).on('click', '.search', function (e) {
        //    $(".loader_div").show();

        //});



        //$(document).on('click', '.clear', function (e) {
        //    $(".loader_div").show();

        //});

    </script>
    <style>
        .search {
            position: absolute;
            top: 3px;
            right: 4px;
            width: 20px;
        }

        .search1 {
            position: absolute;
            top: 3px;
            right: 17px;
            width: 20px;
        }


        .GridPager a,
        .GridPager span {
            display: inline-block;
            padding: 0px 9px;
            margin-right: 4px;
            border-radius: 3px;
            border: solid 1px #c0c0c0;
            background: #e9e9e9;
            box-shadow: inset 0px 1px 0px rgba(255,255,255, .8), 0px 1px 3px rgba(0,0,0, .1);
            font-size: .875em;
            font-weight: bold;
            text-decoration: none;
            color: #717171;
            text-shadow: 0px 1px 0px rgba(255,255,255, 1);
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background: #375b67;
            box-shadow: inset 0px 0px 8px rgba(0,0,0, .5), 0px 1px 0px rgba(255,255,255, .8);
            color: #f0f0f0;
            text-shadow: 0px 0px 3px rgba(0,0,0, .5);
            border: 1px solid #3AC0F2;
        }
.loader_div
        {
            position: fixed;
            top: 0;
            bottom: 0%;
            left: 40%;
            right: 0%;
            z-index: 99;
            opacity: 0.7;
            display: none;
            //background: lightgrey url('../../../images/loader.gif') center center no-repeat;
//background: lightgrey url('../../../images/select2-spinner.gif') center center no-repeat;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <asp:ScriptManager ID="SM1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>

    <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">

        <ContentTemplate>
            <div>
                <div class='block-web' style='float: left; width: 100%; height: 36px;'>
                    <div class="header">
                        <div class="crumbs">
                            <!-- Start : Breadcrumbs -->
                            <ul id="breadcrumbs" class="breadcrumb">
                                <li>
                                    <a class="mn_breadcrumb">Price List</a>
                                </li>
                                <li class="current">List Price</li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <%--<div class="col-md-6 mn_margin" style="margin-top: 2%; max-height:400px;overflow:auto">--%>
            <div class="col-md-6 mn_margin" style="margin-top: 2%;">
                <div class="control-group">
                    <label class="control-label">CUSTOMER NAME : </label>
                   
                        <asp:DropDownList runat="server" ID="CustNameList" style="margin-left: 20px;" AutoPostBack="true" OnSelectedIndexChanged="CustNameList_SelectedIndexChanged1"></asp:DropDownList>
                 
                </div>
                <div class="filter_panel" style="color: white; padding: 9px; font-weight: bold;margin:-1px 0 0 0">Select List Price To Download</div>

                
                <asp:Panel runat="server" ID="pnlEdit" Style="margin-top: 0;overflow: auto;margin-right: -1px;height: 355px;border: solid 1px #ddd;padding: 10px;">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <label class="label" style="color: black; font-size: 14px;margin-left:-39px;">Show </label>
                            <asp:DropDownList ID="ddlselect" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlselect_SelectedIndexChanged">
                                <asp:ListItem Text="10" Value="10" />
                                <asp:ListItem Text="25" Value="25" />
                                <asp:ListItem Text="50" Value="50" />
                                <asp:ListItem Text="100" Value="100" />
                            </asp:DropDownList>
                        </div>
                        <div class="wrapper" style="position: relative">
                            <asp:TextBox runat="server" ID="txtsearch" placeholder="Search" Style="float: right;margin-bottom: 10px;"></asp:TextBox>
                            <asp:ImageButton runat="server" CssClass="search" ImageUrl="images/Search.png" ID="searchbtn" OnClick="searchbtn_Click" />
                            <%--<asp:Button ID="Button1" runat="server" Text="Go" style="position:absolute;top:0;right:0;" />--%>
                        </div>
                    </div>
                    <asp:GridView ID="grdListPrice" CssClass="display compact" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        PageSize="10" OnPageIndexChanging="grdListPrice_PageIndexChanging" AllowSorting="true" Style="border-collapse: collapse; width: 100%; border: azure;">
                        <PagerStyle HorizontalAlign="right" CssClass="GridPager" />
                    <%--<asp:GridView ID="grdListPrice" CssClass="display compact" runat="server" AutoGenerateColumns="false"
                        OnPageIndexChanging="grdListPrice_PageIndexChanging" AllowSorting="true" Style="border-collapse: collapse; width: 100%; border: azure;">--%>
                        <Columns>

                            <asp:TemplateField>
                                <HeaderTemplate>

                                    <%-- <asp:CheckBox ID="chk_All" runat="server" CssClass="chk_multi_All" AutoPostBack="true" onchange="selectAll(this);" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />--%>
                                    <asp:CheckBox ID="chk_All1" runat="server" CssClass="chk_multi_All1" AutoPostBack="true" EnableViewState="false" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />

                                    <asp:HiddenField ID="hdn_chk_All1" runat="server" Value="0" />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%--<asp:CheckBox ID="chk_multi" runat="server" CssClass="chk_multi_add" class="chkbox" />--%>
                                    <asp:CheckBox ID="chk_multi" runat="server" CssClass="chk_multi_add" AutoPostBack="true" OnCheckedChanged="chkbox_CheckedChanged" />
                                    <asp:HiddenField ID="hdn_chk_multi" runat="server" Value='<%# Bind("Item_code") %>' />
                                    <asp:HiddenField ID="Hdnchk" runat="server" Value="0" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Group">
                                <ItemTemplate>
                                    <asp:Label ID="lblgrp" Text='<%#Bind("Group") %>' runat="server" Width="70px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblitem" Text='<%#Bind("Item_code") %>' runat="server" Width="70px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Desc">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemDesc" Text='<%#Bind("Item_Desc") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Product Family">
                                <ItemTemplate>
                                    <asp:Label ID="lblpf" Text='<%#Bind("ProductFamily") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblsc" Text='<%#Bind("StockCode") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="List Price">
                                <ItemTemplate>
 <div style="text-align: right">
                                    <asp:Label ID="lblLP" Text='<%#Bind("Price") %>' runat="server"></asp:Label>
</div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </div>
            <div class="col-md-6 mn_margin" style="margin-top: 4%; ">
                <div class="rightside_box" style="height: 393px;border: solid 1px #ddd; padding: 100px 60px;text-align: center;color: #375b67;">
                    <h4 style="font-weight: bold;font-size: 14px;font-family:Helvetica Neue, HelveticaNeue, Helvetica, Arial, sans-serif;">Please select item from left table to collect your selection in right table for PDF generation.</h4>
                </div>
                <div id="selectedlist" style="color: white; display: none; padding: 5px; font-weight: bold;margin: 0 0 0 0 !important;" class="filter_panel">
                    <label style="margin-top: 3px;">Selected List Price To Download</label>
             <asp:Button runat="server" ID="btnpdf" CssClass="btnSubmit" style="float:right" OnClick="btnpdf_Click" Text="Export PDF" />
                    <asp:Button runat="server" ID="Butclear" CssClass="btnSubmit clear" style="float:right" OnClick="btnclear_Click" Text="Clear" />
                </div>
                <asp:Panel runat="server" ID="Panel2" Style="margin-top: 10px;height: 355px;overflow: auto;margin-left: -1px;padding: 10px;">
                    <div class="col-md-12">
                        <asp:TextBox runat="server" ID="txtsearch1" placeholder="Search" Style="float: right; display: none;margin-bottom: 10px;"></asp:TextBox>
                        <asp:ImageButton runat="server" CssClass="search1" ImageUrl="images/Search.png" Style="display: none" ID="searchbtn1" OnClick="searchbtn1_Click" />
                    </div>
                    <asp:GridView ID="grdListPrice1" CssClass="display compact1" runat="server" AutoGenerateColumns="false" AllowSorting="true" Style="border-collapse: collapse; width: 100%; border: azure;">

                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>

                                    <%-- <asp:CheckBox ID="chk_All" runat="server" CssClass="chk_multi_All" AutoPostBack="true" onchange="selectAll(this);" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />--%>
                                    <asp:CheckBox ID="chk_All" runat="server" CssClass="chk_multi_All" AutoPostBack="true" EnableViewState="false" OnCheckedChanged="chkboxUnSelectAll_CheckedChanged" />

                                    <asp:HiddenField ID="hdn_chk_All" runat="server" Value="0" />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk1" runat="server" CssClass="chk1" Checked="true" AutoPostBack="true" OnCheckedChanged="chkboxSelectAll_CheckedChanged1" />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Group">
                                <ItemTemplate>
                                    <asp:Label ID="lblgrp" Text='<%#Bind("Group") %>' runat="server" Width="70px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblitem" Text='<%#Bind("Item_code") %>' runat="server" Width="70px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Desc">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemDesc" Text='<%#Bind("Item_Desc") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Product Family">
                                <ItemTemplate>
                                    <asp:Label ID="lblpf" Text='<%#Bind("ProductFamily") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblsc" Text='<%#Bind("StockCode") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="List Price">
                                <ItemTemplate>
 <div style="text-align: right">
                                    <asp:Label ID="lblLP" Text='<%#Bind("Price") %>' runat="server"></asp:Label>
</div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </asp:Panel>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnpdf" />
        </Triggers>
    </asp:UpdatePanel>
   <%--<div id="loader_div" class="loader_div"><img class="loader_div" alt="" src="images/loader.gif"></div>--%>
   <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7; visibility:visible" >
                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff" >Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
