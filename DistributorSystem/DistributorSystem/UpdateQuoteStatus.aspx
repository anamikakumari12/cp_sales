﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="UpdateQuoteStatus.aspx.cs" Inherits="DistributorSystem.UpdateQuoteStatus" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="css/Tabs.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.full.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.11/sorting/date-eu.js" type="text/javascript"></script>
    <%--    <script src="https://cdn.datatables.net/plug-ins/1.10.11/sorting/datetime-moment.js" type="text/javascript"></script>--%>
    <style>
        button[disabled], html input[disabled] {
            cursor: not-allowed;
            background: grey;
        }


        .popupControl {
            margin: 5px;
            float: right;
        }

        th,
        td {
            white-space: nowrap;
        }

        div.dataTables_wrapper {
            /*width: 800px;*/
            margin: 0 auto;
        }

        .loader_div {
            position: absolute;
            top: 0;
            bottom: 0%;
            left: 0;
            right: 0%;
            z-index: 99;
            opacity: 0.7;
            display: none;
            background: lightgrey url('../../../tt_dist/images/loader.gif') center center no-repeat;
        }

        .link {
            cursor: pointer;
        }

        .modal a.close-modal {
            top: 0;
            right: 0;
        }

        .blocker {
            z-index: 99;
        }
    </style>
    <script type="text/javascript">

        var table1;
        $(document).ready(function () {
            debugger;
            $("#btnSubmitReason").prop("disabled", false);
            $("#btnSubmitReason1").prop("disabled", false);
            LoadTable();


        });
       
        function LoadTable() {
            var SelectedStart = sessionStorage.getItem("selectedStart");
            var SelectedEnd = sessionStorage.getItem("selectedEnd");
            var start = (SelectedStart == null ? moment().subtract(29, 'days') : SelectedStart);
            var end = (SelectedEnd == null ? moment() : SelectedEnd);
            $('#MainContent_txtDateRange').daterangepicker({
                autoUpdateInput: true,
                locale: {
                    format: 'MM/DD/YYYY'
                },
                startDate: start,
                endDate: end,
                ranges: {
                    'All Date': ['07/20/2019', moment()],
                    'Last Year': [moment().subtract(1, 'year'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                var start = start.format('MM/DD/YYYY');
                var end = end.format('MM/DD/YYYY');
                sessionStorage.setItem('selectedStart', start);
                sessionStorage.setItem('selectedEnd', end);
            });

           

            var divplace = document.getElementById("divplace");

            var head_content = $('#MainContent_grdItemSummary tr:first').html();
            $('#MainContent_grdItemSummary').prepend('<thead></thead>')
            $('#MainContent_grdItemSummary thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdItemSummary tbody tr:first').hide();
            var otable = $('#MainContent_grdItemSummary').dataTable({
                //columnDefs: [{ type: 'date', 'targets': [10] }],
                //"order": [[10, 'desc']]

                "order": [[8, "desc"]], //or asc 
                // "columnDefs": [{ "targets": 11, "type": "date-eu" }],
            });

        }

        $(function () {
            $('#MainContent_txtocnumber').keydown(function (e) {
                if (e.shiftKey || e.ctrlKey || e.altKey) {
                    e.preventDefault();
                }
                else {
                    var key = e.keyCode;
                    if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                        e.preventDefault();
                    }
                }
            });
        });

        $(function () {
            $('#MainContent_txtComment').keydown(function (e) {
                if (e.shiftKey || e.ctrlKey || e.altKey) {
                    e.preventDefault();
                }
                else {
                    var key = e.keyCode;
                    if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                        e.preventDefault();
                    }
                }
            });
        });

        $(function () {
            $('#MainContent_txtcomment1').keydown(function (e) {
                if (e.shiftKey || e.ctrlKey || e.altKey) {
                    e.preventDefault();
                }
                else {
                    var key = e.keyCode;
                    if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                        e.preventDefault();
                    }
                }
            });
        });

        function SubmitOnOrderRecieved() {
            debugger;
            jQuery(".loader_div").show();
            var ID = $('#hdnsubmit').val();
            var status = $('#hdnstatus').val();
            var ocNumber = $('#MainContent_txtocnumber').val();
            var comment = $('#MainContent_txtComment').val();
            var ocflag = "1";
            
            if (!ocNumber.replace(/\s/g, '').length) {
                ocNumber = null;
            }

            if (ocNumber == "" || ocNumber == null || ocNumber == undefined) {
                $('#MainContent_txtocnumber').css("border", "1px solid red");
                jQuery(".loader_div").hide();
            }
            else {
                $("#btnSubmitReason").prop("disabled", true);
                $.ajax({
                    url: 'UpdateQuoteStatus.aspx/SubmitStatus',
                    method: 'post',
                    datatype: 'json',
                    data: '{ID:"' + ID + '", status:"' + status + '", ocNumber:"' + ocNumber + '", ocflag:"' + ocflag + '", comment:"' + comment + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        debugger;
                        alert(msg.d);
                        window.location.reload();
                        //jQuery(".loader_div").hide();
                        //$("#mdReason").modal('toggle');
                        //$("#mdReason .close1").click()

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });
            }
        }

        function SubmitOnOrderLostOrPending() {
            debugger;
            jQuery(".loader_div").show();
            var ID = $('#hdnsubmit1').val();
            var status = $('#hdnstatus1').val();
            var comment = $('#MainContent_txtcomment1').val();
            var ocflag = "0";
            var ocNumber = $('#MainContent_txtocnumber').val();

            if (!comment.replace(/\s/g, '').length) {
                comment = null;
            }
            if (comment == "" || comment == null || comment == undefined) {
                $('#MainContent_txtcomment1').css("border", "1px solid red");
                jQuery(".loader_div").hide();
            }
            else {
                $("#btnSubmitReason1").prop("disabled", true);
                $.ajax({
                    url: 'UpdateQuoteStatus.aspx/SubmitStatus',
                    method: 'post',
                    datatype: 'json',
                    data: '{ID:"' + ID + '", status:"' + status + '", ocNumber:"' + ocNumber + '", ocflag:"' + ocflag + '", comment:"' + comment + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        alert(msg.d);
                        window.location.reload();
                        //jQuery(".loader_div").hide();
                        //$("#mdRejectReason .close1").click()
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });
            }
        }

        function ItemAction(obj, status) {
            debugger;
            $('#MainContent_txtcomment1').css("border", "1px solid gray");
            var searchid = obj.id.substring(27, obj.id.lastIndexOf('_'))
          
            var Quote_id = obj.id.replace(searchid, "hdnitemID");
            var ID = $("#" + Quote_id).val();
            $("#hdnsubmit").val(ID);
            $("#hdnsubmit1").val(ID);
            $("#hdnstatus").val(status);
            $("#hdnstatus1").val(status);
           
        }

        
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loader_div" class="loader_div"></div>
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Quote</a>
                        </li>
                        <li class="current">Update Quote Status</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

    <asp:ScriptManager ID="SM1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
    <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">

        <ContentTemplate>

            <div class="col-md-12 mn_margin">
                <asp:Panel ID="panelref" runat="server" CssClass="filter_panel">
                    <div class="col-md-12 nopadding">

                        <div class="col-md-3">
                            <asp:Label ID="lblRef" Style="float: right;" runat="server" Text="Requested Date Range"></asp:Label>
                        </div>
                        <div class="col-md-6">
                            <div class="controls">
                                <asp:TextBox ID="txtDateRange" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <asp:Button runat="server" ID="btnFilter" CssClass="btnSubmit" Text="Filter" OnClick="btnFilter_Click" />
                        </div>
                    </div>
                </asp:Panel>
                <asp:Label runat="server" ID="lblmessage"></asp:Label>
                <div class="tabbed skin-turquoise round" id="skinable" style="margin-bottom: 10px;">
                    <ul>
                        <%--<li id="rfqList" runat="server" class="active" onclick="tabchange(this);">RFQ Wise</li>--%>
                        <li id="itemList" runat="server" class="active">Item Wise</li>
                    </ul>
                </div>
                
                <div class="col-md-12 nopad" id="divItemSummary" runat="server" style="display: block">


                    <asp:GridView ID="grdItemSummary" CssClass="display compact" runat="server" AutoGenerateColumns="false" OnRowDataBound="grdItemSummary_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <a href="#mdReason" rel="modal:open">
                                        <asp:Button ID="btnItemEscalate1" Visible='<%# Eval("StatusName").ToString() == "Added In PA" ? true : false %>' runat="server" CssClass="btnSubmit" Text="Order Recieved" OnClientClick="ItemAction(this,'Order Recieved');" Enabled='<%# Eval("StatusName").ToString() == "Rejected"? false : true %>' ToolTip='<%# Eval("StatusName").ToString() == "Rejected"? "Escalate Option will not be available for Rejected quote" : "" %>' /></a>
                                    <%--<a href="#mdRejectReason\" rel="modal:open">--%>
                                    <a href="#mdRejectReason" rel="modal:open">
                                        <asp:Button runat="server" ID="btnItemAccept" Visible='<%# Eval("StatusName").ToString() == "Added In PA" ? true : false %>' CssClass="btnSubmit" OnClientClick="ItemAction(this,'Order Lost');" Text="Order Lost" />
                                        <asp:Button runat="server" ID="btnRequest" Visible='<%# Eval("StatusName").ToString() == "Added In PA" ? true : false %>' CssClass="btnSubmit" OnClientClick="ItemAction(this,'Order Pending');" Text="Order Pending" />
                                        
                                    <%--<asp:Image runat="server" ImageUrl="images/info.jpg" ID="imgView" onclick="ItemStatusLog(this);" title="View Status Log" Style="height: 25px;" />--%>
                                   
                                    <br />
               <asp:LinkButton ID="lnkFile" runat="server" CommandArgument='<%# Eval("Escalation_file") %>' Text='<%# Eval("Escalation_file_name") %>' OnCommand="lnkFile_Command"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Reference Number">
                                <ItemTemplate>
                                    <asp:Label ID="lblRef" CssClass="link" Text='<%# Bind("Ref_number")%>' runat="server"></asp:Label>
                                    <asp:HiddenField ID="hdnitemID" Value='<%# Bind("ID")%>' runat="server" />
                                    <asp:HiddenField ID="hdnMOQ" Value='<%# Bind("MOQ_Escalate")%>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblItem1" Text='<%# Bind("Item_code")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Desc">
                                <ItemTemplate>
                                    <asp:Label ID="lblItemDesc1" Text='<%# Bind("Item_Desc")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="WHS">
                                <ItemTemplate>
                                    <asp:Label ID="lblWHS1" Text='<%# Bind("WHS")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Approved Qty">
                                <ItemTemplate>
                                    <asp:Label ID="lblQty1" Text='<%# Bind("Approved_OrderQty")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         
                            <asp:TemplateField HeaderText="Approved Price">
                                <ItemTemplate>
                                    <asp:Label ID="lblAppPrice1" Text='<%# Bind("New_OfferPrice")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                           <%-- <asp:TemplateField HeaderText="Validity(in Days)">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrderValidity1" Text='<%# Bind("Order_Validity")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                            <asp:TemplateField HeaderText="Requested Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblReqDate1" Text='<%# Bind("Requested_date")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus1" Text='<%# Bind("StatusName")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>

            <%--      <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">

                <ContentTemplate>--%>
            <div id="divdetail" style="display: none;">
                <table id="grdDetailedPriceSummary1" class="display responsive nowrap" cellpadding="0" cellspacing="0">
                    <thead style="background-color: #DC5807; color: White; font-weight: bold">
                        <tr style="border: solid 1px #000000">
                            <td>Action</td>
                            <td>Item_code</td>
                            <td>Item_Desc</td>
                            <td>WHS</td>

                            <td>Approved Qty</td>
                       
                            <td>Approved Price</td>
                            <td>Validity(in Days)</td>
                          
                            <td>Uploaded File</td>
                            <td>Status</td>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>

                <div id="divplace" style="display: none;">fdgdsgfdg</div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
    
     <asp:UpdateProgress ID="updateProgress" runat="server">
            <ProgressTemplate>
                <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">

                    <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
   <div id="mdReason" class="modal" style="border: solid 1px #008a8a;">
        <div class="col-md-12 controls">
           <%-- <div class="col-md-5">
                <p class="popupControl">End Customer : </p>
            </div>
            <div class="col-md-7">
                <asp:DropDownList ID="ddlEndCustomer" runat="server" CssClass="ddl" Style="width: 200px;">
                </asp:DropDownList>
            </div>
            <div class="col-md-5">
                <p class="popupControl">Existing Company : </p>
            </div>
            <div class="col-md-7">
                <asp:DropDownList ID="ddlCompanyName" runat="server" CssClass="ddl" Style="width: 200px;">
                </asp:DropDownList>
            </div>
            <div class="col-md-5">
                <p class="popupControl">Existing Product : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtDescription" Style="width: 200px;" CssClass="ddl" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-5">
                <p class="popupControl">Sales Price : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtCompanySP" onkeypress="return isNumberKey(event,this);" CssClass="ddl" Style="width: 80px;" runat="server"></asp:TextBox>

            </div>
            <div class="col-md-5">
                <p class="popupControl">Quantity : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtReqQty" onkeypress="return isNumberKey(event,this);" CssClass="ddl" Style="width: 80px;" runat="server"></asp:TextBox>

            </div>
            </div>--%>
            <div class="ExpPriceDiv">
            <div class="col-md-5">
                <p class="popupControl">OC Number : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtocnumber" CssClass="ddl" Style="width: 80px;border: solid 1px gray;" runat="server"></asp:TextBox>

            </div>
            <div class="col-md-5">
                <p class="popupControl">Comment : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtComment" Rows="4" Columns="40" TextMode="MultiLine" runat="server"></asp:TextBox>
            </div>
           <%-- <div class="col-md-12">
                <asp:FileUpload runat="server" ID="fileUpload" />
            </div>--%>
        </div>
        <div class="col-md-12">
           
            <div class="col-md-7">
                <input type="hidden" id="hdnsubmit" />
                  <input type="hidden" id="hdnstatus" />
                <input type="button" id="btnSubmitReason" class="btnSubmit" style="width:75px;color:white" name="" onclick="SubmitOnOrderRecieved();" title="Submit" value="Submit" />
                <a href="#" class="close1" rel="modal:close">Close</a>
            </div>
        </div>
    </div>
       </div>
    <div id="mdRejectReason" class="modal" style="border: solid 1px #008a8a;">
        <div class="col-md-12 controls">

            <div class="col-md-5">
                <p class="popupControl">Comment : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtcomment1" Rows="4" Columns="40" TextMode="MultiLine" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-12">
           
           <div class="col-md-7">
                 <input type="hidden" id="hdnsubmit1" />
                 <input type="hidden" id="hdnstatus1" />
                <input type="button" id="btnSubmitReason1" class="btnSubmit" style="width:75px;" name="" onclick="SubmitOnOrderLostOrPending();" title="Submit" value="Submit" />
                <a href="#" class="close1" rel="modal:close">Close</a>
            </div>
        </div>
    </div>

</asp:Content>
