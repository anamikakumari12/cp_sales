﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DistributorSystemBO;

namespace DistributorSystemDAL
{
    public class SMDetailsDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataTable getSMDetailsDAL()
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter da = null;
            SqlCommand cmd = null;
            try
            {
                sqlconn.Open();
                cmd = new SqlCommand(ResourceFileDAL.getDistributor, sqlconn);
                cmd.CommandType = CommandType.StoredProcedure;
                da = new SqlDataAdapter(cmd);
                da.Fill(dtOutput);
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable getSM_Mapping_DetailsDAL(SMDetailsBO objSMBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_get_CP_SM_Details, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.DISTRIBUTOR, SqlDbType.VarChar, 10).Value = objSMBO.CP_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
                for (int i = 0; i <= dtOutput.Rows.Count; i++)
                {
                    dtOutput.Rows[i]["Customer_number"]=Convert.ToString(dtOutput.Rows[i]["Customer_number"]).Replace(",",", ");
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable getCustDetailsDAL(SMDetailsBO objSMBO)
        {
            DataTable dt = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter da = null;
            SqlCommand cmd = null;
            try
            {
                sqlconn.Open();
                cmd = new SqlCommand(ResourceFileDAL.sp_GetCustomersBasedOnDistributor, sqlconn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(ResourceFileDAL.DISTRIBUTOR, SqlDbType.VarChar, 10).Value = objSMBO.CP_Number;
                da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

            finally
            {
                sqlconn.Close();
            }

            return dt;
        }

        public SMDetailsBO saveSMDetailsDAL(SMDetailsBO objSMBO)
        {
            SMDetailsBO objOutput = new SMDetailsBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand cmd;
            string output = String.Empty;
            DataTable dtOutput;
            try
            {
                sqlconn.Open();
                dtOutput = new DataTable();
                cmd = new SqlCommand(ResourceFileDAL.sp_saveCP_SM_Mapping, sqlconn);
                cmd.CommandType = CommandType.StoredProcedure;                
                cmd.Parameters.Add(ResourceFileDAL.ID, SqlDbType.Int).Value = objSMBO.ID;
                cmd.Parameters.Add(ResourceFileDAL.CP_Number, SqlDbType.VarChar, 10).Value = objSMBO.CP_Number;
                cmd.Parameters.Add(ResourceFileDAL.CP_Name, SqlDbType.VarChar, -1).Value = objSMBO.CP_Name;
                cmd.Parameters.Add(ResourceFileDAL.Customer_Number, SqlDbType.VarChar, -1).Value = objSMBO.Customer_Number;
                cmd.Parameters.Add(ResourceFileDAL.Customer_Name, SqlDbType.VarChar, -1).Value = objSMBO.Customer_Name;
                cmd.Parameters.Add(ResourceFileDAL.SM_NAME, SqlDbType.VarChar, -1).Value = objSMBO.SM_NAME;    
                cmd.Parameters.Add(ResourceFileDAL.SM_MAIL_ID, SqlDbType.VarChar, -1).Value = objSMBO.SM_MAIL_ID;
                cmd.Parameters.Add(ResourceFileDAL.ERR_CODE, SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add(ResourceFileDAL.ERR_MESSAGE, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                objOutput.ERR_CODE = Convert.ToInt32(cmd.Parameters[ResourceFileDAL.ERR_CODE].Value);
                objOutput.ERR_MESSAGE = Convert.ToString(cmd.Parameters[ResourceFileDAL.ERR_MESSAGE].Value);    
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

    }
}