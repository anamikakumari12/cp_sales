USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[place_order]    Script Date: 21-12-2020 10:54:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[place_order] (@p_item_available_id INT, @p_customer_id INT, @p_order_qty INT, @p_user_id INT,
@Err_code INT OUTPUT,
@Err_Msg VARCHAR(MAX) OUTPUT
)
AS

BEGIN

	DECLARE @l_avail_qty		INT;
	DECLARE @l_left_over_qty	INT;
	DECLARE @qty_Flag		INT


	SELECT @l_avail_qty = current_quantity
	FROM  fs_items_available_for_sale 
	WHERE CURRENT_TIMESTAMP BETWEEN start_date AND end_date
	AND   item_available_id = @p_item_available_id;
	if(@p_order_qty>@l_avail_qty)
	BEGIN
		SET @p_order_qty=@p_order_qty-@l_avail_qty
		SET @qty_Flag=1
	END
	ELSE
		SET @qty_Flag=0

	SET @l_left_over_qty = @l_avail_qty - @p_order_qty;

	IF @l_avail_qty > 0 
	BEGIN

		BEGIN TRANSACTION

		UPDATE fs_items_available_for_sale
		SET    current_quantity = @l_left_over_qty
		WHERE  item_available_id = @p_item_available_id

		INSERT INTO fs_sale_orders(
			item_available_id		,
			customer_id			,
			Qty_ordered			,
			order_placed_on			,
			stock_before_order		,
			Stock_after_order		,
			created_by			,
			Created_on			,
			last_updated_by			,
			last_updated_on			
		) VALUES (
			@p_item_available_id		,
			@p_customer_id			,
			@p_order_qty			,
			CURRENT_TIMESTAMP		,
			@l_avail_qty			,	-- stock_before_order		,
			@l_left_over_qty		,	-- Stock_after_order		,
			@p_user_id			,
			CURRENT_TIMESTAMP		,
			@p_user_id			,
			CURRENT_TIMESTAMP			
		);

		COMMIT TRANSACTION;
		IF @qty_Flag>0
		BEGIN
			SET @Err_code=200
			SET @Err_Msg='Order is placed for only '+CONVERT(VARCHAR(20),@p_order_qty)+' quantity.'
		END
		ELSE
		BEGIN
			SET @Err_code=200
			SET @Err_Msg='Order is not placed successfully.'
		END
	END;
	ELSE
	BEGIN
		SET @Err_code=200
		SET @Err_Msg='Order is not placed as item is out of stock.'
	END
END;