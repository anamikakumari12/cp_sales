﻿using DistributorSystemBL;
using DistributorSystemBO;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DistributorSystem
{
    public partial class UpdateQuoteStatus : System.Web.UI.Page
    {
        #region GlobalDeclaration
        CommonFunctions objCom = new CommonFunctions();
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }

                if (!IsPostBack)
                {
                    QuoteBL objquoteBL = new QuoteBL();

                    //DataTable dtCust = new DataTable();
                    QuoteBO objQuoteBO = new QuoteBO();
                    objQuoteBO.CP_Number = Convert.ToString(Session["DistributorNumber"]);
                    //dtCust = objquoteBL.GetCustomerListBL(objQuoteBO);
                    //Session["dtCustForQuote"] = dtCust;

                    DataTable dtQuote = new DataTable();
                    dtQuote = objquoteBL.GetQuoteDetailsFORPABL(objQuoteBO);
                    Session["dtQuote"] = dtQuote;
                    BindGrid();
                    //DataTable dt = new DataTable();
                    //dt = objquoteBL.GetCompetitorsBL();
                    //Session["dtCompetitors"] = dt;
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            BindGrid();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadTable", "LoadTable()", true);
        }

        protected void grdPriceSummary_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //if (e.CommandName.Equals("Open"))
            //{
            //    QuoteBL objquoteBL = new QuoteBL();
            //    DataTable dtQuote = new DataTable();
            //    QuoteBO objQuoteBO = new QuoteBO();
            //    objQuoteBO.Ref_Number = Convert.ToString(e.CommandArgument);
            //    dtQuote = objquoteBL.GetQuoteDetailsBL(objQuoteBO);
            //    if (dtQuote.Rows.Count > 0)
            //    {
            //        grdDetailedPriceSummary.DataSource = dtQuote;
            //    }
            //    else
            //    {
            //        grdDetailedPriceSummary.DataSource = null;
            //    }
            //    grdDetailedPriceSummary.DataBind();
            // }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            DataTable dtoutput = new DataTable();
            try
            {
                string start_date = string.Empty;
                string end_date = string.Empty;
                string selectedDate = txtDateRange.Text;
                QuoteBO objquoteBO = new QuoteBO();
                if (selectedDate.Contains("/"))
                {
                    string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                    start_date = Convert.ToString(splittedDates[0].TrimEnd().Trim());
                    end_date = Convert.ToString(splittedDates[1].TrimStart().Trim());
                }
                if(start_date =="" && end_date=="")
                {
                    objquoteBO.StartDate = null;
                    objquoteBO.EndDate = null;
                }
                else
                {
                    objquoteBO.StartDate = start_date;
                    objquoteBO.EndDate = end_date;
                }
               
                objquoteBO.CP_Number = Convert.ToString(Session["DistributorNumber"]);
                QuoteBL objQuoteBL = new QuoteBL();
                //dtoutput = objQuoteBL.GetQuoteSummaryBL(objquoteBO);
                //if (dtoutput.Rows.Count > 0)
                //{
                //    if (dtoutput.Rows.Count > 0)
                //    {
                //        grdPriceSummary.DataSource = dtoutput;
                //        grdPriceSummary.DataBind();
                //    }
                //    else
                //    {
                //        grdPriceSummary.DataSource = null;
                //        grdPriceSummary.DataBind();
                //    }

                //}
                //else
                //{
                //    grdPriceSummary.DataSource = null;
                //    grdPriceSummary.DataBind();
                //}
                dtoutput = objQuoteBL.GetQuoteDetailsFORPABL(objquoteBO);
                //dtoutput = (DataTable)Session["dtQuote"];
                if (dtoutput.Rows.Count > 0)
                {
                    if (dtoutput.Rows.Count > 0)
                    {
                        grdItemSummary.DataSource = dtoutput;
                        grdItemSummary.DataBind();
                    }
                    else
                    {
                        grdItemSummary.DataSource = null;
                        grdItemSummary.DataBind();
                    }

                }
                else
                {
                    grdItemSummary.DataSource = null;
                    grdItemSummary.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        [WebMethod]
        public static string SubmitStatus(int ID, string status, string ocNumber, int ocflag, string comment)
        {
            string output = string.Empty;
            CommonFunctions objCom = new CommonFunctions();
            UpdateQuoteStatus objPrice = new UpdateQuoteStatus();
            DateTime changedDate1 = DateTime.Now;
            string changedDate = Convert.ToString(changedDate1);
            string loggedby = HttpContext.Current.Session["UserId"].ToString();
            try
            {

                QuoteBL objBL = new QuoteBL();
                string result = objBL.SubmitStatusBL(ID, status, ocNumber, ocflag, comment, changedDate, loggedby);

                //output = "{\"msg\":\"" + result + "\"}";
                output = result;
            }
            catch (Exception ex)
            {
                output = "{\"code\":\"105\",\"msg\":\"" + ex.Message + "\"}";
                CommonFunctions.StaticErrorLog(ex);
            }
            return output;
        }

        #endregion

        #region reportGenerate

       
        private string Getheading(DataTable dt)
        {

            string output = string.Empty;
            StringBuilder strHTMLBuilder = new StringBuilder();
            string imageURL = string.Empty;
            string Heading = string.Empty;
            string taegutec_add = string.Empty;
            string Customer_Name = string.Empty;
            string Customer_Address = string.Empty;
            string Customer_Number = string.Empty;
            string Quotation_No = string.Empty;
            string Date = string.Empty;
            string remarks = string.Empty;
            string PO_no = string.Empty;
            try
            {
                Heading = Convert.ToString(ConfigurationManager.AppSettings["QuotePO_Heading"]);
                imageURL = Convert.ToString(ConfigurationManager.AppSettings["Logo"]);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        Customer_Name = Convert.ToString(dt.Rows[0]["Customer_Name"]);
                        Customer_Address = Convert.ToString(dt.Rows[0]["Customer_Address"]);
                        Customer_Number = Convert.ToString(dt.Rows[0]["Customer_Number"]);
                        Quotation_No = Convert.ToString(dt.Rows[0]["Quotation_No"]);
                        Date = Convert.ToString(dt.Rows[0]["Date"]);
                        PO_no = Convert.ToString(dt.Rows[0]["PO_No"]);
                        taegutec_add = Convert.ToString(dt.Rows[0]["Taegutec_Address"]);
                        remarks = Convert.ToString(dt.Rows[0]["PurchaseOrderComment"]);

                        strHTMLBuilder.Append("<table style='border: 1px solid darkgray; font-family: Helvetica Neue, HelveticaNeue, Helvetica, Arial, sans-serif; border-collapse:collapse;'>");
                        strHTMLBuilder.Append("<tr style=' background-color: #5faae6c7;'>");
                        strHTMLBuilder.Append("<td colspan='9' style='text-align:center; font-size:30px; font-weight:bold; color:black;'>");
                        strHTMLBuilder.Append(Heading);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr>");
                        strHTMLBuilder.Append("<td colspan='4'><img style='float:left;width: 80%;' src='");
                        strHTMLBuilder.Append(imageURL);
                        strHTMLBuilder.Append("'/>");
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='5' style='font-weight:bold; font-size: 15px;'>");
                        strHTMLBuilder.Append(Customer_Name);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr>");
                        strHTMLBuilder.Append("<td colspan='4' style='font-weight:bold; font-size: 10px;'>");
                        strHTMLBuilder.Append(taegutec_add);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='5' style='font-weight:bold; font-size: 10px;'>");
                        strHTMLBuilder.Append(Customer_Address);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr >");
                        strHTMLBuilder.Append("<td colspan='4' >");
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='5'  style='font-weight:bold; font-size: 10px;'>");
                        strHTMLBuilder.Append("REMARKS:");
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr >");
                        strHTMLBuilder.Append("<td colspan='4' style='padding:0px'>");
                        strHTMLBuilder.Append("<table width='100%'>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>CUSTOMER NO : ");
                        strHTMLBuilder.Append(Customer_Number);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>PURCHASE ORDER NO : ");
                        strHTMLBuilder.Append(PO_no);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>QTN REFERENCE NO : ");
                        strHTMLBuilder.Append(Quotation_No);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>DATE : ");
                        strHTMLBuilder.Append(Date);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("</table>");
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='5' style='font-size: 10px;'>");
                        strHTMLBuilder.Append(remarks);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");

                        strHTMLBuilder.Append("<tr>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>SL<br/>NO</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>ITEM DESCRIPTION</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>CATALOGUE NO</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>ITEM QTY</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>UNIT<br/>PRICE</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>LINE<br/>VALUE</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>SCHEDULE<br/>DATE</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>W/H</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>END<br/>CUSTOMER</td>");
                        strHTMLBuilder.Append("</tr>");
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            strHTMLBuilder.Append("<tr>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(i + 1));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Item"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Catalogue_No"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Item_Qty"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Unit_Price"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Line_Value"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Schedule_Date"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["WHS"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["End_Customer"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("</tr>");
                        }


                        strHTMLBuilder.Append("</table>");
                    }
                }

                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }

        protected void grdItemSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lb = e.Row.FindControl("lnkFile") as LinkButton;
                ScriptManager.GetCurrent(this).RegisterPostBackControl(lb);
            }
        }

        protected void lnkFile_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (Convert.ToString(e.CommandArgument) != string.Empty)
                {

                    // Response.ContentType = "application/octet-stream";
                    string filePath = ConfigurationManager.AppSettings["Escalation_Folder"].ToString() + Convert.ToString(e.CommandArgument);
                    if (File.Exists(filePath))
                    {
                        System.IO.FileInfo file = new System.IO.FileInfo(filePath);
                        if (file.Exists)
                        {
                            //Response.Clear();
                            //Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                            //Response.AddHeader("Content-Length", file.Length.ToString());
                            //Response.ContentType = "application/octet-stream";
                            // download […]
                            Response.ContentType = "application/octet-stream";
                            byte[] bts = System.IO.File.ReadAllBytes(filePath);
                            MemoryStream ms = new MemoryStream(bts);
                            Response.Clear();
                            Response.AddHeader("Content-Disposition", "attachment;filename=\"" + Path.GetFileName(filePath) + "\"");
                            Response.TransmitFile(filePath);
                            Response.End();
                        }
                    }
                    //Response.AddHeader("Content-Disposition", "attachment;filename=\"" + Path.GetFileName(filePath) + "\"");
                    //Response.TransmitFile(Server.MapPath(filePath));
                    //Response.End();
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion
    }
}