﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurchaseDashboard.aspx.cs" Inherits="DistributorSystem.PurchaseDashboard" MasterPageFile="~/MasterPage.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/radar.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

    <link rel="stylesheet" type="text/css" href="https://dc-js.github.io/dc.js/css/dc.css" />
    <script src="https://dc-js.github.io/dc.js/js/d3.js"></script>
    <script src="js/crossfilter.js"></script>

    <script src="js/dc.js"></script>
    <script src="https://rawgit.com/crossfilter/reductio/master/reductio.js"></script>
    <script src="https://npmcdn.com/universe@latest/universe.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {
          var ValuesForFamilyViewChart
            var FamilyChart;
            var SubFamilyChart;
            var ApplicationChart;
          
        });
        function LoadCharts() {
            debugger;
            //AmCharts.addInitHandler(function (chart) {

            //    // Check if `orderByField` is set
            //    if (chart.orderByField === undefined) {
            //        // Nope - do nothing


            //        return;
            //    }
            //    if (chart.dataProvider.length > 0) {

            //        // Re-order the data provider
            //        chart.dataProvider.sort(function (a, b) {
            //            if (a[chart.orderByField] > b[chart.orderByField]) {
            //                return -1;
            //            } else if (a[chart.orderByField] == b[chart.orderByField]) {
            //                return 0;
            //            }
            //            return 1;
            //        });
            //    }

            //}, ["serial"]);
            LoadChartHistoricalPurchase();
            LoadChartConsolidatedPurchaseView();
           LoadChartFamilyView();
            
        }
        
        function LoadChartHistoricalPurchase() {
            var tmp = null;
            $.ajax({
                type: "POST",
                url: 'PurchaseDashboard.aspx/LoadChartHistoricalPurchase',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];
                    var chart = AmCharts.makeChart("ChartHistoricalPurchase", {
                        "type": "serial",
                        "theme": "light",
                        "orderByField": "index",
                        "titles": [{
                            "text": "Historical Purchase"
                        }],
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "gridColor": "#FFFFFF",
                            "gridAlpha": 0,
                            "dashLength": 0
                        }],
                        "gridAboveGraphs": true,
                        "startDuration": 1,
                        "graphs": [{
                            "type": "column",
                            "title": "Value",
                            "balloonText": "[[title]]: <b>[[value]]</b>",
                            "bullet": "round",
                            "bulletSize": 10,
                            "bulletBorderColor": "#ffffff",
                            "bulletBorderAlpha": 1,
                            "bulletBorderThickness": 2,
                            "fillAlphas": 0.8,
                            "lineAlpha": 0.2,
                            "fillColorsField": "color",
                            "valueField": "value"
                        }],
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "Targets",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "gridAlpha": 0
                        },
                        "legend": {}
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
       
        function LoadChartConsolidatedPurchaseView() {
            var tmp = null;
            $.ajax({
                type: "POST",
                url: 'PurchaseDashboard.aspx/LoadChartConsolidatedPurchaseView',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    var d = new Date();
                    var month = new Array();
                    month[0] = "January";
                    month[1] = "February";
                    month[2] = "March";
                    month[3] = "April";
                    month[4] = "May";
                    month[5] = "June";
                    month[6] = "July";
                    month[7] = "August";
                    month[8] = "September";
                    month[9] = "October";
                    month[10] = "November";
                    month[11] = "December";
                    //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];

                    //AmCharts.addInitHandler(function (chart) {

                    //    // Check if `orderByField` is set
                    //    if (chart.orderByField === undefined) {
                    //        // Nope - do nothing
                    //        return;
                    //    }

                    //    // Re-order the data provider
                    //    chart.dataProvider.sort(function (a, b) {
                    //        if (a[chart.orderByField] > b[chart.orderByField]) {
                    //            return -1;
                    //        } else if (a[chart.orderByField] == b[chart.orderByField]) {
                    //            return 0;
                    //        }
                    //        return 1;
                    //    });

                    //}, ["serial"]);

                    var chart = AmCharts.makeChart("ChartConsolidatedPuchaseView", {
                        "type": "serial",
                        "theme": "light",
                        "titles": [{
                            "text": "Consolidated Purchase View(End Of "+month[d.getMonth()]+")"
                        }],
                        "orderByField": "index",
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "gridColor": "#FFFFFF",
                            "gridAlpha": 0,
                            "dashLength": 0
                        }],
                        "gridAboveGraphs": true,
                        "startDuration": 1,
                        "graphs": [{
                            "type": "column",
                            "title": "Value",
                            "balloonText": "[[title]]: <b>[[value]]</b>",
                            "bullet": "round",
                            "bulletSize": 10,
                            "bulletBorderColor": "#ffffff",
                            "bulletBorderAlpha": 1,
                            "bulletBorderThickness": 2,
                            "fillAlphas": 0.8,
                            "lineAlpha": 0.2,
                            "fillColorsField": "color",
                            "valueField": "value"
                        }],
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "Targets",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "gridAlpha": 0
                        },
                        "legend": {}
                    });
                    
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                    
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function print_filter(filter) {
            var f = eval(filter);
            if (typeof (f.length) != "undefined") { } else { }
            if (typeof (f.top) != "undefined") { f = f.top(Infinity); } else { }
            if (typeof (f.dimension) != "undefined") { f = f.dimension(function (d) { return ""; }).top(Infinity); } else { }
            console.log(filter + "(" + f.length + ") = " + JSON.stringify(f).replace("[", "[\n\t").replace(/}\,/g, "},\n\t").replace("]", "\n]"));
        }
        function LoadChartFamilyView() {

            debugger;

            var tmp = null;
            console.log("in chart family view")
            

            $.ajax({
                type: "POST",
                url: 'PurchaseDashboard.aspx/LoadChartFamilyView',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    ValuesForFamilyViewChart = msg.d;
                    console.log("family view data"+tmp)
                    //   bindpiecharts(tmp);
                    bindpiecharts(ValuesForFamilyViewChart)
                    
                },
                error: function (e) {
                    console(e);
                }
            });
            
        }

        function bindpiecharts(tmp1) {
            debugger;

            //var tmp = [{"FAMILY_NAME":"GROOVE/PART","SUBFAMILY_NAME":"T-CLAMP GROOVE","APPLICATION_DESC":"T-CLAMP GRV IN.(NON ROUND SIZE","SALES_MTD_VALUE":"64410","SALES_YTD_VALUE":"490253"},{"FAMILY_NAME":"GROOVE/PART","SUBFAMILY_NAME":"T-CLAMP GROOVE","APPLICATION_DESC":"T-CLAMP T/GRV INS.(ROUND SIZE)","SALES_MTD_VALUE":"10690","SALES_YTD_VALUE":"449158"},{"FAMILY_NAME":"TURN/THREAD","SUBFAMILY_NAME":"TAEGUTURN","APPLICATION_DESC":"TAEGUTURN PVD INSERTS","SALES_MTD_VALUE":"13200","SALES_YTD_VALUE":"88180"},{"FAMILY_NAME":"TURN/THREAD","SUBFAMILY_NAME":"TAEGUTURN","APPLICATION_DESC":"CERAMICS/SIN INSERTS","SALES_MTD_VALUE":"9980","SALES_YTD_VALUE":"67030"},{"FAMILY_NAME":"TURN/THREAD","SUBFAMILY_NAME":"TAEGUTURN","APPLICATION_DESC":"TURN INSERTS FOR ALUMINUM","SALES_MTD_VALUE":"44440","SALES_YTD_VALUE":"311479.2"},{"FAMILY_NAME":"TURN/THREAD","SUBFAMILY_NAME":"TAEGUTURN","APPLICATION_DESC":"TAEGUTURN HOLDERS EXTERNAL","SALES_MTD_VALUE":"3666","SALES_YTD_VALUE":"92495"},{"FAMILY_NAME":"TURN/THREAD","SUBFAMILY_NAME":"TAEGUTURN","APPLICATION_DESC":"TURNRUSH INSERTS","SALES_MTD_VALUE":"9600","SALES_YTD_VALUE":"52370"},{"FAMILY_NAME":"TURN/THREAD","SUBFAMILY_NAME":"TAEGUTHREAD","APPLICATION_DESC":"TAEGUTHREAD INSERTS-GROUND","SALES_MTD_VALUE":"6244","SALES_YTD_VALUE":"21430"},{"FAMILY_NAME":"DRILL/HOLE MACH","SUBFAMILY_NAME":"TOP DRILL/T-DRL","APPLICATION_DESC":"T-DRILLS","SALES_MTD_VALUE":"17286","SALES_YTD_VALUE":"281700.75"},{"FAMILY_NAME":"DRILL/HOLE MACH","SUBFAMILY_NAME":"DRILL RUSH","APPLICATION_DESC":"DRILL RUSH TOOLS","SALES_MTD_VALUE":"1","SALES_YTD_VALUE":"47601"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"ISO MILL","APPLICATION_DESC":"LION WEDGE CLAMPING  INSERTSS","SALES_MTD_VALUE":"2400","SALES_YTD_VALUE":"63490"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"MILL-CHASE(S)","APPLICATION_DESC":"CHASEQUAD INSERTS","SALES_MTD_VALUE":"34720","SALES_YTD_VALUE":"973734"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"MILL-CHASE(S)","APPLICATION_DESC":"CHASEQUAD FACE/FLUT/SLOT CTRS","SALES_MTD_VALUE":"29757","SALES_YTD_VALUE":"103478.85"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"MILL-CHASE(S)","APPLICATION_DESC":"MILL RUSH INSERTS(TRIO)","SALES_MTD_VALUE":"39990","SALES_YTD_VALUE":"1050530"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"TANGENTIAL MILL","APPLICATION_DESC":"TANGENTIAL SPC/OTH INSERTS","SALES_MTD_VALUE":"85300","SALES_YTD_VALUE":"1066250"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"DIE & MOLD PROD","APPLICATION_DESC":"FINE BALL INSERTS","SALES_MTD_VALUE":"3130","SALES_YTD_VALUE":"19610"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"D&M - ROUGHING","APPLICATION_DESC":"CHASE FEED INSERT(SBMT 09,13)","SALES_MTD_VALUE":"24960","SALES_YTD_VALUE":"67080"},{"FAMILY_NAME":"TOOLING SYSTEMS","SUBFAMILY_NAME":"MPT BORING TOOL","APPLICATION_DESC":"MPT HEAD","SALES_MTD_VALUE":"75599","SALES_YTD_VALUE":"826218.4"},{"FAMILY_NAME":"OTHER","SUBFAMILY_NAME":"SPARE PARTS","APPLICATION_DESC":"SPARE PARTS FOR TOOLS","SALES_MTD_VALUE":"1240","SALES_YTD_VALUE":"50926"},{"FAMILY_NAME":"OTHER","SUBFAMILY_NAME":"SPARE PARTS","APPLICATION_DESC":"MILLING SPARE PARTS","SALES_MTD_VALUE":"10210","SALES_YTD_VALUE":"64425.4"},{"FAMILY_NAME":"GROOVE/PART","SUBFAMILY_NAME":"T-CLAMP PARTING","APPLICATION_DESC":"T-CLAMP PARTING OFF INS","SALES_MTD_VALUE":"20855","SALES_YTD_VALUE":"324041"},{"FAMILY_NAME":"GROOVE/PART","SUBFAMILY_NAME":"T-CLAMP GROOVE","APPLICATION_DESC":"T-CLAMP INS FOR ALUMINUM","SALES_MTD_VALUE":"10890","SALES_YTD_VALUE":"68500"},{"FAMILY_NAME":"TURN/THREAD","SUBFAMILY_NAME":"TAEGUTURN","APPLICATION_DESC":"TAEGUTURN UC INSERTS","SALES_MTD_VALUE":"109010","SALES_YTD_VALUE":"1229018"},{"FAMILY_NAME":"TURN/THREAD","SUBFAMILY_NAME":"TAEGUTURN","APPLICATION_DESC":"TAEGUTURN HOLDERS INTERNAL","SALES_MTD_VALUE":"6898","SALES_YTD_VALUE":"89888"},{"FAMILY_NAME":"TURN/THREAD","SUBFAMILY_NAME":"TAEGUTURN","APPLICATION_DESC":"GOLDRUSH PVD INSERTS","SALES_MTD_VALUE":"146200","SALES_YTD_VALUE":"510550"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"HEXA&HEPTA MILL","APPLICATION_DESC":"CHASE 2 HEPTA(14MM) CUTTERS","SALES_MTD_VALUE":"17785","SALES_YTD_VALUE":"247940.9"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"MILL-CHASE(D)","APPLICATION_DESC":"CHASE 2 MILL CUTTERS(11,16MM)","SALES_MTD_VALUE":"38392","SALES_YTD_VALUE":"89070.3"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"MILL-CHASE(D)","APPLICATION_DESC":"CHASE 2 QUAD CUTTER","SALES_MTD_VALUE":"45090","SALES_YTD_VALUE":"243744.4"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"MILL-CHASE(D)","APPLICATION_DESC":"CHASE 2 QUAD INSERTS","SALES_MTD_VALUE":"141458","SALES_YTD_VALUE":"492668"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"MILL-CHASE(D)","APPLICATION_DESC":"CHASE 2 MILL CUTTERS(09,06MM)","SALES_MTD_VALUE":"10497","SALES_YTD_VALUE":"84242.95"},{"FAMILY_NAME":"TOOLING SYSTEMS","SUBFAMILY_NAME":"TOOLING SYSTEMS","APPLICATION_DESC":"SHELL MILL HOLDERS","SALES_MTD_VALUE":"9643","SALES_YTD_VALUE":"9643"},{"FAMILY_NAME":"TOOLING SYSTEMS","SUBFAMILY_NAME":"TOOLING SYSTEMS","APPLICATION_DESC":"T-HOLD/OTHER ACCESSORIES","SALES_MTD_VALUE":"200917","SALES_YTD_VALUE":"200917"},{"FAMILY_NAME":"TOOLING SYSTEMS","SUBFAMILY_NAME":"MPT BORING TOOL","APPLICATION_DESC":"MPT SHANKS","SALES_MTD_VALUE":"29611","SALES_YTD_VALUE":"275161.2"},{"FAMILY_NAME":"GROOVE/PART","SUBFAMILY_NAME":"T-CLAMP GROOVE","APPLICATION_DESC":"T-CLAMP PRESSED INS.("M"TYPE)","SALES_MTD_VALUE":"199635","SALES_YTD_VALUE":"1673899.22"},{"FAMILY_NAME":"TURN/THREAD","SUBFAMILY_NAME":"TAEGUTURN","APPLICATION_DESC":"TAEGUTURN SPECIAL INSERTS","SALES_MTD_VALUE":"65000","SALES_YTD_VALUE":"650000"},{"FAMILY_NAME":"TURN/THREAD","SUBFAMILY_NAME":"TAEGUTURN","APPLICATION_DESC":"TAEGUTURN T-TYPE HOLDERS","SALES_MTD_VALUE":"16000","SALES_YTD_VALUE":"34000"},{"FAMILY_NAME":"DRILL/HOLE MACH","SUBFAMILY_NAME":"TOP DRILL/T-DRL","APPLICATION_DESC":"T-DRILL INSERTS","SALES_MTD_VALUE":"24230","SALES_YTD_VALUE":"419818"},{"FAMILY_NAME":"DRILL/HOLE MACH","SUBFAMILY_NAME":"S.CARB H-DRILL","APPLICATION_DESC":"SOLID CARBIDE SPECIAL DRILLS","SALES_MTD_VALUE":"17488","SALES_YTD_VALUE":"17488"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"SOLID CARB MILL","APPLICATION_DESC":"SOLID-FLAT ENDMILL","SALES_MTD_VALUE":"41838","SALES_YTD_VALUE":"171612.45"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"MILL-CHASE(S)","APPLICATION_DESC":"CHASEMILL INSERTS","SALES_MTD_VALUE":"77508","SALES_YTD_VALUE":"527948"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"MILL-CHASE(S)","APPLICATION_DESC":"CHASEMILL END MILLS","SALES_MTD_VALUE":"9745.15","SALES_YTD_VALUE":"35200.95"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"MILL-CHASE(S)","APPLICATION_DESC":"CHASEQUAD  ENDMILL","SALES_MTD_VALUE":"19210","SALES_YTD_VALUE":"99870.45"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"HEXA&HEPTA MILL","APPLICATION_DESC":"CHASE 2 HEPTA 14 INS","SALES_MTD_VALUE":"32540","SALES_YTD_VALUE":"1731684"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"TANGENTIAL MILL","APPLICATION_DESC":"TOPSLOT INSERT(03-26MM)","SALES_MTD_VALUE":"23080","SALES_YTD_VALUE":"250470"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"MILL-CHASE(D)","APPLICATION_DESC":"CHASE 2 MILL 11,16MM INSERTS","SALES_MTD_VALUE":"12870","SALES_YTD_VALUE":"540843"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"MILL-CHASE(D)","APPLICATION_DESC":"MILL 2 RUSH INS.(DOUBLE SIDE)","SALES_MTD_VALUE":"50200","SALES_YTD_VALUE":"1694382"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"D&M - ROUGHING","APPLICATION_DESC":"CHASE 2 FEED INSERTS","SALES_MTD_VALUE":"90444","SALES_YTD_VALUE":"767914"},{"FAMILY_NAME":"GROOVE/PART","SUBFAMILY_NAME":"OTHER GROOVING","APPLICATION_DESC":"QUADRUSH GROOVING INSERTS","SALES_MTD_VALUE":"40371","SALES_YTD_VALUE":"340451"},{"FAMILY_NAME":"GROOVE/PART","SUBFAMILY_NAME":"OTHER GROOVING","APPLICATION_DESC":"QUADRUSH TOOLS","SALES_MTD_VALUE":"6000","SALES_YTD_VALUE":"24721"},{"FAMILY_NAME":"TURN/THREAD","SUBFAMILY_NAME":"TAEGUTURN","APPLICATION_DESC":"TAEGUTURN CVD INSERTS","SALES_MTD_VALUE":"88380","SALES_YTD_VALUE":"1075260"},{"FAMILY_NAME":"TURN/THREAD","SUBFAMILY_NAME":"TAEGUTURN","APPLICATION_DESC":"CERMET INSERTS","SALES_MTD_VALUE":"417710","SALES_YTD_VALUE":"1437352"},{"FAMILY_NAME":"TURN/THREAD","SUBFAMILY_NAME":"TAEGUTURN","APPLICATION_DESC":"RHINO(ECO) INSERTS","SALES_MTD_VALUE":"105740","SALES_YTD_VALUE":"609125"},{"FAMILY_NAME":"TURN/THREAD","SUBFAMILY_NAME":"TAEGUTURN","APPLICATION_DESC":"GOLDRUSH CVD INSERTS","SALES_MTD_VALUE":"735600","SALES_YTD_VALUE":"3537716"},{"FAMILY_NAME":"DRILL/HOLE MACH","SUBFAMILY_NAME":"TOP DRILL/T-DRL","APPLICATION_DESC":"TOP DRILL INSERTS","SALES_MTD_VALUE":"184173","SALES_YTD_VALUE":"1434129"},{"FAMILY_NAME":"DRILL/HOLE MACH","SUBFAMILY_NAME":"TOP DRILL/T-DRL","APPLICATION_DESC":"TOP DRILL TOOLS","SALES_MTD_VALUE":"75487.9","SALES_YTD_VALUE":"344826.3"},{"FAMILY_NAME":"DRILL/HOLE MACH","SUBFAMILY_NAME":"DRILL RUSH","APPLICATION_DESC":"DRILL RUSH HEADS","SALES_MTD_VALUE":"40741","SALES_YTD_VALUE":"284655.4"},{"FAMILY_NAME":"DRILL/HOLE MACH","SUBFAMILY_NAME":"S.CARB H-DRILL","APPLICATION_DESC":"SC DRILLS WITH COOLANT","SALES_MTD_VALUE":"43696","SALES_YTD_VALUE":"306010.98"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"ISO MILL","APPLICATION_DESC":"LS 2 QUAD/TRIO INS(DOUBLE SIDE","SALES_MTD_VALUE":"55790","SALES_YTD_VALUE":"228440"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"MILL-CHASE(S)","APPLICATION_DESC":"MILL RUSH CUTTERS(TRIO)","SALES_MTD_VALUE":"71003","SALES_YTD_VALUE":"164985.95"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"HEXA&HEPTA MILL","APPLICATION_DESC":"OTHER HEXAGON/HEPTA INSERTS","SALES_MTD_VALUE":"16200","SALES_YTD_VALUE":"73680"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"HEXA&HEPTA MILL","APPLICATION_DESC":"HEXA2MILL INS.(05,10MM)","SALES_MTD_VALUE":"113750","SALES_YTD_VALUE":"809540"},{"FAMILY_NAME":"MILLING","SUBFAMILY_NAME":"D&M - ROUGHING","APPLICATION_DESC":"CHASE 2 FEED TOOLS","SALES_MTD_VALUE":"39013.5","SALES_YTD_VALUE":"137390.75"},{"FAMILY_NAME":"TOOLING SYSTEMS","SUBFAMILY_NAME":"TOOLING SYSTEMS","APPLICATION_DESC":"SPRING COLLETS, KITS & SETS","SALES_MTD_VALUE":"2536","SALES_YTD_VALUE":"34670.4"},{"FAMILY_NAME":"TOOLING SYSTEMS","SUBFAMILY_NAME":"MPT BORING TOOL","APPLICATION_DESC":"MPT TOOLS","SALES_MTD_VALUE":"7813","SALES_YTD_VALUE":"163226.8"}];
            //tmp.forEach(function (d) {
            //    d.SALES_MTD_VALUE = d.SALES_MTD_VALUE;
            //});
            console.log("before:" + tmp1);
            console.log("len : " + tmp1.length);
            if (tmp1.length > 0) {
            var tmp = jQuery.parseJSON(tmp1);
            console.log("familyview1 : " + tmp);
            console.log("familyview1 : " + tmp.d);
            console.log("len1 : " + tmp.length);

           

                var FamilyViewWidth = document.getElementById('ChartFamilyView').offsetWidth;
               
                var SubFamilyViewWidth = document.getElementById('ChartSubFamilyView').offsetWidth;
                var ApplicationViewWidth = document.getElementById('ChartApplicationView').offsetWidth;
                //var tmp = [{ "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "T-CLAMP GROOVE", "APPLICATION_DESC": "T-CLAMP GRV IN.(NON ROUND SIZE", "SALES_MTD_VALUE": "64410", "SALES_YTD_VALUE": "490253" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "T-CLAMP GROOVE", "APPLICATION_DESC": "T-CLAMP T/GRV INS.(ROUND SIZE)", "SALES_MTD_VALUE": "10690", "SALES_YTD_VALUE": "449158" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TAEGUTURN PVD INSERTS", "SALES_MTD_VALUE": "13200", "SALES_YTD_VALUE": "88180" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "CERAMICS/SIN INSERTS", "SALES_MTD_VALUE": "9980", "SALES_YTD_VALUE": "67030" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TURN INSERTS FOR ALUMINUM", "SALES_MTD_VALUE": "44440", "SALES_YTD_VALUE": "311479.2" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TAEGUTURN HOLDERS EXTERNAL", "SALES_MTD_VALUE": "3666", "SALES_YTD_VALUE": "92495" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TURNRUSH INSERTS", "SALES_MTD_VALUE": "9600", "SALES_YTD_VALUE": "52370" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTHREAD", "APPLICATION_DESC": "TAEGUTHREAD INSERTS-GROUND", "SALES_MTD_VALUE": "6244", "SALES_YTD_VALUE": "21430" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "TOP DRILL/T-DRL", "APPLICATION_DESC": "T-DRILLS", "SALES_MTD_VALUE": "17286", "SALES_YTD_VALUE": "281700.75" }, { "FAMILY_NAME": "DRILL/HOLE MACH", "SUBFAMILY_NAME": "DRILL RUSH", "APPLICATION_DESC": "DRILL RUSH TOOLS", "SALES_MTD_VALUE": "1", "SALES_YTD_VALUE": "47601" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "ISO MILL", "APPLICATION_DESC": "LION WEDGE CLAMPING  INSERTSS", "SALES_MTD_VALUE": "2400", "SALES_YTD_VALUE": "63490" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "CHASEQUAD INSERTS", "SALES_MTD_VALUE": "34720", "SALES_YTD_VALUE": "973734" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "CHASEQUAD FACE/FLUT/SLOT CTRS", "SALES_MTD_VALUE": "29757", "SALES_YTD_VALUE": "103478.85" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(S)", "APPLICATION_DESC": "MILL RUSH INSERTS(TRIO)", "SALES_MTD_VALUE": "39990", "SALES_YTD_VALUE": "1050530" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "TANGENTIAL MILL", "APPLICATION_DESC": "TANGENTIAL SPC/OTH INSERTS", "SALES_MTD_VALUE": "85300", "SALES_YTD_VALUE": "1066250" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "DIE & MOLD PROD", "APPLICATION_DESC": "FINE BALL INSERTS", "SALES_MTD_VALUE": "3130", "SALES_YTD_VALUE": "19610" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "D&M - ROUGHING", "APPLICATION_DESC": "CHASE FEED INSERT(SBMT 09,13)", "SALES_MTD_VALUE": "24960", "SALES_YTD_VALUE": "67080" }, { "FAMILY_NAME": "TOOLING SYSTEMS", "SUBFAMILY_NAME": "MPT BORING TOOL", "APPLICATION_DESC": "MPT HEAD", "SALES_MTD_VALUE": "75599", "SALES_YTD_VALUE": "826218.4" }, { "FAMILY_NAME": "OTHER", "SUBFAMILY_NAME": "SPARE PARTS", "APPLICATION_DESC": "SPARE PARTS FOR TOOLS", "SALES_MTD_VALUE": "1240", "SALES_YTD_VALUE": "50926" }, { "FAMILY_NAME": "OTHER", "SUBFAMILY_NAME": "SPARE PARTS", "APPLICATION_DESC": "MILLING SPARE PARTS", "SALES_MTD_VALUE": "10210", "SALES_YTD_VALUE": "64425.4" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "T-CLAMP PARTING", "APPLICATION_DESC": "T-CLAMP PARTING OFF INS", "SALES_MTD_VALUE": "20855", "SALES_YTD_VALUE": "324041" }, { "FAMILY_NAME": "GROOVE/PART", "SUBFAMILY_NAME": "T-CLAMP GROOVE", "APPLICATION_DESC": "T-CLAMP INS FOR ALUMINUM", "SALES_MTD_VALUE": "10890", "SALES_YTD_VALUE": "68500" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TAEGUTURN UC INSERTS", "SALES_MTD_VALUE": "109010", "SALES_YTD_VALUE": "1229018" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "TAEGUTURN HOLDERS INTERNAL", "SALES_MTD_VALUE": "6898", "SALES_YTD_VALUE": "89888" }, { "FAMILY_NAME": "TURN/THREAD", "SUBFAMILY_NAME": "TAEGUTURN", "APPLICATION_DESC": "GOLDRUSH PVD INSERTS", "SALES_MTD_VALUE": "146200", "SALES_YTD_VALUE": "510550" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "HEXA&HEPTA MILL", "APPLICATION_DESC": "CHASE 2 HEPTA(14MM) CUTTERS", "SALES_MTD_VALUE": "17785", "SALES_YTD_VALUE": "247940.9" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(D)", "APPLICATION_DESC": "CHASE 2 MILL CUTTERS(11,16MM)", "SALES_MTD_VALUE": "38392", "SALES_YTD_VALUE": "89070.3" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(D)", "APPLICATION_DESC": "CHASE 2 QUAD CUTTER", "SALES_MTD_VALUE": "45090", "SALES_YTD_VALUE": "243744.4" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(D)", "APPLICATION_DESC": "CHASE 2 QUAD INSERTS", "SALES_MTD_VALUE": "141458", "SALES_YTD_VALUE": "492668" }, { "FAMILY_NAME": "MILLING", "SUBFAMILY_NAME": "MILL-CHASE(D)", "APPLICATION_DESC": "CHASE 2 MILL CUTTERS(09,06MM)", "SALES_MTD_VALUE": "10497", "SALES_YTD_VALUE": "84242.95" }, { "FAMILY_NAME": "TOOLING SYSTEMS", "SUBFAMILY_NAME": "TOOLING SYSTEMS", "APPLICATION_DESC": "SHELL MILL HOLDERS", "SALES_MTD_VALUE": "9643", "SALES_YTD_VALUE": "9643" }, { "FAMILY_NAME": "TOOLING SYSTEMS", "SUBFAMILY_NAME": "TOOLING SYSTEMS", "APPLICATION_DESC": "T-HOLD/OTHER ACCESSORIES", "SALES_MTD_VALUE": "200917", "SALES_YTD_VALUE": "200917" }];
                //console.log("familyview : " + tmp);
                FamilyChart = dc.pieChart("#ChartFamilyView");
                SubFamilyChart = dc.pieChart("#ChartSubFamilyView");
                ApplicationChart = dc.pieChart("#ChartApplicationView");
                if ($.fn.dataTable.isDataTable('#table')) {
                    table = $('#table').DataTable();
                    table.destroy();
                }
              

                var heightOfContainer = 500,
                    legendHeight = 150,
                    legendY = heightOfContainer - legendHeight;

                //var chart = dc.lineChart('#parent');
                //chart.margins().bottom = legendY + 10; // 10 for padding.
                //chart.legend(dc.legend().y(legendY));

                var ndx = crossfilter(tmp);
                   var all= ndx.groupAll();
                    FAMILY_NAMEDim = ndx.dimension(function (d) { return d.FAMILY_NAME; }),
                    SUBFAMILY_NAMEDim = ndx.dimension(function (d) { return d.SUBFAMILY_NAME; }),
                    APPLICATION_DESCDim = ndx.dimension(function (d) { return d.APPLICATION_DESC; }),
                    SALES_MTD_VALUEDim = ndx.dimension(function (d) { return d.SALES_MTD_VALUE; });
                FAMILY_NAME = FAMILY_NAMEDim.group().reduceSum(function (d) { return +(d.SALES_MTD_VALUE); }),
                SUBFAMILY_NAME = SUBFAMILY_NAMEDim.group().reduceSum(function (d) { return +(d.SALES_MTD_VALUE); }),
                APPLICATION_DESC = APPLICATION_DESCDim.group().reduceSum(function (d) { return +(d.SALES_MTD_VALUE); });


                FamilyChart.width(FamilyViewWidth)
                 .height(250)
                 .dimension(FAMILY_NAMEDim)
                 .group(FAMILY_NAME)
                 .innerRadius(10)
                 .legend(dc.legend())
                     .turnOnControls(true)
                 .controlsUseVisibility(true)
               
                 .label(function (d) { return d.key + ': ' + d.value.toFixed(2) });

                SubFamilyChart.width(SubFamilyViewWidth)
                 .height(250)
                 .dimension(SUBFAMILY_NAMEDim)
                 .group(SUBFAMILY_NAME)
                 .innerRadius(15)
                 .legend(dc.legend())
                     .turnOnControls(true)
                 .controlsUseVisibility(true)
                 .label(function (d) { return d.key + ': ' + d.value.toFixed(2) });

                ApplicationChart.width(ApplicationViewWidth)
                 .height(250)
                 .dimension(APPLICATION_DESCDim)
                 .group(APPLICATION_DESC)
                 .innerRadius(15)
                 .legend(dc.legend())
                    .turnOnControls(true)
                 .controlsUseVisibility(true)
                 .label(function (d) { return d.key + ': ' + d.value.toFixed(2) });



                console.log("len2 : " + tmp.length);
                var table = dc.dataTable('#table');
                table.dimension(SALES_MTD_VALUEDim)
        .group(function (d) {
            return d.value;
        })

    .size(tmp.length)

        //.sortBy(function (d) { return +d.SALES_MTD_VALUE; })
        .showGroups(false)


        .columns([
                  {
                      label: 'FAMILY NAME',
                      format: function (d) {
                          return d.FAMILY_NAME;
                      }
                  },

                    {
                        label: 'SUBFAMILY NAME',
                        format: function (d) {
                            return d.SUBFAMILY_NAME;
                        }
                    },
                   {
                       label: 'APPLICATION DESC',
                       format: function (d) {
                           return d.APPLICATION_DESC;
                       }
                   },

                    {
                        label: 'SALES MTD VALUE',
                        format: function (d) {
                            return d.SALES_MTD_VALUE;
                        }
                    },
                  {
                      label: 'SALES YTD VALUE',
                      format: function (d) {
                          return d.SALES_YTD_VALUE;
                      }
                  }

        ]);

                dc.renderAll();
                
                //if ($.fn.dataTable.isDataTable('#table')) {
                //    table = $('#table').DataTable();
                //   table.destroy(true);
                 
                $('#table').DataTable(

                    { "info": false, });
                //}
                //else {
                //    table = $('#table').DataTable({
                //        "info":false,
                //    });
                //}
              
            }
        }
        
        // data reset function (adapted)
       
        function resetFamily() {
            debugger;
           
            //var divf = document.getElementById('divfamilyfitler')
            //divf.hiddden = true;
            //window.location.reload();
            
            $("#divfamilyfitler").load(window.location.href + " #divfamilyfitler");
            $("#divsubfamilyfitler").load(window.location.href + " #divsubfamilyfitler");
            $("#divappfamilyfitler").load(window.location.href + " #divappfamilyfitler");
            LoadChartFamilyView();
          
          
        }
        function resetSubFamily() {
            debugger;
            $("#divsubfamilyfitler").load(window.location.href + " #divsubfamilyfitler");
            $("#divfamilyfitler").load(window.location.href + " #divfamilyfitler");
            $("#divappfamilyfitler").load(window.location.href + " #divappfamilyfitler");
            LoadChartFamilyView();
        }
        function resetApplication() {
            debugger;
            $("#divappfamilyfitler").load(window.location.href + " #divappfamilyfitler");
            $("#divsubfamilyfitler").load(window.location.href + " #divsubfamilyfitler");
            $("#divfamilyfitler").load(window.location.href + " #divfamilyfitler");
            LoadChartFamilyView();
        }
       

    </script>

    <style>
        .amcharts-chart-div a
        {
            display: none!important;
        }

        #ChartHistoricalPurchase
        {
            height: 500px;
            width: 50%;
        }

        #ChartConsolidatedPuchaseView
        {
            height: 500px;
            width: 50%;
        }
        .result
        {
            margin-left: 45%;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Dashboard</a>
                        </li>
                        <li class="current">Purchase Dashboard</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div>
        <asp:Label runat="server" ID="lblResult"></asp:Label>
    </div>
    <div class="col-md-12 mn_margin">
        <div>
            <asp:Label CssClass="result" ID="lblValue" Text="All values are in thousands." runat="server"></asp:Label>

        </div>
        <div>
            <div class="col-md-6" id="ChartHistoricalPurchase"></div>
            <div class="col-md-6" id="ChartConsolidatedPuchaseView"></div>

        </div>
        <div>
            <%--<div class="col-md-10 col-lg-offset-1" id="ChartFamilyView"></div>--%>
            <div class="col-md-4" id="ChartFamilyView">
                <div class="reset" style="visibility: hidden;" id="divfamilyfitler">selected: <span class="filter"></span>
                     <a class="reset" href="javascript:resetFamily();">reset</a>
									  <%--<a onclick="resetFamily();">reset</a>--%>
									</div>
            </div>
            <div class="col-md-4" id="ChartSubFamilyView">
                <div class="reset" style="visibility: hidden;" id="divsubfamilyfitler">selected: <span class="filter"></span>
                     <a class="reset" href="javascript:resetSubFamily();">reset</a>
									 <%-- <a onclick="resetSubFamily();">reset</a>--%>
									</div>
            </div>
            <div class="col-md-4" id="ChartApplicationView">
                <div class="reset" style="visibility: hidden;" id="divappfamilyfitler">selected: <span class="filter"></span>
                     <a class="reset" href="javascript:resetApplication();">reset</a>
									  <%--<a onclick="resetApplication();">reset</a>--%>
									</div>
            </div>


        </div>
        <div >
            
            <table id="table"></table>
        </div>
    </div>
</asp:Content>
