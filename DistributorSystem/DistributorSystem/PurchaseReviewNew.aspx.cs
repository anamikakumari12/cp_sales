﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBO;
using DistributorSystemBL;

namespace DistributorSystem
{
    public partial class PurchaseReviewNew : System.Web.UI.Page
    {
        SummaryBO objSummaryBO;
        SummaryBL objSummaryBL;
        List<string> cssListFamilyHead = new List<string>();
        List<string> cssList = new List<string>();
        List<string> lsType = new List<string>();
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["DistributorNumber"] == null)
                { Response.Redirect("Login.aspx"); return; }

                LoadCSS();
                objSummaryBO = new SummaryBO();
                objSummaryBL = new SummaryBL();
                objSummaryBO.distributor_number = Convert.ToString(Session["DistributorNumber"]);

                DataTable dtMain = objSummaryBL.LoadBudgetSummaryBL(objSummaryBO);
                dtMain = objSummaryBL.GrandTotalforEntryBL(dtMain);
                dtMain = objSummaryBL.AddInsertPerToolsBL(dtMain);
                ViewState["CurrentTableBudget"] = dtMain;
                //Main Grid
                lblResult.Text = "";
                grdviewAllValues.DataSource = null;
                grdviewAllValues.DataBind();
                if (dtMain != null)
                {
                    grdviewAllValues.DataSource = dtMain;
                    grdviewAllValues.DataBind();

                   
                }
                else
                {
                    grdviewAllValues.DataSource = null;
                    grdviewAllValues.DataBind();
                    lblResult.Text = "Refresh the page and try again";
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);


            }
        }

        protected void grdviewAllValues_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblBMFlag = e.Row.FindControl("lbl_BM_flag") as Label;
                var txtQty = e.Row.FindControl("txt_ActualQuantity_sales_qty_year_P") as TextBox;
                var cbQty = e.Row.FindControl("checkbox_ActualQuantity_sales_qty_year_P") as CheckBox;
                var txtvalue = e.Row.FindControl("txt_PricePerUnit_sales_value_year_P") as TextBox;

                if (lblBMFlag != null)
                    if (lblBMFlag.Text == "APPROVE")
                    {
                        if (txtQty != null && cbQty != null)
                        {
                            txtQty.Enabled = false; cbQty.Enabled = false;
                        }

                        if (txtvalue != null)
                        {
                            txtvalue.Enabled = false;
                        }
                    }
                    else
                    {

                        if (txtQty != null && cbQty != null)
                        {
                            if (cbQty.Checked) { txtQty.BackColor = System.Drawing.Color.Red; txtQty.Font.Bold = true; txtQty.ForeColor = System.Drawing.Color.White; } //txtQty.Enabled = false;
                        }
                    }

            }

        }


        protected void LoadCSS()
        {
            cssList.Add("color_3");
            cssList.Add("color_4");
            cssList.Add("color_3");
            //cssList.Add("color_5");
            //cssList.Add("color_4");
            //cssList.Add("color_2");
            //cssList.Add("greendark");

            cssListFamilyHead.Add("heading1");
            cssListFamilyHead.Add("heading2");
            cssListFamilyHead.Add("heading3");
            cssListFamilyHead.Add("heading4");
            cssListFamilyHead.Add("heading5");
            cssListFamilyHead.Add("heading6");
            cssListFamilyHead.Add("heading7");

        }
        protected string GetCSS(int colorIndex)
        {
            string index = Convert.ToString(colorIndex);
            string cIndex = index[index.Length - 1].ToString();

            if (cIndex.Contains("1"))
            { return cssList.ElementAt(1); }
            else if (cIndex.Contains("2"))
            { return cssList.ElementAt(2); }
            //else if (cIndex.Contains("3"))
            //{ return cssList.ElementAt(3); }
            //else if (cIndex.Contains("4"))
            //{ return cssList.ElementAt(4); }
            //else if (cIndex.Contains("5"))
            //{ return cssList.ElementAt(5); }
            else { return cssList.ElementAt(2); }

        }
        protected string GetCssFam(int colorIndex)
        {
            string index = Convert.ToString(colorIndex);
            string cIndex = index[index.Length - 1].ToString();

            if (cIndex.Contains("1"))
            { return cssListFamilyHead.ElementAt(1); }
            else if (cIndex.Contains("2"))
            { return cssListFamilyHead.ElementAt(2); }
            else if (cIndex.Contains("3"))
            { return cssListFamilyHead.ElementAt(3); }
            else if (cIndex.Contains("4"))
            { return cssListFamilyHead.ElementAt(4); }
            else if (cIndex.Contains("5"))
            { return cssListFamilyHead.ElementAt(5); }
            else if (cIndex.Contains("6"))
            { return cssListFamilyHead.ElementAt(6); }
            else { return cssListFamilyHead.ElementAt(0); }
        }

        protected void bindgridColor()
        {
            if (grdviewAllValues.Rows.Count != 0)
            {
                int colorIndex = 0;
                int rowIndex = 1, subRowIndex = 0, productTypeIndex = 1; int color = 0, parent_row_index = 0;
                bool currentRowIsLast = false;
                foreach (GridViewRow row in grdviewAllValues.Rows)
                {

                    var check = row.FindControl("lblSumFlag") as Label;
                    string txt = check.Text;
                    if (txt == "typeSum")
                    {
                        row.CssClass = "product_row_hide subTotalRowGrid subrowindex subrowindex_" + subRowIndex + " ";
                        var txtQuantityP = row.FindControl("txt_ActualQuantity_sales_qty_year_P") as TextBox;
                        txtQuantityP.CssClass += " product_type_sub_total product_type_sub_total_" + productTypeIndex + " product_type_sub_total_row_" + subRowIndex;
                        txtQuantityP.Attributes["data-product_type_sub_total_index"] = subRowIndex.ToString();
                        txtQuantityP.Attributes["data-product_type"] = productTypeIndex.ToString();

                        var lblnextYearValue = row.FindControl("lbl_ActualValue_sales_value_year_P_New") as Label;
                        lblnextYearValue.CssClass += "lblnextYearValue_Sum_" + productTypeIndex + " type_sum_lblnextYearValue_" + subRowIndex;

                        productTypeIndex++;
                    }
                    else if (txt == "SubFamilySum")
                    {
                        row.CssClass = "product_row_hide greendarkSubFamSum subrowindex subrowindex_" + subRowIndex;
                        var txtQuantityP = row.FindControl("txt_ActualQuantity_sales_qty_year_P") as TextBox;
                        txtQuantityP.CssClass += " product_type_SubFamilySum product_type_SubFamilySum_" + subRowIndex;
                        txtQuantityP.Attributes["data-product_type_subfamily_total_index"] = subRowIndex.ToString();

                        var lblnextYearValue = row.FindControl("lbl_ActualValue_sales_value_year_P_New") as Label;
                        lblnextYearValue.CssClass += "subfamily_sum_lblnextYearValue_" + subRowIndex;

                    }
                    else if (txt == "SubFamilyHeading")
                    {
                        row.CssClass = "product_row_hide greendark row_index row_" + rowIndex + " parent_row_index_" + parent_row_index;

                        var image = row.FindControl("Image1") as Image;
                        image.CssClass = "row_index_image";

                        row.Attributes["data-index"] = rowIndex.ToString();
                        rowIndex++;
                        subRowIndex++;
                    }
                    else if (txt == "products")
                    {
                        color++;
                        if (color == 1) { row.CssClass = "color_Product1 "; }
                        else if (color == 2)
                        {
                            row.CssClass = "color_Product2 ";
                            color = 0;
                        }

                        row.CssClass += "product_row_hide product_type_" + productTypeIndex + " subrowindex subrowindex_" + subRowIndex;
                        var txtQuantityP = row.FindControl("txt_ActualQuantity_sales_qty_year_P") as TextBox;
                        txtQuantityP.CssClass += " product_type product_type_" + productTypeIndex;
                        txtQuantityP.Attributes["data-product_type"] = productTypeIndex.ToString();

                        var lblnextYearValue = row.FindControl("lbl_ActualValue_sales_value_year_P_New") as Label;
                        lblnextYearValue.CssClass += "lblnextYearValue_" + productTypeIndex;
                    }
                    else if (txt == "FamilyHeading")
                    {
                        row.CssClass = "parent_row_index";
                        row.Attributes["data-parent_row_index"] = "" + (++parent_row_index);

                        if (colorIndex <= 6)
                        {
                            for (int i = 0; i < row.Cells.Count; i++)
                            {
                                row.Cells[0].CssClass = "TotalRowGridHead1 " + GetCssFam(colorIndex); ;
                                row.Cells[i].CssClass = "TotalRowGridHead " + GetCssFam(colorIndex); ;
                            }
                            //row.CssClass = GetCssFam(colorIndex); 
                            colorIndex++;
                        }
                        else
                        {
                            colorIndex = 0;
                            for (int i = 0; i < row.Cells.Count; i++)
                            {
                                row.Cells[0].CssClass = "TotalRowGridHead1 " + GetCssFam(colorIndex); ;
                                row.Cells[i].CssClass = "TotalRowGridHead " + GetCssFam(colorIndex); ;
                            }
                        }



                    }
                    else if (txt == "FamilySum")
                    {
                        row.CssClass = "product_row_hide TotalRowGrid" + " parent_row_index_" + parent_row_index;
                        currentRowIsLast = true;

                    }
                    else if (txt == "MainSum")
                    {
                        row.CssClass = "MainTotal";
                    }

                    else if (txt == "")
                    {
                        row.CssClass = "empty_row";
                        if (!currentRowIsLast)
                        {
                            row.CssClass += " product_row_hide ";
                        }
                        currentRowIsLast = false;
                        for (int i = 0; i < row.Cells.Count; i++)
                        {
                            row.Cells[i].CssClass = "HidingHeading";// = row.Cells[1].Text + row.Cells[i].Text;
                            //row.Cells[i].BackColor = "HidingHeading";
                            //row.Cells[i].BorderColor = System.Drawing.Color.White;// = row.Cells[1].Text + row.Cells[i].Text;
                            //row.Cells[i].BackColor = System.Drawing.Color.White;
                        }
                    }
                    else if (txt == "HidingHeading")
                    {

                        for (int i = 0; i < row.Cells.Count; i++)
                        { row.Cells[i].CssClass = "greendark MainHeader"; }
                    }


                }
            }




        }

        protected void bindgridCssforValueAdding()
        {
            if (grdviewAllValues.Rows.Count != 0)
            {

                int rowIndex = 1, subRowIndex = 0, productTypeIndex = 1, parent_row_index = 0;
                foreach (GridViewRow row in grdviewAllValues.Rows)
                {
                    var txtValueP = row.FindControl("txt_PricePerUnit_sales_value_year_P") as TextBox;
                    var check = row.FindControl("lblSumFlag") as Label;
                    string txt = check.Text;
                    if (txt == "typeSum")
                    {

                        txtValueP.CssClass += " value_product_type_sub_total value_product_type_sub_total_" + productTypeIndex + " value_product_type_sub_total_row_" + subRowIndex;
                        txtValueP.Attributes["data-value_product_type_sub_total_index"] = subRowIndex.ToString();
                        productTypeIndex++;
                    }
                    else if (txt == "SubFamilySum")
                    {
                        txtValueP.CssClass += " value_product_type_SubFamilySum_" + subRowIndex;
                    }
                    else if (txt == "SubFamilyHeading")
                    {

                        rowIndex++;
                        subRowIndex++;
                    }
                    else if (txt == "products")
                    {
                        txtValueP.CssClass += " value_product_type value_product_type_" + productTypeIndex;
                        txtValueP.Attributes["data-product_type"] = productTypeIndex.ToString();

                    }

                }

            }


        }
    }
}